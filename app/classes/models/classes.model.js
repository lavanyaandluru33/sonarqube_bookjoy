const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js');



exports.getClassSchedulesByStore = (data, result) => {

    let sql = `SELECT cs.store_id, cs.class_schedule_id, cs.class_id, cs.class_name,
    cs.user_id , cs.duration, cs.size, cs.price, cs.deposit, cs.class_date, cs.class_start_time, 
    cs.class_end_time, cs.booked_slots
   FROM  class_master cm, class_schedule cs 
   WHERE cm.class_id=cs.class_id  AND cs.store_id = ${data.store_id}
   AND cs.is_deleted = 0 AND cm.class_is_deleted =0 AND cs.is_online = 1
   ORDER BY cs.class_date ASC, cs.class_start_time ASC`;

           
    
     //console.log(sql);

    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
           
}

exports.getCustomerClassBookings = (data, result) => {

    let sql = `SELECT booking_class_id, store_id,date,start_time,total_cost,class_book_status, "classes" as book_type FROM booking_class_mst WHERE class_cust_id = ${data.cust_id} AND store_id IN (${data.stores}) AND (class_book_status != ${constants.STATUS_PRE_BOOKED}) ORDER BY date DESC`;
  
   //console.log(sql);
     
    db.query(sql , (err,res) => {
           if (err) {
                   //console.log(err);
                  return result(err, null);
                  
                 }
                 //console.log(res);
        
                 return  result(null, res);
                
          });
              
   }



   exports.getClassBookingDetails = (data, result) => {

    let sql = `SELECT b.booking_class_id, b.user_id, b.class_cust_id,b.date,b.start_time,b.end_time,b.duration,b.deposit_paid,c.class_name, up.user_perm_created_by as owner_id, b.store_id, b.class_cust_email FROM booking_class_mst b
        LEFT JOIN class_schedule c ON c.class_schedule_id = b.class_schedule_id LEFT JOIN users_permissions up ON (b.user_id = up.user_id AND b.store_id = up.store_id) WHERE b.booking_class_id IN (${data.classes}) `;
  
   //console.log(sql);
     
    db.query(sql , (err,res) => {
           if (err) {
                   //console.log(err);
                  return result(err, null);
                  
                 }
                 //console.log(res);
        
                 return  result(null, res);
                
          });
              
   }



   exports.getCustomerClassBookingsCount = (data, result) => {

    let sql = `SELECT COUNT(booking_class_id) as total_bookings, SUM(total_cost) as total_sales , (SELECT COUNT(booking_class_id) as total_cancelled FROM booking_class_mst WHERE class_cust_id = ${data.cust_id} AND store_id IN (${data.stores}) AND class_book_status IN (${constants.STATUS_CANCELLED},${constants.STATUS_LATE_CANCELLED},${constants.STATUS_NO_SHOW})) as total_cancelled FROM booking_class_mst WHERE class_cust_id = ${data.cust_id} AND store_id IN (${data.stores}) AND class_book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_CONFIRMED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED})`;
  
   //console.log(sql);
     
    db.query(sql , (err,res) => {
           if (err) {
                   //console.log(err);
                  return result(err, null);
                  
                 }
                 //console.log(res);
        
                 return  result(null, res[0]);
                
          });
              
   }
   

