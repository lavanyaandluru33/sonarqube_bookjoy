const constants = require('../../../config/constants.js');
const ClassesModel = require('../models/classes.model');



exports.getClassSchedulesByStore = (req, res, next) => {
    if(!req.body.store_id) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    ClassesModel.getClassSchedulesByStore(req.body, (err, data) => {
        if (err) {
            //res.status(500).send({message:err});
            next(err);
        }
        else{
            res.status(200).send({status:200, result:data});
        }
    });

   
}