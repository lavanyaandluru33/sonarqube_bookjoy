const ClassesController = require('./controllers/classes.controller');
const auth = require('../../config/auth');
exports.routesConfig = function (app) {
  

    app.post('/classes/get_class_sch_by_store', 
        ClassesController.getClassSchedulesByStore
    );
    
}