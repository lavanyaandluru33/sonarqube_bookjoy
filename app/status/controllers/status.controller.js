const constants = require('../../../config/constants.js');
const StatusModel = require('../models/status.model.js');
const ProductsModel = require('../../products/models/products.model.js');
const plivo = require('plivo');
let client = new plivo.Client(constants.plivo_auth_id, constants.plivo_auth_token);

var async = require("async");
exports.getServerStatus = (req, res, next) => {

    StatusModel.checkDbStatus((err, data) => {
        if (err) {
            next(err);
        } else {

            //console.log(data);
            res.status(200).send("Server is running and MySQL DB is connected");
        }
    })
}

exports.checkPlivo = (req, res, next) => {
    client.messages.create(
            'BOOKJOY',
            req.body.number,
            req.body.message
            ).then(function (message_created) {
        console.log(message_created)
        res.send({status: 200, message: 'sms sent successful'});
    })
            .catch((error) => {
                console.log('Error sending sms:', error);
                //console.log(error);
                next(error);
//                res.send({status: 400, message: 'sms failed'});
            });
}

exports.testAsync = (req, res) => {
    let check_array = [{1: 1}, {2: 2}, {3: 1}],data=[],data1=[],j=0;
    var product_req = {
        store_id: 21

    }
    async.each(check_array, function (i, callback) {
        ProductsModel.getProductsList(product_req, (err, products_data) => {
            if (err) {

                //next(err);
            } else {
                console.log(j)
                data.push(products_data);
            }
            j++;
            callback();
        });
    })

    async.each(check_array, function (i, callback) {
        ProductsModel.getProductDetails(29, (err, data) => {
            console.log(j);
            data1.push(data);
            j++;
            callback();
        });
    },function(err){
        res.send({data:data,data1:data1})
    });
}








