const StatusController = require('./controllers/status.controller');
const auth = require('../../config/auth');
exports.routesConfig = function (app) {

    app.get('/status', 
        StatusController.getServerStatus
    );
    app.post('/check_plivo', 
        StatusController.checkPlivo
    );

    app.get('/test_async',StatusController.testAsync)
    
}