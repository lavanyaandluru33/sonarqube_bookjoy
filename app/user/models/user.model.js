const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js');

exports.getUserStaffType = (user_id,store_id,result) => {

  let sql = `SELECT user_perm_staff_type
    FROM users_permissions WHERE user_id = ${user_id} AND store_id = ${store_id}`;

    //console.log(sql);


      db.query(sql , (err,res) => {
        if (err) {
                //console.log("error: ", err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
     
              return  result(null, res);
        
       
       });
           
}



exports.getUserDetails = (user_id, result) => {

  let sql = `SELECT u.user_id, u.user_first_name, u.user_last_name, u.user_email, u.user_image, u.user_photo, u.user_fcm_registration_token
    FROM users_mst as u WHERE u.user_id = ${user_id}`;
      
    //console.log(sql);

      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              return  result(null, res);
        
       
       });
           
}



exports.getAllOwners = (result) => {

  let sql = `SELECT p.user_perm_id, p.user_id, p.store_id,p.user_perm_type, p.user_perm_staff_type, p.user_staff_order
    FROM users_permissions as p WHERE p.user_perm_is_active = 1 AND p.user_perm_is_removed = 0 AND p.user_perm_type = 1 `;

    //console.log(sql);

      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              return  result(null, res);
        
       
       });
           
}

exports.getUserFcmTokens = (user_id, result) => {

  let sql = `SELECT fcm_token FROM user_tokens WHERE user_id = ${user_id} AND is_active = 1 `;

    //console.log(sql);

      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              return  result(null, res);
        
       
       });
           
}













