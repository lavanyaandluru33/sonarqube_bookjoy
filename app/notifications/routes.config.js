const NotificationsController = require('./controllers/notifications.controller');
const auth = require('../../config/auth');

exports.routesConfig = function (app) {
  
    
    // app.post('/settings/get_staff_by_store_id', auth.authenticateJWT,
    //     StaffController.getStaffByStore
    // );
     app.post('/settings/save_form_data',
         NotificationsController.saveFormData
     );
    
}