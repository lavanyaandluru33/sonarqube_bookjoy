const constants = require('../../../config/constants.js');
const NotificationsModel = require('../models/notifications.model.js');

exports.saveFormData = (req,res,next)=>{
    if(!req.body.store_id || !req.body.form_url || !req.body.form_id){
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
    }else{
        NotificationsModel.saveFromDataModel(req.body,(err,result)=>{
            if(err){
                next(err)
            }else{
                res.status(200).send({'message':`Successfully updated ${result.affectedRows} Rows`});
            }
        })
    }
}
