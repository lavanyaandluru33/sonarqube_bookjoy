const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js');

exports.addNotification = (data, result) => {

    let sql = `INSERT INTO notification_status (sent_to, book_id , cust_id, user_id, store_id , serv_id, booking_type, notif_type, notif_title, notif_response, notif_created_by, notif_created_date) VALUES 
  (${data.sent_to}, ${data.book_id} , ${data.cust_id}, ${data.user_id}, ${data.store_id} , ${data.serv_id}, ${data.booking_type}, ${data.notif_type}, '${data.notif_title}', '${data.notif_response}', ${data.notif_created_by}, '${data.notif_created_date}')`;

    //console.log(sql);
    db.query(sql, (err, res) => {
        if (err) {
            //console.log(err);
            return result({error_code: err.code, message: err.sqlMessage, sql: err.sql}, null);

        }

        return  result(null, res);
    });

}

exports.saveFromDataModel = (data, result) => {

    let sql = `UPDATE notification_settings set form_id = '${data.form_id}', form_url = '${data.form_url}' 
                where store_id = ${parseInt(data.store_id)}`;

    //console.log(sql);
    db.query(sql, (err, res) => {
        if (err) {
            //console.log(err);
            return result({error_code: err.code, message: err.sqlMessage, sql: err.sql}, null);

        }

        return  result(null, res);
    });

}

exports.getNotificationSettings = (store_id, result) => {

    let sql = `SELECT notif_set_id, store_id, form_id, form_url
      FROM notification_settings WHERE store_id = ${store_id}`;
        
      //console.log(sql);
  
  
        db.query(sql , (err,res) => {
          if (err) {
                  //console.log(err);
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                 
                }
       
                return  result(null, res);
          
         
         });
             
  }


