const constants = require('../../../config/constants.js');
const SettingsModel = require('../models/settings.model.js');
const ErrorLogs = require('../../../libs/error_logs.js');

var moment = require('moment'); 

exports.addStaffServiceBlocks = (req, res, next) => {
    
    if(!req.body.staff_service_blocks || !req.body.store_id || !req.body.created_by){
        res.status(400).send({ status:400, message: 'Missing required parameters. Check body and retry'});
        return;
    }

    var blocks = req.body.staff_service_blocks;
    var store_id = req.body.store_id;

    var blocks_values = [];

    var created_date = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

    var blocks_slack_alert = [];

    for(i=0; i< blocks.length; i++)
    {

        console.log(` staff service block added : user_id - ${blocks[i].user_id} , serv_id - ${blocks[i].serv_id} , block created by - ${req.body.created_by} on ${created_date}`);

        var this_block = [blocks[i].user_id, store_id, blocks[i].serv_id, 1, req.body.created_by, created_date, req.body.created_by, created_date, 0];
        blocks_values.push(this_block);

        var this_block_slack = {
            message: "staff service block added",
            user_id: blocks[i].user_id,
            serv_id: blocks[i].serv_id,
            block_created_by: req.body.created_by,
            created_date: created_date

        }

        blocks_slack_alert.push(this_block_slack)

    }

    //console.log("blocks values: ", blocks_values);



    SettingsModel.addStaffServiceBlocks(blocks_values, (err, data) => {
        if (err) {
            
            next(err);
        }
        else{
            //console.log("data: ", data);
            res.status(200).send({status:200, result:{message: "Successfully added staff service blocks", block_id: data.insertId }});

            ErrorLogs.send_to_slack(blocks_slack_alert).then((slack_result) => {
                //console.log(err);
        
                console.log("Slack alert sent for staff service blocks");
            })
            .catch((slack_err) => {
                console.log(slack_err);
            });
        }
    });

   
}


exports.getStaffServiceBlocks = (req, res, next) => {
    
    if(!req.body.store_id){
        res.status(400).send({ status:400, message: 'Missing required parameters. Check body and retry'});
        return;
    }



    SettingsModel.getStaffServiceBlocks(req.body, (err, data) => {
        if (err) {
            
            next(err);
        }
        else{
            //console.log("data: ", data);
            res.status(200).send({status:200, result:data});
        }
    });

   
}

exports.deleteStaffServiceBlocks = (req, res, next) => {
    
    if(!req.body.store_id || !req.body.user_id || !req.body.serv_id || !req.body.deleted_by){
        res.status(400).send({ status:400, message: 'Missing required parameters. Check body and retry'});
        return;
    }
    var today = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")

    console.log(`staff service block delete : user_id - ${req.body.user_id} , serv_id - ${req.body.serv_id} , deleted by ${req.body.deleted_by} on ${today}`);

    var blocks_slack_alert = {
        message: "staff service block deleted",
        user_id:req.body.user_id,
        serv_id: req.body.serv_id,
        block_deleted_by: req.body.deleted_by,
        deleted_date: today

    }


    SettingsModel.deleteStaffServiceBlocks(req.body, (err, data) => {
        if (err) {
            
            next(err);
        }
        else{
            //console.log("data: ", data);
            res.status(200).send({status:200, result:{message: "Successfully deleted staff service blocks"}});

            ErrorLogs.send_to_slack(blocks_slack_alert).then((slack_result) => {
                //console.log(err);
        
                console.log("Slack alert sent for deleting staff service blocks");
            })
            .catch((slack_err) => {
                console.log(slack_err);
            });
        }
    });

   
}




exports.getSquareAccountDetails = (req, res, next) => {
    
    if(!req.body.store_id){
        res.status(400).send({ status:400, message: 'Missing required parameters. Check body and retry'});
        return;
    }



    SettingsModel.getSquareAccountDetails(req.body.store_id, (err, data) => {
        if (err) {
            
            next(err);
        }
        else{
            //console.log("data: ", data);
            res.status(200).send({status:200, result:data});
        }
    });

   
}











