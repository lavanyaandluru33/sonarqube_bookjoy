const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js');

exports.getStaffServiceBlocks = (data, result) => {

  let sql = `SELECT block_id, user_id, store_id, serv_id, is_blocked
    FROM staff_service_blocks WHERE store_id = ${data.store_id}`;

  if(data.serv_id)
  {
    sql += ` AND serv_id = ${data.serv_id}`;
  }

  if(data.staff_id)
  {
    sql += ` AND user_id = ${data.staff_id}`;
  }
    
    //console.log(sql);


      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
     
              return  result(null, res);
        
       
       });
           
}

exports.addStaffServiceBlocks = (data, result) => {

  let sql = `INSERT INTO staff_service_blocks (user_id, store_id, serv_id, is_blocked, block_created_by, block_created_date, block_updated_by, block_updated_date, staff_is_assigned) VALUES ? `;
 
    //console.log(sql);

      db.query(sql , [data], (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);

              return  result(null, res);        
       
        });
           
}

exports.deleteStaffServiceBlocks = (data, result) => {

  let sql = `DELETE FROM staff_service_blocks WHERE store_id = ${data.store_id} AND user_id = ${data.user_id} AND serv_id = ${data.serv_id}`;
 
    //console.log(sql);

      db.query(sql , [data], (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);

              return  result(null, res);        
       
        });
           
}


exports.getSquareAccountDetails= (store_id, result) => {

  let sql = `SELECT store_id, store_api_square_access_token, store_api_square_app_id, store_api_square_location_id, store_api_square_is_active, store_api_square_currency
    FROM store_api_square_mst WHERE store_id = ${store_id} AND store_api_square_is_active = 1`;

    //console.log(sql);


      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
     
              return  result(null, res);
        
       
       });
           
}













