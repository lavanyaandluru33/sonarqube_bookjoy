const SettingsController = require('./controllers/settings.controller');
const auth = require('../../config/auth');
exports.routesConfig = function (app) {
  
    
    app.post('/v2/settings/add_staff_service_blocks_v2',
        SettingsController.addStaffServiceBlocks
    );

    app.post('/v2/settings/get_staff_service_blocks_v2',
        SettingsController.getStaffServiceBlocks
    );

    app.post('/v2/settings/delete_staff_service_blocks_v2',
        SettingsController.deleteStaffServiceBlocks
    );

    app.post('/v2/settings/get_square_account_details_v2',
        SettingsController.getSquareAccountDetails
    );
    
}