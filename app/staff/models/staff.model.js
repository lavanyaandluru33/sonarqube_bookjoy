const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js');

exports.getStaffByStore = (data, result) => {

  let sql = `SELECT p.store_id, u.user_id, u.user_first_name, u.user_last_name, u.user_image, u.user_photo, p.user_perm_is_removed,p.user_staff_order, p.user_perm_staff_type
    FROM users_permissions as p,users_mst as u WHERE p.user_id = u.user_id AND p.store_id = ${data.store_id} `;

    if (data.active_staff) {
      sql += ` AND p.user_perm_is_active = 1 `;
    }

    // if (data.changed_staff_id) {
    //   sql += ` AND (p.user_perm_is_removed = 0 OR u.user_id=${data.changed_staff_id})`;
    // }
    // else{
      sql += ` AND p.user_perm_is_removed = 0`;
    //}

    sql += ` ORDER BY p.user_staff_order `;
    
    //console.log(sql);


      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
             
              return  result(null, res);
        
       
       });
           
}


exports.getStaffByStoreforCalendar = (data, result) => {

  let sql = `SELECT p.store_id, u.user_id as staff_id, u.user_first_name as staff_first_name, u.user_last_name as staff_last_name, u.user_image as staff_image, u.user_photo as staff_photo, p.user_staff_order
    FROM users_permissions as p,users_mst as u
     WHERE p.user_id = u.user_id AND p.store_id = ${data.store_id} AND p.user_perm_is_active = 1 AND p.user_perm_is_removed = 0 
    `;

    if (data.staff_id) {
      sql += ` AND u.user_id IN (${data.staff_id})`;
    }

    sql += ` ORDER BY p.user_staff_order`;



      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              return  result(null, res);
        
       
       });
           
}

exports.getStaffHours = (data, result) => {
  

  let sql = `SELECT * FROM user_hours_week 
        where store_id =${data.store_id} and user_hrwk_is_active =1`;

        if(data.day_of_week || data.day_of_week == 0) 
        {
          sql += ` AND user_hrwk_day_of_week =${data.day_of_week}`;
        }
        

        if(data.staff_id) 
        {
          sql += ` AND user_id = ${data.staff_id}`;
        }
        
    
    //console.log(sql);

      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
     
              return  result(null, res);
        
       
       });
           
}


exports.getStaffHourExceptions = (data, result) => {
  

  let sql = `SELECT * FROM user_hours_exceptions
        WHERE store_id =${data.store_id} AND user_hrex_is_active =1`;

        if(data.date) 
        {
          sql += ` AND user_hrex_specific_date ='${data.date}'`;
        }
        
        if(data.staff_id) 
        {
          sql += ` AND user_id = ${data.staff_id}`;
        }
        
        sql += ` ORDER BY user_hrex_id DESC`
        

    
   //console.log(sql);


      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
     
              return  result(null, res);
        
       
       });
           
}

exports.addStaffWorkingHourException = (data,result) =>{
  
  let sql = `INSERT INTO user_hours_exceptions (store_id, user_id, user_hrex_specific_date, user_hrex_open_time, user_hrex_close_time, user_hrex_all_day_indicator, user_hrex_reason, user_hrex_is_active, user_hrex_created_by, user_hrex_created_date) VALUES (${data.store_id},${data.staff_id},'${data.exception_date}', '${data.start_time}', '${data.end_time}','${data.working_status_indicator}','${data.reason}','1','${data.login_user_id}','${data.created_date}')`;
    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
        
        }
        return  result(null, res);
    });
}



exports.editStaffException = (user_hrex_id,data,result) =>{
  //console.log(data);
  //console.log(user_hrex_id);
  
  let sql = `UPDATE user_hours_exceptions SET ? WHERE user_hrex_id = ${user_hrex_id}`;
  

    db.query(sql ,[data] , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
        
        }
        return  result(null, res);
    });
}



exports.getStaffBookings = (data) => {

//   let sql = `SELECT b.book_id , b.book_start_date , b.book_start_time , b.book_end_time, b.book_start_time - INTERVAL s.serv_prebuffertime MINUTE as start, 
//  b.book_end_time + INTERVAL s.serv_postbuffertime MINUTE as end, b.store_id, b.book_status as book_status, 
//             b.user_id , u.user_first_name 
//             b.serv_id , s.serv_prebuffertime, s.serv_postbuffertime,
//             FROM booking_mst b
//             LEFT JOIN services_mst s ON s.serv_id=b.serv_id 
//             WHERE b.store_id = ${data.store_id} AND b.user_id = ${data.staff_id} AND b.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date = '${data.start_date}'`;
 
//   console.log(sql);

//       db.query(sql , (err,res) => {
//         if (err) {
//                 //console.log(err);
//                return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
//               }
//               return  result(null, res);
        
       
//        });



       return new Promise((resolve, reject) => {

        let sql = `SELECT b.book_start_date, b.book_start_time - INTERVAL b.book_prebuffertime MINUTE as start_time_with_buffer, 
                   b.book_end_time + INTERVAL b.book_postbuffertime MINUTE as end_time_with_buffer,
                   b.book_prebuffertime as serv_prebuffertime, b.book_postbuffertime as serv_postbuffertime, s.is_break FROM booking_mst b
                   LEFT JOIN services_mst s ON s.serv_id=b.serv_id 
                    WHERE b.store_id = ${data.store_id} AND b.user_id = ${data.staff_id} AND b.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date = '${data.date}'`;

 //b.book_id , b.book_start_date , b.book_start_time , b.book_end_time,  b.store_id, b.book_status as book_status, b.user_id , b.serv_id ,
  //console.log(sql);


        db.query(sql, function (err, result) {
            if (err)
                reject({error_code:err.code, message:err.sqlMessage,sql:err.sql});
            else
            //console.log("result: ",result);
                resolve(result);
        });
    });
           
        }



// exports.getStaffDetails = (staff_id, store_id, result) => {

//   let sql = `SELECT p.store_id, u.user_id, u.user_first_name, u.user_last_name, u.user_email, u.user_image, u.user_photo, p.user_perm_staff_type, p.user_staff_order, p.user_perm_is_removed
//     FROM users_permissions as p,users_mst as u WHERE p.user_id = u.user_id AND u.user_id = ${staff_id} AND p.store_id = ${store_id} `;
      
//     //console.log(sql);

//       db.query(sql , (err,res) => {
//         if (err) {
//                 //console.log(err);
//                return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
//               }
//               return  result(null, res);
        
       
//        });
           
//}







