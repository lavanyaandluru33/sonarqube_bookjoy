const StaffController = require('./controllers/staff.controller');
const auth = require('../../config/auth');
exports.routesConfig = function (app) {
  
    
    app.post('/staff/get_staff_by_store_id', 
        StaffController.getStaffByStore
    );

    app.post('/staff/get_staff_for_service', 
        StaffController.getStaffByService
    );
    app.post('/staff/add_staff_exceptions',
        StaffController.addStaffWorkingHourException
    );

    app.post('/v2/staff/get_available_staff_slots_v2', 
        StaffController.getAvailableStaffSlots
    );

    app.post('/v2/staff/check_selected_slot_availability_v2', 
        StaffController.checkSelectedSlotAvailability
    );

    
}