const constants = require('../../../config/constants.js');
const StaffModel = require('../models/staff.model.js');
const SettingsModel = require('../../settings/models/settings.model.js');
const StoresModel = require('../../stores/models/stores.model.js');
const BookingModel = require('../../booking/models/booking.model.js');
const ProductsModel = require('../../products/models/products.model.js');
const ServicesModel = require('../../services/models/services.model.js');

var async = require("async");
var moment = require('moment'); 

exports.getStaffByStore = (req, res, next) => {

    if(!Object.keys(req.body).length) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    StaffModel.getStaffByStore(req.body, (err, data) => {
        if (err) {

            //res.status(500).send({message:err});
            next(err);
        }
        else{
        
            res.status(200).send({status:200, result:data});
        }
    });
   
}

exports.addStaffWorkingHourException = (req,res, next)=>{
        
    if(!(req.body.store_id) || !(req.body.staff_id) || !(req.body.exception_date) || !(req.body.start_time) || !(req.body.end_time)){
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    //  var current_date = new Date();
    // var month = "";
    // var day = "";
    // var hours = "";
    // var minutes = "";
    // var seconds = "";
    // if((current_date.getMonth()+1) < 10){
    //     month = 0+""+(current_date.getMonth()+1);
    // }else month = current_date.getMonth()+1;

    // if((current_date.getDate()) < 10){
    //     day = 0+""+(current_date.getDate());
    // }else day = current_date.getDate();

    // if((current_date.getHours()) < 10){
    //     hours = 0+""+(current_date.getHours());
    // }else hours = current_date.getHours();

    // if((current_date.getMinutes()) < 10){
    //     minutes = 0+""+(current_date.getMinutes());
    // }else minutes = current_date.getMinutes();

    // if((current_date.getSeconds()) < 10){
    //     seconds = 0+""+(current_date.getSeconds());
    // }else seconds = current_date.getSeconds();

    // req.body.created_date = current_date.getFullYear()+"-"+month+"-"+day+" "+hours+":"+minutes+":"+seconds;


    

    // if(req.body.working_status_indicator == 1)
    // {
    //     req.body.start_time = req.body.end_time = "00:00:00";
    // }


     var exception_req = {

        store_id: req.body.store_id,
        staff_id : req.body.staff_id,
        date: req.body.exception_date
     }



    StaffModel.getStaffHourExceptions(exception_req, (err, exceptions) => {
        if (err) {

            //res.status(500).send({message:err});
            next(err);
        }
        else{
            //console.log(exceptions);

            if(exceptions.length)
            {
                var exception_data = {

                 user_hrex_open_time: req.body.start_time, 
                 user_hrex_close_time: req.body.end_time, 
                 user_hrex_all_day_indicator: req.body.working_status_indicator , 
                 user_hrex_reason: req.body.reason, 
                 user_hrex_updated_by: req.body.login_user_id, 
                 user_hrex_updated_date: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')

                }


                StaffModel.editStaffException(exceptions[0].user_hrex_id, exception_data, (err, data) => {
                    if (err) {

                        //res.status(500).send({message:err});
                        next(err);
                    }
                    else{
                    
                        res.status(200).send({status:200, message:"Added Staff Hour Exception "});
                    }
                });

            }
            else
            {
                req.body.created_date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
    
                    StaffModel.addStaffWorkingHourException(req.body, (err, data) => {
                        if (err) {

                            //res.status(500).send({message:err});
                            next(err);
                        }
                        else{
                        
                            res.status(200).send({status:200, message:"Added a new Staff Hour Exception "});
                        }
                    });
            }

        }

    })


}



exports.getStaffByService = (req, res, next) => {

    if(!Object.keys(req.body).length) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }


    var staff_req = req.body;

    staff_req.active_staff =1;
    //get Staff for Store
        StaffModel.getStaffByStore(staff_req, (err, staff) => {
            if (err) {
    
                //res.status(500).send({message:err});
                next(err);
            }
            else{
                staff = JSON.parse(JSON.stringify(staff));
                //console.log(staff);
                                              
                                    SettingsModel.getStaffServiceBlocks(req.body, (err3, data3) => {
                                        if (err3) {
                                
                                            //res.status(500).send({message:err3});
                                            next(err3);
                                        }
                                        else
                                        {
                                           
                                            var staff_serv_blocks = [];


                                            async.each(data3, function(block, callback) {
                                                staff_serv_blocks.push(block.user_id);
                                                    callback();
                                                
                                            }, function(err4) {
                                                if (!err4)
                                                {

                                                    var day_of_week = (moment(req.body.date).isoWeekday()) - 1;
                                                    var i=0;

                                                    //var staff_av = [];
                                                    //console.log(staff_serv_blocks)
                                                    staff.filter(async (staff_obj)=>{
                                                        //console.log(`----------------------in staff filter ${staff_obj.user_id}`);

                                                        staff_obj.staff_hours = {};
                                                        staff_obj.is_mapped ="";

                                                        if((staff_serv_blocks.indexOf(staff_obj.user_id)) > -1)
                                                        {
                                                            //staff blocked
                                                                    //console.log(staff_obj.user_id);
                                                                    staff_obj.is_mapped = 0;
                                                        }
                                                        else
                                                        {
                                                            staff_obj.is_mapped = 1;

                                                        }

                                                        var this_staff={
                                                            store_id : req.body.store_id,
                                                            staff_id: staff_obj.user_id,
                                                            date: req.body.date,
                                                            day_of_week: day_of_week
                                                            } ;

                                                            staff_obj.staff_hours = await staffTimingsForService(this_staff);         
                                                          
                                                            //console.log("in staff hours");
                                                            //console.log(staff_obj.staff_hours);

                                                            i++;

                                                            if(i == staff.length)
                                                            {
                                                                res.status(200).send({status:200, result:staff});

                                                            }

                                                         
                                                        //console.log("-----staff filter end ------- "+staff_obj.user_id);
                                                    })
                                                    //console.log(staff);
                                                    

                                                }
                                                else
                                                {
                                                    //res.status(500).send({message:err4}); 
                                                    next(err4);
                                                }
                                            })
                                                                                      

                                        }
                                    })
                            
                }
     
    });
   
}


function staffTimingsForService(staff){

    var staff_hours ={};

    return new Promise((resolve,reject) =>{
 

        StaffModel.getStaffHourExceptions(staff, (err, excp_obj) => {
            if (err) {
        
                reject(err);
                
            }
            else{

                excp_obj = JSON.parse(JSON.stringify(excp_obj));
                
                //console.log(excp_obj);
                
                if(excp_obj.length != 0)
                {
                    //console.log(" exceptions");

                        staff_hours= {
                    //type:"exp",
                    open_time: excp_obj[0].user_hrex_open_time,
                    close_time: excp_obj[0].user_hrex_close_time,
                    offline:excp_obj[0].user_hrex_all_day_indicator

                    }
                
                resolve(staff_hours);
                            

                }
                else{
                                    
                    StaffModel.getStaffHours(staff, (err, hrs_obj) => {
                        if (err) {
                    
                            reject(err);
                            
                        }
                        else{
                            //console.log("hours into");

                            hrs_obj = JSON.parse(JSON.stringify(hrs_obj));
                            
                            //console.log(hrs_obj);

                            if(hrs_obj.length > 0)
                            {

                                staff_hours= {
                                                //type:"hrs",
                                                open_time: hrs_obj[0].user_hrwk_open_time,
                                                close_time: hrs_obj[0].user_hrwk_close_time,
                                                offline: hrs_obj[0].user_hrwk_all_day_note

                                            }
                            }
                            else
                            {
                                staff_hours= {
                                    //type:"hrs",
                                    open_time: "",
                                    close_time: "",
                                    offline: 1
                                }

                            }
                            resolve(staff_hours)
                                                                    
                        }
                    })
                }
            }
       })
    })
}



exports.getAvailableStaffSlots = (req, res, next) => {

    if(!req.body.store_id || !req.body.serv_id || !req.body.start_date || !req.body.end_date) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }


    var staff_req = req.body;

    staff_req.active_staff =1;
    //get Staff for Store
        StaffModel.getStaffByStore(staff_req, (err, staff) => {
            if (err) {
    
                    next(err);
            }
            else{
                staff = JSON.parse(JSON.stringify(staff));
                //console.log(staff);

                var store_req = {
                    store_id: req.body.store_id
                }


                StoresModel.getStoreHours(store_req, (err, store_hrs) => {
                    if (err) {
            
                            next(err);
                    }
                    else{
                        //staff = JSON.parse(JSON.stringify(staff));
                        //console.log(store_hrs);
                                              
                        SettingsModel.getStaffServiceBlocks(req.body, (blocks_err, blocks_data) => {
                            if (blocks_err) {
                                next(blocks_err);
                            }
                            else
                            {
                                //console.log(blocks_data);

                                var staff_serv_blocks = [];

                                for(i=0; i<blocks_data.length; i++)
                                {

                                    staff_serv_blocks.push(blocks_data[i].user_id);
                                    // console.log("staff-- ",blocks_data[i].user_id);
                                    // console.log("i -- ",i);
                                }

//                                console.log("blocks loop completed : ",staff_serv_blocks);
                                
                            
                                var start_date = moment(req.body.start_date,"YYYY-MM-DD");
                                var end_date = moment(req.body.end_date,"YYYY-MM-DD");
                            
                                var date_array= [];
                                var this_date = start_date;

                                while(this_date <= end_date)
                                {
                                    var this_date_obj = {
                                        date: moment(this_date).format("YYYY-MM-DD"),
                                        day_of_week: (moment(this_date).isoWeekday()) - 1,
                                        store_business_hours:{},
                                        staff:[]
                                    }
                                    
                                    date_array.push(this_date_obj);
                                    this_date = this_date.add(1, 'days');
                                    //console.log(this_date);                            
                        
                                }
//                                console.log("date array completed: ", date_array);
                                       
                                var m=0;
                               
                                date_array.filter((date_obj)=>{
//                                console.log("date ------- ",date_obj)                                    
                                    
                                store_hrs.filter((hrs_obj)=>{
                                    if((hrs_obj.store_hrwk_is_active == 1) && (hrs_obj.store_hrwk_day_of_week == date_obj.day_of_week))
                                    {
                                        date_obj.store_business_hours = {
                                            //day_of_week: hrs_obj.store_hrwk_day_of_week,
                                            open_time: hrs_obj.store_hrwk_open_time,
                                            close_time: hrs_obj.store_hrwk_close_time,
                                            offline: hrs_obj.store_hrwk_all_day_note,
                                            
                                        }
                                        //console.log("store hrs obj ",hrs_obj);
                                    }
//                                    console.log("--store hrs--");
                                
                                    })
                                    
                                    var n=0;
                                                                
                                    staff.filter(async (staff_obj)=>{
//                                        console.log(`----------------------in staff filter ${staff_obj.user_id}`);

                                        if((staff_serv_blocks.indexOf(staff_obj.user_id)) > -1)
                                        {
                                            //staff blocked
                                                               
                                                    n++;
//                                                    console.log("blocked n - ",n); 

                                                    if(n == staff.length)
                                                    {
                                                        m++;
//                                                        console.log(staff.length)
//                                                        console.log("m -- ",m);

                                                    }

                                            
                                                

                                                if(m == date_array.length)
                                                        {
                                                            res.status(200).send({status:200, result:date_array});

                                                        }
                                            

                                        }
                                        else
                                        {
//                                            console.log("in staff hours : ", staff_obj.user_id);

                                            var this_staff={
                                                store_id : req.body.store_id,
                                                staff_id: staff_obj.user_id,
                                                date: date_obj.date,
                                                day_of_week: date_obj.day_of_week
                                                } ;

                                            var this_staff_obj= {
                                                user_id: staff_obj.user_id,
                                                user_first_name: staff_obj.user_first_name,
                                                user_last_name: staff_obj.user_last_name,
                                                user_staff_order: staff_obj.user_staff_order,
                                                user_perm_staff_type: staff_obj.user_perm_staff_type,
                                                staff_timings:{},
                                                staff_bookings:[]
                                            
                                            };

                                            try{
                                                this_staff_obj.staff_timings = await staffTimingsForService(this_staff); 
//                                                console.log("staff timings 1 ------------ : ",this_staff_obj.staff_timings);  
                                            }
                                            catch(err)
                                            {
                                                console.log("error staff timings: ", err)
                                            } 

                                            try{
                                                this_staff_obj.staff_bookings = await StaffModel.getStaffBookings(this_staff);
//                                                console.log("staff bookings 1 ------------: ",this_staff_obj.staff_bookings);  
                                            }
                                            catch(err){
                                                console.log("staff bookings error: ", err);
                                            } 
                                                        
                                            date_obj.staff.push(this_staff_obj);
//                                            console.log("date obj : ",date_obj);

                                                    n++;
//                                                    console.log("n -- ",n);
                                                   
                                                    if(n == staff.length)
                                                    {
                                                        m++;
//                                                        console.log(staff.length)
//                                                        console.log("m -- ",m);

                                                    }

                                            
                                                

                                                if(m == date_array.length)
                                                        {
                                                            res.status(200).send({status:200, result:date_array});

                                                        }
                                            }

                                                
                                            //console.log("-----staff filter end ------- "+staff_obj.user_id);
                                        })
                                        //console.log(date_array);
                                    })
                                        
                                                                            

                            }
                        })
                    }
                })
                            
                }
     
    });
   
}



exports.checkSelectedSlotAvailability = (req, res, next) => {

    if(!req.body.slots) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var slots = req.body.slots;
    var slot_availability = [];

    async.each(slots,(this_slot,callback) => {
        //console.log(this_slot)
        var this_slot_av = this_slot;

    BookingModel.checkSelectedSlotAvailability(this_slot, async (err, slots_data) => {
        if (err) {
            //return err;
            callback(err);
        }
        else{
            //console.log("slots data: ",slots_data);

            var this_staff={
                store_id : this_slot.store_id,
                staff_id: this_slot.staff_id,
                date: this_slot.slot_date,
                day_of_week: (moment(this_slot.slot_date).isoWeekday()) - 1,
                } ;

            try{
                    var staff_timings = await staffTimingsForService(this_staff); 
                    //console.log("staff timings 1 ------------ : ",staff_timings);  
               

                if(staff_timings.offline == 0)
                {

                    var staff_open_moment = moment(this_slot.slot_date+" "+staff_timings.open_time).unix(); 
                    
                    var staff_close_moment = moment(this_slot.slot_date+" "+staff_timings.close_time).unix(); 
                   
                    var slot_start_moment = moment(this_slot.slot_date+" "+this_slot.slot_start_time).unix(); 
                   
                    var slot_end_moment = moment(this_slot.slot_date+" "+this_slot.slot_end_time).unix(); 
                    //console.log("slot_end:",slot_end_moment);

                    if(((staff_open_moment <= slot_start_moment) && (slot_start_moment <= staff_close_moment)) && ((staff_open_moment <= slot_end_moment) && (slot_end_moment<= staff_close_moment)))
                    {
//                        console.log("1");
                        if(slots_data.length > 0)
                        {
                            this_slot_av.is_available = 0;

                        }
                        else
                        {
                            this_slot_av.is_available = 1;   

                        }

                    }
                    else
                    {
                        this_slot_av.is_available = 0;

                    }
                }
                else
                {
                    this_slot_av.is_available = 0;

                }



                  if(this_slot.serv_id)
                  {
                    var serv_details = await ServicesModel.getDetailsByServId(this_slot.serv_id); 
                    //console.log("service details ------------ : ",serv_details); 
                    //this_slot_av.store_id = serv_details[0].store_id;
                    this_slot_av.serv_id = serv_details[0].serv_id;
                    this_slot_av.serv_cat_id = serv_details[0].serv_cat_id
                    this_slot_av.serv_name = serv_details[0].serv_name;
                    this_slot_av.serv_description = serv_details[0].serv_description;
                    this_slot_av.serv_defdur = serv_details[0].serv_defdur ;
                    this_slot_av.serv_saleprice= serv_details[0].serv_saleprice;
                    this_slot_av.serv_deposit = serv_details[0].serv_deposit;
                    this_slot_av.serv_defprice = serv_details[0].serv_defprice;
                    this_slot_av.serv_prebuffertime =  serv_details[0].serv_prebuffertime;
                    this_slot_av.serv_postbuffertime = serv_details[0].serv_postbuffertime;
                    this_slot_av.serv_order = serv_details[0].serv_order;
                    this_slot_av.serv_is_active =  serv_details[0].serv_is_active;
                    this_slot_av.is_promo_service = serv_details[0].is_promo_service;
                    this_slot_av.serv_promo_created_date =  serv_details[0].serv_promo_created_date;
                    this_slot_av.serv_promo_updated_date = serv_details[0].serv_promo_updated_date;
                  }
                  
               
            
            
            //this_slot_av.slots = slots_data;
            //console.log("this_slot_av: ",this_slot_av);
                   slot_availability.push(this_slot_av);
                   callback();

            }
            catch(err)
            {
                    //console.log("error staff timings/service details: ", err);
                    //next(err);
                    //var error = err;
                    callback(err);
            }
                
        
        }
    });
        
    }, function (err) {
            if (err)
            {
                //console.log("err: ",err);
                next(err);
            }
            else
            {

                //console.log(slot_availability);
                var products_availability = [];
                
                if(req.body.products && req.body.products.length > 0)
                {
                    var products = req.body.products;
                    var product_ids = [];
                    var products_req = [];

                    for(i=0; i<products.length; i++)
                    {
                        product_ids.push(products[i].product_id);
                        products_req[products[i].product_id] = products[i];
                        //console.log("i", i);

                    }
                    // console.log("product_ids: ",product_ids);
                    // console.log("products req: ",products_req);

                    ProductsModel.getProductsStockDetails(product_ids, (err, products_data) => {
                        if (err) {
                            //return err;
                            next(err);
                        }
                        else{
                            //console.log("products data: ",products_data);

                            for(i=0; i<products_data.length; i++)
                            {
                               
                                var this_product_id = products_data[i].product_id;
                                 //console.log("this product req: ",products_req[this_product_id]);

                                var is_available = 1;
                                if((products_data[i].product_quantity < products_req[this_product_id].selected_quantity) || (products_data[i].product_is_active == 0))
                                {
                                    is_available = 0;

                                }
                                if(req.body.reschedule){
                                    is_available = 1;
                                }

                                var this_product_availability = {
                                    product_id: this_product_id,
                                    is_available: is_available,
                                    available_quantity: products_data[i].product_quantity,
                                    selected_quantity: products_req[this_product_id].selected_quantity,
                                    product_is_active: products_data[i].product_is_active
                                }
                                
                                products_availability.push(this_product_availability);
                                //console.log("i", i);

                            }
                             //console.log("product availability: ",products_availability);

                             res.status(200).send({status:200, result:{slots:slot_availability, products: products_availability}});

                        }
                    })
                    


                }
                else
                {
                    res.status(200).send({status:200, result:{slots:slot_availability}});
                }
            }
        })
    
   
}













