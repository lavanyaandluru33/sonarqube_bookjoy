const constants = require('../../../config/constants.js');
const ProductsModel = require('../models/products.model.js');
const SettingsModel = require('../../settings/models/settings.model.js');
const StoresModel = require('../../stores/models/stores.model.js');
const BookingModel = require('../../booking/models/booking.model.js');
const TransactionModel = require('../../transactions/models/transaction.model.js');

const ImagesLibrary = require('../../../libs/images.js');


var async = require("async");
var moment = require('moment'); 

exports.saveProduct = (req, res, next) => {

    if(!req.body.store_id || !req.body.product_name || (!req.body.product_quantity && req.body.product_quantity !=0) || (!req.body.product_sale_price && req.body.product_sale_price != 0)) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var product_created_date = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

    if(req.body.product_id && req.body.update_flag && req.body.update_flag == 1)
    {
        req.body.product_name = req.body.product_name.replace("'", "''");
        req.body.product_description = req.body.product_description.replace("'","''");
        var update_details = req.body;
        update_details.product_id = parseInt(req.body.product_id,10);
        update_details.product_updated_date = product_created_date;
        update_details.product_updated_by = req.body.product_created_by;
        //console.log("update details: ", update_details);

        ProductsModel.updateProduct(update_details, (err, data) => {
            if (err) {
    
                next(err);
            }
            else{
                //console.log(data);
                if(data.affectedRows == 0)
                {
                    res.status(400).send({status:400, result:{message:"No Product exists with given product_id in given store_id"}});

                }
                else
                {
                    res.status(200).send({status:200, result:{message:"Product updated successfully", product_id:update_details.product_id}});
                }
            }
        });
        

    }
    else
    {
    
        var product_details =  [ [req.body.store_id, req.body.product_name, 0, req.body.product_sku, req.body.product_barcode, req.body.product_description, req.body.product_quantity, 
            req.body.product_price, req.body.product_sale_price, req.body.product_internal_cost, req.body.product_is_active, product_created_date, req.body.product_created_by ] ];
        //console.log(product_details);

        ProductsModel.addProducts(product_details, (err, data) => {
            if (err) {

                next(err);
            }
            else{
                //console.log(data);
                var product_id = data.insertId;
                var product_images = req.body.product_images;

                if(product_images && product_images.length > 0)
                {
                    var uploaded_images = [];
                    var n = 0;
                    
                    async.each(product_images, function (this_img, callback)
                    {
                        //console.log("this_img: ")


                        ImagesLibrary.imageUploadCloudinary(this_img, function(upload_err, uploaded_details)
                        {


                            if (upload_err)
                                {
                                    //console.log("upload_err: ", upload_err);
                                    return callback({error: upload_err});
                                } else {

                                    var product_image_url = uploaded_details.image.secure_url;
                                    var product_image_public_id = uploaded_details.image.public_id;
                                    var product_thumbnail_url = uploaded_details.thumbnail.secure_url;
                                    var product_thumbnail_public_id = uploaded_details.thumbnail.public_id;

                                    //console.log("n: ",n);


                                    
                                        //insert new image
                                        if(n == 0)
                                        {
                                            var this_upload = [ product_id, req.body.store_id, product_image_url, product_image_public_id, product_thumbnail_url, 
                                                product_thumbnail_public_id, 1, 1, product_created_date, req.body.product_created_by ];

                                        }
                                        else
                                        {
                                            var this_upload = [ product_id, req.body.store_id, product_image_url, product_image_public_id, product_thumbnail_url, 
                                                product_thumbnail_public_id, 1, 0, product_created_date, req.body.product_created_by ];
                                        }

                                        n++;
                                        
                                        uploaded_images.push(this_upload);
                                        callback();

                                }
                            
                        })
                        

                    }, function (err) {
                        if (err) {
                            //res.status(400).send({status: 400, message: err});
                            //console.log("async loop error: " ,err);
                            next(err)
                        } 
                        else
                        {
                            //console.log("async loop completed , uploaded images:" ,uploaded_images);
                            ProductsModel.uploadProductImages(uploaded_images, (err, data) => {
                                if (err) {
                                    console.log(err);
                                    next(err);
                                } else {

                                    ProductsModel.getProductDetails(product_id, (product_err, product_data) => {
                                        if (product_err) {
                                            console.log(product_err);
                                            next(product_err);
                                        } else {

                                    //console.log(data);
                                    res.status(200).send({status:200, result:{message:"Product added Successfully", product_id:product_id, product: product_data}});
                                        }
                                    })
                                }
                            });
                        }


                    });

                }
                else
                {

                    ProductsModel.getProductDetails(product_id, (product_err, product_data) => {
                        if (product_err) {
                            console.log(product_err);
                            next(product_err);
                        } else {

                    //console.log(data);
                    res.status(200).send({status:200, result:{message:"Product added Successfully", product_id:product_id, product: product_data}});
                        }
                    })
                    //console.log("No images added");
                    //res.status(200).send({status:200, result:{message:"Product added Successfully", product_id:product_id}});
                        
                
                }
            }
        });
    }
   
}



exports.getProductsList = (req, res, next) => {

    if(!req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var store_id = parseInt(req.body.store_id,10);

    var product_req = {
        store_id : store_id
        
    }
    product_req.customer_side = 0;
    if(parseInt(req.body.customer_side)){
        product_req.customer_side = 1;
    }

    if(req.body.page_number || req.body.page_number == 0)
    {
        product_req.page_number = req.body.page_number;
    }
    if(req.body.search_data)
    {
        product_req.search_data = req.body.search_data;
    }
    //console.log(product_req);
   
    ProductsModel.getProductsList(product_req, (err, products_data) => {
        if (err) {

            next(err);
        }
        else{
            //console.log(data);
            async.each(products_data, function (this_product, callback)
            {
                var product_id = this_product.product_id;

                this_product.product_default_image = {
                    product_image_id:"",
                    product_image_url: "",
                    product_thumbnail_url : ""
                };



                ProductsModel.getProductDefaultImage(product_id, (image_err, product_image) => {
                    if (err) {
            
                        return callback({error: image_err});
                    }
                    else{
                        //console.log(product_image);

                        if(product_image.length > 0)
                        {

                          this_product.product_default_image.product_image_id = product_image[0].product_image_id;
                          this_product.product_default_image.product_image_url = product_image[0].product_image_url;
                          this_product.product_default_image.product_thumbnail_url = product_image[0].product_thumbnail_url;
                        
                        }
                        callback();

                    }


                
                    
                })
                

            }, function (err) {
                if (err) {
                    
                    //console.log("async loop error: " ,err);
                    next(err)
                } 
                else
                {
                    res.status(200).send({status:200, result:products_data});      
           
                    
                }


            });

        
           
        }
    });
   
}




exports.makeProductImageDefault = (req, res, next) => {

    if(!req.body.product_id || !req.body.product_image_id || !req.body.updated_by) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    
    var product_req = {
        product_id : parseInt(req.body.product_id,10),
        product_image_id: parseInt(req.body.product_image_id,10),
        updated_by:  parseInt(req.body.updated_by,10),
        updated_date : moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
    }
    //console.log(product_req);
   
    ProductsModel.makeProductImageDefault(product_req, (err,data) => {
        if (err) {

            next(err);
        }
        else{
            //console.log(data);
        
            res.status(200).send({status:200, result:{message: "Product image is made default successfully"}});    
           
        }
    });
   
}


exports.updateProductsOrder = (req, res, next) => {

    if(!req.body.store_id || !req.body.products_order || !req.body.updated_by) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var products_order = req.body.products_order;

    // var products_array = [];
    // var order_array = [];
    var product_req = [];
    var j = 0;

    for(i=0; i<products_order.length; i++)
    {

    
        product_req[i] = {
            store_id : parseInt(req.body.store_id,10),
            product_id:products_order[i].product_id,
            product_order: products_order[i].product_order,
            updated_by:  parseInt(req.body.updated_by,10),
            updated_date : moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
        }
        //console.log(product_req[i]);
    
        ProductsModel.updateProductsOrder(product_req[i], (err,data) => {
            if (err) {

                next(err);
            }
            else{
                //console.log(data);
                //console.log("j: ",j);
                j++;

                if(j == products_order.length)
                {
                    res.status(200).send({status:200, result:{message: "Products order is saved successfully"}});
                }    
            
            }
        });
    }
   
}



exports.saveProductImages = (req, res, next) => {

    if(!req.body.product_id || !req.body.image_details || (req.body.image_details.length < 1) || !req.body.created_by || !req.body.store_id || (!req.body.update_flag && req.body.update_flag != 0)) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var created_date = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    
    
   
            var product_id = parseInt(req.body.product_id,10);
            var product_images = req.body.image_details;
            var update_flag = parseInt(req.body.update_flag,10);
            //console.log("update_flag: ",update_flag);

           
                var uploaded_images = [];
                var result_array = [];

                var n= 0;
                var default_image_exists = 0;

                ProductsModel.checkProductDefaultImageExists(product_id, (default_err, default_data) => {
                    if (default_err) {
                        //console.log(err);
                        next(default_err);
                    } else {
                        //console.log(default_data);
                        if(default_data.length > 0)
                        {
                            default_image_exists = 1;
                        }
                
                        async.each(product_images, function (this_img, callback)
                        {
                            

                            ImagesLibrary.imageUploadCloudinary(this_img.image, function(upload_err, uploaded_details)
                            {


                                if (upload_err)
                                {
                                    //console.log("upload_err: ", upload_err);
                                    return callback({error: upload_err});
                                } else {                
                                    
                                        if (this_img.product_image_id && update_flag == 1)
                                        {
                                                //update image
                                                var edit_req = {

                                                    image_url: uploaded_details.image.secure_url,
                                                    image_public_id: uploaded_details.image.public_id,
                                                    thumbnail_url: uploaded_details.thumbnail.secure_url,
                                                    thumbnail_public_id: uploaded_details.thumbnail.public_id,
                                                    product_image_id: this_img.product_image_id,
                                                    //is_default: this_img.is_default,
                                                    updated_by: req.body.created_by,
                                                    updated_date: created_date,
                                                    product_id: product_id
                                                }

                                                var this_image_urls = {
                                                    product_image_id: this_img.product_image_id,
                                                    product_image_url : uploaded_details.image.secure_url,
                                                    image_public_id: uploaded_details.image.public_id,
                                                    product_thumbnail_url : uploaded_details.thumbnail.secure_url,
                                                    thumbnail_public_id: uploaded_details.thumbnail.public_id,

                                                }

                                                result_array.push(this_image_urls);


                                                ProductsModel.updateProductImages(edit_req, (err, data) => {
                                                    if (err) {
                                                        //console.log(err);
                                                        next(err);

                                                    } else {
                                                        //console.log("update image data :", data);
                                                    

                                                        if(data.affectedRows == 0)
                                                        { 
                                                            return callback({error: "Image with given product_image_id not found for given product_id"});

                                                        }

                                                        ImagesLibrary.imageDeleteCloudinary(this_img.image_public_id, this_img.thumbnail_public_id, (delete_err, delete_result) => {
                                                            if (delete_err)
                                                            {
                                                                //console.log("delete_err: ", delete_err);
                                                                return callback({error: delete_err});
                                                            } else {                
                                                                //console.log(delete_result);
                                                                callback();
                                                            }

                                                        })


                                                    }
                                                });

                                        } else
                                        {

                                            var product_image_url = uploaded_details.image.secure_url;
                                            var product_image_public_id = uploaded_details.image.public_id;
                                            var product_thumbnail_url = uploaded_details.thumbnail.secure_url;
                                            var product_thumbnail_public_id = uploaded_details.thumbnail.public_id;
                                    
                                            //insert new image
                                            if(n == 0 && default_image_exists == 0)
                                            {
                                                var this_upload = [ product_id, req.body.store_id, product_image_url, product_image_public_id, product_thumbnail_url, 
                                                    product_thumbnail_public_id, 1, 1, created_date, req.body.created_by ];

                                            }
                                            else
                                            {
                                                var this_upload = [ product_id, req.body.store_id, product_image_url, product_image_public_id, product_thumbnail_url, 
                                                    product_thumbnail_public_id, 1, 0, created_date, req.body.created_by ];
                                            }
                                            
                                            uploaded_images.push(this_upload);
                                            n++;
                                            var this_image_urls = {
                                                product_image_id : "",
                                                product_image_url : product_image_url,
                                                product_image_public_id : product_image_public_id,
                                                product_thumbnail_url : product_thumbnail_url,
                                                product_thumbnail_public_id : product_thumbnail_public_id,

                                            }
                                            result_array.push(this_image_urls);
                                        
                                            callback();
                                        }

                                    }
                                
                            })
                            

                        }, function (err) {
                            if (err) {
                                //console.log("async loop error: " ,err);
                                next(err)
                            } 
                            else
                            {
                                //console.log("async loop completed , uploaded images:" ,uploaded_images);

                                if (update_flag == 1)
                                {
                                    res.status(200).send({status: 200, result: {message:"Product images updated successfully", image_urls:result_array}});
                                }
                                else
                                {
                                    //console.log("add images");
                                    ProductsModel.uploadProductImages(uploaded_images, (err, data) => {
                                        if (err) {
                                            //console.log(err);
                                            next(err);
                                        } else {

                                            //console.log(data);
                                            var product_image_id = data.insertId;
                                            //console.log("product_image_id: ",product_image_id);
                                            for(i=0; i<result_array.length; i++)
                                            {
                                                //console.log(result_array[i]);
                                                result_array[i].product_image_id = product_image_id;
                                                product_image_id++;
                                            }
                                            res.status(200).send({status:200, result:{message:"Product images added successfully", product_id:product_id, image_urls:result_array}});
                                        }
                                    });
                                }
                            }


                        });
                    }
               });
            
        
   
}


exports.deleteProductImages = (req, res, next) => {

    if(!req.body.product_id || !req.body.image_details || (req.body.image_details.length < 1) || !req.body.updated_by) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    //var created_date = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

    var product_images = req.body.image_details;
    var delete_image_ids = [], delete_images = [], delete_images_thumb = [];
    
    
   
            var product_id = parseInt(req.body.product_id,10);
            //var product_images = req.body.image_details;
            

           
                for(i=0;i< product_images.length;i++)
                {
                
                // async.each(product_images, function (this_img, callback)
                // {
                    delete_image_ids.push(product_images[i].product_image_id);
                    delete_images.push(product_images[i].image_public_id);
                
                    delete_images_thumb.push(product_images[i].thumbnail_public_id);
                    delete_images.push(product_images[i].thumbnail_public_id);
                    //callback();
                   // console.log("i: ",i);
                }
                             

                // }, function (err) {
                //     if (err) {
                //         //console.log("async loop error: " ,err);
                //         next(err)
                //     } 
                //     else
                //     {
                        //console.log("async loop completed , uploaded images:" ,uploaded_images);

                       
                            // console.log("delete image ids: ", delete_image_ids);
                            // console.log("delete images: ", delete_images);
                            // console.log("delete images thumb: ", delete_images_thumb);

                            var delete_req = {
                                delete_image_ids: delete_image_ids,
                                product_id: parseInt(req.body.product_id,10)
                            }
                            
                            ProductsModel.deleteProductImages(delete_req, (err, data) => {
                                if (err) {
                                    //console.log(err);
                                    next(err);
                                } else {

                                    //console.log(data);

                                    ImagesLibrary.imageDeleteArrayCloudinary(delete_images, (delete_err, delete_result) => {
                                        if (delete_err)
                                        {
                                            res.status(400).send({error: delete_err});
                                        } else {
                                            res.status(200).send({status:200, result:{message:"Product images deleted successfully", product_id:product_id}});
                                           
                                        }
                                    });
                                    
                                }
                            });
                        
                //     }


                // });
            
        
   
}



exports.getProductDetails = (req, res, next) => {

    if(!req.body.product_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var product_id = parseInt(req.body.product_id,10);
    
   
   
    ProductsModel.getProductDetails(product_id, (err, data) => {
        if (err) {

            next(err);
        }
        else{
            //console.log(data);
            var products_data = data[0];
            products_data.product_images = [];

            ProductsModel.getProductImages(product_id, (images_err, product_images) => {
                if (images_err) {
        
                    next(images_err);
                }
                else{
                    //console.log(product_images);
                    //console.log(products_data);

                    products_data.product_images = product_images;
            
                    res.status(200).send({status:200, result:products_data});      
           
                    
                }


            });

        
           
        }
    });
}

exports.deleteProduct = (req, res, next) => {

    if(!req.body.product_id || !req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var product_id = parseInt(req.body.product_id,10);
    var store_id = parseInt(req.body.store_id,10);

    product_req = {
        product_id : product_id,
        store_id: store_id
    }
    
   
   
    ProductsModel.deleteProduct(product_req, (err, data) => {
        if (err) {

            next(err);
        }
        else{
            //console.log(data);
            if(data.affectedRows <= 0)
            {
                res.status(400).send({status:400, result:{message: "No product exists with given product_id in given store_id"}});      
                return;
            }
            else
            {

                ProductsModel.deleteImagesOfProduct(product_id, (images_err, product_images) => {
                    if (images_err) {
            
                        next(images_err);
                    }
                    else{
                        //console.log(product_images);
                        
                        res.status(200).send({status:200, result:{message: "Product deleted successfully"}});      
            
                        
                    }
                });

            }       
           
        }
    });
}


exports.updateBookingProductQuantity = (req, res, next) => {

    if(!req.body.product_details || !req.body.store_id || !req.body.updated_by || !req.body.updated_through) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var product_details = req.body.product_details
    var product_update = [];
    var booking_update = [];

    var updated_date = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    var booking_product_ids = [];

    for(i=0; i< product_details.length; i++)
    {
        var update_quantity = product_details[i].total_quantity + product_details[i].old_quantity - product_details[i].new_quantity;
        //  var this_product = [product_details[i].product_id, req.body.store_id, update_quantity];
        //  var this_booking = [product_details[i].booking_product_id, product_details[i].product_id, req.body.store_id, product_details[i].new_quantity, req.body.updated_by, updated_date, req.body.updated_through];
         product_update.push([product_details[i].product_id,req.body.store_id,update_quantity]);
         booking_update.push([product_details[i].booking_product_id, product_details[i].new_quantity]);
         booking_product_ids.push(product_details[i].booking_product_id);
         console.log("i: ",i);
    }
    //console.log("product_update: ", product_update);
   // console.log("booking_update: ", booking_update);

    var details_req = {
        booking_product_ids: booking_product_ids,
        updated_by: req.body.updated_by,
        updated_date: updated_date,
        updated_through: req.body.updated_through,

    }

   

       
        ProductsModel.updateBookingProductQuantity(booking_update, (booking_err,booking_data) => {
            if (booking_err) {

                next(booking_err);
            }
            else{
                //console.log("booking_data: ",booking_data);

                ProductsModel.updateBookingProductDetails(details_req, (details_err,details_data) => {
                    if (details_err) {
        
                        next(details_err);
                    }
                    else{
                       // console.log("details_data: ",details_data);

                ProductsModel.updateProductQuantaty(product_update, (product_err,product_data) => {
                    if (product_err) {
        
                        next(product_err);
                    }
                    else{
                        //console.log("product_data: ",product_data);
            
                                res.status(200).send({status:200, result:{message: "Product quantity updated successfully in the booking"}});
                    }
                })
            }
        })
           
            
            }
        });
      
 }



 exports.updateProductBookingStatus = (req, res, next) => {

    if((!req.body.booking_product_id && !req.body.multi_service_book_id) || !req.body.book_status || !req.body.updated_by || !req.body.updated_through) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var updated_date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');

    var multi_id_flag = 0;

    var product_req = {
        book_id: req.body.booking_product_id,
        book_status: req.body.book_status,
        updated_by: req.body.updated_by,
        updated_date: updated_date,
        updated_through: req.body.updated_through,

    }
     
        ProductsModel.updateProductBookingStatus(product_req, multi_id_flag, (product_err, product_data) => {
            if (product_err) {

                next(product_err);
            }
            else{
//                console.log("product_data: ",product_data);

                if((req.body.book_status == constants.STATUS_CANCELLED || req.body.book_status == constants.STATUS_LATE_CANCELLED))
                {

                    ProductsModel.getProductBookingDetails(req.body.booking_product_id, multi_id_flag, (product_err, product_data) => {
                        if (product_err) {
            
                            next(product_err);
                        }
                        else{
//                            console.log("product_data: ",product_data);
                            var old_product_req = [req.body.booking_product_id, product_data[0].booking_old_product_id];

                            var product_quantity_update = [[product_data[0].product_id, product_data[0].store_id, (product_data[0].remaining_product_quantity+product_data[0].product_quantity)]]; 
                            
                            ProductsModel.updateProductQuantaty(product_quantity_update,(update_err,update_data)=>{
                                if(update_err){
//                                    console.log('error updating products')
                                    next(update_err);
                                }
                                else
                                {
//                                    console.log("quantity update_data:", update_data);
                                         
                      
                        
                        var time_diff = moment.duration(moment(product_data[0].product_date).diff(moment())).asHours();
//                        console.log("time diff: ",time_diff);
                        //console.log(moment().format("YYYY-MM-dd HH:mm"));
                            if(time_diff >= 24)
                            {
                                //console.log("adding wallet");
                                let booking_product_ids_array =[],temp_old_book_product_id;
                                if(old_product_req[1]){
                                    temp_old_book_product_id = old_product_req[1];
                                }else{
                                    temp_old_book_product_id = old_product_req[0];
                                }
                                ProductsModel.getOldProductBookings(temp_old_book_product_id, (old_err, old_data) => {
                                    if (old_err) {
                                        next(old_err);
                                    }
                                    else{
//                                        console.log("product old data: ",old_data);
                                        for(i = 0; i< old_data.length; i++)
                                        {
                                            booking_product_ids_array.push(old_data[i].booking_product_id);
                                        }
//                                        console.log("boooking product ids :", booking_product_ids_array);

                                        ProductsModel.getProductBookPaidInvoice(booking_product_ids_array, async (invoice_err, invoice_data) => {
                                            if (invoice_err) {
                                
                                                next(invoice_err);
                                                return;
                                            }
                                            else{
//                                                console.log("invoice_data: ",invoice_data);
                                                var paid_amount = invoice_data[0].total_book_trans_paid_now;
        
                                            
                                                //paid_amount = await TransactionModel.getAmount(book_ids);
                                                if(paid_amount > 0)
                                                {
                                                    let insert_wallet_data = [[
                                                        product_data[0].product_cust_id,
                                                        product_data[0].product_cust_email,
                                                        constants.WALLET_CREDIT,
                                                        paid_amount,
                                                        "added amount for cancellation of product "+req.body.booking_product_id,
                                                        product_data[0].store_id,
                                                        0,
                                                        req.body.booking_product_id
                                                        
                                                    ]];
//                                                    console.log("insert_wallet_data",insert_wallet_data);
                                                    try{
                                                        await TransactionModel.insert_transaction_and_wallet_credit(insert_wallet_data,[]);
                                                    }
                                                    catch(error)
                                                    {
                                                        var wallet_err = error;
//                                                        console.log("catch Error: ", error);
                                                        //next(wallet_err);
                                                    }
                                                }
                                            }
                                        })

                                    }
                                })
                            }
                            
                                res.status(200).send({status:200, result:{message: "Product booking status updated successfully"}});
                            }
                        });
                    }
                })
   
            
            }
        }
        });
      
 }

 



