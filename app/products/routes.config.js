const ProductsController = require('./controllers/products.controller');
const auth = require('../../config/auth');
exports.routesConfig = function (app) {
  
    
    
    app.post('/v2/products/save_product_v2', 
        ProductsController.saveProduct
    );

    app.post('/v2/products/get_products_list_v2', 
        ProductsController.getProductsList
    );

    app.post('/v2/products/make_image_default_v2', 
        ProductsController.makeProductImageDefault
    );

    app.post('/v2/products/update_products_order_v2', 
        ProductsController.updateProductsOrder
    );


    app.post('/v2/products/save_product_images_v2', 
        ProductsController.saveProductImages
    );

    app.post('/v2/products/delete_product_images_v2', 
        ProductsController.deleteProductImages
    );

    app.post('/v2/products/get_product_details_v2', 
        ProductsController.getProductDetails
    );

    app.post('/v2/products/delete_product_v2', 
        ProductsController.deleteProduct
    );

    app.post('/v2/products/update_product_booking_quantity_v2', 
        ProductsController.updateBookingProductQuantity
    );

    app.post('/v2/products/update_product_booking_status_v2', 
        ProductsController.updateProductBookingStatus
    );


    
    
}