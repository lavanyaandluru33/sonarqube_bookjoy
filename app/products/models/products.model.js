const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js');


exports.addProducts = (data, result) => {

  let sql = `INSERT INTO products_mst (store_id, product_name, product_cat_id, product_sku, product_barcode, product_description, product_quantity, 
    product_price, product_sale_price, product_internal_cost, product_is_active, product_created_date, product_created_by) VALUES ? `;
 
    //console.log(sql);

      db.query(sql , [data], (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);

              return  result(null, res);        
       
        });
           
}




exports.uploadProductImages = function (images_data,result) {
  let sql = `INSERT INTO product_images_mst (product_id, store_id, product_image_url, product_image_public_id, product_thumbnail_url, product_thumbnail_public_id, is_active, is_default, created_date, created_by) VALUES ? `;
  
  //console.log(sql);

  db.query(sql ,[images_data], (err,res) => {
      if (err) {
         
          return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
      
      }
     // console.log(res);
      return  result(null, res);
  });
}



exports.getProductsList = (data, result) => {
  

  let sql = `SELECT p.product_id, p.store_id, p.product_name, p.product_description, p.product_order, p.product_sku, p.product_barcode, p.product_quantity, 
    p.product_price, p.product_sale_price, p.product_internal_cost, p.product_is_active FROM products_mst p 
    WHERE p.store_id = ${data.store_id}`;
    if(data.customer_side == 1){
        sql+=` and p.product_is_active = 1`;
    }
    if(data.search_data && data.search_data.length > 0)
    {
      data.search_data = data.search_data.trim();
      sql += ` AND (p.product_name LIKE '%${data.search_data}%' OR p.product_sku LIKE '%${data.search_data}%')`;
    }
    
    sql += ` ORDER BY p.product_order, p.product_id`;

    if(data.page_number || data.page_number == 0)
    {
      var index = data.page_number *10;
      sql += ` LIMIT ${index},10`;

    }
 
    //console.log(sql);

      db.query(sql, (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);

              return  result(null, res);        
       
        });
           
}



exports.getProductDefaultImage = (product_id, result) => {

  let sql = `SELECT product_image_id, product_id, store_id, product_image_url , product_thumbnail_url , is_default FROM product_images_mst
    WHERE product_id = ${product_id} AND is_active = 1 ORDER BY is_default DESC, product_image_id LIMIT 1`;
 
    //console.log(sql);

      db.query(sql, (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);

              return  result(null, res);        
       
        });
           
}

exports.makeProductImageDefault = (data, result) => {

  let sql = `UPDATE product_images_mst SET is_default = CASE WHEN product_image_id = ${data.product_image_id} THEN 1 ELSE 0 END, updated_by = ${data.updated_by},  
  updated_date = '${data.updated_date}' WHERE product_id = ${data.product_id} `;
 
    //console.log(sql);

      db.query(sql, (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);

              return  result(null, res);        
       
        });
           
}

exports.updateProductsOrder = (data, result) => {

  let sql = `UPDATE products_mst SET product_order = ${data.product_order} , product_updated_by = ${data.updated_by}, product_updated_date = '${data.updated_date}' WHERE  product_id = ${data.product_id} AND store_id = ${data.store_id} `;
 
    //console.log(sql);

      db.query(sql,(err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, res);        
       
        });
           
}


exports.updateProductImages = (data, result) => {

  let sql = `UPDATE product_images_mst SET product_image_url = '${data.image_url}', product_image_public_id = '${data.image_public_id}', product_thumbnail_url = '${data.thumbnail_url}' , product_thumbnail_public_id= '${data.thumbnail_public_id}', 
   updated_by= ${data.updated_by}, updated_date = '${data.updated_date}' WHERE product_image_id = ${data.product_image_id} AND product_id = ${data.product_id}`;
  
  //console.log(sql);

  db.query(sql , (err,res) => {
      if (err) {
          console.log(err);
          return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
      }
      return  result(null, res);
  });

}



exports.deleteProductImages = (data,result) =>{
  let sql = `DELETE FROM product_images_mst WHERE product_image_id IN (${data.delete_image_ids}) AND product_id = ${data.product_id}`; 
  //console.log(sql);
  
  db.query(sql , (err,res) => {
       if (err) {
           return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
       }
       return  result(null, res);
   });
}

exports.getProductDetails = (product_id, result) => {

  let sql = `SELECT product_id, store_id, product_name, product_sku, product_barcode, product_description, product_quantity, 
  product_price, product_sale_price, product_internal_cost, product_is_active FROM products_mst WHERE product_id = ${product_id}`;
 
    //console.log(sql);

      db.query(sql, (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);

              return  result(null, res);        
       
        });
           
}



exports.getProductImages = (product_id, result) => {

  let sql = `SELECT product_image_id, product_id, product_image_url ,  product_image_public_id, product_thumbnail_url , product_thumbnail_public_id, is_default FROM product_images_mst
    WHERE product_id = ${product_id} AND is_active = 1 ORDER BY is_default DESC, product_image_id`;
 
    //console.log(sql);

      db.query(sql, (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);

              return  result(null, res);        
       
        });
           
}


exports.updateProduct = (data, result) => {

  let sql = `UPDATE products_mst SET product_name = '${data.product_name}', product_sku = '${data.product_sku}', product_barcode = '${data.product_barcode}', product_description = '${data.product_description}', 
  product_quantity = ${data.product_quantity}, product_price = ${data.product_price}, product_sale_price = ${data.product_sale_price}, product_internal_cost = ${data.product_internal_cost}, product_is_active = ${data.product_is_active}, 
  product_updated_date = '${data.product_updated_date}', product_updated_by = ${data.product_updated_by} WHERE product_id = ${data.product_id} AND store_id = ${data.store_id}`;
 
    //console.log(sql);

      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);

              return  result(null, res);        
       
        });
           
}

exports.updateProductQuantaty = (data, result) => {
    sql =`INSERT into products_mst (product_id,store_id, product_quantity) VALUES ? ON DUPLICATE KEY UPDATE product_quantity = VALUES(product_quantity)`;
    db.query(sql ,[data], (err,res) => {
        if (err) {
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }else{
                  sql = 'delete from products_mst where product_name is null';
                  db.query(sql, (err,res) => {
                    if (err) {
                        return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                    }
                    return  result(null, res);        
                  });
              }
        });
}

exports.deleteProduct = (data,result) =>{
  let sql = `DELETE FROM products_mst WHERE product_id = ${data.product_id} AND store_id = ${data.store_id}`; 
  console.log(sql);
  
  db.query(sql , (err,res) => {
       if (err) {
           return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
       }
       return  result(null, res);
   });
}

exports.deleteImagesOfProduct = (product_id,result) =>{
  let sql = `DELETE FROM product_images_mst WHERE product_id = ${product_id}`; 
  console.log(sql);
  
  db.query(sql , (err,res) => {
       if (err) {
           return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
       }
       return  result(null, res);
   });
}

exports.checkProductDefaultImageExists = (product_id, result) => {

  let sql = `SELECT product_image_id, product_id, store_id, product_image_url , product_thumbnail_url , is_default FROM product_images_mst
    WHERE product_id = ${product_id} AND is_default = 1 AND is_active = 1`;
 
    //console.log(sql);

      db.query(sql, (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);

              return  result(null, res);        
       
        });
           
}

exports.getProductsInvoiceDetails = (book_id,result) => {
  
    let sql = `SELECT b.booking_product_id, b.product_image_url, b.product_thumbnail_url, b.product_name, b.product_sku, b.product_barcode, b.product_cust_id, b.product_cust_first_name, b.product_cust_last_name, b.product_date, 
    b.product_quantity, b.product_deposit, b.product_full_amount, b.product_book_status, b.created_date, b.multi_service_book_id, b.multi_service_booking_type, b.booking_old_product_id , 
    b.created_by, b.updated_date, b.updated_by , b.store_id, b.product_cust_email , pm.product_quantity as product_remaining_quantity, pm.product_price as product_compare_price  
    FROM booking_products_mst b LEFT JOIN products_mst pm ON b.product_id = pm.product_id WHERE b.multi_service_book_id = ${book_id}`;
  
    //console.log(sql);

  db.query(sql , (err,res) => {
    if (err) {
      //console.log(err)
        return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
    }
    //console.log(res);
    
        return result(null,res);
   });

}



// exports.getOldProductBookings = (book_id) => {

//   return new Promise((resolve, reject) => {
//     db.query(`SELECT booking_product_id, book_old_product_id , product_book_status FROM booking_products_mst WHERE (booking_product_id = ${book_id} OR book_old_product_id = ${book_id}) ORDER BY b.book_id`, function (err, result) {
//         if (err)
//             reject({error_code:err.code, message:err.sqlMessage,sql:err.sql});
//         else
//             resolve(result);
//     });
// });
// }



exports.getProductBookPaidInvoice = (book_ids, result) => {


  let sql = `SELECT IFNULL(SUM(book_trans_paid_now),0) as total_book_trans_paid_now, IFNULL(MAX(book_trans_created_date),"") AS last_book_trans_created_date, min(book_trans_gateway_method) payment_type1,max(book_trans_gateway_method) payment_type2 FROM booking_transactions_mst WHERE booking_product_id IN (${book_ids}) AND booking_type = 3`;
  
  //console.log(sql);

  db.query(sql , (err,res) => {
    if (err) {
      //console.log(err)
        return result({error_code:err.code, message:err.sqlMessage,sql:err.sql},null);
        
    }
    //console.log(res);
    
    return result(null , res);
  });
}


exports.ProductsBookingLog = (book_ids, multi_id_flag, result) => {
 
  let sql=`SELECT b.booking_product_id, b.product_name, b.product_sku, b.product_barcode, b.product_cust_id, b.product_cust_first_name, b.product_cust_last_name, b.product_date, 
  b.product_quantity, b.product_deposit, b.product_full_amount, b.product_book_status as book_status, b.store_id, b.product_cust_email, b.created_date as book_created_date, b.created_by, b.updated_date as book_updated_date, b.updated_by , 
  CASE WHEN b.created_by = b.product_cust_id THEN (SELECT concat(c.cust_first_name, " ", c.cust_last_name) from owner_customers_mst c where c.cust_id = b.created_by) ELSE (SELECT concat(u.user_first_name, " ", u.user_last_name) from users_mst u where u.user_id = b.created_by) END created_by_name,
  CASE WHEN b.updated_by = b.product_cust_id THEN (SELECT concat(c.cust_first_name, " ", c.cust_last_name) from owner_customers_mst c where c.cust_id = b.updated_by) ELSE (SELECT concat(u.user_first_name, " ", u.user_last_name) from users_mst u where u.user_id = b.updated_by) END updated_by_name, b.multi_service_book_id, b.multi_service_booking_type, b.booking_old_product_id as book_oldbookid FROM booking_products_mst b
  WHERE (b.product_book_status !=  ${constants.STATUS_PRE_BOOKED})`;
  
  if(multi_id_flag == 1)
  {
      sql += ` AND b.multi_service_book_id IN (${book_ids})`;
  }
  else
  {
    sql += ` AND b.booking_product_id IN (${book_ids})`;

  }
  
   //console.log(sql);

  db.query(sql , (err,res) => {
    if (err) {
      //console.log(err)
        return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
    }
     //console.log(res);
    
        return result(null,res);
  });

}

exports.getOldProductBookings = (book_id, result) => {


  let sql = `SELECT booking_product_id, booking_old_product_id , product_book_status FROM booking_products_mst WHERE (booking_product_id IN (${book_id}) OR booking_old_product_id IN (${book_id}))`;
  
//  console.log(sql);

  db.query(sql , (err,res) => {
    if (err) {
      //console.log(err)
        return result({error_code:err.code, message:err.sqlMessage,sql:err.sql},null);
        
    }else{
    //console.log(res);
    
    return result(null , res);
    }
  });
}

exports.getProductsBookingTransactions = (book_ids, result) => {
  
  let sql = `SELECT book_trans_id,booking_product_id,book_trans_sold_price,book_trans_added_tax,book_trans_total_amount,book_trans_paid_now,book_trans_balance_due,book_trans_confirmed,book_trans_created_date,
  book_trans_updated_date, book_trans_gateway_txn_id , book_trans_gateway_method FROM booking_transactions_mst WHERE booking_product_id IN (${book_ids}) AND booking_type = 3 ORDER BY book_trans_id`;
  
  //console.log(sql);

  db.query(sql , (err,res) => {
    if (err) {
      //console.log(err)
        return result({error_code:err.code, message:err.sqlMessage,sql:err.sql},null);
        
    }
    //booking_data.service_bookings.payment = services_result;
    return result(null , res);
  });
}



exports.updateBookingProductQuantity = (data, result) => {

  //let sql = `UPDATE booking_products_mst SET product_quantity = ${data.product_quantity} , updated_by = ${data.updated_by}, updated_date = '${data.updated_date}', updated_through = ${data.updated_through} WHERE booking_product_id = ${data.booking_product_id} `;
  let sql =  `INSERT into booking_products_mst (booking_product_id, product_quantity) VALUES ? ON DUPLICATE KEY UPDATE product_quantity = VALUES(product_quantity)`;
 
    //console.log(sql);

      db.query(sql, [data], (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, res);        
       
        });
           
}
exports.updateBookingProductQuantityReschedule = (data, result) => {
  //let sql = `UPDATE booking_products_mst SET product_quantity = ${data.product_quantity} , updated_by = ${data.updated_by}, updated_date = '${data.updated_date}', updated_through = ${data.updated_through} WHERE booking_product_id = ${data.booking_product_id} `;
  let sql =  `INSERT into booking_products_mst (booking_product_id, product_quantity,multi_service_book_id) VALUES ? ON DUPLICATE KEY UPDATE product_quantity = VALUES(product_quantity), multi_service_book_id = values(multi_service_book_id)`;
 
    //console.log(sql);

      db.query(sql, [data], (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, res);        
       
        });
           
}

exports.updateBookingProductDetails = (data, result) => {

  let sql = `UPDATE booking_products_mst SET updated_by = ${data.updated_by}, updated_date = '${data.updated_date}', updated_through = ${data.updated_through} WHERE booking_product_id IN (${data.booking_product_ids}) `;
  
    //console.log(sql);

      db.query(sql, [data], (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, res);        
       
        });
           
}

exports.updateProductMultiserviceBookId = (multi_service_book_id, booking_product_ids, multi_id_flag, result) => {

  let sql = `UPDATE booking_products_mst SET multi_service_book_id = ${multi_service_book_id} WHERE booking_product_id IN (${booking_product_ids})`;

   if(multi_id_flag == 1){
      sql += ` OR multi_service_book_id IN (${booking_product_ids})`;
   }
  
      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, { message: `Successfully updated booking`});
       
        });
           
}



exports.productsRescheduleUpdate = (data, result) => {

  let sql = `UPDATE booking_products_mst SET product_book_status = ${data.book_status}, updated_date = '${data.updated_date}' , updated_by = ${data.updated_by}, updated_through = ${data.updated_through} , multi_service_book_id = ${data.multi_service_book_id} WHERE booking_product_id IN (${data.booking_product_ids})`;
   

    //console.log(sql);


      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
            
              return  result(null, res);
        
       
       });
           
}  


exports.updateProductBookingStatus = (data, multi_id_flag, result) => {
    let sql = '';
    /* this is come from whole status */
  if(multi_id_flag){
      sql = `UPDATE booking_products_mst SET product_book_status = ${data.book_status}, updated_date = '${data.book_updated_date}', updated_by = ${data.book_updated_by}, updated_through = ${data.updated_through} WHERE`;
  }else{ /* this is come from individual remove product */
    sql = `UPDATE booking_products_mst SET product_book_status = ${data.book_status},multi_service_book_id=0, updated_date = '${data.updated_date}', updated_by = ${data.updated_by}, updated_through = ${data.updated_through} WHERE`;
  }
   
  if(multi_id_flag == 1)
  {
      sql += ` multi_service_book_id = ${data.book_id}`;
  }
  else
  {
     sql += ` booking_product_id = ${data.book_id}`;
  }

//console.log(sql)
      db.query(sql , (err,res) => {
        if (err) {
                console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
            
              return  result(null, res);
        
       
       });
           
}  


exports.getProductBookingDetails = (booking_id,multi_id_flag, result) => {
  
  let sql = `SELECT b.booking_product_id, b.product_id, b.product_name, b.product_sku, b.product_barcode, b.product_cust_id, b.product_cust_first_name, b.product_cust_last_name, b.product_date, 
  b.product_quantity, b.product_deposit, b.product_full_amount, b.product_book_status, b.created_date, b.multi_service_book_id, b.multi_service_booking_type, b.booking_old_product_id , 
  b.created_by, b.updated_date, b.updated_by , b.store_id, b.product_cust_email, pm.product_quantity as remaining_product_quantity FROM booking_products_mst b LEFT JOIN products_mst pm ON b.product_id = pm.product_id`;
  
  if(multi_id_flag == 1)
  {
    sql += ` WHERE b.multi_service_book_id = ${booking_id}`;

  }
  else
  {
    sql += ` WHERE b.booking_product_id = ${booking_id}`;

  }
  
//  console.log(sql);

db.query(sql , (err,res) => {
  if (err) {
    //console.log(err)
      return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
  }
  //console.log(res);
  
      return result(null,res);
 });

}

exports.updateProductPriceModel=(id,price,method,result)=>{
    let sql;
    switch(method){
        case constants.DEPOSIT_DISCOUNT_PRICE:
            sql = `UPDATE booking_products_mst SET product_discount_full_amount = ${price} WHERE booking_product_id = ${id}`;
            break;
        case constants.FULL_DISCOUNT_PRICE:
            sql = `UPDATE booking_products_mst SET product_discount_full_amount = ${price} WHERE booking_product_id = ${id}`;
            break;
    }
    
        //console.log(sql);
    db.query(sql , (err,sresult) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }

        return result(null , sresult);

    });
}

// exports.getAmount = (book_ids) => {
//   return new Promise((resolve, reject) => {
//       db.query(`SELECT IFNULL(sum(book_trans_paid_now),0) paid_amount,min(book_trans_gateway_method) payment_type1,max(book_trans_gateway_method) payment_type2  FROM booking_transactions_mst b WHERE b.book_id in (${book_ids})`, function (err, result) {
//           if (err) {
//               reject({error_code: err.code, message: err.sqlMessage});
//           } else
//               resolve(result);
//       });
//   });
// }



// exports.getProductBookingDetails = (booking_product_id,result) => {
  
//   let sql = `SELECT b.booking_product_id, b.product_name, b.product_sku, b.product_barcode, b.product_cust_id, b.product_cust_first_name, b.product_cust_last_name, b.product_date, 
//   b.product_quantity, b.product_deposit, b.product_full_amount, b.product_book_status, b.created_date, b.multi_service_book_id, b.multi_service_booking_type, b.booking_old_product_id , 
//   b.created_by, b.updated_date, b.updated_by , b.store_id, b.product_cust_email FROM booking_products_mst b WHERE b.booking_product_id = ${booking_product_id}`;

//   //console.log(sql);

// db.query(sql , (err,res) => {
//   if (err) {
//     //console.log(err)
//       return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
//   }
//   //console.log(res);
  
//       return result(null,res);
//  });

// }

exports.getProductsStockDetails = (product_ids, result) => {

  let sql = `SELECT product_id, store_id, product_name, product_quantity, product_is_active FROM products_mst WHERE product_id IN (${product_ids})`;
 
    //console.log(sql);

      db.query(sql, (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);

              return  result(null, res);        
       
        });
           
}

exports.updateStatusOfProducts = (book_status, booking_product_ids, result) => {

  let sql = `UPDATE booking_products_mst SET product_book_status = ${book_status} WHERE booking_product_id IN (${booking_product_ids}) `;
  
    //console.log(sql);

      db.query(sql, (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, res);        
       
        });
           
}




















