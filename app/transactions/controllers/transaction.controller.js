const constants = require('../../../config/constants.js');
const TransactionModel = require('../models/transaction.model');
const CustomerModel = require('../../customer/models/customer.model');
const BookingModel = require('../../booking/models/booking.model');
const ProductsModel = require('../../products/models/products.model.js');
const StoresModel = require('../../stores/models/stores.model.js');
const SettingsModel = require('../../settings/models/settings.model.js');


const NotificationsLibrary = require('../../../libs/notifications.js');
const ErrorLogs = require('../../../libs/error_logs.js');

var async = require("async");
var moment = require('moment');

var moment_tz = require('moment-timezone');


const { ApiError, Client, Environment } = require('square');

//const { isProduction, SQUARE_ACCESS_TOKEN } = require('./config');

// const client = new Client({
//   environment: Environment.Sandbox,
//   accessToken: "EAAAEC9VBxBi25RmsQoxLvnr_T0kCfp_lnZz8RsSwwNTm2WA9bGEo-Xz1XaFe2vq",
// });

const { nanoid } = require('nanoid');

exports.getBookingsPaymentAmountDetails = (req, res, next) => {
    if (!(req.body)) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    } else {
        let payment_data = req.body;
        let results = [], products = [];
        let book_ids = [], having_multi_book_id = 0, paid_deposit = 0, paid_fullamount = 0, product_multi_book_id = [];
        if (payment_data.multi_book_id && payment_data.multi_book_id != 0) {
            book_ids.push(payment_data.multi_book_id);
            having_multi_book_id = 1;
        } else
            book_ids.push(payment_data.book_id);
        product_multi_book_id = book_ids;
        BookingModel.getServiceBookingDetails(book_ids, having_multi_book_id, 0, (err, result) => {
            if (err)
                //res.status(400).send({status: 200, message: err})
                next(err);
            async.eachSeries(result, async (obj, callback) => {
                if (result[result.indexOf(obj)].book_status == constants.STATUS_BOOKED || result[result.indexOf(obj)].book_status == constants.STATUS_CONFIRMED || result[result.indexOf(obj)].book_status == constants.STATUS_ARRIVED || result[result.indexOf(obj)].book_status == constants.STATUS_COMPLETED) {
                    book_ids = await getOldBookings(obj.book_id);
                    paid_amount = await TransactionModel.getAmount(book_ids);
                    result[result.indexOf(obj)].paid_amount = paid_amount[0].paid_amount;
                    result[result.indexOf(obj)].last_book_trans_created_date = paid_amount[0].last_book_trans_created_date;
                    result[result.indexOf(obj)].due_book_deposit = parseFloat((result[result.indexOf(obj)].book_deposit - paid_amount[0].paid_amount).toFixed(2));
                    if (result[result.indexOf(obj)].book_discount_deposit_amount != -1) {
                        book_deposit_amount = result[result.indexOf(obj)].book_discount_deposit_amount;
                    } else {
                        book_deposit_amount = result[result.indexOf(obj)].book_deposit;
                    }
                    result[result.indexOf(obj)].due_book_deposit = parseFloat((book_deposit_amount - paid_amount[0].paid_amount).toFixed(2));
                    if (result[result.indexOf(obj)].due_book_deposit < 0)
                        result[result.indexOf(obj)].due_book_deposit = 0;
                    if (result[result.indexOf(obj)].book_discount_full_amount != -1) {
                        book_full_amount = result[result.indexOf(obj)].book_discount_full_amount;
                    } else {
                        book_full_amount = result[result.indexOf(obj)].book_fullamount;
                    }
                    result[result.indexOf(obj)].due_book_fullamount = parseFloat((book_full_amount - paid_amount[0].paid_amount).toFixed(2));
                    paid_deposit += result[result.indexOf(obj)].due_book_deposit;
                    paid_fullamount += result[result.indexOf(obj)].due_book_fullamount;
                    CustomerModel.getLastVisited(result[result.indexOf(obj)].store_id, result[result.indexOf(obj)].book_email, result[result.indexOf(obj)].book_start_date, result[result.indexOf(obj)].book_start_time, (err, cust_last_visited) => {
                        if (err) {

                            return err;
                        } else {
                            //console.log(cust_last_visited);
                            result[result.indexOf(obj)].last_visited = cust_last_visited;
//                        callback();
                        }
                        results.push(result[result.indexOf(obj)]);
                    })
                } else {
//                    result.splice(result.indexOf(obj), 1);
                }
            }, function (err) {
                if (err) {
                    //res.status(400).send({status: 200, message: err})
                    next(err);
                }
                BookingModel.getProductDetails(product_multi_book_id, 0, (err, products_data) => {
                    if (err) {
                        next(err);
                    } else {
                        //console.log(products_data)
                        if (products_data.length > 0) {
                            async.eachSeries(products_data, async (obj, callback) => {
                                let all_old_product_ids = [];
                                if (products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_BOOKED || products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_CONFIRMED || products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_ARRIVED || products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_COMPLETED) {
                                    let temp_old_book_product_id;
                                    if (obj.booking_old_product_id) {
                                        temp_old_book_product_id = obj.booking_old_product_id;
                                    } else {
                                        temp_old_book_product_id = obj.booking_product_id;
                                    }
                                    product_book_ids = await TransactionModel.getOldProductBookings(temp_old_book_product_id);
                                    product_book_ids.forEach(value => {
                                        all_old_product_ids.push(value.booking_product_id);
                                    });
                                   // console.log("ids: " + all_old_product_ids);
                                    paid_amount = await TransactionModel.getProductAmount(all_old_product_ids);
                                   // console.log("paid_amount: " + paid_amount);
                                    products_data[products_data.indexOf(obj)].paid_amount = paid_amount[0].paid_amount;
                                    products_data[products_data.indexOf(obj)].last_book_trans_created_date = paid_amount[0].last_book_trans_created_date;
                                    if (products_data[products_data.indexOf(obj)].product_discount_full_amount != -1) {
                                        final_product_amount = products_data[products_data.indexOf(obj)].product_discount_full_amount;
                                    } else {
                                        final_product_amount = products_data[products_data.indexOf(obj)].product_full_amount;
                                    }
                                    products_data[products_data.indexOf(obj)].due_product_fullamount = parseFloat((products_data[products_data.indexOf(obj)].product_quantity * final_product_amount - paid_amount[0].paid_amount).toFixed(2));
                                    paid_fullamount += products_data[products_data.indexOf(obj)].due_product_fullamount;
                                    products.push(products_data[products_data.indexOf(obj)]);
                                } else {
//                                    products_data.splice(products_data.indexOf(obj), 1)
                                }
                            }, function (err) {
                                if (err) {
                                    next(err)
                                } else {
//                                    paid_deposit = paid_deposit - paid_fullamount;
//                                    if (paid_deposit < 0) {
//                                        paid_deposit = 0;
//                                    }
                                    if(paid_fullamount < 0){
                                        paid_fullamount = 0.00;
                                    }
                                    res.status(200).send({status: 200, result: {'bookings': results, 'products': products, 'pay_deposit': paid_deposit, 'pay_fullamount': paid_fullamount}});
                                }
                            });
                        } else {
//                            paid_deposit = paid_deposit - paid_fullamount;
//                            if (paid_deposit < 0) {
//                                paid_deposit = 0;
//                            }
                            if(paid_fullamount < 0){
                                paid_fullamount = 0.00;
                            }
                            res.status(200).send({status: 200, result: {'bookings': results, 'products': [], 'pay_deposit': paid_deposit, 'pay_fullamount': paid_fullamount}});
                        }
                    }
                })
            })
        })
    }
}

exports.getBookingsPaymentAmountDetails_v2 = (req, res, next) => {
    if (!(req.body)) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    } else {
        let payment_data = req.body;
        let book_ids = [], having_multi_book_id = 0, paid_deposit = 0, paid_fullamount = 0, product_multi_book_id = [];
        if (payment_data.multi_book_id && payment_data.multi_book_id != 0) {
            book_ids.push(payment_data.multi_book_id);
            having_multi_book_id = 1;
        } else
            book_ids.push(payment_data.book_id);
        product_multi_book_id = book_ids;
        BookingModel.getServiceBookingDetails(book_ids, having_multi_book_id, 0, (err, result) => {
            if (err)
                //res.status(400).send({status: 200, message: err})
                next(err);
            async.eachSeries(result, async (obj, callback) => {
                // if (result[result.indexOf(obj)].book_status == constants.STATUS_BOOKED || result[result.indexOf(obj)].book_status == constants.STATUS_CONFIRMED || result[result.indexOf(obj)].book_status == constants.STATUS_ARRIVED || result[result.indexOf(obj)].book_status == constants.STATUS_COMPLETED) {
                book_ids = await getOldBookings(obj.book_id);
                paid_amount = await TransactionModel.getAmount(book_ids);
                result[result.indexOf(obj)].paid_amount = paid_amount[0].paid_amount;
                result[result.indexOf(obj)].last_book_trans_created_date = paid_amount[0].last_book_trans_created_date;
                result[result.indexOf(obj)].due_book_deposit = result[result.indexOf(obj)].book_deposit - paid_amount[0].paid_amount;
                if (result[result.indexOf(obj)].book_discount_deposit_amount != -1) {
                    book_deposit_amount = result[result.indexOf(obj)].book_discount_deposit_amount;
                } else {
                    book_deposit_amount = result[result.indexOf(obj)].book_deposit;
                }
                result[result.indexOf(obj)].due_book_deposit = book_deposit_amount - paid_amount[0].paid_amount;
                if (result[result.indexOf(obj)].due_book_deposit < 0)
                    result[result.indexOf(obj)].due_book_deposit = 0;
                if (result[result.indexOf(obj)].book_discount_full_amount != -1) {
                    book_full_amount = result[result.indexOf(obj)].book_discount_full_amount;
                } else {
                    book_full_amount = result[result.indexOf(obj)].book_fullamount;
                }
                result[result.indexOf(obj)].due_book_fullamount = book_full_amount - paid_amount[0].paid_amount;
                paid_deposit += result[result.indexOf(obj)].due_book_deposit;
                paid_fullamount += result[result.indexOf(obj)].due_book_fullamount;
                CustomerModel.getLastVisited(result[result.indexOf(obj)].store_id, result[result.indexOf(obj)].book_email, result[result.indexOf(obj)].book_start_date, result[result.indexOf(obj)].book_start_time, (err, cust_last_visited) => {
                    if (err) {

                        return err;
                    } else {
                        //console.log(cust_last_visited);
                        result[result.indexOf(obj)].last_visited = cust_last_visited;
//                        callback();
                    }
                })
                // } else {
                //     result.splice(result.indexOf(obj), 1);
                // }
            }, function (err) {
                if (err) {
                    //res.status(400).send({status: 200, message: err})
                    next(err);
                }
                BookingModel.getProductDetails(product_multi_book_id, 0, (err, products_data) => {
                    if (err) {
                        next(err);
                    } else {
                        if (products_data.length > 0) {
                            async.eachSeries(products_data, async (obj, callback) => {
                                let all_old_product_ids = [];
//                                if (products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_BOOKED || products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_CONFIRMED || products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_ARRIVED || products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_COMPLETED) {
                                let temp_old_book_product_id;
                                if (obj.booking_old_product_id) {
                                    temp_old_book_product_id = obj.booking_old_product_id;
                                } else {
                                    temp_old_book_product_id = obj.booking_product_id;
                                }
                                product_book_ids = await TransactionModel.getOldProductBookings(temp_old_book_product_id);
                                product_book_ids.forEach(value => {
                                    all_old_product_ids.push(value.booking_product_id);
                                })
                                paid_amount = await TransactionModel.getProductAmount(all_old_product_ids);
                                products_data[products_data.indexOf(obj)].paid_amount = paid_amount[0].paid_amount;
                                products_data[products_data.indexOf(obj)].last_book_trans_created_date = paid_amount[0].last_book_trans_created_date;
                                if (products_data[products_data.indexOf(obj)].product_discount_full_amount != -1) {
                                    final_product_amount = products_data[products_data.indexOf(obj)].product_discount_full_amount;
                                } else {
                                    final_product_amount = products_data[products_data.indexOf(obj)].product_full_amount;
                                }
                                products_data[products_data.indexOf(obj)].due_product_fullamount = products_data[products_data.indexOf(obj)].product_quantity * final_product_amount - paid_amount[0].paid_amount;
                                paid_fullamount += products_data[products_data.indexOf(obj)].due_product_fullamount;
//                                } else {
//                                    products_data.splice(products_data.indexOf(obj), 1)
//                                }
                            }, function (err) {
                                if (err) {
                                    next(err)
                                } else {
                                    if(paid_fullamount < 0){
                                        paid_fullamount = 0.00;
                                    }
                                    res.status(200).send({status: 200, result: {'bookings': result, 'products': products_data, 'pay_deposit': paid_deposit, 'pay_fullamount': paid_fullamount}});
                                }
                            });
                        } else {
                            if(paid_fullamount < 0){
                                paid_fullamount = 0.00;
                            }
                            res.status(200).send({status: 200, result: {'bookings': result, 'products': [], 'pay_deposit': paid_deposit, 'pay_fullamount': paid_fullamount}});
                        }
                    }
                })
            })
        })
    }
}

exports.getCustomerStoreWalletDetilas = async (req, res, next) => {
    if (!(req.body.cust_id) || !(req.body.store_id)) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    } else {
        let data;
        data = await getWalletAmount(req.body);
        if (data.error_code)
        {
            //res.status(400).send({status: 400, message: data});
            next(data);
        } else
        {
            res.status(200).send({status: 200, wallet: data});
        }
    }
}

exports.insertBookingsPaymentAmountDetails = async (req, res, next) => {
    try {
        let payment_data = req.body, total_amount = 0, products = [], results = [], product_multi_book_id;
        let book_ids = [], having_multi_book_id = 0, paid_deposit = 0, paid_fullamount = 0;
        if (payment_data.multi_book_id && payment_data.multi_book_id != 0) {
            book_ids.push(payment_data.multi_book_id);
            having_multi_book_id = 1;
        } else
            book_ids.push(payment_data.book_id);
        product_multi_book_id = book_ids;
        //checking products data when full_amount paid
        BookingModel.getProductDetails(book_ids, 1, async (err, products_data) => {
            if (err) {
                next(err);
            } else {
                if (products_data.length > 0) {
                    for (const obj of products_data) {
                        let all_old_product_ids = [];
                        if (products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_BOOKED || products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_CONFIRMED || products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_ARRIVED || products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_COMPLETED) {
                            let temp_old_book_product_id;
                            if (obj.booking_old_product_id) {
                                temp_old_book_product_id = obj.booking_old_product_id;
                            } else {
                                temp_old_book_product_id = obj.booking_product_id;
                            }
                            product_book_ids = await TransactionModel.getOldProductBookings(temp_old_book_product_id);
                            product_book_ids.forEach(value => {
                                all_old_product_ids.push(value.booking_product_id);
                            })
                            products_data[products_data.indexOf(obj)].type = 'product';
                            paid_amount = await TransactionModel.getProductAmount(all_old_product_ids);
                            products_data[products_data.indexOf(obj)].paid_amount = paid_amount[0].paid_amount;
                            products_data[products_data.indexOf(obj)].due_product_fullamount = products_data[products_data.indexOf(obj)].product_quantity * products_data[products_data.indexOf(obj)].product_full_amount - paid_amount[0].paid_amount;
                            paid_fullamount += products_data[products_data.indexOf(obj)].due_product_fullamount;
                            products.push(products_data[products_data.indexOf(obj)]);
                        }
                    }
                }
                BookingModel.getServiceBookingDetails(book_ids, having_multi_book_id, 0, (err, result) => {
                    if (err)
                        next(err);
                    async.eachSeries(result, async (obj, callback) => {
                        if (result[result.indexOf(obj)].book_status == constants.STATUS_BOOKED || result[result.indexOf(obj)].book_status == constants.STATUS_CONFIRMED || result[result.indexOf(obj)].book_status == constants.STATUS_ARRIVED || result[result.indexOf(obj)].book_status == constants.STATUS_COMPLETED) {
                            book_ids = await getOldBookings(obj.book_id);
                            paid_amount = await TransactionModel.getAmount(book_ids);
                            result[result.indexOf(obj)].paid_amount = paid_amount[0].paid_amount;
                            result[result.indexOf(obj)].type = 'service';
                            result[result.indexOf(obj)].due_book_deposit = result[result.indexOf(obj)].book_deposit - paid_amount[0].paid_amount;
                            if (result[result.indexOf(obj)].due_book_deposit < 0)
                                result[result.indexOf(obj)].due_book_deposit = 0;
                            result[result.indexOf(obj)].due_book_fullamount = result[result.indexOf(obj)].book_fullamount - paid_amount[0].paid_amount;
                            paid_deposit += result[result.indexOf(obj)].due_book_deposit;
                            paid_fullamount += result[result.indexOf(obj)].due_book_fullamount;
                            results.push(result[result.indexOf(obj)])
                        }
                    }, async function (err) {
                        if (err) {
                            //res.status(400).send({status: 200, message: err})
                            next(err);
                        }
                        try {
                            let payments = results;
                            if (!payments.length || req.body.store_id !== payments[0].store_id || req.body.cust_id !== payments[0].cust_id || paid_fullamount !== req.body.pay_fullamount) {
                                res.status(400).send({status: 200, message: "data is insufficant, check all details are correct or not",
                                    paid_amount: paid_fullamount});
                                return;
                            }
                            let customer_amount, all_wallet_transactions = [], all_payment_transactions = [], each_wallet, each_transaction;
                            let dont_send_another_request_for_transaction = 0;
                            customer_amount = await getWalletAmount({'store_id': req.body.store_id, "cust_id": req.body.cust_id});
                            customer_amount = customer_amount[0].amount;
                            if (req.body.wallet_pay <= 0) {
                                customer_amount = 0;
                            }
                            if (req.body.full_amount) {
                                total_amount = paid_fullamount;
                            } else {
                                total_amount = paid_deposit;
                            }
                            async.eachSeries(payments, async (obj, callback) => {
                                if (req.body.using_full_wallet_amount && customer_amount >= total_amount) {
                                    each_wallet = await walletInsertion(req.body, obj);
                                    if (each_wallet) {
                                        all_wallet_transactions.push(each_wallet);
                                    }
                                    each_transaction = await transactionInsertion(req.body, obj, constants.THROUGH_WALEET);
                                    if (each_transaction) {
                                        all_payment_transactions.push(each_transaction);
                                    }
                                } else if (req.body.wallet_pay > 0 && customer_amount > 0 && !req.body.using_full_wallet_amount) {
                                    each_wallet = await walletInsertion(req.body, obj, customer_amount);
                                    if (each_wallet) {
                                        all_wallet_transactions.push(each_wallet);
                                    }
                                    each_transaction = await transactionInsertion(req.body, obj, constants.THROUGH_WALEET, customer_amount);
                                    if (each_transaction) {
                                        all_payment_transactions.push(each_transaction);
                                    }
                                    if (req.body.full_amount) {
                                        if (customer_amount == obj.due_book_fullamount) {
                                            dont_send_another_request_for_transaction = 1;
                                        }
                                        customer_amount = customer_amount - obj.due_book_fullamount;
                                    } else {
                                        if (customer_amount == obj.due_book_deposit) {
                                            dont_send_another_request_for_transaction = 1;
                                        }
                                        customer_amount = customer_amount - obj.due_book_deposit;
                                    }

                                }
                                if (customer_amount <= 0 && req.body.cash_pay > 0 && !req.body.using_full_wallet_amount) {
                                    if (!dont_send_another_request_for_transaction) {
                                        each_transaction = await transactionInsertion(req.body, obj, constants.THROUGH_CASH, customer_amount);
                                    }
                                    if (each_transaction && !dont_send_another_request_for_transaction) {
                                        all_payment_transactions.push(each_transaction);
                                    }
                                    customer_amount = 0;
                                    if (obj.due_book_fullamount < 0) {
                                        req.body.wallet_pay = 1;
                                        each_wallet = await walletInsertion(req.body, obj);
                                        if (each_wallet) {
                                            all_wallet_transactions.push(each_wallet);
                                        }
                                        customer_amount = customer_amount - (obj.due_book_fullamount);
                                    }
                                    dont_send_another_request_for_transaction = 0;
                                }
                            }, async function (err) {
                                if (err)
                                {
                                    //res.status(400).send({status: 400, message: {err_code: 400, error_message: err}});
                                    next(err);
                                } else {
                                    let result;
                                    try {
                                        if (products.length > 0 && req.body.full_amount) {
                                            async.eachSeries(products, async (obj, callback) => {
                                                if (req.body.using_full_wallet_amount && customer_amount >= total_amount) {
                                                    each_wallet = await walletInsertion(req.body, obj);
                                                    if (each_wallet) {
                                                        all_wallet_transactions.push(each_wallet);
                                                    }
                                                    each_transaction = await transactionInsertion(req.body, obj, constants.THROUGH_WALEET);
                                                    if (each_transaction) {
                                                        all_payment_transactions.push(each_transaction);
                                                    }
                                                } else if (req.body.wallet_pay > 0 && customer_amount > 0 && !req.body.using_full_wallet_amount) {
                                                    each_wallet = await walletInsertion(req.body, obj, customer_amount);
                                                    if (each_wallet) {
                                                        all_wallet_transactions.push(each_wallet);
                                                    }
                                                    each_transaction = await transactionInsertion(req.body, obj, constants.THROUGH_WALEET, customer_amount);
                                                    if (each_transaction) {
                                                        all_payment_transactions.push(each_transaction);
                                                    }
                                                    if (req.body.full_amount) {
                                                        if (customer_amount == obj.due_book_fullamount) {
                                                            dont_send_another_request_for_transaction = 1;
                                                        }
                                                        customer_amount = customer_amount - obj.due_book_fullamount;
                                                    } else {
                                                        if (customer_amount == obj.due_book_deposit) {
                                                            dont_send_another_request_for_transaction = 1;
                                                        }
                                                        customer_amount = customer_amount - obj.due_book_deposit;
                                                    }

                                                }
                                                if (customer_amount <= 0 && req.body.cash_pay > 0 && !req.body.using_full_wallet_amount) {
                                                    if (!dont_send_another_request_for_transaction) {
                                                        each_transaction = await transactionInsertion(req.body, obj, constants.THROUGH_CASH, customer_amount);
                                                    }
                                                    if (each_transaction && !dont_send_another_request_for_transaction) {
                                                        all_payment_transactions.push(each_transaction);
                                                    }
                                                    customer_amount = 0;
                                                    if (obj.due_book_fullamount < 0) {
                                                        req.body.wallet_pay = 1;
                                                        each_wallet = await walletInsertion(req.body, obj);
                                                        if (each_wallet) {
                                                            all_wallet_transactions.push(each_wallet);
                                                        }
                                                        customer_amount = customer_amount - (obj.due_book_fullamount);
                                                    }
                                                    dont_send_another_request_for_transaction = 0;
                                                }
                                            }, async function (err) {
                                                if (err) {
                                                    next(err)
                                                } else {
                                                    result = await TransactionModel.insert_transaction_and_wallet_credit(all_wallet_transactions, all_payment_transactions);
                                                    res.status(200).send({status: 200, message: "Payment Successfully Done!.."});
                                                }
                                            });
                                        } else {
                                            result = await TransactionModel.insert_transaction_and_wallet_credit(all_wallet_transactions, all_payment_transactions);
                                            res.status(200).send({status: 200, message: "Payment Successfully Done!.."});
                                        }


                                    } catch (err) {
                                        //res.status(400).send({status: 400, message: err});
                                        next(err);
                                    }
                                }
                            });
                        } catch (err) {
                            //res.status(400).send({status: 400, message: err});
                            next(err);
                        }
                    })
                })
            }
        })

    } catch (err) {
        next(err);
    }
}

exports.insertBookingsPaymentAmountDetailsV2 = async (req, res, next) => {
    try {
        let payment_data = req.body, total_amount = 0, products = [], results = [];
        let book_ids = [], having_multi_book_id = 0, paid_deposit = 0, paid_fullamount = 0;
        if (payment_data.multi_book_id && payment_data.multi_book_id != 0) {
            book_ids.push(payment_data.multi_book_id);
            having_multi_book_id = 1;
        } else
            book_ids.push(payment_data.book_id);
        //checking products data when full_amount paid
        BookingModel.getProductDetails(book_ids, having_multi_book_id, async (err, products_data) => {
            if (err) {
                next(err);
            } else {
                if (products_data.length > 0) {
                    let all_old_product_ids, final_product_amount;
                    for (const obj of products_data) {
                        if (products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_BOOKED || products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_CONFIRMED || products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_ARRIVED || products_data[products_data.indexOf(obj)].product_book_status == constants.STATUS_COMPLETED) {
                            all_old_product_ids = [], final_product_amount = 0;
                            let temp_old_book_product_id;
                            if (obj.booking_old_product_id) {
                                temp_old_book_product_id = obj.booking_old_product_id;
                            } else {
                                temp_old_book_product_id = obj.booking_product_id;
                            }
                            product_book_ids = await TransactionModel.getOldProductBookings(temp_old_book_product_id);
                            product_book_ids.forEach(value => {
                                all_old_product_ids.push(value.booking_product_id);
                            })
                            products_data[products_data.indexOf(obj)].type = 'product';
                            paid_amount = await TransactionModel.getProductAmount(all_old_product_ids);
                            products_data[products_data.indexOf(obj)].paid_amount = paid_amount[0].paid_amount;
                            if (products_data[products_data.indexOf(obj)].product_discount_full_amount != -1) {
                                final_product_amount = products_data[products_data.indexOf(obj)].product_discount_full_amount;
                            } else {
                                final_product_amount = products_data[products_data.indexOf(obj)].product_full_amount;
                            }
                            products_data[products_data.indexOf(obj)].due_product_fullamount = products_data[products_data.indexOf(obj)].product_quantity * final_product_amount - paid_amount[0].paid_amount;
                            paid_fullamount += products_data[products_data.indexOf(obj)].due_product_fullamount;
                            products.push(products_data[products_data.indexOf(obj)])
                        } else {
//                            products_data.splice(products_data.indexOf(obj), 1);
                        }
                    }
                }
                BookingModel.getServiceBookingDetails(book_ids, having_multi_book_id, 0, (err, result) => {
                    let book_deposit_amount, book_full_amount;
                    if (err)
                        next(err);
                    async.eachSeries(result, async (obj, callback) => {
                        if (result[result.indexOf(obj)].book_status == constants.STATUS_BOOKED || result[result.indexOf(obj)].book_status == constants.STATUS_CONFIRMED || result[result.indexOf(obj)].book_status == constants.STATUS_ARRIVED || result[result.indexOf(obj)].book_status == constants.STATUS_COMPLETED) {
                            book_deposit_amount = 0, book_full_amount = 0;
                            book_ids = await getOldBookings(obj.book_id);
                            paid_amount = await TransactionModel.getAmount(book_ids);
                            result[result.indexOf(obj)].paid_amount = paid_amount[0].paid_amount;
                            result[result.indexOf(obj)].type = 'service';
                            if (result[result.indexOf(obj)].book_discount_deposit_amount != -1) {
                                book_deposit_amount = result[result.indexOf(obj)].book_discount_deposit_amount;
                            } else {
                                book_deposit_amount = result[result.indexOf(obj)].book_deposit;
                            }
                            result[result.indexOf(obj)].due_book_deposit = book_deposit_amount - paid_amount[0].paid_amount;
                            if (result[result.indexOf(obj)].due_book_deposit < 0)
                                result[result.indexOf(obj)].due_book_deposit = 0;
                            if (result[result.indexOf(obj)].book_discount_full_amount != -1) {
                                book_full_amount = result[result.indexOf(obj)].book_discount_full_amount;
                            } else {
                                book_full_amount = result[result.indexOf(obj)].book_fullamount;
                            }
                            result[result.indexOf(obj)].book_fullamount = book_full_amount;
                            result[result.indexOf(obj)].due_book_fullamount = book_full_amount - paid_amount[0].paid_amount;
                            paid_deposit += result[result.indexOf(obj)].due_book_deposit;
                            paid_fullamount += result[result.indexOf(obj)].due_book_fullamount;
                            results.push(result[result.indexOf(obj)]);
                        } else {
//                            result.splice(result.indexOf(obj), 1)
                        }
                    }, async function (err) {
                        if (err) {
                            //res.status(400).send({status: 200, message: err})
                            next(err);
                        }
                        try {
                            let payments = results;
                            paid_fullamount = parseFloat(paid_fullamount).toFixed(2);
                            var req_pay_fullamount = parseFloat(req.body.pay_fullamount);
                            req_pay_fullamount = req_pay_fullamount.toFixed(2);
                            if (!payments.length || req.body.store_id !== payments[0].store_id || req.body.cust_id !== payments[0].cust_id || paid_fullamount !== req_pay_fullamount) {
                                res.status(400).send({status: 200, message: "data is insufficant, check all details are correct or not"});
                                return;
                            }
                            let customer_amount, all_wallet_transactions = [], all_payment_transactions = [], each_wallet, each_transaction;
                            let dont_send_another_request_for_transaction = 0, payment = req.body.pay_amount;
                            customer_amount = await getWalletAmount({'store_id': req.body.store_id, "cust_id": req.body.cust_id});
                            customer_amount = customer_amount[0].amount;
                            if (req.body.wallet <= 0) {
                                customer_amount = 0;
                            }
                            async.eachSeries(payments, async (obj, callback) => {
                                if (req.body.wallet > 0 && customer_amount > 0) {
                                    each_wallet = await walletInsertionV2(req.body, obj, customer_amount);
                                    if (each_wallet) {
                                        all_wallet_transactions.push(each_wallet);
                                    }
                                    each_transaction = await transactionInsertionV2(req.body, obj, constants.THROUGH_WALEET, customer_amount);
                                    if (each_transaction) {
                                        all_payment_transactions.push(each_transaction);
                                    }
                                    if (customer_amount == obj.due_book_fullamount) {
                                        dont_send_another_request_for_transaction = 1;
                                    }
                                    obj.temp_due_book_fullamount = obj.due_book_fullamount - customer_amount;
                                    customer_amount = customer_amount - obj.due_book_fullamount;
                                    obj.due_book_fullamount = obj.temp_due_book_fullamount;

                                }
                                if (customer_amount <= 0 && req.body.cash_pay > 0 && payment > 0) {
                                    if (!dont_send_another_request_for_transaction) {
                                        each_transaction = await transactionInsertionV2(req.body, obj, constants.THROUGH_CASH, payment);
                                    }
                                    if (each_transaction && !dont_send_another_request_for_transaction) {
                                        all_payment_transactions.push(each_transaction);
                                    }
                                    if (customer_amount < 0) {
                                        payment = payment + customer_amount; // here customer amoutn in minus value
                                    } else {
                                        payment = payment - obj.due_book_fullamount;
                                    }
                                    customer_amount = 0;
                                    if (obj.due_book_fullamount < 0) {
                                        req.body.wallet = 1;
                                        each_wallet = await walletInsertionV2(req.body, obj);
                                        if (each_wallet) {
                                            all_wallet_transactions.push(each_wallet);
                                        }
                                        customer_amount = customer_amount - (obj.due_book_fullamount);
                                    }
                                    dont_send_another_request_for_transaction = 0;
                                }
                            }, async function (err) {
                                if (err)
                                {
                                    //res.status(400).send({status: 400, message: {err_code: 400, error_message: err}});
                                    next(err);
                                } else {
                                    let result;
                                    try {
                                        if (products.length > 0) {
                                            async.eachSeries(products, async (obj, callback) => {
                                                if (req.body.wallet > 0 && customer_amount > 0) {
                                                    each_wallet = await walletInsertionV2(req.body, obj, customer_amount);
                                                    if (each_wallet) {
                                                        all_wallet_transactions.push(each_wallet);
                                                    }
                                                    each_transaction = await transactionInsertionV2(req.body, obj, constants.THROUGH_WALEET, customer_amount);
                                                    if (each_transaction) {
                                                        all_payment_transactions.push(each_transaction);
                                                    }
                                                    if (customer_amount == obj.due_product_fullamount) {
                                                        dont_send_another_request_for_transaction = 1;
                                                    }
                                                    obj.temp_due_product_fullamount = obj.due_product_fullamount - customer_amount;
                                                    customer_amount = customer_amount - obj.due_product_fullamount;
                                                    obj.due_product_fullamount = obj.temp_due_product_fullamount;
                                                }
                                                if (customer_amount <= 0 && req.body.cash_pay > 0 && payment > 0) {
                                                    if (!dont_send_another_request_for_transaction) {
                                                        each_transaction = await transactionInsertionV2(req.body, obj, constants.THROUGH_CASH, payment);
                                                    }
                                                    if (each_transaction) {
                                                        all_payment_transactions.push(each_transaction);
                                                    }
                                                    if (customer_amount < 0) {
                                                        payment = payment + customer_amount; // here customer amoutn in minus value
                                                    } else {
                                                        payment = payment - obj.due_product_fullamount;
                                                    }
                                                    customer_amount = 0;
                                                    if (obj.due_product_fullamount < 0) {
                                                        req.body.wallet = 1;
                                                        each_wallet = await walletInsertionV2(req.body, obj);
                                                        if (each_wallet) {
                                                            all_wallet_transactions.push(each_wallet);
                                                        }
                                                        customer_amount = customer_amount - (obj.due_product_fullamount);
                                                    }
                                                    dont_send_another_request_for_transaction = 0;
                                                }
                                            }, async function (err) {
                                                if (err) {
                                                    next(err)
                                                } else {
                                                    let id = 0;
                                                    if(payment_data.multi_book_id !=0)
                                                         id = payment_data.multi_book_id;
                                                    else
                                                        id = payment_data.book_id;
                                                    if(payment > 0){
                                                        each_wallet = await extraPaidAmountAddTowallet(req.body, payment, id);;
                                                        all_wallet_transactions.push(each_wallet);
                                                    }
                                                    result = await TransactionModel.insert_transaction_and_wallet_credit(all_wallet_transactions, all_payment_transactions);
                                                    res.status(200).send({status: 200, message: "Payment Successfully Done!.."});
                                                }
                                            });
                                        } else {
                                            let id = 0;
                                            if(payment_data.multi_book_id !=0)
                                                 id = payment_data.multi_book_id;
                                            else
                                                id = payment_data.book_id;
                                            if(payment > 0){
                                                each_wallet = await extraPaidAmountAddTowallet(req.body, payment, id);;
                                                all_wallet_transactions.push(each_wallet);
                                            }
//                                            res.send(each_wallet);
                                            result = await TransactionModel.insert_transaction_and_wallet_credit(all_wallet_transactions, all_payment_transactions);
                                            res.status(200).send({status: 200, message: "Payment Successfully Done!.."});
                                        }


                                    } catch (err) {
                                        //res.status(400).send({status: 400, message: err});
                                        next(err);
                                    }
                                }
                            });
                        } catch (err) {
                            //res.status(400).send({status: 400, message: err});
                            next(err);
                        }
                    })
                })
            }
        })
    } catch (err) {
        next(err);
    }
}

async function getOldBookings(old_book_id) {
    let book_ids_array = [];
    while (old_book_id != 0)
    {
        book_ids_array.push(old_book_id);
        try {
            var old_book_data = await BookingModel.getOldBookings(old_book_id);

            for (i = 0; i < old_book_data.length; i++)
            {
                if (book_ids_array.indexOf(old_book_data[i].book_id) < 0)
                {
                    //console.log(old_book_data[i].book_id);
                    book_ids_array.push(old_book_data[i].book_id);
                }
                //console.log(i);

            }

            old_book_id = old_book_data[0].book_oldbookid;
        } catch (error) {
            throw error;
        }

    }
//console.log("loop completed");
    return book_ids_array;
}

async function getWalletAmount(send_data) {
    let data;
    try {
        data = await TransactionModel.getCustomerStoreWalletAmountDetilas(send_data);
        return data;
    } catch ($e) {
        return $e;
    }
}


async function walletInsertion(data, obj, customer_amount) {
    let paid_now = 0, type = constants.WALLET_DEBIT, book_id = 0, product_id = 0, content = '';
    switch (obj.type) {
        case constants.SERVICE_TYPE:
            if (data.full_amount) {
                if (customer_amount && customer_amount < obj.due_book_fullamount)
                    paid_now = customer_amount;
                else
                    paid_now = obj.due_book_fullamount;
            } else {
                if (customer_amount && customer_amount < obj.due_book_deposit)
                    paid_now = customer_amount;
                else
                    paid_now = obj.due_book_deposit;
            }
            if (obj.due_book_fullamount < 0) {
                paid_now = -(obj.due_book_fullamount);
                type = constants.WALLET_CREDIT;
                content = "added amount for booking of " + obj.book_id;
            } else {
                content = "detected amount for booking of " + obj.book_id;
            }
            book_id = obj.book_id;
            break;
        case constants.PRODUCT_TYPE:
            if (data.full_amount) {
                if (customer_amount && customer_amount < obj.due_product_fullamount)
                    paid_now = customer_amount;
                else
                    paid_now = obj.due_product_fullamount;
            }
            if (obj.due_product_fullamount < 0) {
                paid_now = -(obj.due_product_fullamount);
                type = constants.WALLET_CREDIT;
                content = 'added amount for product of ' + obj.booking_product_id;
            } else {
                content = 'detected amount for product of ' + obj.booking_product_id;
            }
            product_id = obj.booking_product_id;
            break;
    }


    if (paid_now != 0) {
        var insertion = [
            data.cust_id,
            data.cust_email,
            type,
            paid_now,
            content,
            data.store_id,
            book_id,
            product_id
        ];
        return insertion;
    }
}

async function walletInsertionV2(data, obj, customer_amount) {
    let paid_now = 0, type = constants.WALLET_DEBIT, book_id = 0, product_id = 0, content = '';
    switch (obj.type) {
        case constants.SERVICE_TYPE:
            if (customer_amount && customer_amount < obj.due_book_fullamount)
                paid_now = customer_amount;
            else
                paid_now = obj.due_book_fullamount;

            if (obj.due_book_fullamount < 0) {
                paid_now = -(obj.due_book_fullamount);
                type = constants.WALLET_CREDIT;
                content = "added amount for booking of " + obj.book_id;
            } else {
                content = "detected amount for booking of " + obj.book_id;
            }
            book_id = obj.book_id;
            break;
        case constants.PRODUCT_TYPE:
            if (customer_amount && customer_amount < obj.due_product_fullamount)
                paid_now = customer_amount;
            else
                paid_now = obj.due_product_fullamount;
            if (obj.due_product_fullamount < 0) {
                paid_now = -(obj.due_product_fullamount);
                type = constants.WALLET_CREDIT;
                content = 'added amount for product of ' + obj.booking_product_id;
            } else {
                content = 'detected amount for product of ' + obj.booking_product_id;
            }
            product_id = obj.booking_product_id;
            break;
    }


    if (paid_now != 0) {
        var insertion = [
            data.cust_id,
            data.cust_email,
            type,
            paid_now,
            content,
            data.store_id,
            book_id,
            product_id
        ];
        return insertion;
    }
}
async function extraPaidAmountAddTowallet(data, paid_now, multi_book_id) {
    content = "added amount to wallet.extra paid from admin on checkout page for booking of " + multi_book_id;
    var insertion = [
        data.cust_id,
        data.cust_email,
        constants.WALLET_CREDIT,
        paid_now,
        content,
        data.store_id,
        multi_book_id,
        0
    ];
    return insertion;
}
async function transactionInsertion(data, obj, payment_type, customer_amount) {
    let type, paid_now, balance, book_id = 0, class_id = 0, book_product_id = 0, fullamount = 0;
    switch (obj.type) {
        case constants.SERVICE_TYPE:
            type = 1;
            book_id = obj.book_id;
            fullamount = obj.book_fullamount;
            if (data.full_amount) {
                if (customer_amount && customer_amount < obj.due_book_fullamount) {
                    if (customer_amount < 0) {
                        paid_now = -(customer_amount);
                        balance = 0;
                    } else if (customer_amount > 0) {
                        paid_now = customer_amount;
                        balance = obj.due_book_fullamount - customer_amount;
                    }
                } else {
                    paid_now = obj.due_book_fullamount;
                    balance = 0;
                }
            } else {
                if (customer_amount && customer_amount < obj.due_book_deposit) {
                    if (customer_amount < 0) {
                        paid_now = -(customer_amount);
                        balance = obj.due_book_fullamount - obj.due_book_deposit;
                    } else if (customer_amount > 0) {
                        paid_now = customer_amount;
                        balance = obj.due_book_fullamount - customer_amount;

                    }
                } else {
                    paid_now = obj.due_book_deposit;
                    balance = obj.due_book_fullamount - obj.due_book_deposit;
                }
            }
            if (obj.due_book_fullamount < 0) {
                paid_now = obj.due_book_fullamount;
                balance = 0;
            }
            break;
        case constants.CLASS_TYPE:
            type = 2;
            class_id = obj.book_id;
            break;
        case constants.PRODUCT_TYPE:
            type = 3;
            book_product_id = obj.booking_product_id;
            fullamount = (obj.product_quantity * obj.product_full_amount);
            if (data.full_amount) {
                if (customer_amount && customer_amount < obj.due_product_fullamount) {
                    if (customer_amount < 0) {
                        paid_now = -(customer_amount);
                        balance = 0;
                    } else if (customer_amount > 0) {
                        paid_now = customer_amount;
                        balance = obj.due_product_fullamount - customer_amount;
                    }
                } else {
                    paid_now = obj.due_product_fullamount;
                    balance = 0;
                }
            }
            if (obj.due_product_fullamount < 0) {
                paid_now = obj.due_product_fullamount;
                balance = 0;
            }
            break;
    }
    if (paid_now != 0) {
        if (payment_type === constants.THROUGH_CASH && data.pos_pay === 1) {
            payment_type = constants.THROUGH_POINT_SALE;
        }
        return insert_transaction_data = [
            book_id,
            class_id,
            book_product_id,
            type,
            0,
            data.cust_id,
            0,
            0,
            0,
            0,
            fullamount,
            paid_now,
            balance,
            payment_type,
            2,
            0,
            0,
            new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
            0,
            new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
            1,
            0,
            0
        ];
        return insert_transaction_data;
    }
}
async function transactionInsertionV2(data, obj, payment_type, customer_amount) {
    let type, paid_now, balance, book_id = 0, class_id = 0, book_product_id = 0, fullamount = 0;
    switch (obj.type) {
        case constants.SERVICE_TYPE:
            type = 1;
            book_id = obj.book_id;
            fullamount = obj.book_fullamount;
            if (customer_amount && customer_amount < obj.due_book_fullamount) {
                if (customer_amount < 0) {
                    paid_now = -(customer_amount);
                    balance = 0;
                } else if (customer_amount > 0) {
                    paid_now = customer_amount;
                    balance = obj.due_book_fullamount - customer_amount;
                }
            } else {
                paid_now = obj.due_book_fullamount;
                balance = 0;
            }
            if (obj.due_book_fullamount < 0) {
                paid_now = obj.due_book_fullamount;
                balance = 0;
            }
            break;
        case constants.CLASS_TYPE:
            type = 2;
            class_id = obj.book_id;
            break;
        case constants.PRODUCT_TYPE:
            type = 3;
            book_product_id = obj.booking_product_id;
            fullamount = (obj.product_quantity * obj.product_full_amount);
            if (customer_amount && customer_amount < obj.due_product_fullamount) {
                if (customer_amount < 0) {
                    paid_now = -(customer_amount);
                    balance = 0;
                } else if (customer_amount > 0) {
                    paid_now = customer_amount;
                    balance = obj.due_product_fullamount - customer_amount;
                }
            } else {
                paid_now = obj.due_product_fullamount;
                balance = 0;
            }
            if (obj.due_product_fullamount < 0) {
                paid_now = obj.due_product_fullamount;
                balance = 0;
            }
            break;
    }
    if (paid_now != 0) {
        if (payment_type === constants.THROUGH_CASH && data.pos_pay === 1) {
            payment_type = constants.THROUGH_POINT_SALE;
        }
        return insert_transaction_data = [
            book_id,
            class_id,
            book_product_id,
            type,
            0,
            data.cust_id,
            0,
            0,
            0,
            0,
            fullamount,
            paid_now,
            balance,
            payment_type,
            2,
            0,
            0,
            new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
            0,
            new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
            1,
            0,
            0
        ];
        return insert_transaction_data;
    }
}



exports.getPaymentDetailsCustBookings = (req, res, next) => {
    if (!(req.body.bookings)) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    let bookings = req.body.bookings;
    let paid_deposit = 0, paid_fullamount = 0;
    var payment_details = [];

    async.each(bookings, (booking_obj, callback) => {

        var multi_book_id_flag = 0;

        var booking_id = parseInt(booking_obj.booking_id, 10);
        //console.log("booking_id: ", booking_id);
        var multi_service_book_id = parseInt(booking_obj.multi_service_book_id, 10);
        //console.log("multi_service_book_id: ", multi_service_book_id);

        if (multi_service_book_id && multi_service_book_id != 0 && multi_service_book_id != "0")
        {
            var book_id = multi_service_book_id;
            multi_book_id_flag = 1;
        } else
        {
            var book_id = booking_id;
        }


        BookingModel.getServiceBookingDetails(booking_id, 0, 0, async (err, booking_data) => {
            if (err)
            {

                callback(err);
                //return err;
                console.log("error: ", err);
            } else
            {
                //console.log("booking details: ", booking_data)
                var booking_details = booking_data[0];

                var book_ids_array = await getOldBookings(book_id);
                //console.log("book_ids_array: ",book_ids_array);

                paid_amount = await TransactionModel.getAmount(book_ids_array);
                //console.log("paid_amount: ",paid_amount);

                //Products Start

               
                //Products End



                booking_details.paid_amount = paid_amount[0].paid_amount;

                booking_details.due_book_deposit = booking_details.book_deposit - paid_amount[0].paid_amount;
                if (booking_details.due_book_deposit < 0) {
                    booking_details.due_book_deposit = 0;
                }

                booking_details.due_book_fullamount = booking_details.book_fullamount - paid_amount[0].paid_amount;
                if (booking_details.due_book_fullamount < 0) {
                    booking_details.due_book_fullamount = 0;
                }

                paid_deposit += booking_details.due_book_deposit;
                paid_fullamount += booking_details.due_book_fullamount;

                //console.log("booking_details: ",booking_details);
                payment_details.push(booking_details);




                callback();
            }

        })
        //callback();   
    }, async function (err) {
        if (err) {
            //res.status(400).send({status: 200, message: err})
            next(err);
        } else {
            var wallet_req = {
                cust_id: payment_details[0].cust_id,
                store_id: payment_details[0].store_id
            }
            //console.log("wallet_req: ",wallet_req)

            var cust_wallet = await TransactionModel.getCustomerStoreWalletAmountDetilas(wallet_req);
            //console.log("cust_wallet: ",cust_wallet)
            var cust_wallet_amount = cust_wallet[0].amount;
            //console.log("cust_wallet_amount: ",cust_wallet_amount)
            const stripe = require('stripe')('sk_test_LxF6TtSjyB1YMLLWcp2sTdEE');

            const stripe_intents = {};

            if (paid_deposit > 0)
            {

                stripe_intents.total_deposit = await stripe.paymentIntents.create({
                    amount: paid_deposit * 100,
                    currency: 'aud',
                    payment_method_types: ['card'],
                });
            } else {
                stripe_intents.total_deposit = {};

            }


            if (paid_deposit > cust_wallet_amount)
            {

                stripe_intents.deposit_wallet = await stripe.paymentIntents.create({
                    amount: (paid_deposit - cust_wallet_amount) * 100,
                    currency: 'aud',
                    payment_method_types: ['card'],
                });
            } else {
                stripe_intents.deposit_wallet = {};

            }

            if (paid_fullamount > 0)
            {

                stripe_intents.total_fullamount = await stripe.paymentIntents.create({
                    amount: paid_fullamount * 100,
                    currency: 'aud',
                    payment_method_types: ['card'],
                });
            } else {
                stripe_intents.total_fullamount = {};

            }

            if (paid_fullamount > cust_wallet_amount)
            {
                stripe_intents.fullamount_wallet = await stripe.paymentIntents.create({
                    amount: (paid_fullamount - cust_wallet_amount) * 100,
                    currency: 'aud',
                    payment_method_types: ['card'],
                });
            } else
            {
                stripe_intents.fullamount_wallet = {};

            }
            res.status(200).send({status: 200, result: {'bookings': payment_details, 'pay_deposit': paid_deposit, 'pay_fullamount': paid_fullamount, 'stripe_intents': stripe_intents, 'cust_wallet': cust_wallet}})
        }
        ;
    })

}



exports.getPaymentDetailsCustBookings2 = (req, res, next) => {
    if (!(req.body.bookings)) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    let bookings = req.body.bookings;
    let paid_deposit = 0, paid_fullamount = 0;
    var payment_details = [];
    var service_bookings = [];
    var product_bookings = [];

    //Start NEW
    var service_book_ids = [];
    var have_multi_book_id = false;
        var edit_flag = 0;

    for(i=0; i<bookings.length; i++)
    {
       if(bookings[i].type == constants.SERVICE_TYPE)
       {
          service_book_ids.push(bookings[i].booking_id); 
       }

    }
    //console.log("service_book_ids: ",service_book_ids);


  BookingModel.getServiceBookingDetails(service_book_ids,have_multi_book_id, edit_flag, (err, services_data) => {
      if (err) {
          next(err);
      }
      else{
          //console.log(services_data);

          /////Start
          if(services_data.length)
          {
              async.eachSeries(services_data, async (service,callback1)=>{

                var booking_details = service;

                //console.log("booking_details: ", booking_details);

                  let book_ids_array =[],paid_amount=0;  
                  try{

                    book_ids_array = await getOldBookings(service.book_id,book_ids_array);

                    //console.log("book_ids_array: ",book_ids_array);
    
                    paid_amount = await TransactionModel.getAmount(book_ids_array);
                    //console.log("paid_amount: ",paid_amount);

                    //   book_ids = await getOldBookings(service.book_id,book_ids);
                    //   paid_amount = await TransactionModel.getAmount(book_ids);

                      booking_details.paid_amount = paid_amount[0].paid_amount;

                      booking_details.due_book_deposit = booking_details.book_deposit - paid_amount[0].paid_amount;
                      if (booking_details.due_book_deposit < 0) {
                          booking_details.due_book_deposit = 0;
                      }
      
                      booking_details.due_book_fullamount = booking_details.book_fullamount - paid_amount[0].paid_amount;
                      if (booking_details.due_book_fullamount < 0) {
                          booking_details.due_book_fullamount = 0;
                      }
      
                      paid_deposit += booking_details.due_book_deposit;
                      paid_fullamount += booking_details.due_book_fullamount;
                      //console.log("booking_details: ",booking_details);
                      payment_details.push(booking_details);
                      service_bookings.push(booking_details);  
                      //callback1();                   
    
                      
                  }catch(error){
                      throw error;
                  }  
                                            
                  
              },function(err){
                //console.log("in function (err)");
                  if(err){
                      next(err)
                  }else{
                      //console.log("in function (err)");

                      
                      BookingModel.getProductDetails(service_book_ids, edit_flag, async (err, products_data) => {
                          if (err) {
                              next(err);
                          }
                          else{
                              //console.log("products_data:", products_data);
                              //var product_bookings = [];
                              //cust_booking_details.product_bookings = []
                              if(products_data.length)
                              {
                                  async.each(products_data, (product,callback)=>{
                                      let booking_product_ids_array =[] 
                                      //product_paid_amount=0;

                                      var temp_old_book_product_id = product.booking_product_id;

                                      if(product.booking_old_product_id != 0){
                                          temp_old_book_product_id = product.booking_old_product_id;
                                      }

                                      ProductsModel.getOldProductBookings(temp_old_book_product_id, (old_err, old_data) => {
                                          if (old_err) {
                                              next(old_err);
                                          }
                                          else{
                                            // console.log("product old data: ",old_data);
                                              for(i = 0; i< old_data.length; i++)
                                              {
                                                  booking_product_ids_array.push(old_data[i].booking_product_id);
                                              }
                                              //console.log("boooking product ids :", booking_product_ids_array);
      
                                              ProductsModel.getProductBookPaidInvoice(booking_product_ids_array, (invoice_err, invoice_data) => {
                                                  if (invoice_err) {
                                      
                                                      next(invoice_err);
                                                      return;
                                                  }
                                                  else{
                                                      //console.log("invoice_data: ",invoice_data);
                                                      var product_booking_details= product;
                                                      //console.log("product_booking_details: ", product_booking_details);
                                                       product_booking_details.product_paid_amount = invoice_data[0].total_book_trans_paid_now;

                                                       //booking_details.paid_amount = paid_amount[0].paid_amount;

                                                       var total_product_deposit = product_booking_details.product_quantity * product_booking_details.product_full_amount;
                                                       var total_product_full_amount = product_booking_details.product_quantity * product_booking_details.product_full_amount;

                      product_booking_details.due_book_deposit = total_product_deposit - invoice_data[0].total_book_trans_paid_now;
                      if (product_booking_details.due_book_deposit < 0) {
                          product_booking_details.due_book_deposit = 0;
                      }
      
                      product_booking_details.due_book_fullamount = total_product_full_amount -  invoice_data[0].total_book_trans_paid_now;
                      if (product_booking_details.due_book_fullamount < 0) {
                          product_booking_details.due_book_fullamount = 0;
                      }

                       paid_deposit += product_booking_details.due_book_deposit;
                       paid_fullamount += product_booking_details.due_book_fullamount;
                       //console.log("product_booking_details: ",product_booking_details);
                      payment_details.push(product_booking_details);                     
    
      

                                                      product_bookings.push(product_booking_details);
                                                      callback();
                                       

                                                  }
                                              })
                                          }
                                      })
              
                                                  
                                                              
                                      
                                  },async function(err){
                                      if(err){
                                          next(err)
                                      }else{
                                          //console.log("in product bookings");
                                          //cust_booking_details[0].product_bookings = product_bookings;
                                          //console.log("product_bookings: ", product_bookings);
                                          //console.log("bookings: ", cust_booking_details);
                                          //res.status(200).send({status:200, result:{service_bookings:cust_booking_details, product_bookings:product_bookings}});

                                          //Start new

                                          var wallet_req = {
                                            cust_id: payment_details[0].cust_id,
                                            store_id: payment_details[0].store_id
                                        }
                                        //console.log("wallet_req: ",wallet_req)
                            
                                        var cust_wallet = await TransactionModel.getCustomerStoreWalletAmountDetilas(wallet_req);
                                        //console.log("cust_wallet: ",cust_wallet)
                                        var cust_wallet_amount = cust_wallet[0].amount;
                                        //console.log("cust_wallet_amount: ",cust_wallet_amount)

                                        StoresModel.getStoreStripeKey(payment_details[0].store_id, async (err, stripe_data) => {
                                            if (err) {

                                                //res.status(500).send({status:500, result:{error:err}});
                                                next(err);
                                            }
                                            else{
                                                //console.log("stripe key :", stripe_data);

                                        const stripe = require('stripe')(stripe_data[0].store_api_stripe_key);
                            
                                        const stripe_intents = {};

                                    if((stripe_data.length > 0) && (stripe_data[0].store_api_stripe_key) && (stripe_data[0].store_api_stripe_key != ""))
                                    {
                            
                                        if (paid_deposit > 0)
                                        {
                            
                                            stripe_intents.total_deposit = await stripe.paymentIntents.create({
                                                amount: Math.round(paid_deposit * 100),
                                                currency: 'aud',
                                                payment_method_types: ['card'],
                                            });
                                        } else {
                                            stripe_intents.total_deposit = {};
                            
                                        }
                            
                            
                                        if (paid_deposit > cust_wallet_amount)
                                        {
                                            let stripe_amount = paid_deposit - cust_wallet_amount;
                                            if(stripe_amount < 1){
                                                stripe_amount = 1;
                                            }
                                            stripe_intents.deposit_wallet = await stripe.paymentIntents.create({
                                                amount: Math.round(stripe_amount * 100),
                                                currency: 'aud',
                                                payment_method_types: ['card'],
                                            });
                                        } else {
                                            stripe_intents.deposit_wallet = {};
                            
                                        }
                            
                                        if (paid_fullamount > 0)
                                        {
                            
                                            stripe_intents.total_fullamount = await stripe.paymentIntents.create({
                                                amount: Math.round(paid_fullamount * 100),
                                                currency: 'aud',
                                                payment_method_types: ['card'],
                                            });
                                        } else {
                                            stripe_intents.total_fullamount = {};
                            
                                        }
                            
                                        if (paid_fullamount > cust_wallet_amount)
                                        {
                                            let stripe_amount = paid_fullamount - cust_wallet_amount;
                                            if(stripe_amount < 1){
                                                stripe_amount = 1;
                                            }
                                            stripe_intents.fullamount_wallet = await stripe.paymentIntents.create({
                                                amount: Math.round(stripe_amount * 100),
                                                currency: 'aud',
                                                payment_method_types: ['card'],
                                            });
                                        } else
                                        {
                                            stripe_intents.fullamount_wallet = {};
                            
                                        }
                                    }
                                    else
                                    {
                                            stripe_intents.total_deposit = {};
                                            stripe_intents.deposit_wallet = {};
                                            stripe_intents.total_fullamount = {};
                                            stripe_intents.fullamount_wallet = {};

                                    }
                                        res.status(200).send({status: 200, result: {'bookings': {service_bookings: service_bookings, product_bookings: product_bookings}, 'pay_deposit': paid_deposit, 'pay_fullamount': paid_fullamount, 'stripe_intents': stripe_intents, 'cust_wallet': cust_wallet}})
                                    //}
                                    

                                           //End new
                                    }
                                })

                                    }
                                  })
                              }
                              else
                              {

                                 //Start new

                                 var wallet_req = {
                                    cust_id: payment_details[0].cust_id,
                                    store_id: payment_details[0].store_id
                                }
                                //console.log("wallet_req: ",wallet_req)
                    
                                var cust_wallet = await TransactionModel.getCustomerStoreWalletAmountDetilas(wallet_req);
                                //console.log("cust_wallet: ",cust_wallet)
                                var cust_wallet_amount = cust_wallet[0].amount;
                                //console.log("cust_wallet_amount: ",cust_wallet_amount)

                                StoresModel.getStoreStripeKey(payment_details[0].store_id, async (err, stripe_data) => {
                                    if (err) {

                                        //res.status(500).send({status:500, result:{error:err}});
                                        next(err);
                                    }
                                    else{
                                        //console.log("stripe key :", stripe_data);

                                const stripe = require('stripe')(stripe_data[0].store_api_stripe_key);
                                
                    
                                const stripe_intents = {};

                            if((stripe_data.length > 0) && (stripe_data[0].store_api_stripe_key) && (stripe_data[0].store_api_stripe_key != ""))
                            {
                    
                                if (paid_deposit > 0)
                                {
                    
                                    stripe_intents.total_deposit = await stripe.paymentIntents.create({
                                        amount: Math.round(paid_deposit * 100),
                                        currency: 'aud',
                                        payment_method_types: ['card'],
                                    });
                                } else {
                                    stripe_intents.total_deposit = {};
                    
                                }
                    
                    
                                if (paid_deposit > cust_wallet_amount)
                                {
                                    let stripe_amount = paid_deposit - cust_wallet_amount;
                                    if(stripe_amount < 1){
                                        stripe_amount = 1;
                                    }
                                    stripe_intents.deposit_wallet = await stripe.paymentIntents.create({
                                        amount: Math.round(stripe_amount * 100),
                                        currency: 'aud',
                                        payment_method_types: ['card'],
                                    });
                                } else {
                                    stripe_intents.deposit_wallet = {};
                    
                                }
                    
                                if (paid_fullamount > 0)
                                {
                    
                                    stripe_intents.total_fullamount = await stripe.paymentIntents.create({
                                        amount: Math.round(paid_fullamount * 100),
                                        currency: 'aud',
                                        payment_method_types: ['card'],
                                    });
                                } else {
                                    stripe_intents.total_fullamount = {};
                    
                                }
                                if (paid_fullamount > cust_wallet_amount)
                                {
                                    let stripe_amount = paid_fullamount - cust_wallet_amount;
                                    if(stripe_amount < 1){
                                        stripe_amount = 1;
                                    }
                                    stripe_intents.fullamount_wallet = await stripe.paymentIntents.create({
                                        amount: Math.round(stripe_amount * 100),
                                        currency: 'aud',
                                        payment_method_types: ['card'],
                                    });
                                } else
                                {
                                    stripe_intents.fullamount_wallet = {};
                    
                                }
                            }
                            else
                            {
                                    stripe_intents.total_deposit = {};
                                    stripe_intents.deposit_wallet = {};
                                    stripe_intents.total_fullamount = {};
                                    stripe_intents.fullamount_wallet = {};

                            }
                                
                                res.status(200).send({status: 200, result: {'bookings': {service_bookings: service_bookings, product_bookings: []}, 'pay_deposit': paid_deposit, 'pay_fullamount': paid_fullamount, 'stripe_intents': stripe_intents, 'cust_wallet': cust_wallet}})

                                //End new
                            }
                        })

                                
                              }
                          }
          
                      }); 


                      //res.status(200).send({status:200, result:cust_booking_details});
                  }
              })
          }

          //////End
         
      }

  }); 

  //End NEW



 }



exports.insertPaymentDetailsCustBookings = async (req, res, next) => {



    if (!(req.body.bookings)) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    let bookings = req.body.bookings;
    let paid_deposit = 0, paid_fullamount = 0;
    var payment_details = [];
    var book_ids_update_status = [];

    async.each(bookings, (booking_obj, callback) => {

        var multi_book_id_flag = 0;

        var booking_id = parseInt(booking_obj.booking_id, 10);
        book_ids_update_status.push(booking_id);
        //console.log("booking_id: ", booking_id);
        var multi_service_book_id = parseInt(booking_obj.multi_service_book_id, 10);
        //console.log("multi_service_book_id: ", multi_service_book_id);

        if (multi_service_book_id && multi_service_book_id != 0 && multi_service_book_id != "0")
        {
            var book_id = multi_service_book_id;
            multi_book_id_flag = 1;
        } else
        {
            var book_id = booking_id;
        }


        BookingModel.getServiceBookingDetails(booking_id, 0, 0, async (err, booking_data) => {
            if (err)
            {
                callback(err);
                //return err;
                console.log("error: ", err);
            } else
            {
                //console.log("booking details: ",booking_data)
                var booking_details = booking_data[0];
                booking_details.type = booking_obj.type;

                var book_ids_array = await getOldBookings(book_id);
                //console.log("book_ids_array: ",book_ids_array);

                paid_amount = await TransactionModel.getAmount(book_ids_array);
                //console.log("paid_amount: ",paid_amount);

                booking_details.paid_amount = paid_amount[0].paid_amount;

                booking_details.due_book_deposit = booking_details.book_deposit - paid_amount[0].paid_amount;
                if (booking_details.due_book_deposit < 0) {
                    booking_details.due_book_deposit = 0;
                }

                booking_details.due_book_fullamount = booking_details.book_fullamount - paid_amount[0].paid_amount;
                if (booking_details.due_book_fullamount < 0) {
                    booking_details.due_book_fullamount = 0;
                }

                paid_deposit += booking_details.due_book_deposit;
                paid_fullamount += booking_details.due_book_fullamount;

                //console.log("booking_details: ",booking_details);
                payment_details.push(booking_details);




                callback();
            }

        })

    }, async function (err) {
        if (err) {

            next(err);
        }
        try {

            // console.log("paid_deposit: ",paid_deposit); console.log("paid_fullamount: ",paid_fullamount);
            // console.log("payment_details: ",payment_details);
            if (!payment_details.length || req.body.store_id !== payment_details[0].store_id || req.body.cust_id !== payment_details[0].cust_id || paid_deposit !== req.body.pay_deposit || paid_fullamount !== req.body.pay_fullamount) {
                res.status(400).send({status: 400, message: "data is insufficient, check all details are correct or not"});
                return;
            }
            let customer_amount, all_wallet_transactions = [], all_payment_transactions = [], each_wallet, each_transaction;

            customer_amount = await getWalletAmount({'store_id': req.body.store_id, "cust_id": req.body.cust_id});
            customer_amount = customer_amount[0].amount;

            // if(req.body.wallet_pay_amount <= 0) {
            //     customer_amount = 0;
            // }

            if (req.body.full_amount_flag) {
                total_amount = paid_fullamount;
            } else {
                total_amount = paid_deposit;
            }
            var total_amount_sent = 0;

            if (req.body.wallet_pay_flag == 1)
            {
                total_amount_sent += req.body.wallet_pay_amount;
            }



            if (req.body.stripe_pay_flag == 1 && req.body.stripe_payment_intent)
            {
                var stripe_payment_amount = req.body.stripe_payment_intent.amount / 100;
                total_amount_sent += stripe_payment_amount;
            }
            //console.log("total_amount_sent: ", total_amount_sent)

            if (total_amount != total_amount_sent) {
                res.status(400).send({status: 400, message: "payment amount incorrect, check all details are correct or not"});
                return;
            }



            if (req.body.stripe_pay_flag == 1 && req.body.stripe_payment_intent)
            {
                const stripe = require('stripe')('sk_test_LxF6TtSjyB1YMLLWcp2sTdEE');

                const paymentIntent = await stripe.paymentIntents.confirm(
                        req.body.stripe_payment_intent.id, {payment_method: 'pm_card_visa'});
                //console.log("paymentIntent: ",paymentIntent);

                var stripe_amount = paymentIntent.charges.data[0].amount;
                var gateway_txn_id = paymentIntent.charges.data[0].id;


            }



            async.eachSeries(payment_details, async (payment_obj, callback) => {
                //console.log("payment_obj.book_id: ", payment_obj.book_id)



                switch (payment_obj.type)
                {

                    case constants.SERVICE_TYPE:

                        var trans_book_id = payment_obj.book_id;
                        var trans_book_class_id = 0;
                        var type = 1;
                        break;

                    case constants.CLASS_TYPE:

                        var trans_book_id = 0;
                        var trans_book_class_id = payment_obj.book_id;
                        var type = 2;
                        break;
                }

                var this_trans_complete = 0;
                var this_paid_amount = payment_obj.paid_amount;

                if (req.body.full_amount_flag) {
                    var this_trans_amount = payment_obj.due_book_fullamount;
                } else {
                    var this_trans_amount = payment_obj.due_book_deposit;
                }

                // console.log("customer_amount: ", customer_amount);
                // console.log("this_trans_amount: ", this_trans_amount);
                // console.log("this_paid_amount: ", this_paid_amount);


                //WALLET PAY
                if (req.body.wallet_pay_flag) {
                    var wallet_paid_now = 0;
                    if (customer_amount >= this_trans_amount)
                    {
                        //paying with wallet amount only


                        wallet_paid_now = this_trans_amount;
                        //console.log("wallet_paid_now: ", wallet_paid_now);

                        each_wallet = [req.body.cust_id, req.body.cust_email, constants.WALLET_DEBIT, wallet_paid_now, "deducted amount for booking of " + payment_obj.book_id,
                            req.body.store_id, payment_obj.book_id, 0];

                        // console.log("each_wallet: ",each_wallet);

                        all_wallet_transactions.push(each_wallet);

                        each_transaction = [trans_book_id, trans_book_class_id, 0, type, 0, payment_obj.cust_id, 0, 0, 0, 0, payment_obj.book_fullamount,
                            wallet_paid_now, (payment_obj.book_fullamount - (this_paid_amount + wallet_paid_now)), constants.THROUGH_WALEET,
                            2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1];

                        all_payment_transactions.push(each_transaction);

                        //console.log("each_transaction: ",each_transaction);


                        customer_amount = customer_amount - wallet_paid_now;
                        this_trans_amount = this_trans_amount - wallet_paid_now;
                        this_trans_complete = 1;


                        this_paid_amount += wallet_paid_now;

                    } else if (customer_amount > 0 && customer_amount < this_trans_amount)
                    {

                        wallet_paid_now = customer_amount;
                        //console.log("wallet_paid_now: ", wallet_paid_now);


                        each_wallet = [req.body.cust_id, req.body.cust_email, constants.WALLET_DEBIT, wallet_paid_now, "deducted amount for booking of " + payment_obj.book_id,
                            req.body.store_id, payment_obj.book_id, 0];

                        //console.log("each_wallet: ",each_wallet);

                        all_wallet_transactions.push(each_wallet);

                        each_transaction = [trans_book_id, trans_book_class_id, 0, type, 0, payment_obj.cust_id, 0, 0, 0, 0, payment_obj.book_fullamount,
                            wallet_paid_now, (payment_obj.book_fullamount - (this_paid_amount + wallet_paid_now)), constants.THROUGH_WALEET,
                            2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1];

                        //console.log("each_transaction: ",each_transaction);

                        all_payment_transactions.push(each_transaction);

                        customer_amount = customer_amount - wallet_paid_now
                        this_trans_amount = this_trans_amount - wallet_paid_now;

                        this_paid_amount += wallet_paid_now;

                    }

                }

                //STRIPE PAY
                if (this_trans_complete == 0 && this_trans_amount > 0 && req.body.stripe_pay_flag == 1)
                {
                    stripe_paid_now = this_trans_amount;
                    //console.log("stripe_paid_now: ", stripe_paid_now);
                    var each_transaction = [payment_obj.book_id, 0, 0, type, 0, payment_obj.cust_id, gateway_txn_id, 0, 0, 0, payment_obj.book_fullamount,
                        stripe_paid_now, (payment_obj.book_fullamount - (this_paid_amount + stripe_paid_now)), constants.THROUGH_STRIPE, 2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0,
                        new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1];
                    //console.log("each_transaction: ",each_transaction);
                    all_payment_transactions.push(each_transaction);
                    this_trans_complete = 1;


                    this_trans_amount = this_trans_amount - stripe_paid_now;

                    this_paid_amount += stripe_paid_now;






                }

                // console.log("customer_amount: ", customer_amount);
                // console.log("this_trans_amount: ", this_trans_amount);
                // console.log("this_paid_amount: ", this_paid_amount);

            }, async function (err) {
                if (err)
                {

                    next(err);
                } else {
                    // console.log("all_wallet_transactions: ", all_wallet_transactions);
                    // console.log("all_payment_transactions: ", all_payment_transactions);
                    let result;
                    try {
                        result = await TransactionModel.insert_transaction_and_wallet_credit(all_wallet_transactions, all_payment_transactions);
                        //console.log("book_ids_update_status: ", book_ids_update_status);

                        BookingModel.updateStatusOfBookings(constants.STATUS_BOOKED, book_ids_update_status, (update_err, updated_data) => {
                            if (update_err) {
                                next(update_err);
                            } else {
                                //console.log(updated_data);
                                //console.log("payment_details: ", payment_details);


                                var notif_book_id = payment_details[0].book_id;

                                if(payment_details[0].multi_service_book_id && payment_details[0].multi_service_book_id != 0 && payment_details[0].multi_service_book_id != "0")
                                {
                                    notif_book_id = payment_details[0].multi_service_book_id;
                                }

                                var notif_details = {}
                                                            notif_details.book_id = notif_book_id;
                                                            notif_details.cust_id = req.body.cust_id;
                                                            notif_details.cust_name = payment_details[0].book_first_name+" "+payment_details[0].book_last_name;
                                                            notif_details.cust_email = payment_details[0].book_email;
                                                            notif_details.cust_mobile = payment_details[0].book_country_isd_code.toString()+payment_details[0].book_mobile.toString();
                                                            notif_details.store_id = req.body.store_id; 
                                                            notif_details.this_base_url = /*req.protocol + */ "https://" + req.hostname; // until fix the protocal issue we directly use https
                                                            notif_details.email_base_url = req.hostname;
                                                            notif_details.book_created_by = req.body.cust_id;
                                                            notif_details.email_send = 1;
                                                            notif_details.sms_send = 1;


                                                            //console.log("notif details: ", notif_details);

                                                            if(constants.PROD_FLAG == 1)
                                                            {
                                                                    notif_details.have_multi_id = 1;
                            
                                                                    NotificationsLibrary.notification_details(notif_details, function(notif_err,notif_data){
                                                                        if (notif_err) {
                                                            
                                                                            // console.log("error from notifications");
                                                                            // console.log(notif_err)
                                                                            ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                                                //console.log("Slack alert sent");
                                                                            })
                                                                            .catch((slack_err) => {
                                                                                //console.log(slack_err);
                                                                            });
                                                                            
                                                    
                                                                        }
                                                                        else{
                                                                            //console.log("Notifications success");
                                        
                                                                        }
                                                                    })
                                                                }

                                res.status(200).send({status: 200, message: "Payment Successfully Done!.."});
                            }
                        })

                    } catch (err) {

                        next(err);
                    }
                    // console.log("book_ids_update_status: ",book_ids_update_status);


                    // res.status(200).send({status: 200, message: "Payment Successfully Done!.."});

                }
            });
        } catch (err) {

            next(err);
        }
    })
}

exports.insertPaymentDetailsCustBookings2 = async (req, res, next) => {



    if (!(req.body.bookings)) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }


    ////Start NEW

    let bookings = req.body.bookings;
    let paid_deposit = 0, paid_fullamount = 0;
    var payment_details = [];
     var service_bookings = [];
     var product_bookings = [];
    

    //Start NEW
    var service_book_ids = [];
    var have_multi_book_id = false;
        var edit_flag = 0;

        var book_ids_notif = [];

    for(i=0; i<bookings.length; i++)
    {
       if(bookings[i].type == constants.SERVICE_TYPE)
       {
          service_book_ids.push(bookings[i].booking_id); 

        

          if(bookings[i].multi_service_book_id != 0)
          {
              if(!book_ids_notif[bookings[i].multi_service_book_id])
              {
                book_ids_notif[bookings[i].multi_service_book_id] = [];
              }
              book_ids_notif[bookings[i].multi_service_book_id].push(bookings[i].booking_id);

          }
          else
          {
                book_ids_notif[bookings[i].booking_id] = [];
        
                book_ids_notif[bookings[i].booking_id].push(bookings[i].booking_id);

          }
          
       }

    }
    //console.log("service_book_ids: ",service_book_ids);

    //console.log("book ids notif " ,book_ids_notif);

    var notif_ids_array = Object.keys(book_ids_notif);
                            //console.log("notif ids array " ,notif_ids_array);


    var book_ids_update_status = service_book_ids;


  BookingModel.getServiceBookingDetails(service_book_ids,have_multi_book_id, edit_flag, (err, services_data) => {
      if (err) {
          next(err);
      }
      else{
         // console.log(services_data);
          
          if (services_data.length == 0) {
            res.status(400).send({status: 400, message: "No bookings found with given booking_id"});
            return;
        }
          /////Start
          if(services_data.length)
          {
              var is_cust_booking = 0;

              if(services_data[0].booked_through == 2)
              {
                  is_cust_booking =1;
              }

              async.eachSeries(services_data, async (service,callback1)=>{

                var booking_details = service;

                //console.log("booking_details: ", booking_details);

                  let book_ids_array =[],paid_amount=0;  
                  try{

                    book_ids_array = await getOldBookings(service.book_id,book_ids_array);

                    //console.log("book_ids_array: ",book_ids_array);
    
                    paid_amount = await TransactionModel.getAmount(book_ids_array);
                    //console.log("paid_amount: ",paid_amount);

                    //   book_ids = await getOldBookings(service.book_id,book_ids);
                    //   paid_amount = await TransactionModel.getAmount(book_ids);

                    booking_details.type = constants.SERVICE_TYPE;

                      booking_details.paid_amount = paid_amount[0].paid_amount;

                      booking_details.due_book_deposit = booking_details.book_deposit - paid_amount[0].paid_amount;
                      if (booking_details.due_book_deposit < 0) {
                          booking_details.due_book_deposit = 0;
                      }
      
                      booking_details.due_book_fullamount = booking_details.book_fullamount - paid_amount[0].paid_amount;
                      if (booking_details.due_book_fullamount < 0) {
                          booking_details.due_book_fullamount = 0;
                      }
      
                      paid_deposit += booking_details.due_book_deposit;
                      paid_fullamount += booking_details.due_book_fullamount;
                      //console.log("booking_details: ",booking_details);
                      payment_details.push(booking_details);
                      service_bookings.push(booking_details);  
                      //callback1();                   
    
                      
                  }catch(error){
                      throw error;
                  }  
                                            
                  
              },function(err){
                //console.log("in function (err)");
                  if(err){
                      next(err)
                  }else{
                      //console.log("in function (err)");

                      
                      BookingModel.getProductDetails(service_book_ids, edit_flag, async (err, products_data) => {
                          if (err) {
                              next(err);
                          }
                          else{
                              //console.log("products_data:", products_data);
                              //var product_bookings = [];
                              //cust_booking_details.product_bookings = []

                              var product_book_ids_update_status = [];
                              if(products_data.length)
                              {
                                  async.each(products_data, (product,callback)=>{

                                      product_book_ids_update_status.push(product.booking_product_id);
                                      let booking_product_ids_array =[] 
                                      //product_paid_amount=0;

                                      var temp_old_book_product_id = product.booking_product_id;

                                      if(product.booking_old_product_id != 0){
                                          temp_old_book_product_id = product.booking_old_product_id;
                                      }

                                      ProductsModel.getOldProductBookings(temp_old_book_product_id, (old_err, old_data) => {
                                          if (old_err) {
                                              next(old_err);
                                          }
                                          else{
                                            // console.log("product old data: ",old_data);
                                              for(i = 0; i< old_data.length; i++)
                                              {
                                                  booking_product_ids_array.push(old_data[i].booking_product_id);
                                              }
                                              //console.log("boooking product ids :", booking_product_ids_array);
      
                                              ProductsModel.getProductBookPaidInvoice(booking_product_ids_array, (invoice_err, invoice_data) => {
                                                  if (invoice_err) {
                                      
                                                      next(invoice_err);
                                                      return;
                                                  }
                                                  else{
                                                      //console.log("invoice_data: ",invoice_data);
                                                      var product_booking_details= product;
                                                      product_booking_details.type = constants.PRODUCT_TYPE;
                                                      //console.log("product_booking_details: ", product_booking_details);
                                                       product_booking_details.paid_amount = invoice_data[0].total_book_trans_paid_now;


                                                       var total_product_deposit = product_booking_details.product_quantity * product_booking_details.product_full_amount;
                                                       var total_product_full_amount = product_booking_details.product_quantity * product_booking_details.product_full_amount;

                      product_booking_details.due_book_deposit = total_product_deposit - invoice_data[0].total_book_trans_paid_now;
                      if (product_booking_details.due_book_deposit < 0) {
                          product_booking_details.due_book_deposit = 0;
                      }
      
                      product_booking_details.due_book_fullamount = total_product_full_amount -  invoice_data[0].total_book_trans_paid_now;
                      if (product_booking_details.due_book_fullamount < 0) {
                          product_booking_details.due_book_fullamount = 0;
                      }

                       paid_deposit += product_booking_details.due_book_deposit;
                       paid_fullamount += product_booking_details.due_book_fullamount;
                       //console.log("product_booking_details: ",product_booking_details);
                      payment_details.push(product_booking_details);                     
    
      

                                                      product_bookings.push(product_booking_details);
                                                      callback();
                                       

                                                  }
                                              })
                                          }
                                      })
              
                                                  
                                                              
                                      
                                  },async function(err){
                                      if(err){
                                          next(err)
                                      }else{
                                          //console.log("in product bookings");
                                          
                                          //console.log("product_bookings: ", product_bookings);
                                          

                                          //Start new

                                          try {

                                            //  console.log("paid_deposit: ",paid_deposit); console.log("paid_fullamount: ",paid_fullamount);
                                            //  console.log("payment_details: ",payment_details);

                                             paid_deposit = paid_deposit.toFixed(2);
                                             paid_fullamount = paid_fullamount.toFixed(2);

                                             var req_pay_deposit = req.body.pay_deposit;
                                             req_pay_deposit = req_pay_deposit.toFixed(2);
                                             var req_pay_fullamount = req.body.pay_fullamount;
                                             req_pay_fullamount = req_pay_fullamount.toFixed(2);

                                            if (!payment_details.length || req.body.store_id !== payment_details[0].store_id || req.body.cust_id !== payment_details[0].cust_id || paid_deposit !== req_pay_deposit || paid_fullamount !== req_pay_fullamount) {
                                                res.status(400).send({status: 400, message: "data is insufficient, check all details are correct or not"});
                                                return;
                                            }
                                            let customer_amount, all_wallet_transactions = [], all_payment_transactions = [], each_wallet, each_transaction;
                                
                                            customer_amount = await getWalletAmount({'store_id': req.body.store_id, "cust_id": req.body.cust_id});
                                            customer_amount = customer_amount[0].amount;

                                            //console.log("customer_amount:",customer_amount);
                                
                                            // if(req.body.wallet_pay_amount <= 0) {
                                            //     customer_amount = 0;
                                            // }
                                
                                            if (req.body.full_amount_flag) {
                                                total_amount = paid_fullamount;
                                            } else {
                                                total_amount = paid_deposit;
                                            }
                                            var total_amount_sent = 0;
                                
                                            if (req.body.wallet_pay_flag == 1)
                                            {
                                                total_amount_sent += req.body.wallet_pay_amount;
                                            }
                                
                                
                                
                                            if (req.body.stripe_pay_flag == 1 && req.body.stripe_payment_intent)
                                            {
                                                var stripe_payment_amount = req.body.stripe_payment_intent.amount / 100;
                                                total_amount_sent += stripe_payment_amount;
                                            }


                                            //Square
                                            if (req.body.square_pay_flag == 1 && req.body.square_response)
                                            {
                                                var square_payment_amount = req.body.square_response.amount;
                                                total_amount_sent += square_payment_amount;
                                            }
                                            //Square

                                            total_amount_sent = total_amount_sent.toFixed(2);
                                            //console.log("total_amount: ", total_amount)
                                            //console.log("total_amount_sent: ", total_amount_sent)
//                                            res.status(400).send({total_amount:total_amount,total_amount_sent:total_amount_sent})
//                                            return false;
                                            if (parseFloat(total_amount) > parseFloat(total_amount_sent)) {
                                                //console.log("total_amount2: ", total_amount)
                                                res.status(400).send({status: 400, message: "payment amount incorrect, check all details are correct or not"});
                                                return;
                                            }else if(parseFloat(total_amount_sent) > parseFloat(total_amount)){
                                                //console.log("total_amount3: ", total_amount)
                                                let adjust_stripe_amount = ((total_amount_sent-total_amount)*100)/100;
                                                customer_amount = ((customer_amount-adjust_stripe_amount)*100)/100;
                                            }
                                
                                
                                
                                            if (req.body.stripe_pay_flag == 1 && req.body.stripe_payment_intent)
                                            {
                                                //const stripe = require('stripe')('sk_test_LxF6TtSjyB1YMLLWcp2sTdEE');
                                
                                                // const paymentIntent = await stripe.paymentIntents.confirm(
                                                //         req.body.stripe_payment_intent.id, {payment_method: 'pm_card_visa'});
                                                // console.log("paymentIntent: ",paymentIntent);
                                
                                                var stripe_amount = req.body.stripe_payment_intent.amount;
                                                var gateway_txn_id = req.body.stripe_payment_intent.id;
                                
                                
                                            }


                                           

                                            //var trans_booking_notes_array = [];

                                            var booking_notes_array = [];

                                            var product_quantity_array = [];
                                
                                
                                
                                            async.eachSeries(payment_details, async (payment_obj, callback) => {

                                               // console.log("payment_obj: ", payment_obj)
                                                //console.log("payment_obj.book_id: ", payment_obj.book_id)

                                                //var this_trans_booking_notes = "";

                                                if(payment_obj.multi_service_book_id)
                                                {
                                                    var this_id = payment_obj.multi_service_book_id;
                                                }
                                                else
                                                {
                                                     var this_id = payment_obj.book_id;
                                                }



                                                if(!booking_notes_array[this_id])
                                                { 

                                                    booking_notes_array[this_id] = {
                                                        book_ids : [],
                                                        wallet: 0,
                                                        stripe: 0,
                                                        square: 0
                                                        
                                                    }
                                                }
                                            

                                                //booking_notes_array[this_id].book_ids.push(payment_obj.book_id)
                                                
                                
                                
                                
                                                switch (payment_obj.type)
                                                {
                                
                                                    case constants.SERVICE_TYPE:
                                
                                                        var trans_book_id = payment_obj.book_id;
                                                        var trans_book_class_id = 0;
                                                        var trans_book_product_id = 0;
                                                        var type = 1;

                                                        booking_notes_array[this_id].book_ids.push(payment_obj.book_id);

                                                        break;
                                
                                                    case constants.CLASS_TYPE:
                                
                                                        var trans_book_id = 0;
                                                        var trans_book_class_id = payment_obj.book_id;
                                                        var trans_book_product_id = 0;
                                                        var type = 2;
                                                        break;

                                                    case constants.PRODUCT_TYPE:

                                                        var trans_book_id = 0;
                                                        var trans_book_class_id = 0;
                                                        var trans_book_product_id = payment_obj.booking_product_id;
                                                        var type = 3;

                                                        product_quantity_array.push([payment_obj.product_id, req.body.store_id, payment_obj.product_remaining_quantity-payment_obj.product_quantity]); 

                                                        break;

                                                }
                                
                                                var this_trans_complete = 0;
                                                var this_paid_amount = payment_obj.paid_amount;
                                
                                                if (req.body.full_amount_flag) {
                                                    var this_trans_amount = payment_obj.due_book_fullamount;
                                                } else {
                                                    var this_trans_amount = payment_obj.due_book_deposit;
                                                }
                                
                                                // console.log("customer_amount: ", customer_amount);
                                                // console.log("this_trans_amount: ", this_trans_amount);
                                                // console.log("this_paid_amount: ", this_paid_amount);
                                
                                
                                                //WALLET PAY
                                                if (req.body.wallet_pay_flag) {
                                                    var wallet_paid_now = 0;
                                                    if (customer_amount >= this_trans_amount)
                                                    {
                                                        //paying with wallet amount only
                                
                                
                                                        wallet_paid_now = this_trans_amount;
                                                        //console.log("wallet_paid_now: ", wallet_paid_now);
                                                        
                                                        //console.log("payment obj type: ", payment_obj.type);

                                                        if(payment_obj.type == constants.SERVICE_TYPE)
                                                        {
                                                            //console.log("payment obj type: ", payment_obj.type);
                                                            each_wallet = [req.body.cust_id, req.body.cust_email, constants.WALLET_DEBIT, wallet_paid_now, "deducted amount for booking of " + payment_obj.book_id,
                                                            req.body.store_id, payment_obj.book_id, 0];

                                                            each_transaction = [trans_book_id, trans_book_class_id, trans_book_product_id, type, 0, payment_obj.cust_id, 0, 0, 0, 0, payment_obj.book_fullamount,
                                                                wallet_paid_now, (payment_obj.book_fullamount - (this_paid_amount + wallet_paid_now)), constants.THROUGH_WALEET,
                                                                2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1, 0, 0];

                                                                //this_trans_booking_notes += "$"+wallet_paid_now+" paid via wallet on "+moment_tz().tz("Australia/Brisbane").format("YYYY-MM-DD hh:mm:ss A")+". ";
                                                        }
                                                        else if(payment_obj.type == constants.PRODUCT_TYPE)
                                                        {
                                                            //console.log("payment obj type: ", payment_obj.type);

                                                            each_wallet = [req.body.cust_id, req.body.cust_email, constants.WALLET_DEBIT, wallet_paid_now, "deducted amount for booking product " + payment_obj.booking_product_id,
                                                            req.body.store_id, 0, payment_obj.booking_product_id];

                                                            var this_product_full_amount = payment_obj.product_quantity * payment_obj.product_full_amount;

                                                            each_transaction = [trans_book_id, trans_book_class_id, trans_book_product_id, type, 0, payment_obj.product_cust_id, 0, 0, 0, 0, this_product_full_amount,
                                                                wallet_paid_now, (this_product_full_amount - (this_paid_amount + wallet_paid_now)), constants.THROUGH_WALEET,
                                                                2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1, 0, 0];

                                                            
                                                        }
                                
                                                        // console.log("each_wallet: ",each_wallet);
                                
                                                        all_wallet_transactions.push(each_wallet);
                                                        
                                
                                                        // each_transaction = [trans_book_id, trans_book_class_id, trans_book_product_id, type, 0, payment_obj.cust_id, 0, 0, 0, 0, payment_obj.book_fullamount,
                                                        //     wallet_paid_now, (payment_obj.book_fullamount - (this_paid_amount + wallet_paid_now)), constants.THROUGH_WALEET,
                                                        //     2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1];
                                
                                                        all_payment_transactions.push(each_transaction);
                                
                                                        //console.log("each_transaction: ",each_transaction);
                                
                                
                                                        customer_amount = customer_amount - wallet_paid_now;
                                                        this_trans_amount = this_trans_amount - wallet_paid_now;
                                                        this_trans_complete = 1;
                                
                                
                                                        this_paid_amount += wallet_paid_now;

                                                        booking_notes_array[this_id].wallet += wallet_paid_now;
                                
                                                    } else if (customer_amount > 0 && customer_amount < this_trans_amount)
                                                    {
                                
                                                        wallet_paid_now = customer_amount;
                                                        //console.log("wallet_paid_now: ", wallet_paid_now);

                                                        //Start
                                                         //Start

                                                         if(payment_obj.type == constants.SERVICE_TYPE)
                                                         {
                                                             //console.log("payment obj type: ", payment_obj.type);

                                                             each_wallet = [req.body.cust_id, req.body.cust_email, constants.WALLET_DEBIT, wallet_paid_now, "deducted amount for booking of " + payment_obj.book_id,
                                                             req.body.store_id, payment_obj.book_id, 0];

 
                                                             each_transaction = [trans_book_id, trans_book_class_id, trans_book_product_id, type, 0, payment_obj.cust_id, 0, 0, 0, 0, payment_obj.book_fullamount,
                                                                 wallet_paid_now, (payment_obj.book_fullamount - (this_paid_amount + wallet_paid_now)), constants.THROUGH_WALEET,
                                                                 2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1, 0, 0];

                                                                 //this_trans_booking_notes += "$"+wallet_paid_now+" paid via wallet on "+moment_tz().tz("Australia/Brisbane").format("YYYY-MM-DD hh:mm:ss A")+". ";

                                                            
                                                         }
                                                         else if(payment_obj.type == constants.PRODUCT_TYPE)
                                                         {
                                                             //console.log("payment obj type: ", payment_obj.type);
 
                                                             each_wallet = [req.body.cust_id, req.body.cust_email, constants.WALLET_DEBIT, wallet_paid_now, "deducted amount for booking product " + payment_obj.booking_product_id,
                                                             req.body.store_id, 0, payment_obj.booking_product_id];
 
                                                             var this_product_full_amount = payment_obj.product_quantity * payment_obj.product_full_amount;
 
                                                             each_transaction = [trans_book_id, trans_book_class_id, trans_book_product_id, type, 0, payment_obj.product_cust_id, 0, 0, 0, 0, this_product_full_amount,
                                                                 wallet_paid_now, (this_product_full_amount - (this_paid_amount + wallet_paid_now)), constants.THROUGH_WALEET,
                                                                 2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1, 0, 0];

     
                                                         }
                                 
                                                         // console.log("each_wallet: ",each_wallet);
                                 
                                                         all_wallet_transactions.push(each_wallet);
                                
                                                        //console.log("each_transaction: ",each_transaction);
                                
                                                        all_payment_transactions.push(each_transaction);

                                                         //End
                                
                                                        customer_amount = customer_amount - wallet_paid_now
                                                        this_trans_amount = this_trans_amount - wallet_paid_now;
                                
                                                        this_paid_amount += wallet_paid_now;

                                                        booking_notes_array[this_id].wallet += wallet_paid_now;
                                
                                                    }
                                
                                                }
                                
                                                //STRIPE PAY
                                                if (this_trans_complete == 0 && this_trans_amount > 0 && req.body.stripe_pay_flag == 1)
                                                {
                                                    stripe_paid_now = this_trans_amount;
                                                    //console.log("stripe_paid_now: ", stripe_paid_now);

                                                    if(payment_obj.type == constants.SERVICE_TYPE)
                                                    {
                                                        //console.log("payment obj type: ", payment_obj.type);

                                                        var each_transaction = [trans_book_id, trans_book_class_id, trans_book_product_id, type, 0, payment_obj.cust_id, gateway_txn_id, 0, 0, 0, payment_obj.book_fullamount,
                                                        stripe_paid_now, (payment_obj.book_fullamount - (this_paid_amount + stripe_paid_now)), constants.THROUGH_STRIPE, 2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0,
                                                        new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1, 0, 0];

                                                        //this_trans_booking_notes += "$"+stripe_paid_now+" paid via stripe on "+moment_tz().tz("Australia/Brisbane").format("YYYY-MM-DD hh:mm:ss A")+". ";
                                                    }
                                                    else if(payment_obj.type == constants.PRODUCT_TYPE)
                                                    {
                                                        //console.log("payment obj type: ", payment_obj.type);

                                                        var this_product_full_amount = payment_obj.product_quantity * payment_obj.product_full_amount;

                                                        var each_transaction = [trans_book_id, trans_book_class_id, trans_book_product_id, type, 0, payment_obj.product_cust_id, gateway_txn_id, 0, 0, 0, stripe_paid_now,
                                                                stripe_paid_now, (this_product_full_amount - (this_paid_amount + stripe_paid_now)), constants.THROUGH_STRIPE,
                                                                2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1, 0, 0];
                                                    }
                                
                                                    //console.log("STRIPE each_transaction: ",each_transaction);
                                                    all_payment_transactions.push(each_transaction);
                                                    this_trans_complete = 1;
                                
                                
                                                    this_trans_amount = this_trans_amount - stripe_paid_now;
                                
                                                    this_paid_amount += stripe_paid_now;
                                
                                
                                
                                
                                                    booking_notes_array[this_id].stripe += stripe_paid_now;
                                
                                                }



                                                //SQUARE PAY
                                                if (this_trans_complete == 0 && this_trans_amount > 0 && req.body.square_pay_flag == 1)
                                                {
                                                    square_paid_now = this_trans_amount;
                                                    var square_gateway_txn_id = req.body.square_response.id;
                                                    var square_order_id = req.body.square_response.orderId;
                                                    var square_receipt_url = req.body.square_response.receiptUrl;

                                                    //console.log("square_paid_now: ", square_paid_now);

                                                    if(payment_obj.type == constants.SERVICE_TYPE)
                                                    {
                                                        //console.log("payment obj type: ", payment_obj.type);

                                                        var each_transaction = [trans_book_id, trans_book_class_id, trans_book_product_id, type, 0, payment_obj.cust_id, square_gateway_txn_id, 0, 0, 0, payment_obj.book_fullamount,
                                                        square_paid_now, (payment_obj.book_fullamount - (this_paid_amount + square_paid_now)), constants.THROUGH_SQUARE, 2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0,
                                                        new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1, square_order_id, square_receipt_url];

                                                        
                                                    }
                                                    else if(payment_obj.type == constants.PRODUCT_TYPE)
                                                    { 
                                                        //console.log("payment obj type: ", payment_obj.type);

                                                        var this_product_full_amount = payment_obj.product_quantity * payment_obj.product_full_amount;

                                                        var each_transaction = [trans_book_id, trans_book_class_id, trans_book_product_id, type, 0, payment_obj.product_cust_id, square_gateway_txn_id, 0, 0, 0, square_paid_now,
                                                                square_paid_now, (this_product_full_amount - (this_paid_amount + square_paid_now)), constants.THROUGH_SQUARE,
                                                                2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1, square_order_id, square_receipt_url];
                                                    }
                                
                                                    //console.log("STRIPE each_transaction: ",each_transaction);
                                                    all_payment_transactions.push(each_transaction);
                                                    this_trans_complete = 1;
                                
                                
                                                    this_trans_amount = this_trans_amount - square_paid_now;
                                
                                                    this_paid_amount += square_paid_now;
                                
                                
                                
                                
                                                    booking_notes_array[this_id].square += square_paid_now;
                                
                                                }
                                                //Square


                                                // if(payment_obj.book_id && this_trans_booking_notes !== "")
                                                // {

                                                // var this_trans_booking_notes_array = [payment_obj.book_id, this_trans_booking_notes];

                                                // trans_booking_notes_array.push(this_trans_booking_notes_array);
                                                // }
                                
                                                // console.log("customer_amount: ", customer_amount);
                                                // console.log("this_trans_amount: ", this_trans_amount);
                                                // console.log("this_paid_amount: ", this_paid_amount);
                                
                                            }, async function (err) {
                                                //console.log("booking_notes_array: ", booking_notes_array);
                                                //return;
                                                if (err)
                                                {
                                
                                                    next(err);
                                                } else {
                                                     //console.log("all_wallet_transactions: ", all_wallet_transactions);
                                                     //console.log("all_payment_transactions: ", all_payment_transactions);
                                                    let result;
                                                    try {
                                                        result = await TransactionModel.insert_transaction_and_wallet_credit(all_wallet_transactions, all_payment_transactions);
                                                        //console.log("book_ids_update_status: ", book_ids_update_status);
                                
                                                        BookingModel.updateStatusOfBookings(constants.STATUS_BOOKED, book_ids_update_status, (update_err, updated_data) => {
                                                            if (update_err) {
                                                                next(update_err);
                                                            } else {
                                                                //console.log(updated_data);
                                                                //console.log("payment_details: ", payment_details);
                                                                //console.log("trans_booking_notes_array :", trans_booking_notes_array);

                                                                //console.log("product book ids update status :", product_book_ids_update_status);

                                                               ProductsModel.updateStatusOfProducts(constants.STATUS_BOOKED, product_book_ids_update_status, (update_err, updated_data) => {
                                                                   if (update_err) {
                                                                       next(update_err);
                                                                  } else {
                                                                    //console.log("product quantity array: ", product_quantity_array);

                                                                    ProductsModel.updateProductQuantaty(product_quantity_array, (quantity_err, quantity_data) => {
                                                                        if (quantity_err) {
                                                                            next(quantity_err);
                                                                       } else {

                                                                        //console.log("payment_details: ", payment_details);

                                                                       
                                
                                
                                                                var notif_book_id = payment_details[0].book_id;
                                
                                                                if(payment_details[0].multi_service_book_id && payment_details[0].multi_service_book_id != 0 && payment_details[0].multi_service_book_id != "0")
                                                                {
                                                                    notif_book_id = payment_details[0].multi_service_book_id;
                                                                }
                                
                                                                var notif_details = {}
                                                                                            //notif_details.book_id = notif_book_id;
                                                                                            notif_details.cust_id = req.body.cust_id;
                                                                                            notif_details.cust_name = payment_details[0].book_first_name+" "+payment_details[0].book_last_name;
                                                                                            notif_details.cust_email = payment_details[0].book_email;
                                                                                            notif_details.cust_mobile = payment_details[0].book_country_isd_code.toString()+payment_details[0].book_mobile.toString();
                                                                                            notif_details.store_id = req.body.store_id; 
                                                                                            notif_details.this_base_url = /*req.protocol + */ "https://" + req.hostname; // until fix the protocal issue we directly use https
                                                                                            notif_details.email_base_url = req.hostname;
                                                                                            notif_details.book_created_by = req.body.cust_id;
                                                                                            notif_details.email_send = 1;
                                                                                            notif_details.sms_send = 1;

                                                                                                //console.log("is cust booking: ", is_cust_booking);
                                                                                                if(is_cust_booking == 1)
                                                                                                {

                                                                                                    var trans_booking_notes_array = [];

                                                                                                    booking_notes_array.filter((notes_obj)=>{
                                                                                                    
                                                                                                        //console.log("notes_obj: ", notes_obj);
                                                                                                        if(notes_obj)
                                                                                                        {
                                                                                                           
                                                                                                            //console.log("notes_obj1: ", notes_obj);
                                                                                                            var notes_string = "";

                                                                                                            if(notes_obj.book_ids.length >1)
                                                                                                            {
                                                                                                                notes_string += "Multiservice - ";
                                                                                                            }

                                                                                                            if(notes_obj.wallet != 0)
                                                                                                            {
                                                                                                                notes_string += "$"+notes_obj.wallet.toFixed(2)+" paid via wallet. "
                                                                                                            }

                                                                                                            if(notes_obj.stripe != 0)
                                                                                                            {
                                                                                                                notes_string += "$"+notes_obj.stripe.toFixed(2)+" paid via stripe. "
                                                                                                            }

                                                                                                            if(notes_obj.square != 0)
                                                                                                            {
                                                                                                                notes_string += "$"+notes_obj.square.toFixed(2)+" paid via square. "
                                                                                                            }

                                                                                                            for(i=0; i< notes_obj.book_ids.length; i ++)
                                                                                                            {
                                                                                                                var this_trans_booking_notes = [notes_obj.book_ids[i], notes_string];
                                                                                                                trans_booking_notes_array.push(this_trans_booking_notes);
                                                                                                                //console.log("i: ",i);
                                                                                                            }





                                                                                                        }

                                                                                                    });
                                                                                                    //console.log("trans_booking_notes_array: ",trans_booking_notes_array);

                                                                                                BookingModel.updateTransactionBookingNotes(trans_booking_notes_array, (update_err, updated_data) => {
                                                                                                    if (update_err) {
                                                                                                        next(update_err);
                                                                                                    } else {

                                
                                                                                                      res.status(200).send({status: 200, message: "Payment Successfully Done!.."});
                                                                                                    }
                                                                                                })
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                res.status(200).send({status: 200, message: "Payment Successfully Done!.."});

                                                                                            }


                                                                                            //notifications Start
                                                                                        if(constants.PROD_FLAG == 1)
                                                                                        {
  
                                                                                            async.eachSeries(notif_ids_array, (this_notif_id_array, notif_callback) => {

                                                                                                
                                                                                                var this_notif_details = {};

                                                                                        
                                                                                                    this_notif_details = notif_details;
                                                                                                    this_notif_details.book_id = book_ids_notif[this_notif_id_array][0];
                                                                                                    this_notif_details.have_multi_id = 0;

                                                                                                    

                                                                                                    if(book_ids_notif[this_notif_id_array].length > 1)
                                                                                                    {
                                                                                                        this_notif_details.have_multi_id = 1;

                                                                                                    }

                                                                                                    //console.log("this notif details: ", this_notif_details);
                                                                                                    //console.log("this notif id: ", this_notif_id_array);
                                                                                                    NotificationsLibrary.notification_details(this_notif_details, function(notif_err,notif_data){
                                                                                                        if (notif_err) {
                                                                                            
                                                                                                            // console.log("error from notifications");
                                                                                                            // console.log(notif_err)
                                                                                                            ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                                                                                //console.log("Slack alert sent");
                                                                                                            })
                                                                                                            .catch((slack_err) => {
                                                                                                                //console.log(slack_err);
                                                                                                            });
                                                                                                            
                                                                                    
                                                                                                        }
                                                                                                        else{
                                                                                                            console.log("Notifications success");
                                                                                                            notif_callback();
                                                                        
                                                                                                        }
                                                                                                    })


                                                                                            }, function(err){
                                                                                                console.log("all Notifications success");

                                                                                            })
                                                                                        }
                                                                                        //Notifications end

                                                                                        }
                                                                                    })


                                                                                        }
                                                                                     })

                                                            }
                                                        })
                                
                                                    } catch (err) {
                                
                                                        next(err);
                                                    }
                                                    // console.log("book_ids_update_status: ",book_ids_update_status);
                                
                                
                                                    // res.status(200).send({status: 200, message: "Payment Successfully Done!.."});
                                
                                                }
                                            });
                                        } catch (err) {
                                
                                            next(err);
                                        }
                                         
                                                             

                                           //End new
                                    }
                                  })
                              }
                              else
                              {

                                //No products in booking

                                //Start new  
                                
                                try {

                                    // console.log("paid_deposit: ",paid_deposit); console.log("paid_fullamount: ",paid_fullamount);
                                     //console.log("payment_details: ",payment_details);
                                    if (!payment_details.length || req.body.store_id !== payment_details[0].store_id || req.body.cust_id !== payment_details[0].cust_id || paid_deposit !== req.body.pay_deposit || paid_fullamount !== req.body.pay_fullamount) {
                                        res.status(400).send({status: 400, message: "data is insufficient, check all details are correct or not"});
                                        return;
                                    }
                                    let customer_amount, all_wallet_transactions = [], all_payment_transactions = [], each_wallet, each_transaction;
                        
                                    customer_amount = await getWalletAmount({'store_id': req.body.store_id, "cust_id": req.body.cust_id});
                                    customer_amount = customer_amount[0].amount;
                        
                                    // if(req.body.wallet_pay_amount <= 0) {
                                    //     customer_amount = 0;
                                    // }
                                    
                                    if (req.body.full_amount_flag) {
                                        total_amount = paid_fullamount;
                                    } else {
                                        total_amount = paid_deposit;
                                    }
                                    var total_amount_sent = 0;
                        
                                    if (req.body.wallet_pay_flag == 1)
                                    {
                                        total_amount_sent += req.body.wallet_pay_amount;
                                    }
                        
                        
                        
                                    if (req.body.stripe_pay_flag == 1 && req.body.stripe_payment_intent)
                                    {
                                        var stripe_payment_amount = req.body.stripe_payment_intent.amount / 100;
                                        total_amount_sent += stripe_payment_amount;
                                    }

                                    //Square
                                    if (req.body.square_pay_flag == 1 && req.body.square_response)
                                    {
                                        var square_payment_amount = req.body.square_response.amount;
                                        total_amount_sent += square_payment_amount;
                                    }
                                    //Square
                                    
                                    total_amount = total_amount.toFixed(2);
                                    total_amount_sent = total_amount_sent.toFixed(2);
                                    //console.log("total_amount_sent: ", total_amount_sent)
                                    if (parseFloat(total_amount) > parseFloat(total_amount_sent)) {
                                        res.status(400).send({status: 400, message: "payment amount incorrect, check all details are correct or not"});
                                        return;
                                    }else if(parseFloat(total_amount_sent) > parseFloat(total_amount)){
                                        let adjust_stripe_amount = ((total_amount_sent-total_amount)*100)/100;
                                        customer_amount = ((customer_amount-adjust_stripe_amount)*100)/100;
                                    }
                        
                        
                        
                                    if (req.body.stripe_pay_flag == 1 && req.body.stripe_payment_intent)
                                    {
                                        //const stripe = require('stripe')('sk_test_LxF6TtSjyB1YMLLWcp2sTdEE');
                        
                                        // const paymentIntent = await stripe.paymentIntents.confirm(
                                        //         req.body.stripe_payment_intent.id, {payment_method: 'pm_card_visa'});
                                        // console.log("paymentIntent: ",paymentIntent);
                        
                                        var stripe_amount = req.body.stripe_payment_intent.amount;
                                        var gateway_txn_id = req.body.stripe_payment_intent.id;
                        
                        
                                    }

                                    //var trans_booking_notes_array = [];

                                    var booking_notes_array = [];
                        
                        
                        
                                    async.eachSeries(payment_details, async (payment_obj, callback) => {
                                        //console.log("payment_obj.book_id: ", payment_obj.book_id)

                                        //var this_trans_booking_notes = "";

                                                if(payment_obj.multi_service_book_id)
                                                {
                                                    var this_id = payment_obj.multi_service_book_id;
                                                }
                                                else
                                                {
                                                     var this_id = payment_obj.book_id;
                                                }



                                                if(!booking_notes_array[this_id])
                                                { 

                                                    booking_notes_array[this_id] = {
                                                        book_ids : [],
                                                        wallet: 0,
                                                        stripe: 0,
                                                        square: 0
                                                        
                                                    }
                                                }
                                            

                        
                        
                        
                                        switch (payment_obj.type)
                                        {
                        
                                            case constants.SERVICE_TYPE:
                        
                                                var trans_book_id = payment_obj.book_id;
                                                var trans_book_class_id = 0;
                                                var type = 1;

                                                booking_notes_array[this_id].book_ids.push(payment_obj.book_id);

                                                break;
                        
                                            case constants.CLASS_TYPE:
                        
                                                var trans_book_id = 0;
                                                var trans_book_class_id = payment_obj.book_id;
                                                var type = 2;
                                                break;
                                        }
                        
                                        var this_trans_complete = 0;
                                        var this_paid_amount = payment_obj.paid_amount;
                        
                                        if (req.body.full_amount_flag) {
                                            var this_trans_amount = payment_obj.due_book_fullamount;
                                        } else {
                                            var this_trans_amount = payment_obj.due_book_deposit;
                                        }
                        
                                        // console.log("customer_amount: ", customer_amount);
                                        // console.log("this_trans_amount: ", this_trans_amount);
                                        // console.log("this_paid_amount: ", this_paid_amount);
                        
                        
                                        //WALLET PAY
                                        if (req.body.wallet_pay_flag) {
                                            var wallet_paid_now = 0;
                                            if (customer_amount >= this_trans_amount)
                                            {
                                                //paying with wallet amount only
                        
                        
                                                wallet_paid_now = this_trans_amount;
                                                //console.log("wallet_paid_now: ", wallet_paid_now);
                        
                                                each_wallet = [req.body.cust_id, req.body.cust_email, constants.WALLET_DEBIT, wallet_paid_now, "deducted amount for booking of " + payment_obj.book_id,
                                                    req.body.store_id, payment_obj.book_id, 0];
                        
                                                // console.log("each_wallet: ",each_wallet);
                        
                                                all_wallet_transactions.push(each_wallet);
                        
                                                each_transaction = [trans_book_id, trans_book_class_id, 0, type, 0, payment_obj.cust_id, 0, 0, 0, 0, payment_obj.book_fullamount,
                                                    wallet_paid_now, (payment_obj.book_fullamount - (this_paid_amount + wallet_paid_now)), constants.THROUGH_WALEET,
                                                    2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1, 0, 0];
                        
                                                all_payment_transactions.push(each_transaction);

                                                //this_trans_booking_notes += "$"+wallet_paid_now+" paid via wallet on "+moment_tz().tz("Australia/Brisbane").format("YYYY-MM-DD hh:mm:ss A")+". ";
                        
                                                //console.log("each_transaction: ",each_transaction);
                        
                        
                                                customer_amount = customer_amount - wallet_paid_now;
                                                this_trans_amount = this_trans_amount - wallet_paid_now;
                                                this_trans_complete = 1;
                        
                        
                                                this_paid_amount += wallet_paid_now;

                                                booking_notes_array[this_id].wallet += wallet_paid_now;
                        
                                            } else if (customer_amount > 0 && customer_amount < this_trans_amount)
                                            {
                        
                                                wallet_paid_now = customer_amount;
                                                //console.log("wallet_paid_now: ", wallet_paid_now);
                        
                        
                                                each_wallet = [req.body.cust_id, req.body.cust_email, constants.WALLET_DEBIT, wallet_paid_now, "deducted amount for booking of " + payment_obj.book_id,
                                                    req.body.store_id, payment_obj.book_id, 0];
                        
                                                //console.log("each_wallet: ",each_wallet);
                        
                                                all_wallet_transactions.push(each_wallet);
                        
                                                each_transaction = [trans_book_id, trans_book_class_id, 0, type, 0, payment_obj.cust_id, 0, 0, 0, 0, payment_obj.book_fullamount,
                                                    wallet_paid_now, (payment_obj.book_fullamount - (this_paid_amount + wallet_paid_now)), constants.THROUGH_WALEET,
                                                    2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1, 0, 0];
                        
                                                //console.log("each_transaction: ",each_transaction);
                        
                                                all_payment_transactions.push(each_transaction);

                                                //this_trans_booking_notes += "$"+wallet_paid_now+" paid via wallet on "+moment_tz().tz("Australia/Brisbane").format("YYYY-MM-DD hh:mm:ss A")+". ";
                        
                                                customer_amount = customer_amount - wallet_paid_now
                                                this_trans_amount = this_trans_amount - wallet_paid_now;
                        
                                                this_paid_amount += wallet_paid_now;

                                                booking_notes_array[this_id].wallet += wallet_paid_now;
                        
                                            }
                        
                                        }
                        
                                        //STRIPE PAY
                                        if (this_trans_complete == 0 && this_trans_amount > 0 && req.body.stripe_pay_flag == 1)
                                        {
                                            stripe_paid_now = this_trans_amount;
                                            //console.log("stripe_paid_now: ", stripe_paid_now);
                                            var each_transaction = [payment_obj.book_id, 0, 0, type, 0, payment_obj.cust_id, gateway_txn_id, 0, 0, 0, payment_obj.book_fullamount,
                                                stripe_paid_now, (payment_obj.book_fullamount - (this_paid_amount + stripe_paid_now)), constants.THROUGH_STRIPE, 2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0,
                                                new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1, 0, 0];
                                            //console.log("each_transaction: ",each_transaction);
                                            all_payment_transactions.push(each_transaction);
                                            this_trans_complete = 1;
                        
                        
                                            this_trans_amount = this_trans_amount - stripe_paid_now;
                        
                                            this_paid_amount += stripe_paid_now;

                                            //this_trans_booking_notes += "$"+stripe_paid_now+" paid via stripe on "+moment_tz().tz("Australia/Brisbane").format("YYYY-MM-DD hh:mm:ss A")+". ";

                                            booking_notes_array[this_id].stripe += stripe_paid_now;
                        
                        
                                        }



                                        //SQUARE PAY
                                        if (this_trans_complete == 0 && this_trans_amount > 0 && req.body.square_pay_flag == 1)
                                        {
                                            square_paid_now = this_trans_amount;
                                            var square_gateway_txn_id = req.body.square_response.id;
                                            var square_order_id = req.body.square_response.orderId;
                                            var square_receipt_url = req.body.square_response.receiptUrl;

                                            //console.log("square_paid_now: ", square_paid_now);

                                            var each_transaction = [payment_obj.book_id, 0, 0, type, 0, payment_obj.cust_id, square_gateway_txn_id, 0, 0, 0, payment_obj.book_fullamount,
                                                square_paid_now, (payment_obj.book_fullamount - (this_paid_amount + square_paid_now)), constants.THROUGH_SQUARE, 2, 0, 0, new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 0,
                                                new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 1, square_order_id, square_receipt_url];
                                            //console.log("each_transaction: ",each_transaction);
                                            all_payment_transactions.push(each_transaction);
                                            this_trans_complete = 1;
                        
                        
                                            this_trans_amount = this_trans_amount - square_paid_now;
                        
                                            this_paid_amount += square_paid_now;

                                            //this_trans_booking_notes += "$"+stripe_paid_now+" paid via stripe on "+moment_tz().tz("Australia/Brisbane").format("YYYY-MM-DD hh:mm:ss A")+". ";

                                            booking_notes_array[this_id].square += square_paid_now;
                        
                        
                                        }
                                        //Square
                        
                                        // console.log("customer_amount: ", customer_amount);
                                        // console.log("this_trans_amount: ", this_trans_amount);
                                        // console.log("this_paid_amount: ", this_paid_amount);

                                        // if(payment_obj.book_id && this_trans_booking_notes !== "")
                                        // {

                                        // var this_trans_booking_notes_array = [payment_obj.book_id, this_trans_booking_notes];

                                        // trans_booking_notes_array.push(this_trans_booking_notes_array);
                                        // }
                        
                                    }, async function (err) {
                                        if (err)
                                        {
                        
                                            next(err);
                                        } else {
                                             //console.log("all_wallet_transactions: ", all_wallet_transactions);
                                             //console.log("all_payment_transactions: ", all_payment_transactions);
                                            let result;
                                            try {
                                                result = await TransactionModel.insert_transaction_and_wallet_credit(all_wallet_transactions, all_payment_transactions);
                                                //console.log("book_ids_update_status: ", book_ids_update_status);
                        
                                                BookingModel.updateStatusOfBookings(constants.STATUS_BOOKED, book_ids_update_status, (update_err, updated_data) => {
                                                    if (update_err) {
                                                        next(update_err);
                                                    } else {
                                                        //console.log(updated_data);
                                                        //console.log("payment_details: ", payment_details);
                                                        
                                                        //console.log("trans_booking_notes_array :", trans_booking_notes_array);
                        
                                                        var notif_book_id = payment_details[0].book_id;
                        
                                                        if(payment_details[0].multi_service_book_id && payment_details[0].multi_service_book_id != 0 && payment_details[0].multi_service_book_id != "0")
                                                        {
                                                            notif_book_id = payment_details[0].multi_service_book_id;
                                                        }
                        
                                                        var notif_details = {}
                                                                                    //notif_details.book_id = notif_book_id;
                                                                                    notif_details.cust_id = req.body.cust_id;
                                                                                    notif_details.cust_name = payment_details[0].book_first_name+" "+payment_details[0].book_last_name;
                                                                                    notif_details.cust_email = payment_details[0].book_email;
                                                                                    notif_details.cust_mobile = payment_details[0].book_country_isd_code.toString()+payment_details[0].book_mobile.toString();
                                                                                    notif_details.store_id = req.body.store_id; 
                                                                                    notif_details.this_base_url = /*req.protocol + */ "https://" + req.hostname; // until fix the protocal issue we directly use https
                                                                                    notif_details.email_base_url = req.hostname;
                                                                                    notif_details.book_created_by = req.body.cust_id;
                                                                                    notif_details.email_send = 1;
                                                                                    notif_details.sms_send = 1;
                        
                        
                                                                                    //console.log("notif details: ", notif_details);
                        
                                                                                    // if(constants.PROD_FLAG == 1)
                                                                                    // {
                                                                                    //         notif_details.have_multi_id = 0;

                                                                                    //         // if(book_ids_update_status.length > 1)
                                                                                    //         // {
                                                                                    //         //     notif_details.have_multi_id = 1;

                                                                                    //         // }
                                                    
                                                                                    //         NotificationsLibrary.notification_details(notif_details, function(notif_err,notif_data){
                                                                                    //             if (notif_err) {
                                                                                    
                                                                                    //                 // console.log("error from notifications");
                                                                                    //                 // console.log(notif_err)
                                                                                    //                 ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                                                    //                     //console.log("Slack alert sent");
                                                                                    //                 })
                                                                                    //                 .catch((slack_err) => {
                                                                                    //                     //console.log(slack_err);
                                                                                    //                 });
                                                                                                    
                                                                            
                                                                                    //             }
                                                                                    //             else{
                                                                                    //                 //console.log("Notifications success");
                                                                
                                                                                    //             }
                                                                                    //         })
                                                                                    //   }

                                                                                        //console.log("is_cust_booking: ", is_cust_booking);

                                                                                        if(is_cust_booking == 1)
                                                                                        {

                                                                                            var trans_booking_notes_array = [];

                                                                                            booking_notes_array.filter((notes_obj)=>{
                                                                                            
                                                                                                //console.log("notes_obj: ", notes_obj);
                                                                                                if(notes_obj)
                                                                                                {    
                                                                                                    //console.log("notes_obj1: ", notes_obj);
                                                                                                    var notes_string = "";

                                                                                                    if(notes_obj.book_ids.length >1)
                                                                                                    {
                                                                                                        notes_string += "Multiservice - ";
                                                                                                    }

                                                                                                    if(notes_obj.wallet != 0)
                                                                                                    {
                                                                                                        notes_string += "$"+notes_obj.wallet.toFixed(2)+" paid via wallet. "
                                                                                                    }

                                                                                                    if(notes_obj.stripe != 0)
                                                                                                    {
                                                                                                        notes_string += "$"+notes_obj.stripe.toFixed(2)+" paid via stripe. "
                                                                                                    }

                                                                                                    if(notes_obj.square != 0)
                                                                                                    {
                                                                                                        notes_string += "$"+notes_obj.square.toFixed(2)+" paid via square. "
                                                                                                    }

                                                                                                    for(i=0; i< notes_obj.book_ids.length; i ++)
                                                                                                    {
                                                                                                        var this_trans_booking_notes = [notes_obj.book_ids[i], notes_string];
                                                                                                        trans_booking_notes_array.push(this_trans_booking_notes);
                                                                                                        //console.log("i: ",i);
                                                                                                    }


                                                                                                }

                                                                                            });
                                                                                            //console.log("trans_booking_notes_array: ",trans_booking_notes_array);


                                                                                            BookingModel.updateTransactionBookingNotes(trans_booking_notes_array, (update_err, updated_data) => {
                                                                                                if (update_err) {
                                                                                                    next(update_err);
                                                                                                } else {
                                                                                                    res.status(200).send({status: 200, message: "Payment Successfully Done!.."});

                                                                                                }

                                                                                            })
                                                                                        }
                                                                                        else
                                                                                        {

                                                                                        
                        
                                                                                            res.status(200).send({status: 200, message: "Payment Successfully Done!.."});
                                                                                        }

                                                                                        //Notifications Start
                                                                                        if(constants.PROD_FLAG == 1)
                                                                                        {
  
                                                                                            async.eachSeries(notif_ids_array, (this_notif_id_array, notif_callback) => {

                                                                                                
                                                                                                var this_notif_details = {};

                                                                                        
                                                                                                    this_notif_details = notif_details;
                                                                                                    this_notif_details.book_id = book_ids_notif[this_notif_id_array][0];
                                                                                                    this_notif_details.have_multi_id = 0;

                                                                                                    

                                                                                                    if(book_ids_notif[this_notif_id_array].length > 1)
                                                                                                    {
                                                                                                        this_notif_details.have_multi_id = 1;

                                                                                                    }

                                                                                                    //console.log("this notif details: ", this_notif_details);
                                                                                                    //console.log("this notif id: ", this_notif_id_array);
                                                                        
                                                            
                                                            
                                                                                                    NotificationsLibrary.notification_details(this_notif_details, function(notif_err,notif_data){
                                                                                                        if (notif_err) {
                                                                                            
                                                                                                            // console.log("error from notifications");
                                                                                                            // console.log(notif_err)
                                                                                                            ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                                                                                //console.log("Slack alert sent");
                                                                                                            })
                                                                                                            .catch((slack_err) => {
                                                                                                                //console.log(slack_err);
                                                                                                            });
                                                                                                            
                                                                                    
                                                                                                        }
                                                                                                        else{
                                                                                                            console.log("Notifications success");
                                                                                                            notif_callback();
                                                                        
                                                                                                        }
                                                                                                    })


                                                                                            }, function(err){
                                                                                                console.log("all Notifications success");

                                                                                            })
                                                                                        }

                                                                                        //Notifications end
                                                    }
                                                })
                        
                                            } catch (err) {
                        
                                                next(err);
                                            }
                                            // console.log("book_ids_update_status: ",book_ids_update_status);
                        
                        
                                            // res.status(200).send({status: 200, message: "Payment Successfully Done!.."});
                        
                                        }
                                    });
                                } catch (err) {
                        
                                    next(err);
                                }
                           
                    
                    
                               

                                //End new
                                
                              }
                          }
          
                      }); 


                      
                  }
              })
          }

          //////End
         
      }

  }); 

  //End NEW


    ////End New

   
}

// exports.squarePayments = async (req, res, next) => {

//     try {
//         console.log("square");


//         const client = new Client({
//             environment: constants.PROD_FLAG ? Environment.Production : Environment.Sandbox,
//             accessToken: "EAAAEC9VBxBi25RmsQoxLvnr_T0kCfp_lnZz8RsSwwNTm2WA9bGEo-Xz1XaFe2vq",
//           });

//           const idempotencyKey =  nanoid();

//         const response = await client.paymentsApi.createPayment({
//           sourceId: req.body.token,
//           idempotencyKey: idempotencyKey,
//           amountMoney: {
//             amount: 100,
//             currency: 'USD'
//           }
//         });
      
//         console.log(response.result);
//       } catch(error) {
//         console.log(error);
//       }
      

   
// }




exports.completeSquarePayment = (req, res, next) => {
    if (!(req.body.bookings) || (!req.body.square_token) || (!req.body.full_amount_flag && req.body.full_amount_flag !=0) || (!req.body.square_pay_amount) || (!req.body.wallet_pay_flag && req.body.wallet_pay_flag !=0)) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    let bookings = req.body.bookings;
    let paid_deposit = 0, paid_fullamount = 0;
    var payment_details = [];
    var service_bookings = [];
    var product_bookings = [];

    //Start NEW
    var service_book_ids = [];
    var have_multi_book_id = false;
        var edit_flag = 0;

    for(i=0; i<bookings.length; i++)
    {
       if(bookings[i].type == constants.SERVICE_TYPE)
       {
          service_book_ids.push(bookings[i].booking_id); 
       }

    }
    //console.log("service_book_ids: ",service_book_ids);


  BookingModel.getServiceBookingDetails(service_book_ids,have_multi_book_id, edit_flag, (err, services_data) => {
      if (err) {
          next(err);
      }
      else{
          //console.log(services_data);

          /////Start
          if(services_data.length)
          {
              async.eachSeries(services_data, async (service,callback1)=>{

                var booking_details = service;

                //console.log("booking_details: ", booking_details);

                  let book_ids_array =[],paid_amount=0;  
                  try{

                    book_ids_array = await getOldBookings(service.book_id,book_ids_array);

                    //console.log("book_ids_array: ",book_ids_array);
    
                    paid_amount = await TransactionModel.getAmount(book_ids_array);
                    //console.log("paid_amount: ",paid_amount);


                      booking_details.paid_amount = paid_amount[0].paid_amount;

                      booking_details.due_book_deposit = booking_details.book_deposit - paid_amount[0].paid_amount;
                      if (booking_details.due_book_deposit < 0) {
                          booking_details.due_book_deposit = 0;
                      }
      
                      booking_details.due_book_fullamount = booking_details.book_fullamount - paid_amount[0].paid_amount;
                      if (booking_details.due_book_fullamount < 0) {
                          booking_details.due_book_fullamount = 0;
                      }
      
                      paid_deposit += booking_details.due_book_deposit;
                      paid_fullamount += booking_details.due_book_fullamount;
                      //console.log("booking_details: ",booking_details);
                      payment_details.push(booking_details);
                      service_bookings.push(booking_details);  
                      //callback1();                   
    
                      
                  }catch(error){
                      throw error;
                  }  
                                            
                  
              },function(err){
                //console.log("in function (err)");
                  if(err){
                      next(err)
                  }else{
                      //console.log("in function (err)");

                      
                      BookingModel.getProductDetails(service_book_ids, edit_flag, async (err, products_data) => {
                          if (err) {
                              next(err);
                          }
                          else{
                              
                              if(products_data.length)
                              {
                                  async.each(products_data, (product,callback)=>{
                                      let booking_product_ids_array =[] 
                                      //product_paid_amount=0;

                                      var temp_old_book_product_id = product.booking_product_id;

                                      if(product.booking_old_product_id != 0){
                                          temp_old_book_product_id = product.booking_old_product_id;
                                      }

                                      ProductsModel.getOldProductBookings(temp_old_book_product_id, (old_err, old_data) => {
                                          if (old_err) {
                                              next(old_err);
                                          }
                                          else{
                                            // console.log("product old data: ",old_data);
                                              for(i = 0; i< old_data.length; i++)
                                              {
                                                  booking_product_ids_array.push(old_data[i].booking_product_id);
                                              }
                                              //console.log("boooking product ids :", booking_product_ids_array);
      
                                              ProductsModel.getProductBookPaidInvoice(booking_product_ids_array, (invoice_err, invoice_data) => {
                                                  if (invoice_err) {
                                      
                                                      next(invoice_err);
                                                      return;
                                                  }
                                                  else{
                                                      //console.log("invoice_data: ",invoice_data);
                                                      var product_booking_details= product;
                                                      //console.log("product_booking_details: ", product_booking_details);
                                                       product_booking_details.product_paid_amount = invoice_data[0].total_book_trans_paid_now;

                                                       //booking_details.paid_amount = paid_amount[0].paid_amount;

                                                       var total_product_deposit = product_booking_details.product_quantity * product_booking_details.product_full_amount;
                                                       var total_product_full_amount = product_booking_details.product_quantity * product_booking_details.product_full_amount;

                      product_booking_details.due_book_deposit = total_product_deposit - invoice_data[0].total_book_trans_paid_now;
                      if (product_booking_details.due_book_deposit < 0) {
                          product_booking_details.due_book_deposit = 0;
                      }
      
                      product_booking_details.due_book_fullamount = total_product_full_amount -  invoice_data[0].total_book_trans_paid_now;
                      if (product_booking_details.due_book_fullamount < 0) {
                          product_booking_details.due_book_fullamount = 0;
                      }

                       paid_deposit += product_booking_details.due_book_deposit;
                       paid_fullamount += product_booking_details.due_book_fullamount;
                       //console.log("product_booking_details: ",product_booking_details);
                      payment_details.push(product_booking_details);                     
    
      

                                                      product_bookings.push(product_booking_details);
                                                      callback();
                                       

                                                  }
                                              })
                                          }
                                      })
              
                                                  
                                                              
                                      
                                  },async function(err){
                                      if(err){
                                          next(err)
                                      }else{

                                          //Start new

                                          var wallet_req = {
                                            cust_id: payment_details[0].cust_id,
                                            store_id: payment_details[0].store_id
                                        }
                                        //console.log("wallet_req: ",wallet_req)
                            
                                        var cust_wallet = await TransactionModel.getCustomerStoreWalletAmountDetilas(wallet_req);
                                        //console.log("cust_wallet: ",cust_wallet)
                                        var cust_wallet_amount = cust_wallet[0].amount;
                                        // console.log("cust_wallet_amount: ",cust_wallet_amount);
                                        // console.log("paid_deposit: ",paid_deposit);
                                        // console.log("paid_fullamount: ",paid_fullamount);
                                        // console.log("prod: ",constants.PROD_FLAG);
                                        // console.log("service_bookings: ",service_bookings);
                                        // console.log("product_bookings: ",product_bookings);



                                        //paid_deposit = paid_deposit.toFixed(2);
                                        //paid_fullamount = paid_fullamount.toFixed(2);
                           
                                        var total_amount = 0;

                                       if (req.body.full_amount_flag) {
                                           total_amount = paid_fullamount;
                                       } else {
                                           total_amount = paid_deposit;
                                       }

                                       var square_pay_amount_sent = req.body.square_pay_amount;

                                       var square_pay_amount_calculated = 0;
                           
                                       if (req.body.wallet_pay_flag == 1)
                                       {
                                         square_pay_amount_calculated = total_amount - cust_wallet_amount;
                                       }
                                       else
                                       {
                                           square_pay_amount_calculated = total_amount;
                                       }
                           
                           
                                       square_pay_amount_sent = square_pay_amount_sent.toFixed(2);
                                       square_pay_amount_calculated = square_pay_amount_calculated.toFixed(2);
                                     
                                       if (square_pay_amount_sent != square_pay_amount_calculated) {
                                           res.status(400).send({status: 400, message: "Payment amount incorrect, please check if all details are correct"});
                                           return;
                                       }

                                       var square_amount = square_pay_amount_calculated * 100;

                                       SettingsModel.getSquareAccountDetails(payment_details[0].store_id, async (err, square_details) => {
                                            if (err) {
                                                next(err);
                                            }
                                            else{
                                                //console.log("square_details:",square_details);

                                                try {
                                                    //console.log("square");

                                                    var access_token = square_details[0].store_api_square_access_token;
                                                    var square_currency = square_details[0].store_api_square_currency;

                                                    //console.log("square_currency:",square_currency);
                                            
                                            
                                                    const client = new Client({
                                                        environment: constants.PROD_FLAG ? Environment.Production : Environment.Sandbox,
                                                        accessToken: access_token
                                                        
                                                      });
                                            
                                                      const idempotencyKey =  nanoid();
                                                      const square_token = req.body.square_token;
                                            
                                                    const square_response = await client.paymentsApi.createPayment({
                                                      sourceId: square_token,
                                                      idempotencyKey: idempotencyKey,
                                                      amountMoney: {
                                                        amount: square_amount,
                                                        currency: square_currency
                                                      }
                                                    });
                                                  
                                                    //console.log(square_response.result);
                                                    var square_response_result = 
                                                    {
                                                        id:square_response.result.payment.id,
                                                        orderId:square_response.result.payment.orderId,
                                                        receiptUrl:square_response.result.payment.receiptUrl,
                                                        amount: square_pay_amount_calculated,
                                                        status:square_response.result.payment.status
                                                    }
                                                    //console.log("square_response_result:",square_response_result);
                                                    //res.status(200).send({status: 200, result: {'bookings': {service_bookings: service_bookings, product_bookings: []}, 'pay_deposit': paid_deposit, 'pay_fullamount': paid_fullamount, 'stripe_intents': stripe_intents, 'cust_wallet': cust_wallet}})
                                                    res.status(200).send({status: 200, result: {'square_response': square_response_result}});
                                                  } catch(error) {
                                                    console.log("square_error:",error);
                                                    next(error);
                                                  }


                                            }
                                        })
                           
                           
                           
                                        
                                    }
                                  })
                              }
                              else
                              {

                                 //Start new

                                 var wallet_req = {
                                    cust_id: payment_details[0].cust_id,
                                    store_id: payment_details[0].store_id
                                }
                                //console.log("wallet_req: ",wallet_req)
                    
                                var cust_wallet = await TransactionModel.getCustomerStoreWalletAmountDetilas(wallet_req);
                                //console.log("cust_wallet: ",cust_wallet)
                                var cust_wallet_amount = cust_wallet[0].amount;
                                // console.log("cust_wallet_amount: ",cust_wallet_amount);
                                // console.log("paid_deposit: ",paid_deposit);
                                // console.log("paid_fullamount: ",paid_fullamount);
                                // console.log("prod: ",constants.PROD_FLAG);



                                // console.log("service_bookings: ",service_bookings);
                                //     console.log("product_bookings: ",product_bookings);



                                        // paid_deposit = paid_deposit.toFixed(2);
                                        // paid_fullamount = paid_fullamount.toFixed(2);
                           
                                        var total_amount = 0;

                                       if (req.body.full_amount_flag) {
                                           total_amount = paid_fullamount;
                                       } else {
                                           total_amount = paid_deposit;
                                       }

                                       var square_pay_amount_sent = req.body.square_pay_amount;

                                       var square_pay_amount_calculated = 0;
                           
                                       if (req.body.wallet_pay_flag == 1)
                                       {
                                         square_pay_amount_calculated = total_amount - cust_wallet_amount;
                                       }
                                       else
                                       {
                                           square_pay_amount_calculated = total_amount;
                                       }
                           
                                       //console.log("square_pay_amount_calculated:",square_pay_amount_calculated);
                                       square_pay_amount_sent = square_pay_amount_sent.toFixed(2);
                                       square_pay_amount_calculated = square_pay_amount_calculated.toFixed(2);
                                       //console.log("square_pay_amount_calculated:",square_pay_amount_calculated);

                                     
                                       if (square_pay_amount_sent != square_pay_amount_calculated) {
                                           res.status(400).send({status: 400, message: "Payment amount incorrect, please check if all details are correct"});
                                           return;
                                       }


                                       //
                                       var square_amount = square_pay_amount_calculated * 100;

                                       SettingsModel.getSquareAccountDetails(payment_details[0].store_id, async (err, square_details) => {
                                            if (err) {
                                                next(err);
                                            }
                                            else{
                                                //console.log("square_details:",square_details);

                                                try {
                                                    //console.log("square");

                                                    var access_token = square_details[0].store_api_square_access_token;
                                                    var square_currency = square_details[0].store_api_square_currency;
                                            
                                                    //console.log("square_currency:",square_currency);
                                                    const client = new Client({
                                                        environment: constants.PROD_FLAG ? Environment.Production : Environment.Sandbox,
                                                        accessToken: access_token
                                                       
                                                      });
                                            
                                                      const idempotencyKey =  nanoid();
                                                      const square_token = req.body.square_token;
                                            
                                                    const square_response = await client.paymentsApi.createPayment({
                                                      sourceId: square_token,
                                                      idempotencyKey: idempotencyKey,
                                                      amountMoney: {
                                                        amount: square_amount,
                                                        currency: square_currency
                                                      }
                                                    });
                                                  
                                                    //console.log(square_response.result);

                                                    var square_response_result = 
                                                    {
                                                        id:square_response.result.payment.id,
                                                        orderId:square_response.result.payment.orderId,
                                                        receiptUrl:square_response.result.payment.receiptUrl,
                                                        amount: square_pay_amount_calculated,
                                                        //square_response.result.payment.amountMoney,
                                                        status:square_response.result.payment.status
                                                    }
                                                    //console.log("square_response_result:",square_response_result);
                                                    //res.status(200).send({status: 200, result: {'bookings': {service_bookings: service_bookings, product_bookings: []}, 'pay_deposit': paid_deposit, 'pay_fullamount': paid_fullamount, 'stripe_intents': stripe_intents, 'cust_wallet': cust_wallet}})
                                                    res.status(200).send({status: 200, result: {square_response: square_response_result}});
                                                  } catch(error) {
                                                    console.log("square_error:",error);
                                                    next(error);
                                                  }

                                                }
                                            })


                                       //
                           
                           
                                  
                              }
                          }
          
                      }); 


                  }
              })
          }

          //////End
         
      }

  }); 

  //End NEW



 }











