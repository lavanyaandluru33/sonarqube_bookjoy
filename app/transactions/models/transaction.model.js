const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js');
exports.getCustomerStoreWalletAmountDetilas = (data) => {
    let sql = `SELECT s.store_name as store_name, ifnull(Round(SUM
                (CASE WHEN type = 1 THEN w.amount else  w.amount * -1 END),2),0) amount,s.store_id
             FROM stores_mst s
             LEFT JOIN owner_customer_wallet w ON s.store_id = w.store_id
             WHERE s.store_id = '${data.store_id}' and owner_cust_id = '${data.cust_id}' `;
    return new Promise((resolve, reject) => {
        db.query(sql, (err, res) => {
            if (err) {
                //console.log(err);
                reject({error_code: err.code, message: err.sqlMessage,sql:err.sql});
            } else
                resolve(res);
        });

    });
}


exports.getAmount = (book_ids) => {
    return new Promise((resolve, reject) => {
        db.query(`SELECT IFNULL(sum(book_trans_paid_now),0) paid_amount,min(book_trans_gateway_method) payment_type1,max(book_trans_gateway_method) payment_type2,max(book_trans_created_date) last_book_trans_created_date  FROM booking_transactions_mst b WHERE b.book_id in (${book_ids})`, function (err, result) {
            if (err) {
                reject({error_code: err.code, message: err.sqlMessage,sql:err.sql});
            } else
                resolve(result);
        });
    });
}
exports.insert_transaction_and_wallet_credit = (insert_wallet_data, insert_transaction_data) => {
//    console.log("insert_wallet_data in model",insert_wallet_data);
    let result;
    return new Promise((resolve, reject) => {
        db.beginTransaction(async function (err) {
            if (err) {
                reject({error_code: err.code, message: err.sqlMessage,sql:err.sql});
            }
            try {
                if (insert_wallet_data.length > 0) {
                    result = await insert_wallet_credit(insert_wallet_data);
                }
                if (insert_transaction_data.length > 0) {
                    result = await insert_transaction(insert_transaction_data);
                }
                db.commit(function (err) {
                    if (err) {
                        db.rollback(function () {
                            reject({error_code: err.code, message: err.sqlMessage,sql:err.sql});
                        });
                    }
                    resolve(result);
                });
            } catch (err) {
                reject(err);
            }
        });
    });
}
function insert_wallet_credit(insert_wallet_data) {
    let sql = `insert into owner_customer_wallet (owner_cust_id, owner_cust_email, type, amount, notes,
    store_id,book_id,product_book_id) values ?`;
    return new Promise((resolve, reject) => {
        db.query(sql, [insert_wallet_data], function (err, result) {
            if (err) {
                db.rollback(function () {
                    reject({error_code: err.code, message: err.sqlMessage,sql:err.sql});
                });
            } else{
                resolve(result);
            }
        });
    });
}
function insert_transaction(insert_transaction_data) {
    let sql = `INSERT INTO booking_transactions_mst( book_id, book_class_id,booking_product_id, booking_type, bookprodid, customerid, 
                book_trans_gateway_txn_id, book_trans_gateway_cust_id, book_trans_sold_price, 
                book_trans_added_tax, book_trans_total_amount, book_trans_paid_now, 
                book_trans_balance_due, book_trans_gateway_method, book_trans_confirmed, 
                book_trans_default_price, book_trans_created_by, book_trans_created_date, 
                book_trans_updated_by, book_trans_updated_date, book_trans_is_active, 
                book_trans_square_order_id, book_trans_square_receipt_url) VALUES ?`;
    return new Promise((resolve, reject) => {
        db.query(sql, [insert_transaction_data], function (err, result) {
            if (err) {
                db.rollback(function () {
                    reject({error_code: err.code, message: err.sqlMessage,sql:err.sql});
                })
            } else
                resolve(result);
        });
    });
}

exports.getOldProductBookings = (book_id) => {
    return new Promise((resolve, reject) => {
        db.query(`SELECT booking_product_id, booking_old_product_id , product_book_status FROM booking_products_mst WHERE (booking_product_id IN (${book_id}) OR booking_old_product_id IN (${book_id}))`, function (err, result) {
            if (err) {
                reject({error_code: err.code, message: err.sqlMessage,sql:err.sql});
            } else
                resolve(result);
        });
    });
}
exports.getProductAmount = (book_ids) => {
    return new Promise((resolve, reject) => {
        db.query(`SELECT IFNULL(sum(book_trans_paid_now),0) paid_amount,min(book_trans_gateway_method) payment_type1,max(book_trans_gateway_method) payment_type2,max(book_trans_created_date) last_book_trans_created_date  FROM booking_transactions_mst b WHERE b.booking_product_id in (${book_ids})`, function (err, result) {
            if (err) {
                reject({error_code: err.code, message: err.sqlMessage,sql:err.sql});
            } else
                resolve(result);
        });
    });
}
exports.getAwaitOldProductBookings = (book_id) => {

    return new Promise((resolve, reject) => {
        let sql = `SELECT booking_product_id, booking_old_product_id , product_book_status FROM booking_products_mst WHERE (booking_product_id IN (${book_id}) OR booking_old_product_id IN (${book_id}))`;
        db.query(sql, (err, res) => {
            if (err) {
                reject({error_code: err.code, message: err.sqlMessage,sql:err.sql});
            }
            resolve(res);
        });
    })
}

