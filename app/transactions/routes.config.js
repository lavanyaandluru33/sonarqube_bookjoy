const TransactionController = require('./controllers/transaction.controller');
const auth = require('../../config/auth');
exports.routesConfig = function (app) {
    // get bookings payment information
    app.post('/transactions/get_bookings_payment_amount', [
        TransactionController.getBookingsPaymentAmountDetails
    ]); 
    app.post('/transactions/get_bookings_payment_amount_v2', [
        TransactionController.getBookingsPaymentAmountDetails_v2
    ]); 
    // insert bookings payment information
    app.post('/transactions/insert_bookings_payment_amount', [
        TransactionController.insertBookingsPaymentAmountDetails
    ]);  
    // insert bookings payment information partail payment
    app.post('/transactions/insert_bookings_payment_amount_v2', [
        TransactionController.insertBookingsPaymentAmountDetailsV2
    ]);  
    
    // get customer wallet based on store
    app.post('/transactions/get_customer_store_wallet', 
        TransactionController.getCustomerStoreWalletDetilas
    ); 
    
    

    // get bookings payment information Customer Bookings
    app.post('/v2/transactions/get_payment_details_cust_bookings_v2',
        TransactionController.getPaymentDetailsCustBookings2
    ); 

    // get bookings payment information Customer Bookings
    app.post('/v2/transactions/complete_payment_transactions_cust_bookings_v2',
        TransactionController.insertPaymentDetailsCustBookings2
    );
    
    
    // app.post('/v2/transactions/square',
    //     TransactionController.squarePayments
    // );

    app.post('/v2/transactions/square_payment',
        TransactionController.completeSquarePayment
    );
}