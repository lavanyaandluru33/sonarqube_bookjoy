const StoresController = require('./controllers/stores.controller');
const auth = require('../../config/auth');
exports.routesConfig = function (app) {
  
    
    app.post('/stores/user_stores', 
        StoresController.getUserStores
    );

    app.post('/stores/get_stores_by_user', 
        StoresController.getUserStoresList
    );
    
    app.post('/v2/stores/get_store_details_v2', 
        StoresController.getStoreDetailsByUrl
    );
    
}