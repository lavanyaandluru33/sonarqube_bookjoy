const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js');

exports.getUserStores = (data, result) => {

  let sql = `SELECT store_id,store_name,store_tel,store_url_name,store_logo_image,store_pin FROM stores_mst WHERE user_id = ${data.user_id} AND store_is_active = 1 `;
    

    //console.log(sql);


      db.query(sql , (err,res) => {
        if (err) {
                //console.log("error: ", err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
     
              return  result(null, res);
        
       
       });
           
}



exports.getStoreHours = (data, result) => {
  
  let sql = `SELECT * FROM store_hours_week 
        where store_id =${data.store_id} and store_hrwk_is_active =1`;

        if(data.day_of_week || data.day_of_week == 0) 
        {
          sql += ` AND store_hrwk_day_of_week =${data.day_of_week}`;
        }
        

        if(data.user_id) 
        {
          sql += ` AND user_id = ${data.user_id}`;
        }
        
    
    //console.log(sql);

      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
     
              return  result(null, res);      
       });
           
}

exports.getUserStoresList = (data, result) => {

  let sql = `SELECT st.store_id, st.store_name, st.store_add1, st.store_add2, st.store_state, st.store_country, st.store_pin, st.store_tel, st.store_url_name, st.store_banner_image, st.store_logo_image, st.store_is_active, IFNULL(se.total_services,0) as total_services, IFNULL(se.services_min_price,0) as services_min_price FROM stores_mst as st 
  LEFT JOIN (SELECT store_id, COUNT(serv_id) as total_services, MIN(serv_saleprice) as services_min_price FROM services_mst WHERE is_break = 0  AND serv_is_active = 1 GROUP BY store_id) AS se ON (se.store_id = st.store_id) WHERE st.user_id = ${data.user_id}`;
  
    //console.log(sql);
      db.query(sql , (err,res) => {
        if (err) {
                //console.log("error: ", err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, res);      
       });
           
}



exports.getStoreDetails = (store_id, result) => {

  let sql = `SELECT s.store_id, s.store_name, s.user_id, s.store_add1, s.store_add2, s.store_city, s.store_state, s.store_country, s.store_pin, s.store_tel, s.store_url_name, s.store_banner_image, s.store_logo_image, s.store_is_active, st.store_sett_terms_and_conditions, st.store_sett_class_terms_and_conditions FROM stores_mst as s 
  LEFT JOIN store_settings st ON s.store_id = st.store_id WHERE s.store_id = ${store_id}`; 
  //AND s.user_id = ${data.user_id}`;
  
      db.query(sql , (err,res) => {
        if (err) {
                //console.log("error: ", err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, res);      
       });
           
}



exports.getStoreDetailsByUrl = (store_url_name, result) => {

  let sql = `SELECT s.store_id, s.store_name, s.user_id, s.store_add1, s.store_add2, s.store_city, s.store_state, s.store_country, s.store_pin, s.store_tel, s.store_url_name, s.store_banner_image, s.store_logo_image, s.store_is_active, st.store_sett_terms_and_conditions, st.store_sett_class_terms_and_conditions,
   tr.stores_track_id, tr.stores_track_google_analytics, tr.stores_track_facebookpixel, tr.stores_track_bingtracking, stripe.store_api_publish_key, square.store_api_square_app_id, square.store_api_square_location_id FROM stores_mst as s 
  LEFT JOIN store_settings st ON s.store_id = st.store_id LEFT JOIN stores_tracking_mst tr ON s.store_id = tr.store_id LEFT JOIN store_api_stripe_mst stripe ON s.store_id = stripe.store_id LEFT JOIN store_api_square_mst square ON s.store_id = square.store_id WHERE s.store_url_name = '${store_url_name}' ORDER BY st.store_sett_created_date DESC, tr.stores_track_created_date DESC`; 
  
  
    //console.log(sql);
      db.query(sql , (err,res) => {
        if (err) {
                //console.log("error: ", err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, res);      
       });
           
}




exports.getStoreOwner = (store_id, result) => {

  let sql = `SELECT s.store_id, s.user_id FROM stores_mst as s WHERE s.store_id = ${store_id}`;
  
  //console.log(sql);
  
  
      db.query(sql , (err,res) => {
        if (err) {
                //console.log("error: ", err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, res);      
       });
           
}




// exports.getStoreTermsAndConditions = (store_id, result) => {

//   let sql = `SELECT store_id, store_settings_id, store_sett_terms_and_conditions, store_sett_class_terms_and_conditions, store_available_days
//    FROM store_settings WHERE store_id = ${store_id}`;
  
//     console.log(sql);
//       db.query(sql , (err,res) => {
//         if (err) {
//                 //console.log("error: ", err);
//                return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
//               }
//               //console.log(res);
//               return  result(null, res);      
//        });
           
// }

exports.getStoreStripeKey = (store_id, result) => {

  let sql = `SELECT s.store_id, s.store_name, s.user_id, stripe.store_api_publish_key, stripe.store_api_stripe_key FROM stores_mst as s LEFT JOIN store_api_stripe_mst stripe ON s.store_id = stripe.store_id WHERE s.store_id = ${store_id} `; 
  
  
    //console.log(sql);
      db.query(sql , (err,res) => {
        if (err) {
                //console.log("error: ", err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, res);      
       });
           
}









