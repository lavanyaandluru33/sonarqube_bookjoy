const constants = require('../../../config/constants.js');
const StoresModel = require('../models/stores.model.js');

exports.getUserStores = (req, res, next) => {

    if(!req.body.user_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

   
    
   StoresModel.getUserStores(req.body, (err, data) => {
        if (err) {

            //res.status(500).send({message:err});
            next(err);
        }
        else{
        
            res.status(200).send({status:200, result:data});
        }
    });
   
}



exports.getUserStoresList = (req, res, next) => {

    if(!req.body.user_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    } 
    
   StoresModel.getUserStoresList(req.body, (err, data) => {
        if (err) {

            //res.status(500).send({status:500, result:{error:err}});
            next(err);
        }
        else{
        
            res.status(200).send({status:200, result:data});
        }
    });
   
}



exports.getStoreDetails = (req, res, next) => {

    if(!req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    } 
    
    var store_details= {};
    
   StoresModel.getStoreDetails(req.body.store_id, (err, store_data) => {
        if (err) {

            //res.status(500).send({status:500, result:{error:err}});
            next(err);
        }
        else{
            //console.log(store_data);

            if(store_data.length)
            {
                store_details= store_data[0];

                StoresModel.getStoreHours(req.body, (err, hrs_data) => {
                    if (err) {
            
                        //res.status(500).send({status:500, result:{error:err}});
                        next(err);
                    }
                    else{
                        store_details.store_hours = [];
                        //console.log(hrs_data);
                        hrs_data.filter((hrs_obj)=>{

                            var this_hrs = {
                                day_of_week : hrs_obj.store_hrwk_day_of_week,
                                open_time: hrs_obj.store_hrwk_open_time,
                                close_time: hrs_obj.store_hrwk_close_time,
                                offline: hrs_obj.store_hrwk_all_day_note
                            }
                            store_details.store_hours.push(this_hrs);
                            //console.log(hrs_obj.store_hrwk_id);
                        })

                        //console.log("filter ended");       
                        res.status(200).send({status:200, result:store_details});
                    }
                })
            }
            else
            {
                res.status(400).send({status:400, result:{message: "No store found with given user_id and store_id"}});
            }
        }
    });
   
}


exports.getStoreDetailsByUrl = (req, res, next) => {

    if(!req.body.store_url_name) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    } 
    
    var store_details= {};
    
   StoresModel.getStoreDetailsByUrl(req.body.store_url_name, (err, store_data) => {
        if (err) {

            //res.status(500).send({status:500, result:{error:err}});
            next(err);
        }
        else{
            //console.log(store_data);

            if(store_data.length)
            {
                store_details= store_data[0];

                var hrs_req = {
                    store_id : store_data[0].store_id
                }

                StoresModel.getStoreHours(hrs_req, (err, hrs_data) => {
                    if (err) {
            
                        //res.status(500).send({status:500, result:{error:err}});
                        next(err);
                    }
                    else{
                        store_details.store_hours = [];
                        //console.log(hrs_data);
                        hrs_data.filter((hrs_obj)=>{

                            var this_hrs = {
                                day_of_week : hrs_obj.store_hrwk_day_of_week,
                                open_time: hrs_obj.store_hrwk_open_time,
                                close_time: hrs_obj.store_hrwk_close_time,
                                offline: hrs_obj.store_hrwk_all_day_note
                            }
                            store_details.store_hours.push(this_hrs);
                            //console.log(hrs_obj.store_hrwk_id);
                        })

                        //console.log("filter ended");       
                        res.status(200).send({status:200, result:store_details});
                    }
                })
            }
            else
            {
                res.status(400).send({status:400, result:{message: "No store found with given store_url_name"}});
            }
        }
    });
   
}











