const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js');


exports.getBookings = (data, result) => {
    //const params =[];
 

   let sql = `SELECT b.book_id, b.book_oldbookid, "Services" as book_type, b.book_first_name, b.book_last_name, b.book_email, b.book_country_isd_code, b.book_mobile,
             b.book_start_date, b.book_end_date, b.book_start_time, b.book_end_time, b.book_duration, b.book_fullamount,b.book_created_date, b.book_status ,
             CASE WHEN b.booked_through = 1 THEN u.user_first_name ELSE b.book_first_name END created_by_name,
             CASE WHEN b.book_email != "" THEN (select is_vip from owner_customers_mst where cust_id = b.cust_id) END is_vip,
             CASE WHEN b.book_email != "" THEN (select has_allergy from owner_customers_mst where cust_id = b.cust_id) END has_allergy,
             b.user_id as staff_id, u.user_first_name as staff_name, 
             b.serv_id, s.serv_name, s.serv_defdur, b.book_prebuffertime serv_prebuffertime, b.book_postbuffertime serv_postbuffertime,
             b.cust_id, c.cust_first_name, c.cust_last_name, c.cust_email, c.cust_notes, c.cust_picture, b.form_url, b.form_filled
             FROM booking_mst b
             LEFT JOIN users_mst u ON u.user_id = b.user_id
             LEFT JOIN services_mst s ON s.serv_id=b.serv_id 
             LEFT JOIN customers_mst c ON c.cust_id = b.cust_id
             WHERE b.store_id = ${data.store_id} AND b.book_status != ${constants.STATUS_PRE_BOOKED}  AND b.book_start_date >= '${data.start_date}' AND b.book_end_date <= '${data.end_date}' `;
  
      //staff_id filter
      if (data.staff_id) {
        sql += ` AND b.user_id IN (${data.staff_id})`;
        
      }
  
      //book_status filter : 2 → confirm booking , 8 → reschedule booking , 10-> cancel booking
      if (data.book_status) {
        sql += ` AND b.book_status = ${data.book_status}`;
        
      }
  
  
      sql += ` ORDER BY b.book_id DESC`;
  
      //console.log(sql);
  
  
        db.query(sql , (err,res) => {
          if (err) {
                  //console.log(err);
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                 
                }
        
                return  result(null, res);
              
              
    
  
          
         
         });
             
  }
  
  
  exports.getClassBookingSchedules = (data, result) => {
  
  
    // let sql1= `SELECT cs.*, cs.class_schedule_id FROM class_master cm, class_schedule cs 
    //           WHERE cm.class_id=cs.class_id and cs.is_deleted = 0 and cm.class_is_deleted =0  AND cm.store_id = ${data.store_id}
    //           AND cs.is_online = 1 AND cs.class_date >= '${data.start_date}' AND cs.class_date <= '${data.end_date}' `;
  
   let sql = `SELECT cs.class_schedule_id, cs.class_id, cs.class_name, cs.store_id, "Classes" as book_type,
              cs.user_id as staff_id , cs.duration, cs.size, cs.price, cs.deposit, cs.class_date, cs.class_start_time, 
              cs.class_end_time, cs.booked_slots,
              u.user_first_name as staff_name
             FROM  class_master cm, class_schedule cs 
             LEFT JOIN users_mst u ON u.user_id = cs.user_id
             WHERE cm.class_id=cs.class_id and cs.is_deleted = 0 and cm.class_is_deleted =0  
             AND cm.store_id = ${data.store_id} AND cs.is_online = 1 
             AND cs.class_date >= '${data.start_date}' AND cs.class_date <= '${data.end_date}' `;
  
  
      if (data.staff_id) {
        sql += ` AND cs.user_id = ${data.staff_id}`;
        
      }
      //console.log(sql);
  
  
        db.query(sql , (err,res) => {
          if (err) {
                  //console.log(err);
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                 
                }
         
       
                return  result(null, res);
        
         });
             
  }
  
  
  exports.getClassBookings = (data, result) => {
  
  
   let sql = `SELECT bc.booking_class_id,bc.class_schedule_id , bc.store_id,
              bc.class_cust_id, bc.class_cust_first_name, bc.class_cust_last_name, bc.class_cust_email,
              bc.class_cust_mobile, bc.class_country_code, bc.total_cost, bc.deposit_paid, bc.balance_due,
              bc.class_book_status, bc.booked_through
              FROM  booking_class_mst as bc, class_schedule as cs, stores_mst as s , users_mst as u 
             WHERE bc.class_schedule_id = cs.class_schedule_id  AND bc.store_id = s.store_id AND u.user_id = bc.user_id
             AND bc.class_schedule_id = ${data.class_schedule_id} AND bc.class_book_status != ${constants.STATUS_PRE_BOOKED} AND bc.class_book_status != ${constants.STATUS_RESCHEDULED}`;

             //book_status filter : 2 → confirm booking , 8 → reschedule booking , 10-> cancel booking
       if (data.class_book_status) {
           sql += ` AND bc.class_book_status IN (${data.class_book_status})`;
       }

      sql+= ` ORDER BY bc.class_book_status`;
  
  
            // LEFT JOIN users_mst u ON u.user_id = bc.user_id    
    //$this->db->select('bc.*, case when bc.booked_through = 1 then u.user_first_name else bc.class_cust_first_name end created_by_name',false);
  
      
      //console.log(sql);
  
  
        db.query(sql , (err,res) => {
          if (err) {
                  //console.log(err);
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                 
                }
              
       
                return  result(null, res);
        
         });
             
  }


//update booking status from calendar
  exports.updateBookedServiceStatus = (data, multi_id_flag, result) => {

    let sql = `UPDATE booking_mst SET book_status = ${data.book_status}, book_updated_date = '${data.book_updated_date}', book_updated_by = ${data.book_updated_by}, updated_through = ${data.updated_through} WHERE book_id = ${data.book_id}`;
     
    if(multi_id_flag == 1)
    {
        sql += ` OR multi_service_book_id = ${data.book_id}`;
    }
  
      //console.log(sql);
  
  
        db.query(sql , (err,res) => {
          if (err) {
                  //console.log(err);
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                 
                }
                //console.log(res);
              
                return  result(null, res);
          
         
         });
             
  }  

  


  exports.addBooking = (booking_array, result) => {

    let sql = `INSERT INTO booking_mst (store_id, serv_id , serv_cat_id, user_id, user_staff_type_id , book_start_date, book_end_date, book_start_time , book_end_time,book_duration ,
               book_fullamount, book_deposit, cust_id,book_first_name, book_last_name , book_email, book_country_isd_code, book_mobile , book_status, book_staff_note, book_created_by, book_created_date, book_updated_date,
               booked_through, book_oldbookid, multi_service_book_id, updated_through, book_locationcode,book_prebuffertime,book_postbuffertime,book_discount_deposit_amount,book_discount_full_amount) VALUES ? `;

               

    // var sql_values = [];
    // for(var i=0; i< newBooking.serv_id.length; i++)
    // {
     
    //   var values_array =  [newBooking.store_id, newBooking.serv_id[i] , newBooking.serv_cat_id[i], newBooking.user_id[i] , newBooking.user_staff_type_id[i] ,
    //     newBooking.book_start_date[i], newBooking.book_end_date[i], newBooking.book_start_time[i] , newBooking.book_end_time[i],
    //     newBooking.book_duration[i] , newBooking.book_fullamount[i], newBooking.book_deposit[i], newBooking.cust_id,
    //     newBooking.book_first_name, newBooking.book_last_name ,newBooking.book_email,newBooking.book_country_isd_code,
    //    newBooking.book_mobile ,newBooking.book_status,newBooking.book_staff_note[i],newBooking.book_created_by, newBooking.book_created_date, newBooking.book_updated_date,
    //     newBooking.booked_through];
        
    //     sql_values[i]= values_array;
     
    // }

    //console.log(booking_array);

    
   
        db.query(sql , [booking_array], (err,res) => {
          if (err) {
                  //console.log(err);
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                 
                }
                //console.log(res);
                
       
                return  result(null, res);
                
          
         
          });
             
  }
  
  

exports.getServiceBookingDetails = (book_ids,have_multi_book_id,edit_flag,result) => {
        
    let sql = `SELECT b.book_discount_full_amount,b.book_discount_deposit_amount,b.book_oldbookid, b.cust_id, concat(b.book_first_name," ",b.book_last_name) as name,b.book_first_name,b.book_last_name,b.book_email,b.book_country_isd_code,b.book_mobile, b.book_id,b.cust_id,b.user_id, b.user_staff_type_id, b.book_start_date, b.book_end_date, b.book_start_time,b.book_end_time,b.book_created_date,b.book_updated_date, b.book_created_by, b.booked_through, b.book_updated_by, b.updated_through, 
    b.book_duration,b.book_deposit,b.book_fullamount, b.book_staff_note, b.book_oldbookid, b.multi_service_book_id, b.book_status, b.serv_id, b.serv_cat_id, s.serv_name, s.serv_description,s.serv_defdur,s.serv_saleprice,s.serv_deposit,s.serv_prebuffertime,s.serv_postbuffertime, b.book_prebuffertime book_prebuffertime, b.book_postbuffertime book_postbuffertime,s.serv_promo_created_date,s.serv_promo_updated_date,s.is_promo_service, u.user_first_name as staff_first_name,u.user_last_name as staff_last_name, u.user_email as staff_email, up.user_perm_created_by as owner_id,b.store_id, b.book_email, b.form_url, b.form_filled FROM booking_mst b
    LEFT JOIN services_mst s ON s.serv_id = b.serv_id LEFT JOIN users_mst u ON u.user_id = b.user_id LEFT JOIN users_permissions up ON (b.user_id = up.user_id AND b.store_id = up.store_id) WHERE`;
        

        if(have_multi_book_id == true){
          sql += ` (b.multi_service_book_id IN (${book_ids}))`;
        }
        else{
          sql += ` b.book_id IN (${book_ids})`;
        }

        if(edit_flag == 1)
        {
           sql += ` AND (b.book_status = ${constants.STATUS_BOOKED} OR b.book_status = ${constants.STATUS_ARRIVED})`
        }


        //console.log(sql);
        db.query(sql , (err,services_result) => {
            if (err) {
                return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
            }
            
                return result(null , services_result);
          
        });
}

//Booking Service Images
exports.getBookingImages = (book_id, result) => {

 let sql = `SELECT booking_service_image_id, booking_id, user_id, image_public_id, thumbnail_public_id, image_url, image_tag_id, is_active, created_by FROM booking_service_images WHERE booking_id = ${book_id} `;

      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              
              return  result(null, res);
             
       });
           
}

exports.getBookingLog = (book_ids, result) => {
 
    let sql=`SELECT b.book_id, b.serv_id, b.book_staff_note,b.updated_through, b.book_status,b.book_created_by,b.book_updated_by ,
    CASE WHEN b.book_created_by = b.cust_id THEN (SELECT concat(c.cust_first_name, " ", c.cust_last_name) from owner_customers_mst c where c.cust_id = b.book_created_by) ELSE (SELECT concat(u.user_first_name, " ", u.user_last_name) from users_mst u where u.user_id = b.book_created_by) END created_by_name,
    CASE WHEN b.book_updated_by = b.cust_id THEN (SELECT concat(c.cust_first_name, " ", c.cust_last_name) from owner_customers_mst c where c.cust_id = b.book_updated_by) ELSE (SELECT concat(u.user_first_name, " ", u.user_last_name) from users_mst u where u.user_id = b.book_updated_by) END updated_by_name,
    b.book_created_date, b.book_updated_date, b.book_oldbookid, b.multi_service_book_id, s.serv_name FROM booking_mst b 
    LEFT JOIN services_mst s ON b.serv_id = s.serv_id 
    WHERE (b.book_status !=  ${constants.STATUS_PRE_BOOKED}) AND (b.book_id IN (${book_ids}) || b.multi_service_book_id IN (${book_ids}))`;
    
    //console.log(sql);

    db.query(sql , (err,res) => {
      if (err) {
        //console.log(err)
          return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
      }
       //console.log(res);
      
          return result(null,res);
    });

}

exports.updateBookingNotes = (data, result) => {

  let sql = `UPDATE booking_mst SET book_staff_note = '${data.book_notes}' WHERE`;

  if(data.multi_service_book_id && data.multi_service_book_id != 0 && data.multi_service_book_id != "0")
  {
     sql += ` multi_service_book_id = ${data.multi_service_book_id}`;
  }
  else
  {
      sql += ` book_id = ${data.book_id}`;
  }

  //console.log(sql);

  db.query(sql , (err,res) => {
    if (err) {
      //console.log(err)
      return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);         
    }
    return  result(null, res);
  });
}



exports.getBookingsStaffwise = (data, result) => {

  let services_sql = `SELECT b.book_id as id,b.book_deposit, b.serv_cat_id, "service" as book_type, b.book_start_date as date, b.book_start_time as time, concat(b.book_start_date," ",b.book_start_time) as start, 
  concat(b.book_end_date," ",b.book_end_time) as end, b.store_id as store_id,  b.book_first_name as first_name,  
   b.book_last_name as last_name, b.book_email as email, b.book_country_isd_code as country_code, b.book_mobile as mobile,
             b.book_duration as duration, b.book_fullamount as book_fullamount, b.book_discount_full_amount,
            CASE WHEN b.booked_through = 1 THEN u.user_first_name ELSE b.book_first_name END created_by_name,
            CASE WHEN b.book_email != "" THEN (select is_vip from owner_customers_mst where cust_id = b.cust_id) END is_vip,
            CASE WHEN b.book_email != "" THEN (select has_allergy from owner_customers_mst where cust_id = b.cust_id) END has_allergy,
            b.book_status as book_status, "" as booked_slots, "" as size, b.book_staff_note as book_staff_note, b.multi_service_book_id as multi_service_book_id, b.book_oldbookid,
            b.user_id as staff_id, b.book_created_date created_date, b.book_updated_date updated_date, u.user_first_name as staff_name, 
            b.serv_id as service_code, s.serv_name as service_name, s.serv_saleprice as service_price, b.book_prebuffertime serv_prebuffertime, b.book_postbuffertime serv_postbuffertime,
            b.cust_id as cust_id, c.cust_first_name, c.cust_last_name, c.cust_email, c.cust_notes, c.cust_picture, c.cust_mobile, c.pay_fullpayment, c.is_banned, b.form_url, b.form_filled
            FROM booking_mst b
            LEFT JOIN users_mst u ON u.user_id = b.user_id
            LEFT JOIN services_mst s ON s.serv_id=b.serv_id 
            LEFT JOIN owner_customers_mst c ON c.cust_id = b.cust_id
            WHERE b.store_id = ${data.store_id} AND b.book_status != ${constants.STATUS_PRE_BOOKED} AND b.book_status != ${constants.STATUS_RESCHEDULED} AND b.book_start_date >= '${data.start_date}' AND b.book_end_date <= '${data.end_date}'`;
 
     //staff_id filter
     if (data.staff_id) {
       services_sql += ` AND b.user_id IN (${data.staff_id})`;
       
     }
 
     //book_status filter : 2 → confirm booking , 8 → reschedule booking , 10-> cancel booking
     if (data.book_status) {
       services_sql += ` AND b.book_status IN (${data.book_status})`;
       
     }

           //serv_id filter
           if (data.serv_id) {
            services_sql += ` AND b.serv_id IN (${data.serv_id})`;
            
          }
    
 
 
 
     let classes_sql = `SELECT cs.class_schedule_id as id, cs.deposit as book_deposit, 0 as serv_cat_id, "class" as book_type, cs.class_date as date, cs.class_start_time as time, concat(cs.class_date," ",cs.class_start_time) as start,concat(cs.class_date," ",cs.class_end_time) as end, 
     cs.store_id as store_id, "" as first_name, "" as last_name, "" as email, "" as country_code, "" as mobile,
     cs.duration as duration, "" as book_fullamount, "" as book_discount_full_amount,
     "" as created_by_name, "" as is_vip, "" as has_allergy,"" as book_status, cs.booked_slots as booked_slots, cs.size as size, "" as book_staff_note, "" as multi_service_book_id, "" as book_oldbookid,
     cs.user_id as staff_id, cs.class_schedule_created_date created_date,cs.class_schedule_updated_date updated_date, u.user_first_name as staff_name,
     cs.class_schedule_id as service_code, cs.class_name as service_name,cs.price as service_price, "" as serv_prebuffertime, "" as serv_postbuffertime,
     "" as cust_id, "" as cust_first_name, "" as cust_last_name, "" as cust_email, "" as cust_notes, "" as cust_picture, "" as cust_mobile, "" as pay_fullpayment, "" as is_banned, "" as form_url, "" as form_filled
     FROM class_schedule cs 
    LEFT JOIN users_mst u ON u.user_id = cs.user_id
    WHERE  cs.store_id = ${data.store_id} AND cs.is_deleted = 0 AND cs.is_online = 1 
    AND cs.class_date >= '${data.start_date}' AND cs.class_date <= '${data.end_date}'`;
 
 
       if (data.staff_id) {
       classes_sql += ` AND cs.user_id IN (${data.staff_id})`;
 
       }

        //class filter
        if (data.class_schedule_id) {
          classes_sql += ` AND cs.class_schedule_id IN (${data.class_schedule_id})`;
    
          }
   
  

       let sql = '';

       if(data.book_type == "Services")
       {
          sql= services_sql;
       }
       else if(data.book_type == "Classes")
       {
           sql= classes_sql;
       }
       else
       {
 
          sql = '('+services_sql+') UNION ALL ('+classes_sql+')';
       }
       
 
 
     //console.log(sql);
 
 
       db.query(sql , (err,res) => {
         if (err) {
                 //console.log(err);
                return result({error_code:err.code,message:err.sqlMessage,sql:err.sql}, null);
               }
               return  result(null, res);
        
        });
           
      }



exports.getCustomerServiceBookings = (data, result) => {

  let sql = `SELECT book_id,store_id,book_start_date,book_start_time,book_fullamount,book_status,"services" as book_type FROM booking_mst WHERE cust_id = ${data.cust_id} AND store_id IN (${data.stores}) AND (book_status != ${constants.STATUS_PRE_BOOKED}) ORDER BY book_start_date DESC`;
 //console.log(sql);
   
  db.query(sql , (err,res) => {
         if (err) {
                 //console.log(err);
                return result(err, null);
                
               }
               //console.log(res);
      
               return  result(null, res);
              
        });
            
 }


 exports.getOldBookings = (book_id) => {

    return new Promise((resolve, reject) => {
      db.query(`SELECT b.book_id, b.book_oldbookid ,b.book_status FROM booking_mst b WHERE (b.book_id = ${book_id} OR b.book_oldbookid = ${book_id}) ORDER BY b.book_id`, function (err, result) {
          if (err)
              reject({error_code:err.code, message:err.sqlMessage,sql:err.sql});
          else
              resolve(result);
      });
  });
  }



  exports.getBookingTransactions = (book_ids, result) => {
  
      let sql = `SELECT book_trans_id,book_id,book_trans_sold_price,book_trans_added_tax,book_trans_total_amount,book_trans_paid_now,book_trans_balance_due,book_trans_confirmed,book_trans_created_date,book_trans_updated_date, book_trans_gateway_txn_id , book_trans_gateway_method FROM booking_transactions_mst WHERE book_id IN (${book_ids}) ORDER BY book_trans_id`;
      
      //console.log(sql);

      db.query(sql , (err,res) => {
        if (err) {
          //console.log(err)
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql},null);
            
        }
        //booking_data.service_bookings.payment = services_result;
        return result(null , res);
      });
  }



  exports.editBooking = (book_id, data, result) => {

    let sql = `UPDATE booking_mst SET ? WHERE book_id = ${book_id}`;
    //console.log(data);

    
        db.query(sql , [data], (err,res) => {
          if (err) {
                  //console.log(err);
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                 
                }
                //console.log(res);
                return  result(null, { message: `Successfully updated booking`});
         
          });
             
  }
  
  exports.deleteBooking = (book_id, result) => {

    let sql = `DELETE FROM booking_mst WHERE book_id = ${book_id}`;

    
        db.query(sql , (err,res) => {
          if (err) {
                  //console.log(err);
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                 
                }
                //console.log(res);
                return  result(null, { message: `Successfully deleted booking`});
         
          });
             
  }

  exports.getCustomerServiceBookingsCount = (data, result) => {

    let sql = `SELECT COUNT(b.book_id) as total_bookings, SUM(b.book_fullamount) as total_sales , (SELECT COUNT(book_id) FROM booking_mst WHERE cust_id = ${data.cust_id} AND store_id IN (${data.stores}) AND book_status IN (${constants.STATUS_CANCELLED},${constants.STATUS_LATE_CANCELLED},${constants.STATUS_NO_SHOW})) as total_cancelled FROM booking_mst b WHERE b.cust_id = ${data.cust_id} AND b.store_id IN (${data.stores}) AND (b.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_CONFIRMED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}))`;
    
   //console.log(sql);
     
    db.query(sql , (err,res) => {
           if (err) {
                   //console.log(err);
                  return result(err, null);
                  
                 }
                 //console.log(res);
        
                 return  result(null, res[0]);
                
          });
              
   }
   

  exports.updateMultiserviceBookId = (multi_service_book_id, book_ids, multi_id_flag, result) => {

    let sql = `UPDATE booking_mst SET multi_service_book_id = ${multi_service_book_id} WHERE book_id IN (${book_ids})`;

     if(multi_id_flag == 1){
        sql += ` OR multi_service_book_id IN (${book_ids})`;
     }
    //console.log(sql);
    
        db.query(sql , (err,res) => {
          if (err) {
                  //console.log(err);
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                 
                }
                //console.log(res);
                return  result(null, { message: `Successfully updated booking`});
         
          });
             
  }

  exports.getDetailsOfBooking = (book_id,multi_id_flag, result) => {
   
    let sql = `SELECT * FROM booking_mst WHERE book_id IN (${book_id})`;

    if(multi_id_flag == 1)
    {
      sql += ` OR multi_service_book_id IN (${book_id})`;
    }

    //console.log(sql);

        
        db.query(sql , (err,res) => {
            if (err) {
                return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
            }
           
                return result(null ,res);
            
        });

    
  }



  exports.getServiceBookingInvoiceDetails = (book_id,result) => {
  

      //return new Promise((resolve, reject) => {

        let sql = `SELECT b.book_id,b.cust_id,b.user_id, b.book_start_date, b.book_start_time,b.book_end_time,b.book_created_date,b.book_duration,b.book_deposit,b.book_fullamount,b.book_discount_type,b.book_discount_value,b.book_oldbookid,b.multi_service_book_id, b.book_status, 
        b.book_created_by,b.book_updated_date, CASE WHEN b.book_created_by = b.cust_id THEN (SELECT concat(c.cust_first_name, " ", c.cust_last_name) from owner_customers_mst c where c.cust_id = b.book_created_by) ELSE (SELECT concat(u.user_first_name, " ", u.user_last_name) from users_mst u where u.user_id = b.book_created_by) END book_created_by_name,
        s.serv_name,u.user_first_name as staff_first_name,u.user_last_name as staff_last_name,b.store_id,b.book_email FROM booking_mst b
    LEFT JOIN services_mst s ON s.serv_id = b.serv_id LEFT JOIN users_mst u ON u.user_id = b.user_id WHERE (b.book_id = ${book_id} || b.multi_service_book_id = ${book_id})`;
       
      //console.log(sql);
        // db.query(sql, function (err, result) {
        //     if (err)
        //         reject({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        //     else
        //         resolve(result);
        // });
      
  
      db.query(sql , (err,res) => {
        if (err) {
          //console.log(err)
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        //console.log(res);
        
            return result(null,res);
       });
  
  }



  exports.getBookPaidInvoice = (book_ids, result) => {

    // return new Promise((resolve, reject) => {

    //   let sql = `SELECT SUM(book_trans_paid_now),SUM(book_trans_balance_due),book_trans_id,book_id,book_trans_sold_price,book_trans_added_tax,book_trans_total_amount,book_trans_paid_now,book_trans_balance_due,book_trans_confirmed,book_trans_created_date,book_trans_updated_date, book_trans_gateway_txn_id , book_trans_gateway_method FROM booking_transactions_mst WHERE book_id IN (${book_ids})`;
    // console.log(sql);
    //   db.query(sql, function (err, result) {
    //       if (err)
    //           reject({error_code:err.code, message:err.sqlMessage,sql:err.sql});
    //       else
    //           resolve(result);
    //   });
    


  
    let sql = `SELECT IFNULL(SUM(book_trans_paid_now),0) as total_book_trans_paid_now, IFNULL(MAX(book_trans_created_date),"") AS last_book_trans_created_date FROM booking_transactions_mst WHERE book_id IN (${book_ids})`;
    
    //console.log(sql);

    db.query(sql , (err,res) => {
      if (err) {
        //console.log(err)
          return result({error_code:err.code, message:err.sqlMessage,sql:err.sql},null);
          
      }
      //console.log(res);
      
      return result(null , res);
    });
}



exports.getDetailsOfBookingMultiService = (book_id) => {

  return new Promise((resolve, reject) => {
   
  let sql = `SELECT * FROM booking_mst WHERE (book_id IN (${book_id}) || multi_service_book_id IN (${book_id}))`;
  //console.log(sql);
      db.query(sql, function (err, result) {
          if (err)
              reject({error_code:err.code, message:err.sqlMessage,sql:err.sql});
          else
              resolve(result);
      }); 
 })
}

exports.RescheduleUpdate = (data, result) => {

  let sql = `UPDATE booking_mst SET book_status = ${data.book_status}, book_updated_date = '${data.book_updated_date}' , book_updated_by = ${data.book_updated_by}, updated_through = ${data.updated_through} WHERE book_id IN (${data.book_ids})`;
   

    //console.log(sql);


      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
            
              return  result(null, res);
        
       
       });
           
}  
exports.getMonthBookingsCountModel = (data,result)=>{
    let sql = `SELECT count(*) count, book_start_date FROM booking_mst WHERE 
               book_start_date >= '${data.start_date}' and book_start_date <= '${data.end_date}' and 
               store_id = ${data.store_id}`;
    //staff_id filter
    if (data.staff_id) {
      sql += ` AND b.user_id IN (${data.staff_id})`;
    }
    //book_status filter : 2 → confirm booking , 8 → reschedule booking , 10-> cancel booking
    if (data.book_status) {
      sql += ` AND book_status IN (${data.book_status})`;
      
    }
    sql += ` GROUP BY book_start_date ORDER BY book_start_date ASC`;
    db.query(sql , (err,res) => {
      if (err) {
          return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
      }
      return  result(null, res);
    });
}



exports.checkSelectedSlotAvailability = (data, result) => {

  let sql = `SELECT book_id,store_id,book_start_date,book_start_time,book_end_time,book_status FROM booking_mst WHERE store_id = ${data.store_id} AND user_id = ${data.staff_id} 
  AND book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND book_start_date = '${data.slot_date}' 
  AND ( ((book_start_time <= '${data.slot_start_time}') AND ('${data.slot_start_time}' < book_end_time)) OR ((book_start_time < '${data.slot_end_time}') AND ('${data.slot_end_time}' <= book_end_time)) OR
  (('${data.slot_start_time}' <= book_start_time) AND (book_start_time < '${data.slot_end_time}')) OR (('${data.slot_start_time}' < book_end_time) AND (book_end_time <= '${data.slot_end_time}')) )`;

  //console.log(sql);
   
  db.query(sql , (err,res) => {
         if (err) {
                 //console.log(err);
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                
               }
               //console.log(res);
      
               return  result(null, res);
              
        });

 }



 exports.getDetailsByBookId = (book_id) => {

  return new Promise((resolve, reject) => {
   
  let sql = `SELECT * FROM booking_mst WHERE book_id = ${book_id}`;
//  console.log(sql);
      db.query(sql, function (err, result) {
          if (err)
              reject({error_code:err.code, message:err.sqlMessage,sql:err.sql});
          else
              resolve(result);
      }); 
 })
}



//update booking status after transaction
exports.updateStatusOfBookings = (book_status, book_ids, result) => {

  let sql = `UPDATE booking_mst SET book_status = ${book_status} WHERE book_id IN (${book_ids})`;
   
    //console.log(sql);


      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
            
              return  result(null, res);
        
       
       });
           
}  

  
  
exports.addProduct = (product_array, result) => {

    let sql = `INSERT INTO booking_products_mst( 
                product_id,product_name,product_sku,product_barcode,product_image_url,product_thumbnail_url,
                store_id, product_cust_id, product_cust_first_name, 
                product_cust_last_name, product_cust_email, product_cust_mobile, 
                product_cust_country_code, product_date, product_quantity, product_full_amount, 
                product_book_status, product_book_notes, created_date, created_by, 
                updated_date, updated_by, booked_through, updated_through, 
                multi_service_book_id, multi_service_booking_type, booking_old_product_id) VALUES ? `;
   
        db.query(sql , [product_array], (err,res) => {
            if (err) {
                  //console.log(err);
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                 
            }
            return  result(null, res);
        });
             
  }
  
 
exports.getProductDetails = (book_ids,edit_flag,result) => {
        
    let sql = `SELECT p.*, pm.product_quantity as product_remaining_quantity, pm.product_price as product_compare_price from booking_products_mst p LEFT JOIN products_mst pm ON p.product_id = pm.product_id WHERE p.multi_service_book_id IN (${book_ids})`;
        
        if(edit_flag == 1)
        {
           sql += ` AND (p.product_book_status = ${constants.STATUS_BOOKED} OR p.product_book_status = ${constants.STATUS_ARRIVED})`
        }


        //console.log(sql);
        db.query(sql , (err,products_result) => {
            if (err) {
                return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
            }
            
                return result(null , products_result);
          
        });
}
exports.updatePriceModel=(id,price,method,result)=>{
    let sql;
    switch(method){
        case constants.DEPOSIT_DISCOUNT_PRICE:
            sql = `UPDATE booking_mst SET book_discount_deposit_amount = ${price} WHERE book_id = ${id}`;
            break;
        case constants.FULL_DISCOUNT_PRICE:
            sql = `UPDATE booking_mst SET book_discount_full_amount = ${price} WHERE book_id = ${id}`;
            break;
    }
    
        //console.log(sql);
    db.query(sql , (err,sresult) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }

        return result(null , sresult);

    });
}

exports.updateBookingFormUrl = (form_url, book_ids, result) => {

  let sql = `UPDATE booking_mst SET form_url = '${form_url}' WHERE book_id IN (${book_ids})`;

  //console.log(sql);
  
      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, { message: `Successfully updated booking`});
       
        });
           
}


exports.updateFilledFormUrl = (form_pdf_url, book_id, result) => {

  let sql = `UPDATE booking_mst SET form_url = '${form_pdf_url}' , form_filled = 1 WHERE (book_id = ${book_id} OR multi_service_book_id = ${book_id}) `;

  //console.log(sql);
  
      db.query(sql , (err,res) => {
        if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
              }
              //console.log(res);
              return  result(null, res);
       
        });
           
}

exports.updateTransactionBookingNotes = (book_notes_array, result) => {
  let book_ids = [];
  let sql = `update booking_mst set book_staff_note = (case book_id `;
  for(let i = 0; i < book_notes_array.length; i++){
      console.log(book_notes_array[i]);
      sql += ` when '${book_notes_array[i][0]}' then '${book_notes_array[i][1]}'`;
      book_ids.push(book_notes_array[i][0]);
  }
  sql+= ` end) where book_id in (${book_ids})`;
//  console.log(sql);
 
      db.query(sql , (err,res) => {
          if (err) {
                //console.log(err);
               return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
               
          }
          return  result(null, res);
        });

    }
      
