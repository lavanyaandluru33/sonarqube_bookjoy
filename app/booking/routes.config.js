const BookingController = require('./controllers/booking.controller');
const auth = require('../../config/auth');
exports.routesConfig = function (app) {
    app.post('/get_booking_details', [
        BookingController.getBookingDetails
    ]);

    //Get Calendar bookings
    app.post('/booking/list', 
    BookingController.getBookings
    );

    //Add Booking
    app.post('/booking/insert', 
      BookingController.addBooking
    );
    
     //Update Booking Status (1-Arrived , 2-Completed, 3-Cancelled, 4-No Show)
    app.post('/booking/book_service_status_update', 
     BookingController.updateBookedServiceStatus   
    );
    
    //Get Calendar Staffwise bookings
    app.post('/booking/staffwise', 
    BookingController.getBookingsStaffwise
    );
    

    //Get Booking Log
    app.post('/booking_log', 
    BookingController.getBookingLog
    )
    //update booking notes
    app.post('/booking/update_booking_notes', 
    BookingController.updateBookingNotes
    );


    //get invoice
    app.post('/booking/get_invoice', 
    BookingController.getBookingInvoice
    );

    //get invoice
    app.post('/booking/get_month_bookings_count', 
    BookingController.getMonthBookingsCount
    );

    app.post('/v2/booking/get_cust_booking_details_v2',
      BookingController.getCustomerBookingDetails
    );
    
    app.post('/v2/booking/get_cust_each_booking_details_v2',
      BookingController.getCustomerEachBookingDetails
    );


    //Add Booking
    app.post('/v2/booking/add_bookings_v2', 
      BookingController.addCustomerBookings
    );
    
    // update prices
    app.post('/v2/booking/update_prices_v2',
        BookingController.updatePrices
    )

     // update filled form url
    app.post('/v2/booking/update_form_pdf_url_v2',
       BookingController.updateFilledFormUrl
    )

     // send form url
    app.post('/v2/booking/send_form_v2',
       BookingController.sendFormUrl
    )
    

  
}