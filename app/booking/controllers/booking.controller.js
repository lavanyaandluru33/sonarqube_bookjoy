const constants = require('../../../config/constants.js');
const BookingModel = require('../models/booking.model');
const StaffModel = require('../../staff/models/staff.model.js');
const CustomerModel = require('../../customer/models/customer.model');
const TransactionModel = require('../../transactions/models/transaction.model');
const ServicesModel = require('../../services/models/services.model.js');
const UserModel = require('../../user/models/user.model.js');
const ClassesModel = require('../../classes/models/classes.model');
const StoresModel = require('../../stores/models/stores.model.js');
const ProductsModel = require('../../products/models/products.model.js');
const NotificationsModel = require('../../notifications/models/notifications.model.js');

const NotificationsLibrary = require('../../../libs/notifications.js');

const ErrorLogs = require('../../../libs/error_logs.js');

var async = require("async");
var moment = require('moment'); 
var moment_tz = require('moment-timezone');

//Get Bookings for Calendar
exports.getBookings = (req, res, next) => {

    if(!req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var today_date = new Date(); 
    var today = today_date.toISOString().slice(0,10);

    if(req.body.start_date && req.body.end_date && (req.body.start_date > req.body.end_date))
    {
      //given start date > end date, then return error
      return res.status(400).send({message: 'Invalid request parameters. Given end_date must be greater than start_date.'});
      
    }
    else if(!req.body.start_date && !req.body.end_date)
    {

      //if dates are not given , then start date = end date = today
      req.body.start_date = req.body.end_date = today;
     
    }
    else if(req.body.start_date && !req.body.end_date)  
    {
      //only start date is given

        if(req.body.start_date <= today)
        {
           // start date <= today , then end date = today
            req.body.end_date = today;
        }
        else
        {
            //start date > today, then end date = start date + 1 day
            var start = new Date(req.body.start_date);
            start.setDate(start.getDate() + 1);
            req.body.end_date = start.toISOString().slice(0,10);
        }
     
    }
    else if(req.body.end_date && !req.body.start_date)
    {
        //only end date is given

        if(req.body.end_date >= today)
        {
          // end date >= today, then start date = today
            req.body.start_date = today;
        }
        else
        {
            // end date < today , then send error
            return res.status(400).send({message: 'Invalid request parameters. End date must be greater than start date.'});
           
        }
    
       
    }
    
   var service_bookings = [];
   //var class_bookings = [];

 BookingModel.getBookings(req.body, (err, data1) => {
    if (err) {


        //res.status(500).send({status:500, result:{error:err}});
        next(err);
            
       
    }
    else{

        service_bookings = data1;

                async.each(service_bookings, function (booking_obj, callback) {
 
                    booking_obj.booking_service_images = [];
                        
                // var service_bookings_obj = {
                //           id           : booking_obj.book_id,
                //           booking_type : "service_booking",
                //           staff_id     : booking_obj.staff_id,
                //         start_date : booking_obj.book_start_date,
                //         end_date : booking_obj.book_end_date,
                //         start_time : booking_obj.book_start_time,
                //         end_time : booking_obj.book_end_time,
                //         store_id: booking_obj.store_id,
                //         service_code: booking_obj.serv_id,
                //         service_name: booking_obj.serv_name,
                //         service_defdur: booking_obj.serv_defdur,
                //         service_prebuff : booking_obj.serv_prebuffertime,
                //         service_postbuff : booking_obj.serv_postbuffertime,
                //         service_price : price,
                //         b_cid : booking_obj.cust_id,
                //         book_first_name : booking_obj.book_first_name,
                //         book_last_name : booking_obj.book_last_name,
                //         email : booking_obj.book_email,
                //          country_code :booking_obj.book_country_isd_code, 
                //         mobile :booking_obj.book_mobile, 
                //         staff_note :booking_obj.book_staff_note,
                //         book_created :booking_obj.created_by_name
                // }
                        
                        
                        BookingModel.getBookingImages(booking_obj.book_id, (err, images) => {
                                if (err) {

                                    return err;
                                }
                                else{
                                    images = JSON.parse(JSON.stringify(images));
                                    booking_obj.booking_service_images = images;
                                    callback();
                                }
                        }) 
                })

       

        //bookings.push(service_bookings);
    
    
        //class_bookings = 
        BookingModel.getClassBookingSchedules(req.body, (err, class_schedules) => {
                if (err) {

                    //res.status(500).send({status:500, result:{error:err}});
            
                    
                    next(err);

                }
                else{

                    //var class_schedules = JSON.parse(JSON.stringify(data));

                    async.each(class_schedules, function (schedule_obj, callback) {

                    schedule_obj.customer_details = [];
                        
                        BookingModel.getClassBookings(schedule_obj.class_schedule_id, (err, class_bookings) => {
                                if (err) {

                                    return err;
                                }
                                else{
         
                                    class_bookings = JSON.parse(JSON.stringify(class_bookings));
                                    // console.log("schedule");
                                    // console.log(class_bookings); 
                                    
                                    // console.log("customer details");
                                    schedule_obj.customer_details = class_bookings;
                                    callback();
                                }
                            }) 
                            


                    }, function (err) {
                    
                    //console.log("result");
                    //console.log(class_schedules);
                    //res.status(200).send({status:200, result:class_schedules});
                    //return class_schedules;
                    //bookings.push(class_schedules);
                    //console.log("classes added");
                    //console.log(bookings);

                        if(req.body.book_type == "Services")
                        {
                            res.status(200).send({status:200, service_bookings:service_bookings});
                        }
                        else if(req.body.book_type == "Classes")
                        {
                            res.status(200).send({status:200, class_bookings:class_schedules});
                        }
                        else
                        {
                            res.status(200).send({status:200, service_bookings:service_bookings, class_bookings: class_schedules});
                        }
                    });
                    
                    
                }
            
            });
       }
    });
}


//Add Booking
exports.addBooking = async (req, res, next) => {
    let buffer_times;

    // Validate request
    if(!Object.keys(req.body).length) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }
    // ​


    var book_created_date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
    var book_updated_date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
    
    switch(req.body.booking_type)
    {
        case constants.ADD_BREAK_BOOKING:

            ServicesModel.getBreakService(req.body.store_id, (err, service) => {
                if (err) {
        
                    //res.status(500).send({status:500, result:{error:err}});
                    next(err);
                }
                else{
                    //console.log(service);

                    CustomerModel.getBreakCustomer(req.body.user_id, (err, cust) => {
                        if (err) {
                
                            //res.status(500).send({status:500, result:{error:err}});
                            next(err);
        
                        }
                        else{
                            //console.log(cust);
                            UserModel.getUserStaffType(req.body.user_id, req.body.store_id, (err, staff_type) => {
                                if (err) {
                        
                                    //res.status(500).send({status:500, result:{error:err}});
                                    next(err);
                
                                }
                                else{

                        var break_booking =  [ [req.body.store_id, service[0].serv_id , service[0].serv_cat_id, req.body.user_id , staff_type[0].user_perm_staff_type ,
                            req.body.book_start_date, req.body.book_end_date, req.body.book_start_time , req.body.book_end_time,
                            req.body.book_duration , "0", "0", cust[0].cust_id, cust[0].cust_first_name, cust[0].cust_last_name ,cust[0].cust_email,cust[0].cust_country_isd_code,
                            cust[0].cust_mobile , constants.STATUS_BOOKED, req.body.book_staff_note,req.body.book_created_by, book_created_date, book_updated_date,
                            req.body.booked_through,"0","0","0","0","0","0","-1","-1"] ];
                           // console.log(break_booking);

                        //Insert break
                            BookingModel.addBooking(break_booking, (err, data) => {
                                if (err) {

                                  
                                    //res.status(500).send({status:500, result:{error:err}});
                                    next(err);
                
                                }
                                else{
                                
                                    res.status(200).send({status:200, message:"Successfully added Break"});
                                }
                            })       
                        }
                })
                }
            });
            }
        })
    break;

    case constants.ADD_SERVICE_BOOKING:
        //console.log("add service");
        if(req.body.save_unpaid_status == 1)
        {
             var book_status = constants.STATUS_BOOKED;
        }
        else
        {
             var book_status = constants.STATUS_PRE_BOOKED;
        }

        var services = req.body.service_details;
        var booking_array = [];

        for(let i=0; i<services.length; i++)
        {

            if((!services[i].serv_prebuffertime && services[i].serv_prebuffertime!=0)  || (!services[i].serv_postbuffertime && services[i].serv_postbuffertime!=0)){
                try{
                    buffer_times = await ServicesModel.getServiceDetailsModel(services[i].serv_id);
                    buffer_times = buffer_times[0];
                }catch(err){
                    res.send(err);
                }
            }else{
                buffer_times =
                    {
                        serv_prebuffertime:parseInt(services[i].serv_prebuffertime),
                        serv_postbuffertime:parseInt(services[i].serv_postbuffertime)
                    };
            }
            var this_booking =  [req.body.store_id, services[i].serv_id , services[i].serv_cat_id, services[i].user_id , services[i].user_staff_type_id ,
                services[i].book_start_date, services[i].book_end_date, services[i].book_start_time , services[i].book_end_time,
                services[i].book_duration , services[i].book_fullamount, services[i].book_deposit, req.body.cust_id,
                req.body.book_first_name, req.body.book_last_name ,req.body.book_email,req.body.book_country_isd_code,
                req.body.book_mobile , book_status,services[i].book_staff_note,req.body.book_created_by, book_created_date, book_updated_date,
                req.body.booked_through,"0","0","0","0",buffer_times.serv_prebuffertime,buffer_times.serv_postbuffertime,"-1","-1"];
                

            booking_array.push(this_booking);
            //console.log(i);
        }

        //console.log(booking_array);
  
      //Insert booking in db
        BookingModel.addBooking(booking_array, (err, data) => {
            if (err) {
              //res.status(500).send({status:500, result:{error:err}});
              next(err);

            }
            else{

                //console.log(data);

                
                    // var ids_array = [];
                    // var return_ids_obj = [];
                    // var first_id = data.insertId;

                    NotificationsModel.getNotificationSettings(req.body.store_id, (notif_sett_err, notif_sett_data) => {
                        if (notif_sett_err) {
                          
                          next(notif_sett_err);
            
                        }
                        else{
            
                            //console.log("notif_sett_data: ", notif_sett_data);
                            //console.log("data: ", data);
                            var form_url_exist = 0;
                            var form_url = '';

                            

                            var ids_array = [];
                            var return_ids_obj = [];
                            var first_id = data.insertId;

                            if(notif_sett_data[0].form_id !== 'none')
                            {
                                form_url_exist = 1;
                                form_url = notif_sett_data[0].form_url + "/" + first_id ;

                            }
                            //console.log("form_url: ",form_url);

                        
                   
                    for(let i=0; i<data.affectedRows; i++)
                    {
                        var id = data.insertId + i;
                        ids_array.push(id);
//                        return_ids_obj.push({book_id:id, book_deposit:services[i].book_deposit, book_fullamount:services[i].book_fullamount, type:constants.SERVICE_TYPE});
                    }
                    //console.log("ids_array: ",ids_array);

                    if(req.body.product_details){
                        if(req.body.product_details.length > 0 ){
                            let cust_details = {
                                store_id:req.body.store_id,
                                cust_id : req.body.cust_id,
                                book_first_name : req.body.book_first_name, 
                                book_last_name : req.body.book_last_name,
                                book_email : req.body.book_email,
                                book_country_isd_code : req.body.book_country_isd_code,
                                book_mobile : req.body.book_mobile, 
                                book_status : book_status,
                                book_created_by: req.body.book_created_by,
                                book_created_date: book_created_date, 
                                book_updated_date: book_updated_date,
                                booked_through: req.body.booked_through
                            };

                            insertProducts(req.body.product_details,first_id,cust_details);                            
                        }
                    }
                    var notif_details = {}
                                    notif_details.book_id = first_id;
                                    notif_details.cust_id = req.body.cust_id;
                                    notif_details.cust_name = req.body.book_first_name+" "+req.body.book_last_name;
                                    notif_details.cust_email =req.body.book_email;
                                    notif_details.cust_mobile = req.body.book_country_isd_code.toString()+req.body.book_mobile.toString();
                                    notif_details.store_id = req.body.store_id;
                                    
                                    // notif_details.service_id = services[0].serv_id;
                                    // notif_details.staff_id = services[0].user_id;
                                    // notif_details.date = services[0].book_start_date+" "+services[0].book_start_time;
                                    // notif_details.duration = services[0].book_duration;
                                    notif_details.this_base_url = /*req.protocol*/ "https://" + req.hostname; // until fix the protocal issue we directly use https
                                    notif_details.email_base_url = req.hostname;
                                    //notif_details.service_details = services;
                                    notif_details.book_created_by = req.body.book_created_by;
                        if (req.body.hasOwnProperty("email_send")) {
                            if(req.body.email_send){
                                notif_details.email_send = true;
                            }else{
                                notif_details.email_send = false;
                            }                            
                        }else{
                            notif_details.email_send = true;
                        }
                        if (req.body.hasOwnProperty("sms_send")) {
                            if(req.body.sms_send){
                                notif_details.sms_send = true;
                            }else{
                                notif_details.sms_send = false;
                            }                            
                        }else{
                            notif_details.sms_send = true;
                        }
                                   


                if(data.affectedRows>1)
                {

                    BookingModel.updateMultiserviceBookId(first_id, ids_array, "0", (err2,data2) => {
                        if (err2) {
            
                          //res.status(500).send({status:500, result:{error:err2}});
                          next(err2);
      
                        }
                        else{
                            //console.log(data2);
                           return_ids_obj.push({book_id:first_id,multi_book_id:first_id});

                                   if(constants.PROD_FLAG == 1)
                                   {
                                        notif_details.have_multi_id = 1;

                                        NotificationsLibrary.notification_details(notif_details, function(notif_err,notif_data){
                                            if (notif_err) {
                                
                                                console.log("error from notifications");
                                                console.log(notif_err)
                                                ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                    console.log("Slack alert sent");
                                                })
                                                .catch((slack_err) => {
                                                    console.log(slack_err);
                                                });
                                                
                        
                                            }
                                            else{
                                                console.log("Notifications success");
            
                                            }
                                        })
                                   }

                                   //console.log("form_url2: ",form_url);
                                   //console.log("form_url_exist 2: ",form_url_exist);

                                   
                            if(form_url_exist == 1)
                            {

                                BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                    if (form_err) {
                        
                                        next(form_err);
                                    }
                                    else{
                                        //console.log(form_data);
                                        res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully inserted ${data.affectedRows} bookings`}});
                                    }
                                })
                            }
                            else
                            {
                                
                                    
                                    res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully inserted ${data.affectedRows} bookings`}});
                            }

                        }
                    })
                }
                else
                {
                    return_ids_obj.push({book_id:first_id,multi_book_id:0});
                    // console.log("constants.PROD_FLAG ");
                    // console.log(constants.PROD_FLAG);
                    if(constants.PROD_FLAG == 1)
                    {
                        //console.log(constants.PROD_FLAG);
                        notif_details.have_multi_id = 0;
                        
                       // NotificationsLibrary.notification_details(notif_details);
                           NotificationsLibrary.notification_details(notif_details, function(notif_err,notif_data){
                                if (notif_err) {
                    
                                    console.log("error from notifications");
                                    console.log(notif_err)
                                    ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                        console.log("Slack alert sent");
                                    })
                                    .catch((slack_err) => {
                                        console.log(slack_err);
                                    });
                                    
            
                                }
                                else{
                                    console.log("Notifications success");

                                }
                            })
                    }

                    //console.log("form_url2: ",form_url);
                    //console.log("form_url_exist 2: ",form_url_exist);
                    if(form_url_exist == 1)
                    {

                        BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                        if (form_err) {
            
                            next(form_err);
                        }
                        else{
                            //console.log(form_data);

                            res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully inserted booking`}});
                        }
                })
            }
            else{
                res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully inserted booking`}});

            }
    }

            }
        })

            }
        

       });
    break;


    case constants.EDIT_BREAK_BOOKING:

                CustomerModel.getBreakCustomer(req.body.user_id, (err, cust) => {
                    if (err) {
            
                      
                        //res.status(500).send({status:500, result:{error:err}});
                        next(err);
                    }
                    else{
                        //console.log(cust);
                        UserModel.getUserStaffType(req.body.user_id, req.body.store_id, (err, staff_type) => {
                            if (err) {
                    
                             
                                //res.status(500).send({status:500, result:{error:err}});
                                next(err);
                            }
                            else{
                                //console.log(staff_type[0].user_perm_staff_type);
        

                        const break_obj = {
                            //store_id : req.body.store_id,
                            //serv_id : [service[0].serv_id],
                            //serv_cat_id : [service[0].serv_cat_id],
                            user_id : req.body.user_id,
                            user_staff_type_id : staff_type[0].user_perm_staff_type,
                            book_start_date : req.body.book_start_date,
                            book_end_date : req.body.book_end_date,
                            book_start_time : req.body.book_start_time,
                            book_end_time : req.body.book_end_time,
                            book_duration : req.body.book_duration,
                            cust_id : cust[0].cust_id,
                            book_first_name : cust[0].cust_first_name,
                            book_last_name : cust[0].cust_last_name,
                            book_email : cust[0].cust_email,
                            book_country_isd_code : cust[0].cust_country_isd_code,
                            book_mobile : cust[0].cust_mobile,
                            book_staff_note : req.body.book_staff_note,
                            book_updated_by : req.body.book_updated_by,
                            book_updated_date : new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                            updated_through : req.body.updated_through
                        };
                    //Update break
                        BookingModel.editBooking(req.body.book_id, break_obj, (err, data) => {
                            if (err) {

                             
                                //res.status(500).send({status:500, result:{error:err}});
                                next(err);
                            }
                            else{
                            
                                res.status(200).send({status:200, message:"Successfully updated Break"});
                            }
                        })       
                    }
               })
            }
         });
    break;

    case constants.DELETE_BREAK_BOOKING:
                    
            BookingModel.deleteBooking(req.body.book_id, (err, data) => {
                if (err) {

                   
                    //res.status(500).send({status:500, result:{error:err}});
                    next(err);
                }
                else{
                
                    res.status(200).send({status:200, message:"Successfully deleted Break"});
                }
            });
    break;
 
    // ---------------------------- reschedule booking ----------------------
    case constants.RESCHEDULE_SERVICE_BOOKING:

        var services = req.body.service_details;
        var products = [];
        if(req.body.product_details)
            products = req.body.product_details;
        var booking_array = [];
        var rescheduling_bookings = 0;

        var notif_details = {}
        notif_details.book_id = '';
        notif_details.book_status_message = ''
        notif_details.cust_id = req.body.cust_id;
        notif_details.cust_name = req.body.book_first_name+" "+req.body.book_last_name;
        notif_details.cust_email =req.body.book_email;
        notif_details.cust_mobile = req.body.book_country_isd_code.toString()+req.body.book_mobile.toString();
        notif_details.store_id = req.body.store_id;
                                    
                                    // notif_details.service_id = services[0].serv_id;
                                    // notif_details.staff_id = services[0].user_id;
                                    // notif_details.date = services[0].book_start_date+" "+services[0].book_start_time;
                                    // notif_details.duration = services[0].book_duration;
        notif_details.this_base_url = /*req.protocol*/  "https://" + req.hostname; // until fix the protocal issue we directly use https
        notif_details.email_base_url = req.hostname;
        //notif_details.service_details = services;
        notif_details.book_created_by = req.body.book_created_by;
        if (req.body.hasOwnProperty("email_send")) {
            if(req.body.email_send){
                notif_details.email_send = true;
            }else{
                notif_details.email_send = false;
            }                            
        }else{
            notif_details.email_send = true;
        }
        if (req.body.hasOwnProperty("sms_send")) {
            if(req.body.sms_send){
                notif_details.sms_send = true;
            }else{
                notif_details.sms_send = false;
            }                            
        }else{
            notif_details.sms_send = true;
        }
        for(let i=0; i<services.length; i++)
        { 
             if((!services[i].serv_prebuffertime && services[i].serv_prebuffertime!=0)  || (!services[i].serv_postbuffertime && services[i].serv_postbuffertime!=0)){
                try{
                    buffer_times = await ServicesModel.getServiceDetailsModel(services[i].serv_id);
                    buffer_times = buffer_times[0];
                }catch(err){
                    res.send(err);
                }
            }else{
                buffer_times =
                    {
                        serv_prebuffertime:parseInt(services[i].serv_prebuffertime),
                        serv_postbuffertime:parseInt(services[i].serv_postbuffertime)
                    };
            }
            if(services[i].book_id)
            {
                rescheduling_bookings = 1;
                if(services[i].multi_service_book_id != 0)
                {
                   var this_book_id = services[i].multi_service_book_id;
                }
                else
                {
                    var this_book_id = services[i].book_id;
                }

               
               try{

                  var booking_details = await BookingModel.getDetailsOfBookingMultiService(this_book_id);
               }
               catch(error){
                   console.log(error);
                   var reschedule_error = error;
                   break;
               }
                        //console.log(booking_details);
                        //console.log(services[i].book_id);

                        var book_ids_update = [];
                        var update_multi_ids = [];

                        for(let j=0; j<booking_details.length; j++)
                        {
                            //console.log(booking_details[j].book_id);
                            //console.log(services[i].book_id);
                            
                            if(booking_details[j].book_oldbookid != 0)
                            {
                                var old_book_id = booking_details[j].book_oldbookid;
                            }
                            else
                            {
                                var old_book_id = booking_details[j].book_id;
                            }
                            
                            if(booking_details[j].book_id == services[i].book_id)
                            {
                               //rescheduling bookings 
                               if((!services[i].book_discount_deposit_amount&&services[i].book_discount_deposit_amount!=0)||(!services[i].book_discount_full_amount&&services[i].book_discount_full_amount!=0)){
                                   buffer_times.book_discount_deposit_amount = -1;
                                   buffer_times.book_discount_full_amount = -1;
                               }else{
                                   buffer_times.book_discount_deposit_amount = services[i].book_discount_deposit_amount;
                                   buffer_times.book_discount_full_amount = services[i].book_discount_full_amount;
                               }

                                var this_booking =  [booking_details[j].store_id, services[i].serv_id , services[i].serv_cat_id, services[i].user_id , services[i].user_staff_type_id ,
                                services[i].book_start_date, services[i].book_end_date, services[i].book_start_time , services[i].book_end_time,
                                services[i].book_duration , services[i].book_fullamount, services[i].book_deposit, booking_details[j].cust_id,
                                booking_details[j].book_first_name, booking_details[j].book_last_name ,booking_details[j].book_email,booking_details[j].book_country_isd_code,
                                booking_details[j].book_mobile , constants.STATUS_BOOKED,services[i].book_staff_note,req.body.book_created_by, book_created_date, book_updated_date,
                                req.body.booked_through,old_book_id,"0", "0", "0",buffer_times.serv_prebuffertime,buffer_times.serv_postbuffertime,buffer_times.book_discount_deposit_amount,buffer_times.book_discount_full_amount];

                                booking_array.push(this_booking);
                                book_ids_update.push(booking_details[j].book_id);
                                update_multi_ids.push(booking_details[j].book_id);

                            }
                            else
                            {  
                                //other non-rescheduled bookings in the multi-services
                                update_multi_ids.push(booking_details[j].book_id);
                            }

                            //console.log("j-"+j);       
                        }

                        // console.log(booking_array);
                        // console.log("rescheduling-"); console.log(book_ids_update); console.log(update_multi_ids);
            }
            else
            {
              //new booking
             
//                console.log("new_booking");
                var this_booking =  [req.body.store_id, services[i].serv_id , services[i].serv_cat_id, services[i].user_id , services[i].user_staff_type_id ,
                services[i].book_start_date, services[i].book_end_date, services[i].book_start_time , services[i].book_end_time,
                services[i].book_duration , services[i].book_fullamount, services[i].book_deposit, req.body.cust_id,
                req.body.book_first_name, req.body.book_last_name ,req.body.book_email,req.body.book_country_isd_code,
                req.body.book_mobile , constants.STATUS_BOOKED,services[i].book_staff_note,req.body.book_created_by, book_created_date, book_updated_date,
                req.body.booked_through,"0","0","0","0",buffer_times.serv_prebuffertime,buffer_times.serv_postbuffertime,"-1","-1"];
                
                booking_array.push(this_booking); 
            }

            //console.log("i"+i);
        }
        //console.log(booking_array);
        //console.log(this_book_id);

        if(reschedule_error)
        {
           
            //res.status(500).send({status:500, result:{error:reschedule_error}});
            next(reschedule_error);
        }
        else
            {
                //Insert bookings in db
                BookingModel.addBooking(booking_array, (err, inserted_data) => {
                    if (err) {

                        //res.status(500).send({status:500, result:{error:err}});
                        next(err);

                    } else {
                        //console.log(inserted_data);


                        NotificationsModel.getNotificationSettings(req.body.store_id, (notif_sett_err, notif_sett_data) => {
                            if (notif_sett_err) {
                              
                              next(notif_sett_err);
                
                            }
                            else{
                
                                //console.log("notif_sett_data: ", notif_sett_data);
                                //console.log("data: ", inserted_data);
                                var form_url_exist = 0;
                                var form_url = '';
    

                                var ids_array = [];
                                var return_ids_obj = [];
                                var first_id = inserted_data.insertId;
    
                                if(notif_sett_data[0].form_id !== 'none')
                                {
                                    form_url_exist = 1;
                                    form_url = notif_sett_data[0].form_url + "/" + first_id ;
    
                                }
                                //console.log("form_url: ",form_url);
    

                        for (i = 0; i < inserted_data.affectedRows; i++)
                        {
                            var id = inserted_data.insertId + i;
                            ids_array.push(id);
//                        return_ids_obj.push({book_id:id, book_deposit:services[i].book_deposit, book_fullamount:services[i].book_fullamount, type:constants.SERVICE_TYPE});
                        }

                        if (rescheduling_bookings == 0)
                        {
                            //only added new bookings

                            // console.log(ids_array);
                            // console.log(first_id);

                            if (inserted_data.affectedRows > 1)
                            {
                                //if new services are added along with rescheduled, update multi_service_book_id
                                BookingModel.updateMultiserviceBookId(first_id, ids_array, "0", (err4, data4) => {
                                    if (err4) {

                                        //res.status(500).send({status:500, result:{error:err4}});
                                        next(err4);
                                    } else {
                                        //console.log(data4);
                                        return_ids_obj.push({book_id: first_id, multi_book_id: first_id});
                                        if (constants.PROD_FLAG == 1)
                                        {
                                            notif_details.have_multi_id = 1;
                                            notif_details.book_id = first_id;

                                            NotificationsLibrary.notification_details(notif_details, function (notif_err, notif_data) {
                                                if (notif_err) {

                                                    console.log("error from notifications");
                                                    console.log(notif_err)
                                                    ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                        console.log("Slack alert sent");
                                                    })
                                                            .catch((slack_err) => {
                                                                console.log(slack_err);
                                                            });


                                                } else {
                                                    console.log("Notifications success");

                                                }
                                            })
                                        }

                                        if(form_url_exist == 1)
                                        {

                                                BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                if (form_err) {
                                    
                                                    next(form_err);
                                                }
                                                else{
                                                    //console.log(form_data);

                                                    res.status(200).send({status: 200, result: {bookings: return_ids_obj, message: `Successfully rescheduled bookings`}});

                                                }
                                            })
                                        }
                                        else
                                        {
                                            res.status(200).send({status: 200, result: {bookings: return_ids_obj, message: `Successfully rescheduled bookings`}});
                                        }
                                    }
                                })

                            } else
                            {
                                return_ids_obj.push({book_id: first_id, multi_book_id: 0});
                                if (constants.PROD_FLAG == 1)
                                {
                                    notif_details.have_multi_id = 0;
                                    notif_details.book_id = first_id;

                                    NotificationsLibrary.notification_details(notif_details, function (notif_err, notif_data) {
                                        if (notif_err) {

                                            console.log("error from notifications");
                                            console.log(notif_err)
                                            ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                console.log("Slack alert sent");
                                            })
                                                    .catch((slack_err) => {
                                                        console.log(slack_err);
                                                    });


                                        } else {
                                            console.log("Notifications success");

                                        }
                                    })
                                }
                                if(form_url_exist == 1)
                                {

                                        BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                        if (form_err) {
                            
                                            next(form_err);
                                        }
                                        else{
                                            //console.log(form_data);

                                            res.status(200).send({status: 200, result: {bookings: return_ids_obj, message: `Successfully rescheduled bookings`}});

                                        }
                                    })
                                }
                                else
                                {
                                    res.status(200).send({status: 200, result: {bookings: return_ids_obj, message: `Successfully rescheduled bookings`}});
                                }
                            }

                        } else {
                            var reschedule_req = {
                            book_ids:book_ids_update,
                            book_status:constants.STATUS_RESCHEDULED,
                            book_updated_date:book_updated_date,
                            book_updated_by: req.body.book_created_by,
                            updated_through: req.body.booked_through
                        }
                            //console.log(reschedule_req);

                            //update rescheduled booking status and updated date
                            BookingModel.RescheduleUpdate(reschedule_req, (err2, data2) => {
                                if (err2) {

                                    //res.status(500).send({status:500, result:{error:err2}});
                                    next(err2);

                                } else {
                                    //console.log(data2);
                                    var new_multi_service_book_id = 0;

                                    //update multi_service_book_id = 0 for multi bookings 
                                    BookingModel.updateMultiserviceBookId(new_multi_service_book_id, update_multi_ids, "0", async (err3, data3) => {
                                        if (err3) {

                                            //res.status(500).send({status:500, result:{error:err3}});

                                            next(err3);
                                        } else {
                                            //console.log(data3);
//                                            console.log(ids_array);
                                            //console.log(first_id);
                                            var general_data = {
                                                book_updated_date: book_updated_date,
                                                book_updated_by: req.body.book_created_by,
                                                book_created_by: req.body.book_created_by,
                                                updated_through: req.body.booked_through,
                                                cust_id: req.body.cust_id,
                                                book_first_name:req.body.book_first_name,
                                                book_last_name:req.body.book_last_name,
                                                book_email:req.body.book_email,
                                                book_country_isd_code:req.body.book_country_isd_code.toString(),
                                                book_mobile:req.body.book_mobile.toString(),
                                                store_id: req.body.store_id,
                                                booked_through:req.body.booked_through
                                            };
                                            try{
                                                if(products.length>0)
                                                    await rescheduleProducts(first_id,products,general_data);
                                                if (inserted_data.affectedRows > 1)
                                                {
                                                    //if new services are added along with rescheduled, update multi_service_book_id
                                                    BookingModel.updateMultiserviceBookId(first_id, ids_array, "0", (err4, data4) => {
                                                        if (err4) {

                                                            //res.status(500).send({status:500, result:{error:err4}});
                                                            next(err4);
                                                        } else {
                                                            //console.log(data4);


                                                            return_ids_obj.push({book_id: first_id, multi_book_id: new_multi_service_book_id});
                                                            if (constants.PROD_FLAG == 1)
                                                            {
                                                                notif_details.have_multi_id = 1;
                                                                notif_details.book_id = first_id;
                                                                notif_details.book_status_message = 'Booking Rescheduled!';
                                                                NotificationsLibrary.notification_details(notif_details, function (notif_err, notif_data) {
                                                                    if (notif_err) {

                                                                        console.log("error from notifications");
                                                                        console.log(notif_err)
                                                                        ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                                            console.log("Slack alert sent");
                                                                        })
                                                                                .catch((slack_err) => {
                                                                                    console.log(slack_err);
                                                                                });


                                                                    } else {
                                                                        console.log("Notifications success");

                                                                    }
                                                                })
                                                            }

                                                            //console.log("form_url2: ",form_url);
                                                                //console.log("form_url_exist 2: ",form_url_exist);
                                                            if(form_url_exist == 1)
                                                            {

                                                                    BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                                    if (form_err) {
                                                        
                                                                        next(form_err);
                                                                    }
                                                                    else{
                                                                        //console.log(form_data);

                                                                        res.status(200).send({status: 200, result: {bookings: return_ids_obj, message: `Successfully rescheduled bookings`}});

           
                                                                    }
                                                                })
                                                            }
                                                            else
                                                            {
                                                                res.status(200).send({status: 200, result: {bookings: return_ids_obj, message: `Successfully rescheduled bookings`}});
                                                            }
                                                        }
                                                    })
                                                } else
                                                {
                                                    return_ids_obj.push({book_id: first_id, multi_book_id: 0});
                                                    if (constants.PROD_FLAG == 1)
                                                    {
                                                        notif_details.have_multi_id = 0;
                                                        notif_details.book_id = first_id;
                                                        notif_details.book_status_message = 'Booking Rescheduled!'
                                                        NotificationsLibrary.notification_details(notif_details, function (notif_err, notif_data) {
                                                            if (notif_err) {

                                                                console.log("error from notifications");
                                                                console.log(notif_err)
                                                                ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                                    console.log("Slack alert sent");
                                                                })
                                                                        .catch((slack_err) => {
                                                                            console.log(slack_err);
                                                                        });


                                                            } else {
                                                                console.log("Notifications success");

                                                            }
                                                        })
                                                    }

                                                    if(form_url_exist == 1)
                                                    {

                                                            BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                            if (form_err) {
                                                
                                                                next(form_err);
                                                            }
                                                            else{
                                                                //console.log(form_data);

                                                                res.status(200).send({status: 200, result: {bookings: return_ids_obj, message: `Successfully rescheduled bookings`}});
    
                                                            }
                                                        })
                                                    }
                                                    else
                                                    {
                                                        res.status(200).send({status: 200, result: {bookings: return_ids_obj, message: `Successfully rescheduled bookings`}});
                                                    }
                                                }
                                                
                                            }catch($e){
                                                next($e)
                                            }
                                        }
                                    })
                                }
                            })
                        }

                    }

                })


                    }
                });
            }
    break;

    case constants.EDIT_SERVICE_BOOKING:
    //Edit Service Booking
    var services = req.body.service_details;
    var booking_array = [];
    var rescheduling_bookings = 0;
    var book_ids_update = [];
    var notif_details = {}
        notif_details.book_id = '';
        notif_details.cust_id = req.body.cust_id;
        notif_details.cust_name = req.body.book_first_name+" "+req.body.book_last_name;
        notif_details.cust_email =req.body.book_email;
        notif_details.cust_mobile = req.body.book_country_isd_code.toString()+req.body.book_mobile.toString();
        notif_details.store_id = req.body.store_id;
        notif_details.this_base_url = /*req.protocol*/  "https://" + req.hostname; // until fix the protocal issue we directly use https
        notif_details.email_base_url = req.hostname;
        notif_details.book_created_by = req.body.book_created_by;
        if (req.body.hasOwnProperty("email_send")) {
            if(req.body.email_send){
                notif_details.email_send = true;
            }else{
                notif_details.email_send = false;
            }                            
        }else{
            notif_details.email_send = true;
        }
        if (req.body.hasOwnProperty("sms_send")) {
            if(req.body.sms_send){
                notif_details.sms_send = true;
            }else{
                notif_details.sms_send = false;
            }                            
        }else{
            notif_details.sms_send = true;
        }

        var products = req.body.product_details;
        
        var edit_only_products_flag = req.body.edit_only_products_flag;
        var update_products = [];
        var product_array = [];
        var product_quantity_update = [];
        var add_product_quantity_update = [];
    
        if(edit_only_products_flag && edit_only_products_flag == 1)
        {
            var booking_product_ids_array = [];
    
            for(let i=0; i<products.length; i++)
            { 
                //console.log("i: ",i);
                if(products[i].booking_product_id)
                {
                    booking_product_ids_array.push(products[i].booking_product_id);

                    //update product quantity
                    var update_quantity = products[i].remaining_product_quantity;
                    product_quantity_update.push([products[i].product_id,req.body.store_id,update_quantity]);
                    update_products.push([products[i].booking_product_id, products[i].product_quantity]);

                }
                else
                {
                //new product booking
                    //console.log("new product booking");
                    this_product =  [products[i].product_id, products[i].product_name, products[i].product_sku, products[i].product_barcode,
                                     products[i].product_image_url, products[i].product_thumbnail_url, req.body.store_id, req.body.cust_id,
                                     req.body.book_first_name, req.body.book_last_name ,req.body.book_email, req.body.book_mobile ,req.body.book_country_isd_code,
                                     products[i].product_date, products[i].product_quantity, products[i].product_full_amount, constants.STATUS_BOOKED, "",book_created_date,
                                     req.body.book_created_by, book_updated_date,"0", req.body.booked_through,"0",req.body.multi_service_book_id,"1",0];              
                    product_array.push(this_product);
    
                    add_product_quantity_update.push([products[i].product_id,req.body.store_id,products[i].remaining_product_quantity]);   
           
                    
                }
    
                //console.log("i"+i);
            }

            if(update_products.length > 0)
            {

                var details_req = {
                    booking_product_ids: booking_product_ids_array,
                    updated_by: req.body.book_updated_by,
                    updated_date: book_updated_date,
                    updated_through: req.body.updated_through,
            
                }
            
                                         
                    ProductsModel.updateBookingProductQuantity(update_products, (booking_err,booking_data) => {
                        if (booking_err) {
            
                            next(booking_err);
                        }
                        else{
                            //console.log("booking_data: ",booking_data);
            
                            ProductsModel.updateBookingProductDetails(details_req, (details_err,details_data) => {
                                if (details_err) {
                    
                                    next(details_err);
                                }
                                else{
                                    //console.log("details_data: ",details_data);
            
                                    ProductsModel.updateProductQuantaty(product_quantity_update, (product_err,product_data) => {
                                        if (product_err) {
                            
                                            next(product_err);
                                        }
                                        else{
                                            //console.log("product_data: ",product_data);

                                            if(product_array.length > 0)
                                            {
                                                
                                                BookingModel.addProduct(product_array, (booking_err,booking_data) => {
                                                    if (booking_err) {
                                        
                                                        next(booking_err);
                                                    }
                                                    else{
                                                        //console.log("booking_data: ",booking_data);
                                        
                                                            ProductsModel.updateProductQuantaty(add_product_quantity_update, (product_err,product_data) => {
                                                                if (product_err) {
                                                    
                                                                    next(product_err);
                                                                }
                                                                else{
                                                                    //console.log("product_data: ",product_data);
                                                        
                                                                            res.status(200).send({status:200, result:{message: "Products updated successfully in the booking"}});
                                                                }
                                                            })
                                                
                                                    }
                                                });
                                            }
                                            else
                                            {
                                                res.status(200).send({status:200, result:{message: "Product quantity updated successfully in the booking"}});

                                            }

                                
                                                   
                                        }
                                    })
                                }
                            })
                       
                        
                        }
                    });
                  
            }
            else
            {

                if(product_array.length > 0)
                {
            
                    BookingModel.addProduct(product_array, (booking_err,booking_data) => {
                        if (booking_err) {
            
                            next(booking_err);
                        }
                        else{
//                            console.log("booking_data: ",booking_data);
            
                                ProductsModel.updateProductQuantaty(add_product_quantity_update, (product_err,product_data) => {
                                    if (product_err) {
                        
                                        next(product_err);
                                    }
                                    else{
//                                        console.log("product_data: ",product_data);
                            
                                                res.status(200).send({status:200, result:{message: "Product quantity updated successfully in the booking"}});
                                    }
                                })
                    
                        }
                    });
                }
            }


        }
        else
        {
            
        /* Services edited */
            /*       Services Loop Start      */
            for(let i=0; i<services.length; i++)
            { 
                //console.log("i: ",i);
                if((!services[i].serv_prebuffertime && services[i].serv_prebuffertime!=0)  || (!services[i].serv_postbuffertime && services[i].serv_postbuffertime!=0)){
                    try{
                        buffer_times = await ServicesModel.getServiceDetailsModel(services[i].serv_id);
                        buffer_times = buffer_times[0];
                    }catch(err){
                        res.send(err);
                    }
                }else{
                    buffer_times =
                        {
                            serv_prebuffertime:parseInt(services[i].serv_prebuffertime),
                            serv_postbuffertime:parseInt(services[i].serv_postbuffertime)
                        };
                }
                
                if(services[i].book_id)
                {
                    rescheduling_bookings = 1;
                    if(services[i].multi_service_book_id != 0)
                    {
                    var this_book_id = services[i].multi_service_book_id;
                    }
                    else
                    {
                        var this_book_id = services[i].book_id;
                    }
                    
                                //console.log("book_id: ",services[i].book_id);
                                
                                if(services[i].book_oldbookid != 0)
                                {
                                    var old_book_id = services[i].book_oldbookid;
                                }
                                else
                                {
                                    var old_book_id = services[i].book_id;
                                }

                                //console.log("1i: ",i);

                                try{

                                    var this_booking_details = await BookingModel.getDetailsByBookId(services[i].book_id);
                                }
                                catch(error){
                                    //console.log("book details error: ",error);
                                    var book_details_error = error;
            
                                    break;
                                }
                                //  console.log("this booking details: ");
                                //  console.log(this_booking_details); 
                                //  console.log("2i: ",i);
                                //if(this_booking_details[0].book_start_date == services[i].book_start_date)
                                if(moment(this_booking_details[0].book_start_date).isSame(services[i].book_start_date))
                                {
                                    //console.log("same date");
                                    var this_book_status = this_booking_details[0].book_status;

                                }
                                else
                                {
                                    //console.log("different date");
                                    var this_book_status = constants.STATUS_BOOKED;

                                } 
                                if((!services[i].book_discount_deposit_amount&&services[i].book_discount_deposit_amount!=0)||(!services[i].book_discount_full_amount&&services[i].book_discount_full_amount!=0)){
                                   buffer_times.book_discount_deposit_amount = -1;
                                   buffer_times.book_discount_full_amount = -1;
                               }else{
                                   buffer_times.book_discount_deposit_amount = services[i].book_discount_deposit_amount;
                                   buffer_times.book_discount_full_amount = services[i].book_discount_full_amount;
                               }
                            
                                var this_booking =  [req.body.store_id, services[i].serv_id , services[i].serv_cat_id, services[i].user_id , services[i].user_staff_type_id ,
                                            services[i].book_start_date, services[i].book_end_date, services[i].book_start_time , services[i].book_end_time,
                                            services[i].book_duration , services[i].book_fullamount, services[i].book_deposit, req.body.cust_id,
                                            req.body.book_first_name, req.body.book_last_name ,req.body.book_email,req.body.book_country_isd_code,
                                            req.body.book_mobile , this_book_status,services[i].book_staff_note,req.body.book_created_by, book_created_date, book_updated_date,
                                            req.body.booked_through,old_book_id,"0","0","0",buffer_times.serv_prebuffertime,buffer_times.serv_postbuffertime,buffer_times.book_discount_deposit_amount,buffer_times.book_discount_full_amount];

                                            
                                            // console.log("this_booking: "+this_booking);  
                                            // console.log("3i: ",i); 
                                        

                                    booking_array.push(this_booking);
                                    book_ids_update.push(services[i].book_id);
                                    

                                    


                            // console.log(booking_array);
                            // console.log("rescheduling-"); console.log(book_ids_update); console.log(update_multi_ids);
                }
                else
                {
                //new booking
                    //console.log("new_booking");
                    var this_booking =  [req.body.store_id, services[i].serv_id , services[i].serv_cat_id, services[i].user_id , services[i].user_staff_type_id ,
                    services[i].book_start_date, services[i].book_end_date, services[i].book_start_time , services[i].book_end_time,
                    services[i].book_duration , services[i].book_fullamount, services[i].book_deposit, req.body.cust_id,
                    req.body.book_first_name, req.body.book_last_name ,req.body.book_email,req.body.book_country_isd_code,
                    req.body.book_mobile , constants.STATUS_BOOKED,services[i].book_staff_note,req.body.book_created_by, book_created_date, book_updated_date,
                    req.body.booked_through,"0","0","0","0",buffer_times.serv_prebuffertime,buffer_times.serv_postbuffertime,"-1","-1"];
                    
                    booking_array.push(this_booking); 
                }

                //console.log("i"+i);
            }
            //console.log(booking_array);
            //console.log(this_book_id);

            /*       Services Loop End         */


            if(book_details_error)
            {
            next(book_details_error);
            }
            else
            {

                //Insert bookings in db
                BookingModel.addBooking(booking_array, (err, inserted_data) => {
                    if (err) {
                        
                        //res.status(500).send({status:500, result:{error:err}});
                        next(err);
                    }
                    else{
                        //console.log(inserted_data);





                        NotificationsModel.getNotificationSettings(req.body.store_id, (notif_sett_err, notif_sett_data) => {
                            if (notif_sett_err) {
                              
                              next(notif_sett_err);
                
                            }
                            else{
                
                                //console.log("notif_sett_data: ", notif_sett_data);
                                //console.log("data: ", inserted_data);
                                var form_url_exist = 0;
                                var form_url = '';
    

                                var ids_array = [];
                                var return_ids_obj = [];
                                var first_id = inserted_data.insertId;
    
                                if(notif_sett_data[0].form_id !== 'none')
                                {
                                    form_url_exist = 1;
                                    form_url = notif_sett_data[0].form_url + "/" + first_id ;
    
                                }
                                //console.log("form_url: ",form_url);


                        var ids_array = [];
                        var return_ids_obj = [];
                        var first_id = inserted_data.insertId;
                    
                        for(let i=0; i<inserted_data.affectedRows; i++)
                        {
                            var id = inserted_data.insertId + i;
                            ids_array.push(id);
        //                    return_ids_obj.push({book_id:id, book_deposit:services[i].book_deposit, book_fullamount:services[i].book_fullamount, type:constants.SERVICE_TYPE});
                        }

                        if(rescheduling_bookings == 0)
                        {
                            //only added new bookings
                            
                            // console.log(ids_array);
                            // console.log(first_id);
                                                            
                            if(inserted_data.affectedRows>1)
                            {
                                //if new services are added along with rescheduled, update multi_service_book_id
                                BookingModel.updateMultiserviceBookId(first_id, ids_array, "0", (err4,data4) => {
                                    if (err4) {                                  
                                        
                                        //res.status(500).send({status:500, result:{error:err4}});
                                        next(err4);
                                    }
                                    else{
                                        //console.log(data4);
                                        return_ids_obj.push({book_id:first_id,multi_book_id:first_id});

                                        if(req.body.product_details && req.body.product_details.length > 0)
                                        {

                                            var product_booking_details = {
                                                store_id: req.body.store_id,
                                                cust_id: req.body.cust_id,
                                                book_first_name :req.body.book_first_name, 
                                                book_last_name: req.body.book_last_name ,
                                                book_email : req.body.book_email,
                                                book_mobile:req.body.book_mobile,
                                                book_country_isd_code : req.body.book_country_isd_code,
                                                book_created_date: book_updated_date,
                                                booked_through: req.body.booked_through,
                                                book_created_by: req.body.book_created_by 

                                            }


                                            //products start
                                            editProductsBooking(req.body.product_details, first_id, product_booking_details, function(products_err,products_data){
                                                if (products_err) {
                                    
                                                    //console.log("error from products: ", products_err);
                                                    next(products_err);  
                            
                                                }
                                                else{
                                                    //console.log("products data :", products_data);


                                                if(form_url_exist == 1)
                                                {

                                                    BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                        if (form_err) {
                                        
                                                            next(form_err);
                                                        }
                                                        else{
                                                            //console.log(form_data);

                                                            res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                

                                                        }
                                                    })
                                                }
                                                else
                                                {
                                                    res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                                                }
                
                                                }
                                            })
                                            //products end
                                        }
                                        else
                                        {

                                            
                                            if(form_url_exist == 1)
                                            {

                                                BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                    if (form_err) {
                                    
                                                        next(form_err);
                                                    }
                                                    else{
                                                        //console.log(form_data);

                                                        res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
            

                                                    }
                                                })
                                            }
                                            else
                                            {
                        
                                                res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                                            }
            
                                            

                                        }

                                        if(constants.PROD_FLAG == 1)
                                        {
                                                notif_details.have_multi_id = 1;
                                                notif_details.book_id = first_id;

                                                NotificationsLibrary.notification_details(notif_details, function(notif_err,notif_data){
                                                    if (notif_err) {
                                        
                                                        console.log("error from notifications");
                                                        console.log(notif_err)
                                                        ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                            console.log("Slack alert sent");
                                                        })
                                                        .catch((slack_err) => {
                                                            console.log(slack_err);
                                                        });
                                                        
                                
                                                    }
                                                    else{
//                                                        console.log("Notifications success");
                    
                                                    }
                                                })
                                        }

                                        //res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                                    }
                                })
                            }
                            else
                            {
                                return_ids_obj.push({book_id:first_id,multi_book_id:0});

                                    if(req.body.product_details && req.body.product_details.length > 0)
                                    {

                                            var product_booking_details = {
                                                store_id: req.body.store_id,
                                                cust_id: req.body.cust_id,
                                                book_first_name :req.body.book_first_name, 
                                                book_last_name: req.body.book_last_name ,
                                                book_email : req.body.book_email,
                                                book_mobile:req.body.book_mobile,
                                                book_country_isd_code : req.body.book_country_isd_code,
                                                book_created_date: book_updated_date,
                                                booked_through: req.body.booked_through,
                                                book_created_by: req.body.book_created_by 

                                            }


                                            //products start
                                            editProductsBooking(req.body.product_details, first_id, product_booking_details, function(products_err,products_data){
                                                if (products_err) {
                                    
                                                    //console.log("error from products: ", products_err);
                                                    next(products_err);  
                            
                                                }
                                                else{
                                                    //console.log("products data :", products_data);

                                            
                                                        if(form_url_exist == 1)
                                                        {
            
                                                            BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                                if (form_err) {
                                                
                                                                    next(form_err);
                                                                }
                                                                else{
                                                                    //console.log(form_data);
            
                                                                    res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                        
            
                                                                }
                                                            })
                                                        }
                                                        else
                                                        {

                                                            res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                                                            
                                                        }
                                                                                
        
                
                                                }
                                            })
                                            //products end
                                    }
                                    else
                                    {
                                        if(form_url_exist == 1)
                                                        {
            
                                                            BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                                if (form_err) {
                                                
                                                                    next(form_err);
                                                                }
                                                                else{
                                                                    //console.log(form_data);
            
                                                                    res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                        
            
                                                                }
                                                            })
                                                        }
                                                        else
                                                        {

                                                            res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                                                            
                                                        }
                                                                 
                                    

                                    }

                                    if(constants.PROD_FLAG == 1)
                                    {
                                            notif_details.have_multi_id = 0;
                                            notif_details.book_id = first_id;

                                                NotificationsLibrary.notification_details(notif_details, function(notif_err,notif_data){
                                                    if (notif_err) {
                                        
                                                        console.log("error from notifications");
                                                        console.log(notif_err)
                                                        ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                            console.log("Slack alert sent");
                                                        })
                                                        .catch((slack_err) => {
                                                            console.log(slack_err);
                                                        });
                                                        
                                
                                                    }
                                                    else{
                                                        console.log("Notifications success");
                    
                                                    }
                                                })
                                    }

                                //res.status(200).send({status:200, result:{ bookings: return_ids_obj, message: `Successfully updated bookings`}});
                            }

                        }
                        else{
                            //rescheduling bookings

                            var reschedule_req = {
                                book_ids:book_ids_update,
                                book_status:constants.STATUS_RESCHEDULED,
                                book_updated_date:book_updated_date,
                                book_updated_by: req.body.book_created_by,
                                updated_through: req.body.updated_through
                            }
                            //console.log(reschedule_req);
                    
                            //update rescheduled booking status and updated date
                            BookingModel.RescheduleUpdate(reschedule_req, (err2, data2) => {
                                if (err2) {
                                
                                    //res.status(500).send({status:500, result:{error:err2}});
                                    next(err2);
                                }
                                else{
                                    //console.log(data2);
                                    var new_multi_service_book_id = 0;
                                
                                    //update multi_service_book_id = 0 for multi bookings 
                                    BookingModel.updateMultiserviceBookId(new_multi_service_book_id, book_ids_update, "0", (err3,data3) => {
                                        if (err3) {
                                            //res.status(500).send({message: err3});
                                            next(err3);
                                        }
                                        else{
                                            //console.log(data3);
                                            //console.log(ids_array);
                                            //console.log(first_id);
                                                                
                                                if(inserted_data.affectedRows>1)
                                                {
                                                    //if new services are added along with rescheduled, update multi_service_book_id
                                                    BookingModel.updateMultiserviceBookId(first_id, ids_array, "0", (err4,data4) => {
                                                        if (err4) {                                  
                                                        
                                                            //res.status(500).send({status:500, result:{error:err4}});
                                                            next(err4);
                                                        }
                                                        else{
                                                            //console.log(data4);
                                                            return_ids_obj.push({book_id:first_id,multi_book_id:first_id});

                                                            if(req.body.product_details && req.body.product_details.length > 0)
                                                            {

                                                                var product_booking_details = {
                                                                    store_id: req.body.store_id,
                                                                    cust_id: req.body.cust_id,
                                                                    book_first_name :req.body.book_first_name, 
                                                                    book_last_name: req.body.book_last_name ,
                                                                    book_email : req.body.book_email,
                                                                    book_mobile:req.body.book_mobile,
                                                                    book_country_isd_code : req.body.book_country_isd_code,
                                                                    book_created_date: book_updated_date,
                                                                    booked_through: req.body.booked_through,
                                                                    book_created_by: req.body.book_created_by 

                                                                }

                                                                //products start
                                                                editProductsBooking(req.body.product_details, first_id, product_booking_details, function(products_err,products_data){
                                                                    if (products_err) {
                                                        
                                                                        //console.log("error from products: ", products_err);
                                                                        next(products_err);  
                                                
                                                                    }
                                                                    else{
                                                                        //console.log("products data :", products_data);

                                                                        if(form_url_exist == 1)
                                                        {
            
                                                            BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                                if (form_err) {
                                                
                                                                    next(form_err);
                                                                }
                                                                else{
                                                                    //console.log(form_data);
            
                                                                    res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                        
            
                                                                }
                                                            })
                                                        }
                                                        else
                                                        {

                                                            res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                                                            
                                                        }
                                                                 
                                                    
                                    
                                                                    }
                                                                })
                                                                //products end
                                                            }
                                                            else
                                                            {
                                                                if(form_url_exist == 1)
                                                                {
                
                                                                    BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                                        if (form_err) {
                                                    
                                                                            next(form_err);
                                                                        }
                                                                        else{
                                                                            //console.log(form_data);
            
                                                                            res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                        
            
                                                                        }
                                                                    })
                                                                }
                                                                else
                                                                {

                                                                    res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                                                            
                                                                }
                                                                 

                                                            }


                                                            if(constants.PROD_FLAG == 1)
                                                            {
                                                                notif_details.have_multi_id = 1;
                                                                notif_details.book_id = first_id;
                                                                notif_details.book_status_message = 'Booking Updated!'
                                                                NotificationsLibrary.notification_details(notif_details, function(notif_err,notif_data){
                                                                    if (notif_err) {
                                                        
                                                                        console.log("error from notifications");
                                                                        console.log(notif_err)
                                                                        ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
//                                                                            console.log("Slack alert sent");
                                                                        })
                                                                        .catch((slack_err) => {
                                                                            console.log(slack_err);
                                                                        });
                                                                        
                                                
                                                                    }
                                                                    else{
                                                                        console.log("Notifications success");
                                    
                                                                    }
                                                                })
                                                            }

                                                            //res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                                                        }
                                                    })
                                                }
                                                else
                                                {
                                                    return_ids_obj.push({book_id:first_id,multi_book_id:0});

                                                    if(req.body.product_details && req.body.product_details.length > 0)
                                                    {

                                                        var product_booking_details = {
                                                            store_id: req.body.store_id,
                                                            cust_id: req.body.cust_id,
                                                            book_first_name :req.body.book_first_name, 
                                                            book_last_name: req.body.book_last_name ,
                                                            book_email : req.body.book_email,
                                                            book_mobile:req.body.book_mobile,
                                                            book_country_isd_code : req.body.book_country_isd_code,
                                                            book_created_date: book_updated_date,
                                                            booked_through: req.body.booked_through,
                                                            book_created_by: req.body.book_created_by 

                                                        }


                                                        //products start
                                                        editProductsBooking(req.body.product_details, first_id, product_booking_details, function(products_err,products_data){
                                                            if (products_err) {
                                                
                                                                //console.log("error from products: ", products_err);
                                                                next(products_err);  
                                        
                                                            }
                                                            else{
                                                                //console.log("products data :", products_data);

                                                                if(form_url_exist == 1)
                                                                {
                
                                                                    BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                                        if (form_err) {
                                                    
                                                                            next(form_err);
                                                                        }
                                                                        else{
                                                                            //console.log(form_data);
            
                                                                            res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                        
            
                                                                        }
                                                                    })
                                                                }
                                                                else
                                                                {
                                                                    res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                                                                }
                            
                                                            }
                                                        })
                                                        //products end
                                                    }
                                                    else
                                                    {
                                                                if(form_url_exist == 1)
                                                                {
                
                                                                    BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                                        if (form_err) {
                                                    
                                                                            next(form_err);
                                                                        }
                                                                        else{
                                                                            //console.log(form_data);
            
                                                                            res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                        
            
                                                                        }
                                                                    })
                                                                }
                                                                else
                                                                {
                                                                    res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully updated bookings`}});
                                                                }

                                                    }

                                                    if(constants.PROD_FLAG == 1)
                                                    {
                                                        notif_details.have_multi_id = 0;
                                                        notif_details.book_id = first_id;
                                                        notif_details.book_status_message = 'Booking Updated!'
                                                        NotificationsLibrary.notification_details(notif_details, function(notif_err,notif_data){
                                                            if (notif_err) {
                                                
                                                                console.log("error from notifications");
                                                                console.log(notif_err)
                                                                ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                                    console.log("Slack alert sent");
                                                                })
                                                                .catch((slack_err) => {
                                                                    console.log(slack_err);
                                                                });
                                                                
                                        
                                                            }
                                                            else{
//                                                                console.log("Notifications success");
                            
                                                            }
                                                        })
                                                    }
                                                    
                                                    //res.status(200).send({status:200, result:{ bookings: return_ids_obj, message: `Successfully updated bookings`}});
                                                }
                                        }
                                    })
                                }
                            })
                        }
                    }
                        })

                    }
                });
            }
        }
    
        break;


 

    }
    
   
    
}





//update booking status from calendar
exports.updateBookedServiceStatus = (req, res, next) => {
           
    if(!(req.body.book_id) || !(req.body.book_status) || (!req.body.multi_service_book_id && req.body.multi_service_book_id !=0) || !(req.body.book_updated_by) || !(req.body.updated_through)){
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var multi_service_book_id = parseInt(req.body.multi_service_book_id,10);
//    console.log("multi_service_book_id: ",multi_service_book_id);

    if(multi_service_book_id != 0)
    {
        var book_id = multi_service_book_id;
        var multi_id_flag = 1;
    }
    else
    {
        var book_id = parseInt(req.body.book_id,10);
        var multi_id_flag = 0;
    }
    var book_updated_date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');

    var status_req = {
        book_id:book_id,
        book_status:req.body.book_status,
        book_updated_date:book_updated_date,
        book_updated_by:req.body.book_updated_by,
        updated_through:req.body.updated_through
    }

    
   BookingModel.updateBookedServiceStatus(status_req, multi_id_flag,async (err, data) => {
        if (err) {
            next(err);
        }
        else{
            ProductsModel.updateProductBookingStatus(status_req, 1, (product_err, product_data) => {
                if (product_err) {
                    next(product_err);
                }
                else{
                    if((req.body.book_status == constants.STATUS_CANCELLED || req.body.book_status == constants.STATUS_LATE_CANCELLED))
                    {

                        BookingModel.getDetailsOfBooking([book_id], multi_id_flag, async (err, book_details) => {
                            if (err) {
                                //console.log(err);
                                next(err);
                            }
                            else{
                               //console.log("book_details",book_details);

                                //Notifications Start
                                var notif_details = {}

                               
                                                notif_details.book_id = book_id;
                                                notif_details.have_multi_id = multi_id_flag;
                                   
                                    notif_details.cust_id = book_details[0].cust_id;
                                    notif_details.cust_name = book_details[0].book_first_name+" "+book_details[0].book_last_name;
                                    notif_details.cust_email = book_details[0].book_email;
                                    notif_details.cust_mobile = book_details[0].book_country_isd_code.toString()+book_details[0].book_mobile.toString();
                                    notif_details.store_id = book_details[0].store_id;
                                    notif_details.this_base_url = /*req.protocol*/  "https://" + req.hostname; // until fix the protocal issue we directly use https
                                    notif_details.email_base_url = req.hostname;
                                    notif_details.book_created_by = book_details[0].book_created_by;
                                    if (req.body.hasOwnProperty("email_send")) {
                                        if(req.body.email_send){
                                            notif_details.email_send = true;
                                        }else{
                                            notif_details.email_send = false;
                                        }                            
                                    }else{
                                        notif_details.email_send = true;
                                    }
                                    if (req.body.hasOwnProperty("sms_send")) {
                                        if(req.body.sms_send){
                                            notif_details.sms_send = true;
                                        }else{
                                            notif_details.sms_send = false;
                                        }                            
                                    }else{
                                        notif_details.sms_send = true;
                                    }
                              //Notfications End

                              var book_date_time = book_details[0].book_start_date+" "+book_details[0].book_start_time;
                              //console.log("book_date_time: ", book_date_time);
                              //var this_time = moment().add(10,'h');
                              var this_time = moment_tz().tz("Australia/Brisbane").format("YYYY-MM-DD HH:mm:ss");
                              var time_diff = moment.duration(moment(book_date_time).diff(this_time)).asHours();
                              //console.log("time diff: ",time_diff);
                              //console.log("Current time:", this_time);

    
                                async.each(book_details,async (obj,callback) => {
                                    //var book_date_time = obj.book_start_date+" "+obj.book_start_time;
        //                            var today = moment.utc().tz(constants.TIME_ZONE).format('YYYY-MM-DD HH:mm');
        //                            console.log(today)
                                    //var time_diff = moment.duration(moment(book_date_time).diff(moment())).asHours();
                                    //console.log("time diff: ",time_diff);
                                    //console.log(moment().format("YYYY-MM-dd HH:mm"));
                                        if(time_diff >= 24)
                                        {
                                            //console.log("adding wallet");
                                            let book_ids =[],paid_amount=0;
                                            book_ids = await getOldBookings(obj.book_id,book_ids);
                                            paid_amount = await TransactionModel.getAmount(book_ids);
                                            if(paid_amount[0].paid_amount > 0){
                                                let insert_wallet_data = [[
                                                    obj.cust_id,
                                                    obj.book_email,
                                                    constants.WALLET_CREDIT,
                                                    paid_amount[0].paid_amount,
                                                    "added amount for cancelation of "+obj.book_id,
                                                    obj.store_id,
                                                    obj.book_id,
                                                    0
                                                ]];
                                                //console.log("insert_wallet_data",insert_wallet_data);
                                                await TransactionModel.insert_transaction_and_wallet_credit(insert_wallet_data,[]);
                                        }
                                    }
                                }, function (err) {
                                        if (err)
                                            res.status(400).send({status: 400, message: {err_code: 400, error_message: err}});
                                        else{

                                            //Cancel booking notification Start
                                            if(constants.PROD_FLAG == 1)
                                            {
                                                
                                                notif_details.book_status_message = 'Booking Cancelled!'
                                                NotificationsLibrary.notification_details(notif_details, function(notif_err,notif_data){
                                                    if (notif_err) {
                                        
                                                        console.log("error from notifications");
                                                        console.log(notif_err)
                                                        ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                            console.log("Slack alert sent");
                                                        })
                                                        .catch((slack_err) => {
                                                            console.log(slack_err);
                                                        });
                                                        
                                
                                                    }
                                                    else{
                                                        console.log("Notifications success");
                    
                                                    }
                                                })
                                            }
                                            //Cancel booking notification End

                                            if(book_details[0].multi_service_book_id != 0 && multi_id_flag == 0)
                                            {
                                                var cancel_multi_id_flag = 1;
                                                var update_multi_book_ids = [book_details[0].multi_service_book_id];
                                                if(req.body.edit_flag == 1)
                                                {
                                                   cancel_multi_id_flag = 0;
                                                   update_multi_book_ids = [req.body.book_id];
                                                }
                                                //console.log("cancel_multi_id_flag-");console.log(cancel_multi_id_flag);console.log(update_multi_book_ids);

                                                BookingModel.updateMultiserviceBookId("0", update_multi_book_ids, cancel_multi_id_flag, (err2,data2) => {
                                                    if (err2) {                    
                                                        //res.status(500).send({message: err2});
                                                        next(err2);
                                                    }
                                                    else{
                                                        
                                                        //console.log("product time diff1: ",time_diff);
                                                        

                                                            productsAmountToWallet(status_req,time_diff,res,next);
//                                                            res.status(200).send({status:200,results:"updated successfully"});
//                                                        res.status(200).send({status:200, message:`Successfully updated Booking Status for ${data.affectedRows} bookings`});
                                                    }
                                                })
                                            }
                                            else
                                            {
                                                
                                                //console.log("product time diff2: ",time_diff);
                                                    productsAmountToWallet(status_req,time_diff,res,next);
//                                                    res.status(200).send({status:200,results:"updated successfully"});
//                                                res.status(200).send({status:200, message:`Successfully updated Booking Status for ${data.affectedRows} bookings`});
                                            }
                                        }
                                });                        
                            }               
                        })
                    }else if (req.body.book_status == constants.STATUS_NO_SHOW) {
                        ProductsModel.getProductBookingDetails(status_req.book_id, 1, (product_err, product_data) => {
                            if (product_err) {

                                next(product_err);
                            } else {
                                if(product_data.length > 0){
                                    let product_quantity_update = [];
                                    async.each(product_data, (obj, callback) => {
                                        product_quantity_update.push([obj.product_id, obj.store_id, (obj.remaining_product_quantity + obj.product_quantity)]);
                                        callback();
                                    }, function (err) {
                                        if(err){
                                            next(err.toString())
                                        }
                                        ProductsModel.updateProductQuantaty(product_quantity_update, (update_err, update_data) => {
                                            if (update_err) {
                                                //                                    console.log('error updating products')
                                                next(update_err);
                                            } else
                                            {
                                                res.status(200).send({status:200, message:`Successfully updated Booking Status`});
                                            }
                                        })
                                    });
                                }else{
                                    res.status(200).send({status:200, message:`Successfully updated Booking Status`});
                                }
                            }
                        })
                    }else{
                        res.status(200).send({status:200, message:`Successfully updated Booking Status`});
                    }

                }
            });
            
            
            
        }
    });
   
}

exports.getBookingDetails = (req, res, next) => {
    if(!(req.body.services) && !(req.body.classes) && !(req.body.products)) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var service_bookings = [];
    var class_bookings =[];
    var product_bookings = [];
    if(req.body.services)
    {
        var given_book_id = req.body.services;
        var book_ids = [];
        var have_multi_book_id = false;
        // for(i=0; i<given_book_ids.length; i++)
        // {
            
              if(given_book_id.multi_service_book_id != 0)
              {
                book_ids.push(given_book_id.multi_service_book_id);
                have_multi_book_id = true;
                //console.log(i);
              }
              else
              {
                book_ids.push(given_book_id.book_id);
                have_multi_book_id = false;
                //console.log(i);
              }

              var edit_flag = 0;

              if(req.body.edit_flag == 1)
              {
                  edit_flag = 1;
              }
            BookingModel.getProductDetails(book_ids, edit_flag, (err, products_data) => {
                if (err) {
                    next(err);
                } else {
                    product_bookings = products_data;
                }
            })
              
        //}
        //console.log("for loop completed"); 
        //console.log(book_ids);

        BookingModel.getServiceBookingDetails(book_ids,have_multi_book_id, edit_flag, (err, services_data) => {
            if (err) {
                //res.status(500).send({message:err});
                next(err);
            }
            else{
                //console.log(services_data);

                async.each(services_data,function(this_serv_booking,callback){
                    this_serv_booking.last_visited = [];

                    CustomerModel.getLastVisited(this_serv_booking.store_id,this_serv_booking.book_email,this_serv_booking.book_start_date,this_serv_booking.book_start_time, (err, cust_last_visited) => {
                        if (err) {

                            return err;
                        }
                        else{
                            //console.log(cust_last_visited);
                            this_serv_booking.last_visited= cust_last_visited;
                            callback();
                        }
                    })

                },function(service_bookings_err){

                    if(req.body.classes && req.body.classes.length >0)
                    {
                        ClassesModel.getClassBookingDetails(req.body, (err, classes_data) => {
                            if (err) {
                                //res.status(500).send({message:err});
                                next(err);
                            }
                            else{
                                //console.log(classes_data);
                
                                async.each(classes_data,function(this_class_booking,callback2){
                                    this_class_booking.last_visited = {}
                
                                    CustomerModel.getLastVisited(this_class_booking.store_id,this_class_booking.class_cust_email,this_class_booking.date,this_class_booking.start_time, (err, cust_last_visited_class) => {
                                        if (err) {
                
                                            return err;
                                        }
                                        else{
//                                            console.log(cust_last_visited_class);
                                            this_class_booking.last_visited= cust_last_visited_class;
                                            callback2();
                                        }
                                    })
                
                                },function(class_bookings_err){
                                    res.status(200).send({status:200, result:{services:services_data, classes:classes_data, products:[]}});
                                    
                
                                })
                                
                            }
                
                        }); 
                    }
                    else
                    {
                        res.status(200).send({status:200, result:{services:services_data,products:product_bookings, classes:[]}});

                    }  
                    

                })
                //res.status(200).send({status:200, result:data});
            }

        }); 
    }
    else if(req.body.classes && req.body.classes.length >0)
    {
        ClassesModel.getClassBookingDetails(req.body, (err, classes_data) => {
            if (err) {
                //res.status(500).send({message:err});
                next(err);
            }
            else{
                //console.log(classes_data);

                async.each(classes_data,function(this_class_booking,callback2){
                    this_class_booking.last_visited = {}

                    CustomerModel.getLastVisited(this_class_booking.store_id,this_class_booking.class_cust_email,this_class_booking.date,this_class_booking.start_time, (err, cust_last_visited_class) => {
                        if (err) {

                            return err;
                        }
                        else{
//                            console.log(cust_last_visited_class);
                            this_class_booking.last_visited= cust_last_visited_class;
                            callback2();
                        }
                    })

                },function(class_bookings_err){
                    res.status(200).send({status:200, result:{services:[], classes:classes_data}});
                    

                })
                
            }

        }); 
    }
  
}


exports.getBookingLog = (req, res, next) => {
    if((!req.body.services) || (req.body.services.length < 1)) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var booking_data={
        service_bookings:{booking_info:[],payment:[]},
        product_bookings:{booking_info:[],payment:[]},
        class_bookings_info:[]
      }

        var given_book_id = req.body.services;
        var book_ids_req = [];
            
              if(given_book_id.multi_service_book_id != 0)
              {
                book_ids_req.push(given_book_id.multi_service_book_id);
                
              }
              else
              {
                book_ids_req.push(given_book_id.book_id);
                
              }
              //console.log(book_ids_req);

    BookingModel.getBookingLog(book_ids_req, async (err, book_data) => {
        if (err) {
            
            next(err);
        }
        else{
            var book_ids = [];
            


            async.each(book_data, function (book_obj,callback) {
                   //console.log("i: ",i);
//                   console.log("book_id: ",book_obj.book_id);

                   book_ids.push(book_obj.book_id);
//                   console.log(book_ids);
                   
                // try{
                   
                //     book_ids = await getOldBookings(book_obj.book_oldbookid,book_ids);
                    
                //     console.log("book_id after await: ",book_obj.book_id);
                
                //     console.log("book_ids: ",book_ids);
                //     callback();
                
                    
                // }
                // catch(error){                  
                //     console.log("return error:",error);
                //     return error;
                //     //break;
                //     //throw Error(error);
                //     //return;
                // }



                getOldBookings(book_obj.book_oldbookid,book_ids).then((result) => {
                    //console.log("in function book_id: ",book_obj.book_id);
                    
                    book_ids = result;
//                    console.log(`book_ids result for ${book_obj.book_id}: `,result);
                    callback();
                   
                })
                .catch((err) => {
                    //console.log("old book error1 :",err);
                    //return err;
                    callback(err);
                });
                
                
            }, function(err){
                //console.log("for loop completed");

                    if(err)
                    {
                        //console.log("old book error: ",err)
                        next(err);
                    }
                    else
                    {
//                        console.log("final book ids: ",book_ids)

                        //booking details and transactions including details of old bookings

                        BookingModel.getBookingLog(book_ids, (booking_err, book_data2) => {
                            if (booking_err) {
                                next(booking_err);
                            }
                            else{
                                //console.log(book_data2);
                                booking_data.service_bookings.booking_info = book_data2;
            
                                BookingModel.getBookingTransactions(book_ids, (err, transactions_data) => {
                                    if (err) {
                                        
                                        next(err);
                                    }
                                    else{
                                        
                                        //console.log(transactions_data);
                                        booking_data.service_bookings.payment = transactions_data;

                                        ProductsModel.ProductsBookingLog(book_ids_req, "1", (product_booking_err, product_book_data) => {
                                            if (product_booking_err) {
                                                next(product_booking_err);
                                            }
                                            else{
//                                                console.log("product_book_data: ",product_book_data);

                                                if(product_book_data.length > 0)
                                                {
                                                    var product_book_ids = [];

                                                    for(let i=0; i< product_book_data.length; i++)
                                                    {
                                                        product_book_ids.push(product_book_data[i].booking_product_id);

                                                    }

//                                                    console.log("product_book_ids: ",product_book_ids);

                                                    ProductsModel.getOldProductBookings(product_book_ids, (product_old_book_err, product_old_book_data) => {
                                                        if (product_old_book_err) {
                                                            next(product_old_book_err);
                                                        }
                                                        else{
//                                                            console.log("product_old_book_data: ",product_old_book_data);
                                                            var final_product_book_ids = [];

                                                            for(let i=0; i< product_old_book_data.length; i++)
                                                            {
                                                                final_product_book_ids.push(product_old_book_data[i].booking_product_id);

                                                            }

//                                                            console.log("final product book ids: ", final_product_book_ids);

                                                            ProductsModel.ProductsBookingLog(final_product_book_ids, "0", (product_booking_err2, product_book_data2) => {
                                                                if (product_booking_err2) {
                                                                    next(product_booking_err2);
                                                                }
                                                                else{
//                                                                    console.log("product_book_data2: ",product_book_data2);
            

                                                                    booking_data.product_bookings.booking_info = product_book_data2;

                                                                    ProductsModel.getProductsBookingTransactions(final_product_book_ids, (product_transactions_err, product_transactions_data) => {
                                                                        if (product_transactions_err) {
                                                                            
                                                                            next(product_transactions_err);
                                                                        }
                                                                        else{
                                                                            
//                                                                            console.log("product_transactions_data: ",product_transactions_data);
                                                                            booking_data.product_bookings.payment = product_transactions_data;
                                    
                                                
                                                                            res.status(200).send({status:200, result:booking_data});
                                                                        }
                                                                    })
                                                                }
                                                            
                                                    
                                                            })
                                                        }
                                                    })
                                                }
                                                else
                                                {
                                                    res.status(200).send({status:200, result:booking_data});
                                                }
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
            })
            
        }
  
    });   
}

exports.updateBookingNotes = (req, res, next) => {
    if(!(req.body.book_id) || (!(req.body.book_notes) && req.body.book_notes != "")) {
        res.status(400).send({status:400,message: 'Invalid request parameters. Check headers and body'});
        return;
    }
    if(req.body.book_notes.includes("'")){
        var book_notes = [...req.body.book_notes];
        req.body.book_notes = '';
        for(let i = 0; i < book_notes.length; i++)
        {
            if(book_notes[i] == "'"){
                req.body.book_notes +="'"
            }
            req.body.book_notes += book_notes[i];
        }
    }
   BookingModel.updateBookingNotes(req.body, (err, data) => {
        if (err) {

            //res.status(500).send({status:500,message:err});
            next(err);
        }
        else{
        
            res.status(200).send({status:200, message:"Updated the notes successfully"});
        }
    });
   
}


//Get Staffwise Bookings for Calendar
exports.getBookingsStaffwise = (req, res, next) => {

    if(!req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var today_date = new Date(); 
    var today = today_date.toISOString().slice(0,10);

    if(req.body.start_date && req.body.end_date && (req.body.start_date > req.body.end_date))
    {
      //given start date > end date, then return error
      return res.status(400).send({message: 'Invalid request parameters. Given end_date must be greater than start_date.'});
      
    }
    else if(!req.body.start_date && !req.body.end_date)
    {

      //if dates are not given , then start date = end date = today
      req.body.start_date = req.body.end_date = today;
     
    }
    else if(req.body.start_date && !req.body.end_date)  
    {
      //only start date is given

        if(req.body.start_date <= today)
        {
           // start date <= today , then end date = today
            req.body.end_date = today;
        }
        else
        {
            //start date > today, then end date = start date + 1 day
            var start = new Date(req.body.start_date);
            start.setDate(start.getDate() + 1);
            req.body.end_date = start.toISOString().slice(0,10);
        }
     
    }
    else if(req.body.end_date && !req.body.start_date)
    {
        //only end date is given

        if(req.body.end_date >= today)
        {
          // end date >= today, then start date = today
            req.body.start_date = today;
        }
        else
        {
            // end date < today , then send error
            return res.status(400).send({message: 'Invalid request parameters. End date must be greater than start date.'});
           
        }
    
       
    }

    var start_date = moment(req.body.start_date,"YYYY-MM-DD");
    var end_date = moment(req.body.end_date,"YYYY-MM-DD");
  
    var date_array= [];
    var this_date = start_date;

   //Get Staff for Store
   StaffModel.getStaffByStoreforCalendar(req.body, (err, staff) => {
    if (err) {

        //res.status(500).send({message:err});
        next(err);
       
    }
    else{
        //console.log(staff);
        
        while(this_date <= end_date)
        {
            var new_obj={};
            new_obj.date = moment(this_date).format("YYYY-MM-DD");
            new_obj.day = (moment(this_date).isoWeekday()) - 1;
            new_obj.staff= JSON.parse(JSON.stringify(staff)); 
            new_obj.business_hours = {};
            date_array.push(new_obj);
            this_date = this_date.add(1, 'days');
        }
        //console.log(date_array);

            //get Bookings 
            BookingModel.getBookingsStaffwise(req.body, (err, data1) => {
                if (err) {

                    //res.status(500).send({message:err});
                    next(err);
                
                }
                else{
                    //console.log("get bookings");
                    //console.log(data1);

                async.each(data1, function (booking_obj, callback) {

                    booking_obj.booking_service_images = [];
                    booking_obj.customer_details = [];
                    booking_obj.last_visited = [];


                           if(booking_obj.book_type == "service")
                           {
                    
                                BookingModel.getBookingImages(booking_obj.id, (err, images) => {
                                        if (err) {

                                            return err;
                                        }
                                        else{
                                            images = JSON.parse(JSON.stringify(images));
                                            booking_obj.booking_service_images = images;

                                            CustomerModel.getLastVisited(req.body.store_id,booking_obj.email,booking_obj.date,booking_obj.time, async (err, cust_last_visited) => {
                                                if (err) {
        
                                                    return err;
                                                }
                                                else{
                                                    //console.log(cust_last_visited);

                                                    if(cust_last_visited.length > 0)
                                                    {

                                                        booking_obj.last_visited= cust_last_visited;
                                                    }

                                                    var trans_book_ids = [booking_obj.id];

                                                    try{
                                                        //console.log(trans_book_ids);
                                                     
                                                       trans_book_ids = await getOldBookings(booking_obj.book_oldbookid,trans_book_ids);
                                                    }
                                                    catch(error){ 
                                                        console.log(error);
                                                      
                                                        var old_book_error = error;
                                                        //break;
                                                    }

                                                    if(old_book_error)
                                                    {
                                                         return old_book_error;
                                                    }
                                                    else
                                                    {
                                                    
                                                        BookingModel.getBookPaidInvoice(trans_book_ids, (err, book_trans) => {
                                                            if (err) {
                    
                                                                return err;
                                                            }
                                                            else{
                                                                // console.log(trans_book_ids);
                                                                // console.log(book_trans);
                                                                booking_obj.transaction_details = book_trans;
                                                                
                                                                date_array.filter((date_obj)=>{
                                                                    //console.log("services");console.log(date_obj);

                                                                    if(date_obj.date == booking_obj.date) 
                                                                    {
                                                                
                                                                        date_obj.staff.filter((obj) =>{
                                                                            if(!obj.bookings)
                                                                            {
                                                                                obj.bookings=[];
                                                                            }

                                                                        
                                                                            if(obj.staff_id == booking_obj.staff_id) 
                                                                            {
                                                                                obj.bookings.push(booking_obj);
                                                                            
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                                callback();
                                                            }
                                            
                                                        })         
                                
                                                    }
                                                }
                                            })    
                                            
                                        }
                                })
                            }
                            else if(booking_obj.book_type == "class")
                            {
                                var class_book_req = {};
                                  if(req.body.book_status)
                                  {
                                      class_book_req= {
                                          class_schedule_id:booking_obj.id,
                                          class_book_status:req.body.book_status
                                      }
                                  }
                                  else
                                  {
                                      class_book_req= {
                                         class_schedule_id:booking_obj.id 
                                      }

                                  }

                                
                        
                                BookingModel.getClassBookings(class_book_req, (err, class_bookings) => {
                                        if (err) {

                                            return err;
                                        }
                                        else{
                
                                            class_bookings = JSON.parse(JSON.stringify(class_bookings));
                                           
                                            booking_obj.customer_details = class_bookings;


                           //staff_array = 

                           date_array.filter((date_obj)=>{

                            if(date_obj.date == booking_obj.date) 
                            {
                          
                           date_obj.staff.filter((obj) =>{

                                                     if(!obj.bookings)
                                                    {
                                                        obj.bookings=[];
                                                    }
                              
                                

                            if(obj.staff_id == booking_obj.staff_id) 
                            {
                                obj.bookings.push(booking_obj);
                            }
                        });
                    }
                    });
                   
   
                            callback();
                                            
                                            
                                        }
                                }) 
                            }

                
                       
                              },function (err) {
                                //   console.log("this is final error");
                                //   console.log(err);

                                  if(err)
                                  {
                                    //res.status(500).send({message:err});
                                    next(err);
                                  }
                                  else
                                  {

                               //Get Staff Timings
                                StaffTimings(date_array,req.body.store_id,(err1,bookings)=>{
                                    //console.log("back");
                                    //console.log(bookings);
                                    if(!err1)
                                    {
                                      res.status(200).send({status:200, staffwise_bookings:bookings});
                                    }
                                    else
                                    {
                                        //res.status(500).send({message:err1});
                                        next(err1);
                                    }


                                });
                            } 
                            



                              });
                            }

                    
                               
                                   
                        });
                    }
          
                
            
                 });
                    
        
    
}


//Get Staff Timings
function StaffTimings(date_array,store_id,result){

    //j - to track date array length
       var j=0;
            date_array.filter((date_obj)=>{

                    //i- to track staff array length for each date
                    var i=0;

                    var store_req = {
                        store_id : store_id,
                        day_of_week: date_obj.day
                        } ;
    
                StoresModel.getStoreHours(store_req, (store_err, store_hrs) => {
                        if (store_err) {
                    
                            //res.status(500).send({message:store_err});
                            next(store_err);
                            
                        }
                        else{
                            //console.log(store_hrs);

                            if(store_hrs.length)
                            {

                                date_obj.business_hours= {
                                    open_time: store_hrs[0].store_hrwk_open_time,
                                    close_time: store_hrs[0].store_hrwk_close_time,
                                    offline:store_hrs[0].store_hrwk_all_day_note
                                }
                            }
                            else
                            {
                                date_obj.business_hours= {
                                    open_time: "",
                                    close_time: "",
                                    offline:1
                                }

                            }

                                                  
                        date_obj.staff.filter((obj) =>{
                                     if(!obj.bookings)
                                    {
                                            obj.bookings=[];
                                    }
                                    obj.date = date_obj.date;

                            var this_staff={
                                store_id : store_id,
                                staff_id: obj.staff_id,
                                date: date_obj.date,
                                day_of_week: date_obj.day
                                } ;
                                //console.log(this_staff);

                                StaffModel.getStaffHourExceptions(this_staff, (err, excp_obj) => {
                                if (err) {
                            
                                    //res.status(500).send({message:err});
                                    next(err);
                                    
                                }
                                else{

                                    excp_obj = JSON.parse(JSON.stringify(excp_obj));
                                    
                                    //console.log(excp_obj);
                                    
                                    if(excp_obj.length != 0)
                                    {
                                        //console.log(" exceptions");

                                            obj.staff_hours= {
                                        //type:"exp",
                                        open_time: excp_obj[0].user_hrex_open_time,
                                        close_time: excp_obj[0].user_hrex_close_time,
                                        offline:excp_obj[0].user_hrex_all_day_indicator

                                        }
                                        i++;
                                        //console.log(i);console.log(obj);console.log(date_obj.staff.length);

                                        if(i ==date_obj.staff.length){
                                            j++;
                                            if(j == date_array.length){
                                                //console.log("j" ` ${j}`);
                                                return result(null,date_array);}
                                              
                                            }
                                                

                                    }
                                    else{
                                                          
                                        StaffModel.getStaffHours(this_staff, (err, hrs_obj) => {
                                            if (err) {
                                        
                                                //res.status(500).send({message:err});
                                                next(err);
                                                
                                            }
                                            else{
                                                //console.log("hours into");

                                                if(hrs_obj.length)
                                                {

                                                    hrs_obj = JSON.parse(JSON.stringify(hrs_obj));
                                                    
                                                    //console.log(hrs_obj);
                                                    

                                                    obj.staff_hours= {
                                                                    //type:"hrs",
                                                                    open_time: hrs_obj[0].user_hrwk_open_time,
                                                                    close_time: hrs_obj[0].user_hrwk_close_time,
                                                                    offline: hrs_obj[0].user_hrwk_all_day_note

                                                                }
                                                }
                                                else
                                                {
                                                    obj.staff_hours= {
                                                        //type:"hrs",
                                                        open_time: "",
                                                        close_time: "",
                                                        offline: 1
                                                    }

                                                }
                                                            i++;

                                                            if(i ==date_obj.staff.length){
                                                               // console.log(date_obj.staff); 
                                                                j++;
                                                                if(j == date_array.length){
                                                                    return result(null,date_array);
                                                                }
                                                                   
                                                            }                               
                                            }
                                        })
                                    }
                                }
                           })
                       
                        })
                    }
               })
            })

}


async function getOldBookings(old_book_id,book_ids_array){

    while(old_book_id != 0)
    {
        book_ids_array.push(old_book_id);
        //console.log(old_book_id);
        //console.log("in while loop");

        try{

            var old_book_data = await BookingModel.getOldBookings(old_book_id);
            // console.log(old_book_data);
            // console.log(book_ids_array);

            for(let i=0;i<old_book_data.length;i++)
            {
               if(book_ids_array.indexOf(old_book_data[i].book_id) < 0)
               {
                   //console.log(old_book_data[i].book_id);
                   book_ids_array.push(old_book_data[i].book_id);
               }
               //console.log(i);
                
            }
             //console.log("for loop completed");

            old_book_id = old_book_data[0].book_oldbookid;
        }
        catch(error){
            throw error;

        }
        
    }

    //console.log("old book loop completed");
    //console.log(book_ids_array);
    return book_ids_array;

}



exports.getBookingInvoice = (req, res, next) => {
    if((!req.body.book_id) || (!req.body.book_type) || (!req.body.multi_service_book_id && req.body.multi_service_book_id !=0)) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var booking_info = {
        booking_details: {},
        product_bookings:{},
        total_payment_book_trans_paid_now: 0
    }

    

    if(req.body.book_type == "service")
    {
  
      
        // BookingModel.getDetailsOfBooking(req.body.book_id, (err, book_details) => {
        //     if (err) {
        //         //console.log(err);
        //         res.status(500).send({ message:err});
        //     }
        //     else{
        //         //console.log(book_details);

                //booking_info.service_bookings ={};

               //booking_info.book_id = book_details[0].book_id;
                
                
                if(req.body.multi_service_book_id != 0)
                {
                    var book_req_id = req.body.multi_service_book_id;
                }
                else
                {
                    var book_req_id = req.body.book_id;

                }

                BookingModel.getServiceBookingInvoiceDetails(book_req_id, async (err, book_data) => {
                    if (err) {
                        //console.log(err);
                        //res.status(500).send({ message:err});
                        next(err);
                    }
                    else{

                        
                        //var book_data = await BookingModel.getServiceBookingInvoiceDetails(book_req_id);
                        //console.log(booking_info);

                        booking_info.booking_details  = book_data;
                        booking_info.payment   = [];

                        try{
                        
                            var trans_book_ids = await getInvoiceDetails(book_data);
                        }
                        catch(error){

                            var invoice_error = error;
                            //console.log("this is error-"+error);
                        }

                            //booking details and transactions including details of old bookings

                        if(!invoice_error)
                        {
                            //booking_info.booking_details.payment =  await 
                            BookingModel.getBookPaidInvoice(trans_book_ids, (err, transactions_data) => {
                                        if (err) {
                                            //console.log(err);
                                            //res.status(500).send({ message:err});
                                            next(err);
                                        }
                                        else{
                                            
                                            //console.log(transactions_data);
                                            booking_info.payment = transactions_data[0];
                                            booking_info.total_payment_book_trans_paid_now += transactions_data[0].total_book_trans_paid_now;

                                            ProductsModel.getProductsInvoiceDetails(book_req_id, async (products_err, products_data) => {
                                                if (products_err) {
                                                    console.log(err);
                                                    next(products_err);
                                                }
                                                else{
                                                    //console.log("products_data: ", products_data);
                                                    if(products_data.length > 0)
                                                    {
                                                        booking_info.product_bookings.booking_details = products_data;
                                                        var booking_product_ids = [];
                                                        for(let i=0; i<products_data.length; i++)
                                                        {
                                                            booking_product_ids.push(products_data[i].booking_product_id);


                                                        }
                                                        //console.log("booking_product_ids: ",booking_product_ids);

                                                        ProductsModel.getOldProductBookings(booking_product_ids, (product_old_book_err, product_old_book_data) => {
                                                            if (product_old_book_err) {
                                                                next(product_old_book_err);
                                                            }
                                                            else{
//                                                                console.log("product_old_book_data: ",product_old_book_data);
                                                                var product_trans_book_ids = [];
    
                                                                for(let i=0; i< product_old_book_data.length; i++)
                                                                {
                                                                    product_trans_book_ids.push(product_old_book_data[i].booking_product_id);
    
                                                                }
    
                                                                //console.log("product trans book ids: ", product_trans_book_ids);

                                                                ProductsModel.getProductBookPaidInvoice(product_trans_book_ids, (product_invoice_err, product_transactions_data) => {
                                                                    if (product_invoice_err) {
                                                                        //console.log(err);
                                                                        
                                                                        next(product_invoice_err);
                                                                    }
                                                                    else{
                                                                        
                                                                        //console.log("product_transactions_data: ", product_transactions_data);
                                                                        booking_info.product_bookings.payment = product_transactions_data[0];
                                                                        booking_info.total_payment_book_trans_paid_now += product_transactions_data[0].total_book_trans_paid_now;
                            
                                                                                res.status(200).send({status:200, result:booking_info});
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                    else
                                                    {
                                                        res.status(200).send({status:200, result:booking_info});

                                                    }
                                                }
                                            })
                                        }
                            })
                        }
                        else
                        {
                            //res.status(500).send({status:500,result:{ message:invoice_error}});
                            next(invoice_error);
                        }
                          
                    }
                })
           // }
        //})
    }
    else
    {
        //for future class bookings
        res.status(200).send({status:200, result:[]});
    }

}



async function getInvoiceDetails(book_data){
             
            var trans_book_ids = [];
            

            for(let j=0; j<book_data.length; j++)
            {
                   //console.log(j);console.log(book_data[j]);
                   var all_book_ids = [book_data[j].book_id];

                try{
                     all_book_ids = await getOldBookings(book_data[j].book_oldbookid,all_book_ids);
                }
                catch(error){
                    throw error;
                   
                 }

                 //console.log(all_book_ids);
                 trans_book_ids.push(all_book_ids)
            }
            //console.log("for loop completed");
            //console.log(trans_book_ids);
            
                
              return trans_book_ids;


}
//Get Bookings for Calendar
exports.getMonthBookingsCount = (req, res, next) => {

    if(!req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var today_date = new Date(); 
    var today = today_date.toISOString().slice(0,10);

    if(req.body.start_date && req.body.end_date && (req.body.start_date > req.body.end_date))
    {
      //given start date > end date, then return error
      return res.status(400).send({message: 'Invalid request parameters. Given end_date must be greater than start_date.'});
      
    }
    else if(!req.body.start_date && !req.body.end_date)
    {

      //if dates are not given , then start date = end date = today
      req.body.start_date = req.body.end_date = today;
     
    }
    else if(req.body.start_date && !req.body.end_date)  
    {
      //only start date is given

        if(req.body.start_date <= today)
        {
           // start date <= today , then end date = today
            req.body.end_date = today;
        }
        else
        {
            //start date > today, then end date = start date + 1 day
            var start = new Date(req.body.start_date);
            start.setDate(start.getDate() + 1);
            req.body.end_date = start.toISOString().slice(0,10);
        }
     
    }
    else if(req.body.end_date && !req.body.start_date)
    {
        //only end date is given

        if(req.body.end_date >= today)
        {
          // end date >= today, then start date = today
            req.body.start_date = today;
        }
        else
        {
            // end date < today , then send error
            return res.status(400).send({message: 'Invalid request parameters. End date must be greater than start date.'});
           
        }
    
       
    }
    BookingModel.getMonthBookingsCountModel(req.body, (err, result) => {
        if (err) {
            //res.status(500).send({message:err});
            next(err);
        }
        else{
            res.status(200).send({status:200, bookings_count:result});
        }
    
    })
}



exports.getCustomerBookingDetails = (req, res, next) => {
    if(!(req.body.bookings)) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }
        var bookings = req.body.bookings;
        var service_book_ids = [];
        var class_book_ids = [];
        var product_book_ids = [];

        var cust_booking_details =[];
     
        var have_multi_book_id = false;
        var edit_flag = 0;

          for(let i=0; i<bookings.length; i++)
          {
             if(bookings[i].type == constants.SERVICE_TYPE)
             {
                service_book_ids.push(bookings[i].booking_id); 
             }

          }
          //console.log("service_book_ids: ",service_book_ids);


        BookingModel.getServiceBookingDetails(service_book_ids,have_multi_book_id, edit_flag, (err, services_data) => {
            if (err) {
                next(err);
            }
            else{
                //console.log(services_data);

                /////Start
                if(services_data.length)
                {
                    async.eachSeries(services_data,async (service,callback)=>{
                        let book_ids =[],paid_amount=0;
                        try{
                            book_ids = await getOldBookings(service.book_id,book_ids);
                            paid_amount = await TransactionModel.getAmount(book_ids);

                            var serv_booking_obj= {
                                booking_id: service.book_id,
                                type: constants.SERVICE_TYPE,
                                service_name: service.serv_name,
                                service_code: service.serv_id,
                                staff_id: service.user_id,
                                staff_name: service.staff_first_name+ " "+service.staff_last_name,
                                date: service.book_start_date,
                                start_time: service.book_start_time,
                                end_time: service.book_end_time,
                                duration: service.book_duration,
                                service_price: service.book_fullamount,
                                deposit: service.book_deposit,
                                staff_note: service.book_staff_note,
                                multi_service_book_id: service.multi_service_book_id,
                                is_promo_service:service.is_promo_service,
                                serv_promo_updated_date:service.serv_promo_updated_date,
                                serv_promo_created_date:service.serv_promo_created_date,
                                serv_postbuffertime:service.book_postbuffertime,
                                serv_prebuffertime:service.book_prebuffertime,                            
                                book_discount_full_amount:service.book_discount_full_amount,
                                book_discount_deposit_amount:service.book_discount_deposit_amount,
                                paid_amount: paid_amount[0].paid_amount,
                                payment_type1: paid_amount[0].payment_type1,
                                payment_type2: paid_amount[0].payment_type2,
                            };
                            
                            cust_booking_details.push(serv_booking_obj);
                            //callback();
                        }catch(error){
                            throw error;
                        }                            
                        
                    },function(err){
                        if(err){
                            next(err)
                        }else{

                            
                            BookingModel.getProductDetails(service_book_ids, edit_flag, (err, products_data) => {
                                if (err) {
                                    next(err);
                                }
                                else{
                                    //console.log("products_data:", products_data);
                                    var product_bookings = [];
                                    //cust_booking_details.product_bookings = []
                                    if(products_data.length)
                                    {
                                        async.each(products_data, (product,callback)=>{
                                            let booking_product_ids_array =[] 
                                            //product_paid_amount=0;

                                            var temp_old_book_product_id = product.booking_product_id;

                                            if(product.booking_old_product_id != 0){
                                                temp_old_book_product_id = product.booking_old_product_id;
                                            }

                                            ProductsModel.getOldProductBookings(temp_old_book_product_id, (old_err, old_data) => {
                                                if (old_err) {
                                                    next(old_err);
                                                }
                                                else{
                                                  // console.log("product old data: ",old_data);
                                                    for(let i = 0; i< old_data.length; i++)
                                                    {
                                                        booking_product_ids_array.push(old_data[i].booking_product_id);
                                                    }
                                                    //console.log("boooking product ids :", booking_product_ids_array);
            
                                                    ProductsModel.getProductBookPaidInvoice(booking_product_ids_array, (invoice_err, invoice_data) => {
                                                        if (invoice_err) {
                                            
                                                            next(invoice_err);
                                                            return;
                                                        }
                                                        else{
            //                                                console.log("invoice_data: ",invoice_data);
                                                            var product_booking_obj= product;
                                                             product_booking_obj.product_paid_amount = invoice_data[0].total_book_trans_paid_now;

                                                            product_bookings.push(product_booking_obj);
                                                            callback();
                                             

                                                        }
                                                    })
                                                }
                                            })
                    
                                                        
                                                                    
                                            
                                        },function(err){
                                            if(err){
                                                next(err)
                                            }else{
                                                //console.log("in product bookings");
                                                //cust_booking_details[0].product_bookings = product_bookings;
                                                //console.log("product_bookings: ", product_bookings);
                                                //console.log("bookings: ", cust_booking_details);
                                                res.status(200).send({status:200, result:{service_bookings:cust_booking_details, product_bookings:product_bookings}});
                                            }
                                        })
                                    }
                                    else
                                    {
                                        res.status(200).send({status:200, result:{service_bookings:cust_booking_details, product_bookings: []}});
                                    }
                                }
                
                            }); 


                            //res.status(200).send({status:200, result:cust_booking_details});
                        }
                    })
                }

                //////End
               
            }

        }); 
    

  
}


//Add Customer Booking
exports.addCustomerBookings = async (req, res,next) => {
    let buffer_times;    
    // Validate request
    if(!Object.keys(req.body).length) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }
    var cust_email = req.body.book_email;
    cust_email = cust_email.trim().toLowerCase();

    var cust_req= {
        store_id:req.body.store_id,
        cust_email: cust_email 
    }


    CustomerModel.getOwnerCustIdByEmail(cust_req, async (cust_err, cust_data) => {
        if (cust_err) {
          next(cust_err);

        }
        else{   
                //console.log(cust_data);
                var this_cust = {};

                if(cust_data.length > 0)
                {
                    this_cust = cust_data[0];

                    var owner_cust_id = cust_data[0].cust_id;
                   
                }
                else
                {
                    try{
                        this_cust = await addOwnerCustFromCust(req.body.store_id, cust_email);
                        var owner_cust_id = this_cust.owner_cust_id;
                    }
                    catch(err){
                        console.log("catch err: ",err);
                        next(err);


                    }

           

                }
                // console.log("owner_cust_id: ", owner_cust_id);

                // console.log("this cust: ", this_cust);

                    var book_created_date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    //console.log(book_created_date);
                    var book_updated_date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');

                    
                    
                    switch(req.body.booking_type)
                    {
                    

                        case constants.ADD_SERVICE_BOOKING:
                            //console.log("add service");
                            if(req.body.is_unpaid == 1)
                            {
                                var book_status = constants.STATUS_BOOKED;
                            }
                            else
                            {
                                var book_status = constants.STATUS_PRE_BOOKED;
                            }

                            var bookings = req.body.bookings;
                            var services = [];
                            var classes = [];
                            var products = [];
                            var booking_array =[];

                            bookings.sort((a,b)=>{
                                var a_start = a.start_date+" "+a.start_time;
                                var b_start = b.start_date+" "+b.start_time;

                                return new Date(a_start) - new Date(b_start);


                            })
                            //console.log("sorted bookings : ",bookings);
                            
                            for(let i=0; i<bookings.length; i++)
                            {
                                //console.log(" ---- for loop start ----- ");
                                
                                switch(bookings[i].type)
                                {
                                    case constants.SERVICE_TYPE:
                                        var key = bookings[i].start_date;
                                         if((!bookings[i].serv_prebuffertime && bookings[i].serv_prebuffertime!=0)  || (!bookings[i].serv_postbuffertime && bookings[i].serv_postbuffertime!=0)){
                                            try{
                                                buffer_times = await ServicesModel.getServiceDetailsModel(bookings[i].service_code);
                                                buffer_times = buffer_times[0];
                                            }catch(err){
                                                res.send(err);
                                            }
                                        }else{
                                            buffer_times =
                                                {
                                                    serv_prebuffertime:parseInt(services[i].serv_prebuffertime),
                                                    serv_postbuffertime:parseInt(services[i].serv_postbuffertime)
                                                };
                                        }
                                        var this_booking =  [req.body.store_id, bookings[i].service_code , bookings[i].service_category, bookings[i].user_id , bookings[i].user_staff_type_id ,
                                            bookings[i].start_date, bookings[i].end_date, bookings[i].start_time , bookings[i].end_time,
                                            bookings[i].duration , bookings[i].service_price, bookings[i].deposit, owner_cust_id,
                                            this_cust.cust_first_name, this_cust.cust_last_name ,cust_email,this_cust.cust_country_isd_code,
                                            this_cust.cust_mobile , book_status,bookings[i].staff_note,owner_cust_id, book_created_date, book_updated_date,
                                            req.body.booked_through,"0","0","0","0",buffer_times.serv_prebuffertime,buffer_times.serv_postbuffertime,"-1","-1"];

                                            if(!booking_array[key])
                                            {
                                                booking_array[key] = [];

                                            }
                                    
                                        booking_array[key].push(this_booking);
                                    break;
                            
                                }
                                //console.log("i - ",i);
                                //console.log("booking_array - ",booking_array);
                            }

                            //console.log("final booking array --" ,booking_array);
                            var dates_array = Object.keys(booking_array);
                            //console.log("dates array " ,dates_array);

                            var m=0;
                            var return_ids_obj = [];
                            var return_book_ids = [];
                            
                            for(let i=0;i<dates_array.length;i++)
                            {
                                var booking_date = dates_array[i];
                                // console.log("booking date -- ",booking_date);
                                // console.log("i - ",i);
                                // console.log("m - ",m);
                                //console.log("booking array[i] -- ",booking_array[booking_date]);

                                
                                //Insert booking in db
                                BookingModel.addBooking(booking_array[booking_date], (err, data) => {
                                    if (err) {
                                    next(err);
                        
                                    }
                                    else{   
                                        //console.log(data);

                                        NotificationsModel.getNotificationSettings(req.body.store_id, (notif_sett_err, notif_sett_data) => {
                                            if (notif_sett_err) {
                                              
                                              next(notif_sett_err);
                                
                                            }
                                            else{
                                
                                                // console.log("notif_sett_data: ", notif_sett_data);
                                                // console.log("data: ", data);
                                                var form_url_exist = 0;
                                                var form_url = '';
                    

                                                var ids_array = [];
                                                var first_id = data.insertId;
                    
                                                if(notif_sett_data[0].form_id !== 'none')
                                                {
                                                    form_url_exist = 1;
                                                    form_url = notif_sett_data[0].form_url + "/" + first_id ;
                    
                                                }
                                                //console.log("form_url: ",form_url);
                    

                                        
                                            for(let i=0; i<data.affectedRows; i++)
                                            {
                                                var id = data.insertId + i;
                                                ids_array.push(id);
                                                return_book_ids.push(id);
                                                if(data.affectedRows>1)
                                                {           
                                                    return_ids_obj.push({booking_id:id, multi_service_book_id:first_id, type:constants.SERVICE_TYPE});
                                                }
                                                else
                                                {
                                                    return_ids_obj.push({booking_id:id, multi_service_book_id:0, type:constants.SERVICE_TYPE});
                                                }
                                            }
                                            //console.log("ids array : ",ids_array);
                        
                        
                                            var notif_details = {}
                                                            notif_details.book_id = first_id;
                                                            notif_details.cust_id = owner_cust_id;
                                                            notif_details.cust_name = this_cust.cust_first_name+" "+this_cust.cust_last_name;
                                                            notif_details.cust_email = cust_email;
                                                            notif_details.cust_mobile = this_cust.cust_country_isd_code.toString()+this_cust.cust_mobile.toString();
                                                            notif_details.store_id = req.body.store_id; 
                                                            notif_details.this_base_url = /*req.protocol + */ "https://" + req.hostname; // until fix the protocal issue we directly use https
                                                            notif_details.email_base_url = req.hostname;
                                                            notif_details.book_created_by = owner_cust_id;
                                                            notif_details.email_send = 1;
                                                            notif_details.sms_send = 1;
                                                        
                                                            //console.log("notif details: ",notif_details);
                        
                                                            //console.log(" m:", m);
                                        if(data.affectedRows>1)
                                        {
                        
                                            BookingModel.updateMultiserviceBookId(first_id, ids_array, "0", (err2,data2) => {
                                                if (err2) {
                                                next(err2);
                            
                                                }
                                                else{
                                                    //console.log("data2: ",data2);
                                                //return_ids_obj.push({book_id:first_id,multi_book_id:first_id});

                                                
                                                
                                                //Products Start
                                                
                                                if(m == 0){
                                                     //console.log("return book ids : ",return_book_ids);
                                                    
                                                    if(req.body.products && req.body.products.length > 0){
                                                        let cust_details = {
                                                            store_id:req.body.store_id,
                                                            cust_id : owner_cust_id,
                                                            book_first_name : this_cust.cust_first_name,  
                                                            book_last_name : this_cust.cust_last_name,
                                                            book_email : cust_email,
                                                            book_country_isd_code : this_cust.cust_country_isd_code,
                                                            book_mobile : this_cust.cust_mobile, 
                                                            book_status : book_status,
                                                            book_created_by: owner_cust_id,
                                                            book_created_date: book_created_date, 
                                                            book_updated_date: book_updated_date,
                                                            booked_through: req.body.booked_through
                                                        };
                                                        //console.log("return_book_ids 1: ",return_book_ids);
                            
                                                        insertProducts(req.body.products, return_book_ids[0], cust_details);                            
                                                    }
                                                }
                                                //products End
                        
                                                        if(constants.PROD_FLAG == 1 && book_status == constants.STATUS_BOOKED)
                                                        {
                                                                notif_details.have_multi_id = 1;
                        
                                                                NotificationsLibrary.notification_details(notif_details, function(notif_err,notif_data){
                                                                    if (notif_err) {
                                                        
                                                                        console.log("error from notifications");
                                                                        console.log(notif_err)
                                                                        ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
                                                                            console.log("Slack alert sent");
                                                                        })
                                                                        .catch((slack_err) => {
                                                                            console.log(slack_err);
                                                                        });
                                                                        
                                                
                                                                    }
                                                                    else{
                                                                        console.log("Notifications success");
                                    
                                                                    }
                                                                })
                                                        }
                                                        m++;
                                                        //console.log("m - ",m);
                                                            if(form_url_exist == 1)
                                                            {

                                                                BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                                    if (form_err) {
                                                        
                                                                        next(form_err);
                                                                    }
                                                                    else{
                                                                        //console.log(form_data);

                                                                        if(m == dates_array.length)
                                                                        {
                                                                            //console.log("response1");
                                                                            m++;
                                                                                res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully inserted bookings`}});
                                                                        }
                                                                    }
                                                                })
                                                            }
                                                            else
                                                            {
                                                                if(m == dates_array.length)
                                                                {
                                                                    //console.log("response2");
                                                                        res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully inserted bookings`}});
                                                                }
                                                            }
                                                }
                                            })
                                        }
                                        else
                                        {
                                            //Products Start
                                            //console.log("products m:", m);
                                                                                   
                                                                                    if(m == 0){
                                                                                         //console.log("return ids array2 : ",return_book_ids);
                                                                                        
                                                                                        if(req.body.products && req.body.products.length > 0){
                                                                                            let cust_details = {
                                                                                                store_id:req.body.store_id,
                                                                                                cust_id : owner_cust_id,
                                                                                                book_first_name : this_cust.cust_first_name,  
                                                                                                book_last_name : this_cust.cust_last_name,
                                                                                                book_email : cust_email,
                                                                                                book_country_isd_code : this_cust.cust_country_isd_code,
                                                                                                book_mobile : this_cust.cust_mobile, 
                                                                                                book_status : book_status,
                                                                                                book_created_by: owner_cust_id,
                                                                                                book_created_date: book_created_date, 
                                                                                                book_updated_date: book_updated_date,
                                                                                                booked_through: req.body.booked_through
                                                                                            };
                                                                
                                                                                            insertProducts(req.body.products, return_book_ids[0],cust_details);                            
                                                                                        }
                                                                                    }
                                                                                    //products End
                                                            
                                            //return_ids_obj.push({book_id:first_id,multi_book_id:0});
                                            if(constants.PROD_FLAG == 1 && book_status == constants.STATUS_BOOKED)
                                            {
                                            
                                                notif_details.have_multi_id = 0;
                                                
                                            
                                                NotificationsLibrary.notification_details(notif_details, function(notif_err,notif_data){
                                                        if (notif_err) {
                                            
                                                            console.log("error from notifications");
                                                            console.log(notif_err)
                                                            ErrorLogs.send_to_slack(notif_err).then((slack_result) => {
//                                                                console.log("Slack alert sent");
                                                            })
                                                            .catch((slack_err) => {
                                                                console.log(slack_err);
                                                            });
                                                            
                                    
                                                        }
                                                        else{
//                                                            console.log("Notifications success");
                        
                                                        }
                                                    })
                                            }
                                            m++;
                                            
                                            //console.log("m - ",m);
                                            if(form_url_exist == 1)
                                            {

                                                BookingModel.updateBookingFormUrl(form_url, ids_array, (form_err,form_data) => {
                                                    if (form_err) {
                                        
                                                        next(form_err);
                                                    }
                                                    else{
                                                        //console.log(form_data);
                                                        
                                                        if(m == dates_array.length)
                                                        {
                                                            //console.log("response");
                                                            m++;
                                                                res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully inserted bookings`}});

                                                        }
                                                    }
                                                })
                                            }
                                            else
                                            {
                                                if(m== dates_array.length)
                                                {
                                                    //console.log("response4");
                                                    res.status(200).send({status:200, result:{ bookings: return_ids_obj , message: `Successfully inserted bookings`}});
                                                }
                                            }
                                        }
                                    }
                                })
                        
                                    }
                            });
                            }
                    

                        break;


                    }

                //}
            }

        })
    }

exports.getCustomerEachBookingDetails = (req, res, next) => {
    var cust_req = {
        book_id : parseInt(req.body.book_id,10),
        multi_service_book_id : parseInt(req.body.multi_service_book_id,10),
        type : req.body.type
    }
    if(!req.body.book_id || req.body.multi_service_book_id == null || isNaN(cust_req.multi_service_book_id) || !req.body.type) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }
        var service_book_ids = [];
        var class_book_ids = [];
        var product_book_ids = [];
        var cust_booking_details =[];     
        var have_multi_book_id = false;
        var edit_flag = 0;
        if(cust_req.multi_service_book_id > 0 && !isNaN(cust_req.multi_service_book_id)){
            have_multi_book_id = true;
            service_book_ids.push(cust_req.multi_service_book_id);
        }else{
            service_book_ids.push(cust_req.book_id);
        }
        if(req.body.type == constants.SERVICE_TYPE){            
            BookingModel.getServiceBookingDetails(service_book_ids,have_multi_book_id, edit_flag, (err, services_data) => {
                if (err) {
                    next(err);
                }
                else{
                    //console.log(services_data);
                    if(services_data.length)
                    {
                        async.each(services_data,async (service,callback)=>{
                            let book_ids =[],paid_amount=0;
                            try{
                                book_ids = await getOldBookings(service.book_id,book_ids);
                                paid_amount = await TransactionModel.getAmount(book_ids);
                                var serv_booking_obj= {
                                booking_id: service.book_id,
                                book_status: service.book_status,
                                cust_name: service.name,
                                cust_id: service.cust_id,
                                cust_country_isd_code: service.book_country_isd_code,
                                cust_mobile: service.book_mobile,
                                cust_email: service.book_email,
                                type: constants.SERVICE_TYPE,
                                store_id: service.store_id,
                                serv_id: service.serv_id,
                                serv_cat_id: service.serv_cat_id,
                                serv_name: service.serv_name,
                                book_oldbookid:service.book_oldbookid,
                                book_first_name: service.book_first_name,
                                book_last_name: service.book_last_name,
                                serv_description: service.serv_description,
                                serv_prebuffertime: service.serv_prebuffertime,
                                serv_postbuffertime: service.serv_postbuffertime,
                                book_prebuffertime: service.book_prebuffertime,
                                book_postbuffertime: service.book_postbuffertime,
                                staff_id: service.user_id,
                                staff_name: service.staff_first_name,
                                slot_date: service.book_start_date,
                                slot_start_time: service.book_start_time,
                                slot_end_time: service.book_end_time,
                                book_duration: service.book_duration,
                                serv_defdur: service.serv_defdur,
                                book_fullamount: service.book_fullamount,
                                book_deposit: service.book_deposit,
                                serv_saleprice: service.serv_saleprice,
                                serv_deposit: service.serv_deposit,
                                paid_amount: paid_amount[0].paid_amount,
                                payment_type1: paid_amount[0].payment_type1,
                                payment_type2: paid_amount[0].payment_type2,
                                staff_note: service.book_staff_note,
                                multi_service_book_id: service.multi_service_book_id,
                                serv_promo_created_date:service.serv_promo_created_date,
                                serv_promo_updated_date:service.serv_promo_updated_date,
                                is_promo_service:service.is_promo_service,
                                book_discount_full_amount:service.book_discount_full_amount,
                                book_discount_deposit_amount:service.book_discount_deposit_amount                                
                                }
                            cust_booking_details.push(serv_booking_obj);
                            }catch(error){
                                throw error;
                            }                            
                            
                        },function(err){
                            if(err){
                                next(err)
                            }else{

                                
                                BookingModel.getProductDetails(service_book_ids, edit_flag, (err, products_data) => {
                                    if (err) {
                                        next(err);
                                    }
                                    else{
                                        //console.log("products_data:", products_data);
                                        var product_bookings = [];
                                        //cust_booking_details.product_bookings = []
                                        if(products_data.length)
                                        {
                                            async.each(products_data, (product,callback)=>{
                                                let booking_product_ids_array =[] 
                                                //product_paid_amount=0;

                                                var temp_old_book_product_id = product.booking_product_id;

                                                if(product.booking_old_product_id != 0){
                                                    temp_old_book_product_id = product.booking_old_product_id;
                                                }

                                                ProductsModel.getOldProductBookings(temp_old_book_product_id, (old_err, old_data) => {
                                                    if (old_err) {
                                                        next(old_err);
                                                    }
                                                    else{
                                                      // console.log("product old data: ",old_data);
                                                        for(let i = 0; i< old_data.length; i++)
                                                        {
                                                            booking_product_ids_array.push(old_data[i].booking_product_id);
                                                        }
                                                        //console.log("boooking product ids :", booking_product_ids_array);
                
                                                        ProductsModel.getProductBookPaidInvoice(booking_product_ids_array, (invoice_err, invoice_data) => {
                                                            if (invoice_err) {
                                                
                                                                next(invoice_err);
                                                                return;
                                                            }
                                                            else{
                //                                                console.log("invoice_data: ",invoice_data);
                                                                var product_booking_obj= product;
                                                                 product_booking_obj.product_paid_amount = invoice_data[0].total_book_trans_paid_now;
                                                                 product_booking_obj.payment_type1 = invoice_data[0].payment_type1;
                                                                 product_booking_obj.payment_type2 = invoice_data[0].payment_type2;
                                                                product_bookings.push(product_booking_obj);
                                                                callback();
                                                 

                                                            }
                                                        })
                                                    }
                                                })
                        
                                                            
                                                                        
                                                
                                            },function(err){
                                                if(err){
                                                    next(err)
                                                }else{
                                                    //console.log("in product bookings");
                                                    //cust_booking_details[0].product_bookings = product_bookings;
                                                    //console.log("product_bookings: ", product_bookings);
                                                    //console.log("bookings: ", cust_booking_details);
                                                    //console.log("response5");
                                                    res.status(200).send({status:200, result:{service_booking_details:cust_booking_details, product_booking_details:product_bookings}});
                                                }
                                            })
                                        }
                                        else
                                        {
                                            //console.log("response6");
                                            res.status(200).send({status:200, result:{service_booking_details:cust_booking_details, product_booking_details: []}});
                                        }
                                    }
                    
                                }); 


                                //res.status(200).send({status:200, result:cust_booking_details});
                            }
                        })
                    }
                }

            }); 
        }
 
}    

function insertProducts(products,book_id,cust_details){
    //console.log("cust_details:", cust_details);
    
    let product_array = [],this_product,update_product_array=[];
    for(let i=0; i<products.length; i++)
    {
        this_product =  [products[i].product_id,
            products[i].product_name,
            products[i].product_sku,
            products[i].product_barcode,
            products[i].product_image_url,
            products[i].product_thumbnail_url,
            cust_details.store_id,
            cust_details.cust_id,
            cust_details.book_first_name, cust_details.book_last_name ,cust_details.book_email,
            cust_details.book_mobile ,cust_details.book_country_isd_code,
            products[i].product_date,
            products[i].product_quantity,products[i].product_full_amount,  
            cust_details.book_status, "",cust_details.book_created_date,
            cust_details.book_created_by, cust_details.book_updated_date,"0",
            cust_details.booked_through,"0",book_id,"1","0"];              
        product_array.push(this_product);
        
        update_product_array.push([products[i].product_id,cust_details.store_id,products[i].total_product_quantity-products[i].product_quantity]); 
        //console.log("update_product_array: ", update_product_array);     
    }
    BookingModel.addProduct(product_array, (err, data) => {
        if (err) {
          console.log(err);
            ErrorLogs.send_to_slack(err).then((slack_result) => {
                console.log("Slack alert sent");
            })
            .catch((slack_err) => {
                console.log(slack_err);
            });
        }else{
            //console.log("product added data:", data);

            if(cust_details.book_status == constants.STATUS_BOOKED)
            {
                ProductsModel.updateProductQuantaty(update_product_array,(err,data)=>{
                    if(err){
                        console.log('error updating products')
                        ErrorLogs.send_to_slack(err).then((slack_result) => {
                            console.log("Slack alert sent");
                        })
                        .catch((slack_err) => {
                            console.log(slack_err);
                        });
                    }
                });
            }
        }
    });
}



function addOwnerCustFromCust(store_id, cust_email)
{
    return new Promise((resolve, reject) => {
        StoresModel.getStoreOwner(store_id, (store_err, store_data) => {
            if (store_err) {
            //next(store_err);
            reject(store_err);

            }
            else{ 
//                console.log("store_data: ",store_data);
                var owner_id = store_data[0].user_id;

                CustomerModel.getCustDetailsByEmail(cust_email, (cust_err1, cust_data1) => {
                    if (cust_err1) {
                        //next(cust_err1);
                        reject(cust_err1);
            
                    }
                    else{   
                        
//                            console.log(" create owner cust :", cust_data1);
                            var this_cust = cust_data1[0];

                            var cust_first_name = NameUpperCase(cust_data1[0].cust_first_name,"1");
                            var cust_last_name = NameUpperCase(cust_data1[0].cust_last_name,"1");
                        
                            var cust_created_date = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");



                            var new_cust_req = [ [owner_id, cust_first_name, cust_last_name, cust_email, cust_data1[0].cust_country_isd_code, cust_data1[0].cust_mobile, cust_data1[0].cust_password, cust_data1[0].cust_id, cust_created_date, "", ""] ];
//                            console.log("new cust req: ",new_cust_req);

                            CustomerModel.addOwnerCustomers(new_cust_req, (new_cust_err, new_cust_data) => {
                                if (new_cust_err) {
                                //next(new_cust_err);
                                reject(new_cust_err);
                        
                                }
                                else{  
                                    
                                    // console.log("new cust: ", new_cust_data);
                                    this_cust.owner_cust_id = new_cust_data.insertId;
                                    resolve(this_cust);
                        
                                    
                                }

                            })

                        }
                })
            }
       })
    })
}


function NameUpperCase(string,is_owner_array){

    string = string.trim();
    var first_char = string.charAt(0).toUpperCase();
    var string2 = string.slice(1);
    var output_string = first_char + string2;
    if(is_owner_array == "1")
    {
        output_string = output_string.replace(/"/g,'\\"')
        
    }
    else
    {
        output_string = output_string.replace(/'/g,"\\'")
        
    }
    //console.log(output_string);
    return output_string;
}


function editProductsBooking(products, multi_service_book_id, booking_details, result)
{

    
    var product_array = [];
    var product_quantity_update = [];
    var rescheduling_products = 0;

    
        var product_ids_update = [];

        var return_ids_obj = [];

        for(let i=0; i<products.length; i++)
        { 
            //console.log("i: ",i);
            if(products[i].booking_product_id)
            {
                rescheduling_products = 1;
                
                            
                            if(products[i].booking_old_product_id != 0)
                            {
                                var old_product_id = products[i].booking_old_product_id;
                            }
                            else
                            {
                                var old_product_id = products[i].booking_product_id;
                            }

                            //console.log("1i: ",i);

                            var update_quantity = products[i].remaining_product_quantity;
                            product_quantity_update.push([products[i].product_id,booking_details.store_id,update_quantity]);

                            
                        
                            var this_product =  [products[i].product_id, products[i].product_name, products[i].product_sku, products[i].product_barcode,
                                 products[i].product_image_url, products[i].product_thumbnail_url, booking_details.store_id, booking_details.cust_id,
                                 booking_details.book_first_name, booking_details.book_last_name ,booking_details.book_email, booking_details.book_mobile ,booking_details.book_country_isd_code,
                                 products[i].product_date, products[i].product_quantity, products[i].product_full_amount, constants.STATUS_BOOKED, "",booking_details.book_created_date,
                                 booking_details.book_created_by, booking_details.book_created_date,"0", booking_details.booked_through,"0",multi_service_book_id,"1",old_product_id];

                            product_array.push(this_product);

                                        
                                        // console.log("this_product: "+this_product); 

                                
                                product_ids_update.push(products[i].booking_product_id);

                        // console.log(booking_array);
                        // console.log("rescheduling-"); console.log(book_ids_update); console.log(update_multi_ids);
            }
            else
            {
                //new product booking
//                console.log("new product booking");
                var this_product =  [products[i].product_id, products[i].product_name, products[i].product_sku, products[i].product_barcode,
                                 products[i].product_image_url, products[i].product_thumbnail_url, booking_details.store_id, booking_details.cust_id,
                                 booking_details.book_first_name, booking_details.book_last_name ,booking_details.book_email, booking_details.book_mobile ,booking_details.book_country_isd_code,
                                 products[i].product_date, products[i].product_quantity, products[i].product_full_amount, constants.STATUS_BOOKED, "",booking_details.book_created_date,
                                 booking_details.book_created_by, booking_details.book_updated_date,"0", booking_details.booked_through,"0",multi_service_book_id,"1",0];

                product_quantity_update.push([products[i].product_id,booking_details.store_id,products[i].remaining_product_quantity]);   

                product_array.push(this_product);
            }

            //console.log("i"+i);
        }


        //Add Product Bookings
        BookingModel.addProduct(product_array, (err, data) => {
            if (err) {
              console.log(err);
                return result(err,null);
            }else{
//                console.log("added products data: ", data);

                var first_id = data.insertId;
                return_ids_obj.push({booking_product_id:first_id,multi_book_id:multi_service_book_id});
                ProductsModel.updateProductQuantaty(product_quantity_update,(err,data)=>{
                    if(err){
//                        console.log('error updating products')
                        return result(err,null);
                    }
                    else
                    {

                
           

                        if(rescheduling_products == 1)
                        {
                            //only added new products
                            
                            // console.log(ids_array);
                            // console.log(first_id);

                            var reschedule_req = {
                                booking_product_ids:product_ids_update,
                                book_status:constants.STATUS_RESCHEDULED,
                                updated_date:booking_details.book_created_date,
                                updated_by: booking_details.book_created_by,
                                updated_through: booking_details.booked_through,
                                multi_service_book_id : 0
                            }
                            //console.log(reschedule_req);
                    
                            //update rescheduled booking status and updated date
                            ProductsModel.productsRescheduleUpdate(reschedule_req, (err2, data2) => {
                                if (err2) {
                                
                                    return result(err2,null);
                                    
                                }
                                else{
                                    //console.log(data2);
                                    //return_ids_obj.push({book_id:first_id,multi_book_id:0});
                                    return result(null, return_ids_obj);                          

                                        
                                    }
                                })
                            

                        }
                        else
                        {
                            return result(null, return_ids_obj);

                        }
                    }
                    });

                    }
                });

}


function cancelProductBooking(product_details, result) {

    ProductsModel.getProductBookingDetails(product_details.booking_product_id, multi_id_flag, (product_err, product_data) => {
        if (product_err) {

            next(product_err);
        }
        else{
            if(product_data.length > 0)
            {

    var updated_date = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

    var multi_id_flag = 1;

    var product_req = {
        book_id: product_details.book_id,
        book_status: product_details.book_status,
        updated_by: product_details.updated_by,
        updated_date: updated_date,
        updated_through: product_details.updated_through,

    }
     
        ProductsModel.updateProductBookingStatus(product_req, multi_id_flag, (product_err, product_data) => {
            if (product_err) {

                next(product_err);
            }
            else{
//                console.log("product_data: ",product_data);
                
                if(product_data.length > 0)
                {
                if((product_details.book_status == constants.STATUS_CANCELLED || product_details.book_status == constants.STATUS_LATE_CANCELLED))
                {

                    
//                            console.log("product_data: ",product_data);
                            var old_product_req = [product_details.booking_product_id, product_data[0].booking_old_product_id];

                            var product_quantity_update = [[product_data[0].product_id, product_data[0].store_id, req.body.remaining_product_quantity]]; 
                            
                            ProductsModel.updateProductQuantaty(product_quantity_update,(update_err,update_data)=>{
                                if(update_err){
                                    console.log('error updating products')
                                    next(update_err);
                                }
                                else
                                {
                                  //  console.log("quantity update_data:", update_data);
                                         
                      
                        
                        var time_diff = moment.duration(moment(product_data[0].product_date).diff(moment())).asHours();
//                        console.log("time diff: ",time_diff);
                        //console.log(moment().format("YYYY-MM-dd HH:mm"));
                            if(time_diff >= 24)
                            {
                                //console.log("adding wallet");
                                let booking_product_ids_array =[];
                                

                                ProductsModel.getOldProductBookings(old_product_req, (old_err, old_data) => {
                                    if (old_err) {
                        
                                        next(old_err);
                                    }
                                    else{
                                        //console.log("product old data: ",old_data);
                                        for(let i = 0; i< old_data.length; i++)
                                        {
                                            booking_product_ids_array.push(old_data[i].booking_product_id);
                                        }
//                                        console.log("boooking product ids :", booking_product_ids_array);

                                        ProductsModel.getProductBookPaidInvoice(booking_product_ids_array, async (invoice_err, invoice_data) => {
                                            if (invoice_err) {
                                
                                                next(invoice_err);
                                            }
                                            else{
//                                                console.log("invoice_data: ",invoice_data);
                                                var paid_amount = invoice_data[0].total_book_trans_paid_now;
        
                                            
                                                //paid_amount = await TransactionModel.getAmount(book_ids);
                                                if(paid_amount > 0)
                                                {
                                                    let insert_wallet_data = [[
                                                        product_data[0].product_cust_id,
                                                        product_data[0].product_cust_email,
                                                        constants.WALLET_CREDIT,
                                                        paid_amount,
                                                        "added amount for cancellation of product "+req.body.booking_product_id,
                                                        product_data[0].store_id,
                                                        0,
                                                        req.body.booking_product_id
                                                        
                                                    ]];
//                                                    console.log("insert_wallet_data",insert_wallet_data);
                                                    try{
                                                        await TransactionModel.insert_transaction_and_wallet_credit(insert_wallet_data,[]);
                                                    }
                                                    catch(error)
                                                    {
                                                        var wallet_err = error;
                                                        console.log("catch Error: ", error);
                                                        //next(wallet_err);
                                                    }
                                                }
                                            }
                                        })

                                    }
                                })
                            }
                            
                                res.status(200).send({status:200, result:{message: "Product booking status updated successfully"}});
                            }
                        });
                   
   
            
            }
        }
    
      }
      })
    }
    }
    });
      
 }
 exports.updatePrices = (req,res,next)=>{
    if((!req.body.price && req.body.price!=0) || !req.body.type || !req.body.id || !req.body.discount_method){
        res.status(400).send({status:400,message:'check required parameters send properly or not'});
    }else{
        let price = req.body.price;
        //parseInt(req.body.price,10);
        let id = parseInt(req.body.id,10);
        let method = parseInt(req.body.discount_method,10);
        let type = req.body.type.toString();
        switch(type){
            case constants.SERVICE_TYPE:
                BookingModel.updatePriceModel(id,price,method,(err,data)=>{
                    if(err){
                        next(err);
                    }else{
                        res.status(200).send({status:200,results:"updated successfully"});
                    }
                });
                break;
            case constants.PRODUCT_TYPE:
                ProductsModel.updateProductPriceModel(id,price,method,(err,data)=>{
                    if(err){
                        next(err);
                    }else{
                        res.status(200).send({status:200,results:"updated successfully"});
                    }
                });
                break;
                
        }
    }
}
function productsAmountToWallet(data,time_diff,res,next){
    let insert_wallet_data=[];
    ProductsModel.getProductBookingDetails(data.book_id, 1, (product_err, product_data) => {
        if (product_err) {

            next(product_err);
        } else {
                                      // console.log("product_data: ",product_data);
            async.eachSeries(product_data,(obj,callback)=>{
    //                            console.log("product_data: ",product_data);
                var old_product_req = [obj.booking_product_id, obj.booking_old_product_id];

                var product_quantity_update = [[obj.product_id, obj.store_id, (obj.remaining_product_quantity + obj.product_quantity)]];

                ProductsModel.updateProductQuantaty(product_quantity_update, (update_err, update_data) => {
                    if (update_err) {
    //                                    console.log('error updating products')
                        next(update_err);
                    } else
                    {
                        //console.log('updating product quantity: ',product_quantity_update);
                            if(time_diff >= 24){
                                //console.log("adding wallet");
                                let booking_product_ids_array = [], temp_old_book_product_id;
                                if (old_product_req[1]) {
                                    temp_old_book_product_id = old_product_req[1];
                                } else {
                                    temp_old_book_product_id = old_product_req[0];
                                }
                                ProductsModel.getOldProductBookings(temp_old_book_product_id, (old_err, old_data) => {
                                    if (old_err) {
                                        next(old_err);
                                    } else {
        //                                        console.log("product old data: ",old_data);
                                        for (i = 0; i < old_data.length; i++)
                                        {
                                            booking_product_ids_array.push(old_data[i].booking_product_id);
                                        }
        //                                        console.log("boooking product ids :", booking_product_ids_array);

                                        ProductsModel.getProductBookPaidInvoice(booking_product_ids_array, async (invoice_err, invoice_data) => {
                                            if (invoice_err) {

                                                next(invoice_err);
                                                return;
                                            } else {
        //                                                console.log("invoice_data: ",invoice_data);
                                                var paid_amount = invoice_data[0].total_book_trans_paid_now;


                                                //paid_amount = await TransactionModel.getAmount(book_ids);
                                                if (paid_amount > 0)
                                                {
                                                    insert_wallet_data.push([
                                                            product_data[0].product_cust_id,
                                                            product_data[0].product_cust_email,
                                                            constants.WALLET_CREDIT,
                                                            paid_amount,
                                                            "added amount for cancellation of product " + obj.booking_product_id,
                                                            product_data[0].store_id,
                                                            0,
                                                            obj.booking_product_id

                                                        ]);
    //                                                        console.log("insert_wallet_data",insert_wallet_data);
                                                }
                                            }
                                            callback();
                                        })

                                    }
                                })
                                
                            }
                            else
                            {
                                callback();
                            }
                    }
                });
                
            },function(err){
                if(err){
                    next(err.toString())
                }else{
                    TransactionModel.insert_transaction_and_wallet_credit(insert_wallet_data, []);
                    res.status(200).send({status:200,results:"updated successfully"});
                }
            })
        }
    })
}

function rescheduleProducts(new_multi_serv_id, products, general_data) {
    return new Promise((resolve, reject) => {
        var book_created_date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var book_updated_date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var booking_product_ids_array = [], product_quantity_update = [], update_products = [], product_array = [];
        var add_product_quantity_update = [];

        for (i = 0; i < products.length; i++)
        {
//            console.log("i: ",i);
            if (products[i].booking_product_id)
            {
                booking_product_ids_array.push(products[i].booking_product_id);

                //update product quantity
                var update_quantity = products[i].remaining_product_quantity;
                product_quantity_update.push([products[i].product_id, general_data.store_id, update_quantity]);
                update_products.push([products[i].booking_product_id, products[i].product_quantity, new_multi_serv_id]);

            } else
            {
                //new product booking
//                console.log("new product booking");
                this_product = [products[i].product_id, products[i].product_name, products[i].product_sku, products[i].product_barcode,
                    products[i].product_image_url, products[i].product_thumbnail_url, general_data.store_id, general_data.cust_id,
                    general_data.book_first_name, general_data.book_last_name, general_data.book_email, general_data.book_mobile, general_data.book_country_isd_code,
                    products[i].product_date, products[i].product_quantity, products[i].product_full_amount, constants.STATUS_BOOKED, "", book_created_date,
                    general_data.book_created_by, book_updated_date, "0", general_data.booked_through, "0", new_multi_serv_id, "1", 0];
                product_array.push(this_product);

                add_product_quantity_update.push([products[i].product_id, general_data.store_id, products[i].remaining_product_quantity]);


            }

            //console.log("i"+i);
        }

        if (update_products.length > 0)
        {

            var details_req = {
                booking_product_ids: booking_product_ids_array,
                updated_by: general_data.book_updated_by,
                updated_date: book_updated_date,
                updated_through: general_data.updated_through,

            }
            ProductsModel.updateBookingProductQuantityReschedule(update_products, (booking_err, booking_data) => {
                if (booking_err) {

                    reject(booking_err);
                } else {

                    ProductsModel.updateBookingProductDetails(details_req, (details_err, details_data) => {
                        if (details_err) {

                            reject(details_err);
                        } else {

                            ProductsModel.updateProductQuantaty(product_quantity_update, (product_err, product_data) => {
                                if (product_err) {

                                    reject(product_err);
                                } else {
//                                    console.log("product_data: ", product_data);

                                    if (product_array.length > 0)
                                    {

                                        BookingModel.addProduct(product_array, (booking_err, booking_data) => {
                                            if (booking_err) {

                                                reject(booking_err);
                                            } else {
//                                                console.log("booking_data: ", booking_data);

                                                ProductsModel.updateProductQuantaty(add_product_quantity_update, (product_err, product_data) => {
                                                    if (product_err) {

                                                        reject(product_err);
                                                    } else {
                                                        resolve('success')
                                                    }
                                                })

                                            }
                                        });
                                    } else
                                    {
                                        resolve('success');
                                    }
                                }
                            })
                        }
                    })
                }
            });
        }else{
            if (product_array.length > 0)
            {

                BookingModel.addProduct(product_array, (booking_err, booking_data) => {
                    if (booking_err) {

                        reject(booking_err);
                    } else {
//                        console.log("booking_data: ", booking_data);

                        ProductsModel.updateProductQuantaty(add_product_quantity_update, (product_err, product_data) => {
                            if (product_err) {

                                reject(product_err);
                            } else {
                                resolve('success')
                            }
                        })

                    }
                });
            } else
            {
                resolve('success');
            }
        }
    })
}


exports.updateFilledFormUrl = (req, res, next) => {
    
    if(!req.body.form_pdf_url || !req.body.book_id){
        res.status(400).send({ status:400, message: 'Missing required parameters. Check body and retry'});
        return;
    }

    //var form_url = req.body.form_url;
    //var form_pdf_url = req.body.form_pdf; 

    //var form_url_array = form_url.split("/");

    //var url_length = form_url_array.length;
     //var book_id = form_url_array[url_length - 1];

     var form_pdf_url = req.body.form_pdf_url;
     var book_id = req.body.book_id; 

     //console.log("book_id: ",book_id);



    BookingModel.updateFilledFormUrl(form_pdf_url, book_id, (err, data) => {
        if (err) {
            
            next(err);
        }
        else{
            //console.log("data: ", data);
            res.status(200).send({status:200, result:{message: "Successfully updated form pdf url for bookings"}});
        }
    });

   
}
exports.sendFormUrl = (req,res,next) =>{
    if(!req.body.form_url || !req.body.cust_email){
        res.status(400).send({ status:400, message: 'Missing required parameters. Check body and retry'});
        return;
    }
    let form_dtails = {
        url : req.body.form_url,
        cust_email: req.body.cust_email,
        email_base_url: req.hostname
    };
    if(constants.PROD_FLAG === 1){
        NotificationsLibrary.sendFormLink(form_dtails,(result,err)=>{
            if(err){
                next(err)
            }else{
                res.status(200).send({status:200,result:result});
            }
        });        
    }else{
        res.status(400).send({status:400,result:'check prod flag enabled or not'});
    }
}