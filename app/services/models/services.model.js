const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js');

exports.getServicesByCategory = (data, result) => {
    var today = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
    //console.log(today);
    let sql = `SELECT s.serv_name, s.serv_defdur, s.serv_description, s.serv_saleprice, s.serv_id, s.serv_cat_id, s.serv_deposit, s.serv_defprice, 
               s.serv_prebuffertime, s.serv_postbuffertime, s.serv_order, s.serv_is_active, s.is_promo_service, s.serv_promo_created_date, s.serv_promo_updated_date
               FROM services_mst as s WHERE s.is_break = 0 AND s.serv_cat_id =  ${data.category_id} AND s.serv_is_active = 1
               AND serv_id NOT IN (SELECT s2.serv_id FROM services_mst as s2 WHERE s2.is_break = 0 AND s2.serv_cat_id =  ${data.category_id} AND s2.is_promo_service = 1 AND '${today}' NOT BETWEEN s2.serv_promo_created_date and s2.serv_promo_updated_date)
               ORDER BY s.serv_order ASC`;
    
     //console.log(sql);

    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
           
}



exports.getServicesByStore = (data, result) => {

    let sql = `SELECT s.store_id, s.serv_id, s.serv_cat_id, s.serv_name, s.serv_description, s.serv_defdur, s.serv_saleprice, s.serv_deposit, s.serv_defprice, s.serv_image1,
               s.serv_prebuffertime, s.serv_postbuffertime, s.serv_order, s.serv_is_active, s.is_promo_service, s.serv_promo_created_date, s.serv_promo_updated_date
               FROM services_mst as s WHERE s.is_break = 0 AND s.store_id =  ${data.store_id} AND s.serv_is_active = 1
               ORDER BY s.serv_order ASC`;

           
    
     //console.log(sql);

    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
           
}



exports.getBreakService = (store_id, result) => {

    let sql = `SELECT s.store_id, s.serv_id, s.serv_name, s.serv_cat_id 
               FROM services_mst as s WHERE s.is_break = 1 AND s.store_id =  ${store_id} AND s.serv_name = "Break"`;

           
    
     //console.log(sql);

    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
           
}
exports.getServiceDetailsModel = (serv_id) => {
    return new Promise((resolve,reject)=>{
        let sql = `SELECT s.serv_prebuffertime, s.serv_postbuffertime from services_mst as s WHERE s.serv_id = ${serv_id}`;
        db.query(sql , (err,res) => {
        if (err) {
                //console.log("error: ", err);
            reject({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
            resolve(res);      
       });
    })
}


// exports.getServiceDetails = (service_id) => {

//     // let sql = `SELECT s.store_id, s.serv_id, s.serv_cat_id, s.serv_name, s.serv_description, s.serv_defdur, s.serv_saleprice, s.serv_deposit, s.serv_defprice, 
//     // s.serv_prebuffertime, s.serv_postbuffertime, s.serv_order, s.serv_is_active, s.is_promo_service, s.serv_promo_created_date, s.serv_promo_updated_date
//     // FROM services_mst as s WHERE s.serv_id = ${service_id}`;

           
    
//     //  //console.log(sql);

//     // db.query(sql , (err,res) => {
//     //     if (err) {
//     //         return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
//     //     }
//     //     return  result(null, res);
//     // });



//     return new Promise((resolve, reject) => {

//         let sql = `SELECT s.store_id, s.serv_id, s.serv_cat_id, s.serv_name, s.serv_description, s.serv_defdur, s.serv_saleprice, s.serv_deposit, s.serv_defprice, 
//         s.serv_prebuffertime, s.serv_postbuffertime, s.serv_order, s.serv_is_active, s.is_promo_service, s.serv_promo_created_date, s.serv_promo_updated_date
//         FROM services_mst as s WHERE s.serv_id = ${service_id}`;
//         db.query(sql, function (err, result) {
//             if (err)
//                 reject({error_code:err.code, message:err.sqlMessage,sql:err.sql});
//             else
//                 resolve(result);
//         });
//     });
           
// }


exports.getDetailsByServId = (serv_id) => {


    return new Promise((resolve, reject) => {

        let sql = `SELECT s.store_id, s.serv_id, s.serv_cat_id, s.serv_name, s.serv_description, s.serv_defdur, s.serv_saleprice, s.serv_deposit, s.serv_defprice, 
        s.serv_prebuffertime, s.serv_postbuffertime, s.serv_order, s.serv_is_active, s.is_promo_service, s.serv_promo_created_date, s.serv_promo_updated_date
        FROM services_mst as s WHERE s.serv_id = ${serv_id}`;
        db.query(sql, function (err, result) {
            if (err)
                reject({error_code:err.code, message:err.sqlMessage,sql:err.sql});
            else
                resolve(result);
        });
    });
           
}


