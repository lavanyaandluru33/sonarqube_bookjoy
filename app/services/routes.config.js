const ServicesController = require('./controllers/services.controller');
const auth = require('../../config/auth');
exports.routesConfig = function (app) {
  
    
    app.post('/services/get_services_by_category', 
        ServicesController.getServicesByCategory
    );

    app.post('/services/get_services_by_store', 
        ServicesController.getServicesByStore
    );

    app.post('/services/get_services_list_by_store', 
        ServicesController.getServicesListByStore
    );

    app.post('/services/get_services_by_staff', 
        ServicesController.getServicesByStaff
    );

    app.post('/v2/services/get_services_list_by_url_v2', 
        ServicesController.getServicesListByStoreUrl
    );
    
    
}