const constants = require('../../../config/constants.js');
const ServicesModel = require('../models/services.model');
const CategoriesModel = require('../../categories/models/categories.model');
const SettingsModel = require('../../settings/models/settings.model.js');
const StoresModel = require('../../stores/models/stores.model.js');
const StaffModel = require('../../staff/models/staff.model.js');

var async = require("async");
var moment = require('moment'); 

exports.getServicesByCategory = (req, res, next) => {
    if(!Object.keys(req.body).length) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }
    if(!req.body.category_id){
        res.status(400).send({ status:400, message: 'Missing required parameters. Check body and retry'});
        return;
    }

    ServicesModel.getServicesByCategory(req.body, (err, data) => {
        if (err) {
            //res.status(500).send({message:err});
            next(err);
        }
        else{
            res.status(200).send({status:200, result:data});
        }
    });

   
}

exports.getServicesByStore = (req, res, next) => {
    if(!req.body.store_id) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    ServicesModel.getServicesByStore(req.body, (err, data) => {
        if (err) {
            //res.status(500).send({message:err});
            next(err);
        }
        else{
            res.status(200).send({status:200, result:data});
        }
    });

   
}


exports.getServicesListByStore = (req, res, next) => {
    if(!req.body.store_id) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    StoresModel.getStoreDetails(req.body.store_id, (store_err, store_details) => {
        if (store_err) {
           
            next(store_err);
        }
        else{

            //console.log("store_details: ",store_details);

            CategoriesModel.getServiceCategories(req.body, (err, categories) => {
                if (err) {
                    //res.status(500).send({message:err});
                    next(err);
                }
                else{
                    //console.log(categories);       
                    ServicesModel.getServicesByStore(req.body, (err, services) => {
                        if (err) {
                            //res.status(500).send({message:err});
                            next(err);
                        }
                        else{
                            //console.log(services);
                            categories.filter((this_cat)=>{

                                this_cat.cat_total_services =0;
                                this_cat.services = [];
                                
                                //console.log(this_cat);

                                services.filter((this_serv)=>{
                                    //console.log(this_serv.serv_id);

                                    if(this_serv.serv_cat_id == this_cat.serv_cat_id)
                                    {
                                        this_serv.store_tel = store_details[0].store_tel;
                                        this_cat.services.push(this_serv);
                                        this_cat.cat_total_services++;
                                    }

                                })


                            })
                            //console.log(categories);
                            res.status(200).send({status:200, result:categories});
                        }
                    });
                }
            })

        }   
    })

   
}



exports.getServicesByStaff = (req, res, next) => {

    if(!req.body.store_id || !req.body.staff_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    CategoriesModel.getServiceCategories(req.body, (err, categories) => {
        if (err) {
            //res.status(500).send({message:err});
            next(err);
        }
        else{
            //console.log(categories);      


            ServicesModel.getServicesByStore(req.body, (err, services) => {
                if (err) {
        
                    //res.status(500).send({message:err});
                    next(err);
                }
                else{
                    
                    //console.log("services:");console.log(services);

                    var mapped_services   = [];
                    var unmapped_services = [];
                                                
                        SettingsModel.getStaffServiceBlocks(req.body, (err2, blocks_data) => {
                            if (err2) {
                    
                                //res.status(500).send({message:err2});
                                next(err2);
                            }
                            else
                            {
                                //console.log("blocks-"); console.log(blocks_data);
                                
                                var staff_serv_blocks = [];


                                async.each(blocks_data, function(block, callback) {
                                    staff_serv_blocks.push(block.serv_id);
                                        callback();
                                    
                                }, function(err4) {
                                    if (!err4)
                                    {
                                        //console.log(staff_serv_blocks);
                                        var i=0;

                                        //console.log(`---- categories filter start ----`);
                                                
                                        categories.filter((this_cat)=>{

                                            var mapped = [];
                                            var unmapped = [];
                                            var cat_mapped = {};
                                            var cat_unmapped = {};
                                                        
                                            // console.log(`-------- in category -${this_cat.serv_cat_id}`);
                                            // console.log(`---- services filter start ----`);

                                            services.filter((this_service)=>{

                                                //console.log(`-------------in service - ${this_service.serv_id} --start--`);

                                                if(this_cat.serv_cat_id == this_service.serv_cat_id)
                                                {
                                                    //console.log(`-----in cat - ${this_cat.serv_cat_id} services -${this_service.serv_id} - filter`);

                                                        if((staff_serv_blocks.indexOf(this_service.serv_id)) > -1)
                                                        {
                                                            //blocked
                                                                //console.log("blocked");console.log(this_service.serv_id);
                                                                unmapped.push(this_service);
                                                                    
                                                        }
                                                        else
                                                        {
                                                                //console.log("in mapped");console.log(this_service.serv_id);
                                                                mapped.push(this_service);
                                                        }
                                                }

                                                //console.log(`-------------in service - ${this_service.serv_id} -- end --`);   
                                                
                                            })


                                            
                                            //console.log("-services filter ended-"); 
                            
                                            //console.log("--mapped unmapped started ----");

                                            if(mapped.length >0)
                                            {
                                                cat_mapped.category = this_cat;
                                                
                                                cat_mapped.services = mapped;
                                                mapped_services.push(cat_mapped);
                                                //console.log("mapped_services1");console.log(mapped_services);  
                                                
                                            }

                                        
                                            if(unmapped.length >0)
                                            {
                                                cat_unmapped.category = this_cat;
                                                
                                                cat_unmapped.services = unmapped;
                                                unmapped_services.push(cat_unmapped);
                                                //console.log("unmapped_services1");console.log(unmapped_services);
                                                
                                                
                                            }

                                            //console.log("--mapped unmapped ended ----");
                                            
                                            //console.log("----- this cat filter end ------- "+this_cat.serv_cat_id);
                                        
                                            i++;

                                                if(i == categories.length)
                                                {
                                                    //console.log(i);
                                            
                                                    res.status(200).send({status:200, result:{mapped_services:mapped_services, unmapped_services: unmapped_services}});

                                                }
                                       })
                                      //console.log("-categories filter ended-"); 
                                        
                                    }
                                    else
                                    {
                                        //res.status(500).send({message:err4}); 
                                        next(err4);
                                    }
                                })
                                                                            

                            }
                        })
                        
                }
     
            });
        }
    })
   
}



exports.getServicesListByStoreUrl = (req, res, next) => {
    if(!req.body.store_url_name) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    StoresModel.getStoreDetailsByUrl(req.body.store_url_name, (err, store_data) => {
        if (err) {

            //res.status(500).send({status:500, result:{error:err}});
            next(err);
        }
        else{
            //console.log(store_data);

           
            if(store_data.length)
            {

                var categories_services = {
                    store_id: store_data[0].store_id,
                    store_name: store_data[0].store_name,
                    store_url_name: store_data[0].store_url_name,
                    store_tel: store_data[0].store_tel
    
                }
    

                var services_req = {
                    store_id : store_data[0].store_id
                }
                //console.log(services_req);

                CategoriesModel.getServiceCategories(services_req, (err, categories) => {
                    if (err) {
                        //res.status(500).send({message:err});
                        next(err);
                    }
                    else{
                        //console.log(categories);       
                        ServicesModel.getServicesByStore(services_req, (err, services) => {
                            if (err) {
                                //res.status(500).send({message:err});
                                next(err);
                            }
                            else{
                                //console.log("services: ",services);

                                var staff_req = services_req;
                                staff_req.active_staff =1;
                                //console.log("staff_req: ",staff_req);


                                StaffModel.getStaffByStore(staff_req, (err, staff) => {
                                    if (err) {
                            
                                            next(err);
                                    }
                                    else{

                                        //console.log("staff: ",staff);
                                        var staff_ids = [];
                                        
                                        for(i=0; i<staff.length; i++)
                                        {
                                            staff_ids.push(staff[i].user_id);
                                            //console.log("i: ",i);
                                        }
                                        //console.log("staff ids: ",staff_ids);


                                        SettingsModel.getStaffServiceBlocks(services_req, (blocks_err, blocks_data) => {
                                            if (blocks_err) {
                                                next(blocks_err);
                                            }
                                            else
                                            {
                                                //console.log("blocks :",blocks_data);

                                                var blocks = {};
                                        
                                                for(i=0; i<blocks_data.length; i++)
                                                {
                                                    if(!blocks[blocks_data[i].serv_id])
                                                    {
                                                        blocks[blocks_data[i].serv_id] = [];

                                                    }
                                                    // if(blocks[blocks_data[i].serv_id].indexOf(blocks_data[i].user_id) <= -1)
                                                    // {
                                                    blocks[blocks_data[i].serv_id].push(blocks_data[i].user_id);
                                                    //}
                                                    //console.log("i: ",i);
                                                }
                                                //console.log("blocks: ",blocks);
                        
                               
                                                categories.filter((this_cat)=>{

                                                    //this_cat.cat_total_services =0;
                                                    this_cat.services = [];

                                                    services.filter((this_serv)=>{
                                                        //console.log("this serv id: ",this_serv.serv_id);

                                                        if(this_serv.serv_cat_id == this_cat.serv_cat_id)
                                                        {



                                                            var this_serv_blocks  = [];

                                                            //console.log(`---- cat_id : ${this_cat.serv_cat_id} ---- serv id: ${this_serv.serv_id} ----`);

                                                            var this_staff = staff_ids;
                                                             if(blocks[this_serv.serv_id])
                                                             {
                                                                this_serv_blocks = blocks[this_serv.serv_id];
                                                             }

                                                            //console.log("this serv blocks: ",this_serv_blocks);
                                                            //console.log("this serv blocks: ",this_serv_blocks.indexOf(this_staff[1]));

                                                            var this_av_staff =[];
                                                            
                                                            for(i=0;i<this_staff.length;i++)
                                                            {
                                                                if(this_serv_blocks.indexOf(this_staff[i]) <= -1)
                                                                {
                                                                    this_av_staff.push(this_staff[i])

                                                                }
                                                                //console.log("staff i: ",i);

                                                            }
                                                            //console.log("this_av_staff: ",this_av_staff);

                                                            if(this_av_staff.length > 0)
                                                            {

                                                                this_cat.services.push(this_serv);
                                                            }
                                                                
                                                                //console.log("this_cat.services: ",this_cat.services);
                                                                //console.log("-----this_cat.services complete----- ");
                                                        }

                                                    })


                                                })

                                            //console.log(categories);
                                            categories_services.services_details = categories
                                            res.status(200).send({status:200, result: categories_services});
                                    }
                                });

                            }
                        })
                            }
                       
                        });
                    }
                })
            }
            else
            {
                res.status(400).send({status:400, result:{message: "No store found with given store_url_name"}});

            }

        }
    })

   
}



