const constants = require('../../../config/constants.js');
const ServicesReportModel = require('../models/servicereport.model');
const db = require('../../../config/db.js');

var moment = require('moment');
const { constant } = require('async');
var async = require("async");

//All Customers
exports.getAllCustomers = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = (req.body.start_date);
    var end_date = (req.body.end_date);

    ServicesReportModel.getAllCustomers(req.body, (err, data) => {
        if (err) {
            //res.status(500).send({message:err});
            next(err);
        }
        else {
            res.status(200).send({ status: 200, result: data });
        }
    });


}
//VIP customers
exports.getVipCustomers = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = (req.body.start_date);
    var end_date = (req.body.end_date);

    ServicesReportModel.getVipCustomers(req.body, (err, data) => {
        if (err) {
            //res.status(500).send({message:err});
            next(err);
        }
        else {
            res.status(200).send({ status: 200, result: data });
        }
    });


}
//New customers
exports.getNewCustomers = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = (req.body.start_date);
    var end_date = (req.body.end_date);

    ServicesReportModel.getNewCustomers(req.body, (err, data) => {
        if (err) {
            //res.status(500).send({message:err});
            next(err);
        }
        else {
            res.status(200).send({ status: 200, result: data });
        }
    });


}
//Returning customers
exports.getReturningCustomers = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = (req.body.start_date);
    var end_date = (req.body.end_date);

    ServicesReportModel.getReturningCustomers(req.body, (err, data) => {
        if (err) {
            //res.status(500).send({message:err});
            next(err);
        }
        else {
            res.status(200).send({ status: 200, result: data });
        }
    });


}
//Risk customers
exports.getRiskCustomers = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = (req.body.start_date);
    var end_date = (req.body.end_date);

    ServicesReportModel.getRiskCustomers(req.body, (err, data) => {
        if (err) {
            //res.status(500).send({message:err});
            next(err);
        }
        else {
            res.status(200).send({ status: 200, result: data });
        }
    });
}

//Get revenue of salon based on salon id

exports.getRevenueBasedOnSalonID = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = moment(req.body.start_date).format("YYYY-MM-DD");
    var end_date = moment(req.body.end_date).format("YYYY-MM-DD");
    var this_date = start_date;
    var date_array = [];
    ServicesReportModel.getRevenueBasedOnSalonID(req.body, (err, revenue_details) => {
        var service_data = JSON.parse(JSON.stringify(revenue_details.revenue.Services));
        var new_service_data = [];
        var service_obj = {};
        for (var i = 0; i < service_data.length; i++) {
            if (!service_obj.hasOwnProperty(service_data[i].labels)) {
                service_obj[service_data[i].labels] = 0;
            }
            service_obj[service_data[i].labels] += service_data[i].amount;
        }
        var obj_final = [];
        for (var prop in service_obj) {
            obj_final.push({ labels: prop, amount: service_obj[prop] });
        }
        while (this_date <= end_date) {
            var new_obj = {};
            new_obj.date = moment(this_date).format("YYYY-MM-DD");

            date_array.push(new_obj);
            var dateExistsIndex = obj_final.map((item) => { return item.labels; }).indexOf(this_date)
            if (dateExistsIndex >= 0) {
                new_service_data.push({ labels: obj_final[dateExistsIndex].labels, amount: obj_final[dateExistsIndex].amount });
            }
            else {
                new_service_data.push({ labels: this_date, amount: 0 });

            }
            var this_date1 = this_date;
            this_date = moment(this_date1, "YYYY-MM-DD").add(1, 'd').format("YYYY-MM-DD");

        }
        var service_details = {}
        if (revenue_details.revenue.Services.length) {
            service_details = {
                "total_bookings": revenue_details.revenue.Services.length,
                "total_booking_revenue": revenue_details.revenue.Services.map(obj => obj.amount).reduce((a, c) => { return a + c }),
                "graph_data": {
                    "labels": new_service_data.map((obj) => obj.labels),
                    "values": new_service_data.map((obj) => obj.amount)

                }
            };
        }
        else {
            service_details = {

                "total_bookings": revenue_details.revenue.Services.length,
                "total_booking_revenue": 0,
                "graph_data": {
                    "labels": new_service_data.map((obj) => obj.labels),
                    "values": new_service_data.map((obj) => obj.amount)

                }

            }
        }
        var class_data = JSON.parse(JSON.stringify(revenue_details.revenue.booking_classes));

        var new_class_data = [];
        var class_obj = {};
        for (var i = 0; i < class_data.length; i++) {
            if (!class_obj.hasOwnProperty(class_data[i].labels)) {
                class_obj[class_data[i].labels] = 0;
            }
            class_obj[class_data[i].labels] += class_data[i].amount;
        }
        var class_obj_final = [];
        for (var prop in class_obj) {
            class_obj_final.push({ labels: prop, amount: class_obj[prop] });
        }
        var class_this_date = start_date;
        while (class_this_date <= end_date) {
            var new_obj = {};
            new_obj.date = moment(class_this_date).format("YYYY-MM-DD");

            date_array.push(new_obj);
            var dateExistsIndex = class_obj_final.map((item) => { return item.labels; }).indexOf(class_this_date)
            if (dateExistsIndex >= 0) {
                new_class_data.push({ labels: class_obj_final[dateExistsIndex].labels, amount: class_obj_final[dateExistsIndex].amount });
            }
            else {
                new_class_data.push({ labels: class_this_date, amount: 0 });

            }
            var class_this_date1 = class_this_date;
            class_this_date = moment(class_this_date1, "YYYY-MM-DD").add(1, 'd').format("YYYY-MM-DD");

        }
        var class_booking_details = {}
        if (revenue_details.revenue.booking_classes.length) {

            class_booking_details = {
                "total_bookings": revenue_details.revenue.booking_classes.length,
                "total_booking_revenue": revenue_details.revenue.booking_classes.map(obj => obj.amount).reduce((a, c) => { return a + c }),
                "graph_data": {
                    "labels": new_class_data.map((obj) => obj.labels),
                    "values": new_class_data.map((obj) => obj.amount)

                }

            };
        }
        else {
            class_booking_details = {

                "total_bookings": revenue_details.revenue.booking_classes.length,
                "total_booking_revenue": 0,
                "graph_data": {
                    "labels": new_class_data.map((obj) => obj.labels),
                    "values": new_class_data.map((obj) => obj.amount)

                }

            }
        }
        var product_data = JSON.parse(JSON.stringify(revenue_details.revenue.products));
        var new_product_data = [];
        var product_obj = {};
        for (var i = 0; i < product_data.length; i++) {
            if (!product_obj.hasOwnProperty(product_data[i].labels)) {
                product_obj[product_data[i].labels] = 0;
            }
            product_obj[product_data[i].labels] += product_data[i].total_spent;
        }
        var product_obj_final = [];
        for (var prop in product_obj) {
            product_obj_final.push({ labels: prop, amount: product_obj[prop] });
        }
        var product_this_date = start_date;
        while (product_this_date <= end_date) {
            var new_obj = {};
            new_obj.date = moment(product_this_date).format("YYYY-MM-DD");

            date_array.push(new_obj);
            var dateExistsIndex = product_obj_final.map((item) => { return item.labels; }).indexOf(product_this_date)
            if (dateExistsIndex >= 0) {
                new_product_data.push({ labels: product_obj_final[dateExistsIndex].labels, amount: product_obj_final[dateExistsIndex].amount });
            }
            else {
                new_product_data.push({ labels: product_this_date, amount: 0 });

            }
            var product_this_date1 = product_this_date;
            product_this_date = moment(product_this_date1, "YYYY-MM-DD").add(1, 'd').format("YYYY-MM-DD");

        }
        var product_details = {}
        if (revenue_details.revenue.products.length) {
            product_details = {
                "total_bookings": revenue_details.revenue.products.length,
                "total_booking_revenue": revenue_details.revenue.products.map(obj => obj.total_spent).reduce((a, c) => { return a + c }),
                "graph_data": {

                    "labels": new_product_data.map((obj) => obj.labels),
                    "values": new_product_data.map((obj) => obj.amount)

                }
            };
        }
        else {
            product_details = {

                "total_bookings": revenue_details.revenue.products.length,
                "total_booking_revenue": 0,
                "graph_data": {
                    "labels": new_product_data.map((obj) => obj.labels),
                    "values": new_product_data.map((obj) => obj.amount)

                }

            }
        }

        var total_revenue = class_booking_details.total_booking_revenue + service_details.total_booking_revenue + product_details.total_booking_revenue;
        var revenue = {
            "revenue": {
                "total_revenue": total_revenue,
                "services": service_details,
                "classes": class_booking_details,
                "products": product_details

            }
        };

        if (err) {
            //res.status(500).send({message:err});
            next(err);
        }
        else {
            res.status(200).send({ status: 200, result: revenue });

        }
    });


}
//Revenue of services,products,classes by clicking on MOre
exports.getRevenueByClickingMore = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date && req.body.type)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = (req.body.start_date);
    var end_date = (req.body.end_date);
    var type = req.body.type;
    if (req.body.type == constants.SERVICE_TYPE) {
        ServicesReportModel.getRevenueByClickingMoreOnService(req.body, (err, data) => {
            var data_parse = JSON.parse(JSON.stringify(data));
            for (i = 0; i < data_parse.length; i++) {
                if (data_parse[i].total_revenue == null) {
                    data_parse[i].total_revenue = 0;
                }
            }
            if (err) {
                //res.status(500).send({message:err});
                next(err);
            }
            else {
                res.status(200).send({ status: 200, result: data_parse });
            }
        });
    }
    else if (req.body.type == constants.CLASS_TYPE) {
        ServicesReportModel.getRevenueByClickingMoreOnClass(req.body, (err, data) => {
            var class_data_parse = JSON.parse(JSON.stringify(data));

            if (err) {
                //res.status(500).send({message:err});
                next(err);
            }
            else {
                for (i = 0; i < class_data_parse.length; i++) {
                    if (class_data_parse[i].total_revenue == null) {
                        class_data_parse[i].total_revenue = 0;
                    }
                }
                res.status(200).send({ status: 200, result: class_data_parse });
            }
        });
    }
    else if (req.body.type == constants.PRODUCT_TYPE) {
        ServicesReportModel.getRevenueByClickingMoreOnProducts(req.body, (err, data) => {

            if (err) {
                //res.status(500).send({message:err});
                next(err);
            }
            else {
                var product_data_parse = JSON.parse(JSON.stringify(data));
                var product_obj_arr = product_data_parse.product_revenue_details;

                var targetObj = {};
                for (var i = 0; i < product_obj_arr.length; i++) {
                    if (!targetObj.hasOwnProperty(product_obj_arr[i].product_id)) {
                        targetObj[product_obj_arr[i].product_id] = 0;
                    }
                    // if (product_obj_arr[i].product_discount_full_amount == -1) {
                    //     targetObj[product_obj_arr[i].product_id] += product_obj_arr[i].product_full_amount * product_obj_arr[i].product_quantity;
                    // }
                    // else {
                    targetObj[product_obj_arr[i].product_id] += product_obj_arr[i].product_full_amount * product_obj_arr[i].product_quantity;
                    // }
                }


                var obj = []
                for (var i in targetObj) {
                    obj.push({ "product_id": i, "amount": targetObj[i] })
                }
                var product_booking_details = product_data_parse.booking_product_details;
                for (var i = 0; i < product_booking_details.length; i++) {
                    for (var j = 0; j < obj.length; j++) {
                        if (product_booking_details[i].product_id == obj[j].product_id) {

                            product_booking_details[i].total_revenue = obj[j].amount;
                        }
                    }
                }
                for (i = 0; i < product_data_parse.length; i++) {
                    if (product_data_parse[i].online == null) {
                        product_data_parse[i].online = 0;
                    }
                }
                product_booking_details.sort((a, b) => parseFloat(b.total_revenue) - parseFloat(a.total_revenue));
                res.status(200).send({ status: 200, result: product_booking_details });
            }
        });
    }
    else if (req.body.type == constants.STYLIST_TYPE) {
        if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
            res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
            return;
        }

        const store_id = parseInt(req.body.store_id, 10);
        var start_date = moment(req.body.start_date, "YYYY-MM-DD");
        var end_date = moment(req.body.end_date, "YYYY-MM-DD");
        var date_array = [];
        var this_date = start_date;
        var details = {};
        var total_hours = [];

        ServicesReportModel.getRevenueByClickingMoreonStylists(req.body, (err, data) => {
            if (err) {
                next(err);
            }
            else {
                var stylist_data_parse = JSON.parse(JSON.stringify(data));
                details = stylist_data_parse;
                ServicesReportModel.getStaffBookedTimings(req.body, (err, data) => {
                    if (err) {
                        next(err);
                    }
                    else {
                        var stylist_booked_data = JSON.parse(JSON.stringify(data));

                        for (var i = 0; i < details.length; i++) {
                            for (var j = 0; j < stylist_booked_data.length; j++) {
                                if (details[i].user_id == stylist_booked_data[j].user_id) {
                                    details[i].booked_hrs = stylist_booked_data[j].booking_hours;
                                }
                            }
                        }
                        ServicesReportModel.getStaffByStoreforCalendar(req.body, (err, staff) => {
                            if (err) {
                                next(err);
                            }
                            else {
                                while (this_date <= end_date) {
                                    var new_obj = {};
                                    new_obj.date = moment(this_date).format("YYYY-MM-DD");
                                    new_obj.day = (moment(this_date).isoWeekday()) - 1;
                                    new_obj.staff = JSON.parse(JSON.stringify(staff));
                                    date_array.push(new_obj);
                                    this_date = this_date.add(1, 'days');
                                }
                                async.eachSeries(staff, (obj, callback) => {
                                    total_hours[obj.staff_id] = 0;
                                    async.eachSeries(date_array, (each_obj, callback1) => {

                                        var this_staff = {
                                            store_id: store_id,
                                            staff_id: obj.staff_id,
                                            date: each_obj.date,
                                            day_of_week: each_obj.day
                                        };

                                        ServicesReportModel.getStaffHourExceptions(this_staff, (err, excp_obj) => {
                                            if (err) {

                                                next(err);
                                            }
                                            else {
                                                excp_obj = JSON.parse(JSON.stringify(excp_obj));
                                                if (excp_obj.length != 0 && !excp_obj.user_id) {

                                                    var open_time = excp_obj[0].user_hrex_open_time;
                                                    var close_time = excp_obj[0].user_hrex_close_time;
                                                    var opening_time = moment(open_time, "HH:mm:ss");
                                                    var closing_time = moment(close_time, "HH:mm:ss");
                                                    var hours_diff = closing_time.diff(opening_time, 'minutes');
                                                    if (excp_obj[0].user_hrex_all_day_indicator == 0) {
                                                        total_hours[obj.staff_id] += hours_diff;
                                                    }
                                                    callback1();

                                                }
                                                else {
                                                    ServicesReportModel.getStaffHours(this_staff, async (err, hrs_obj) => {
                                                        if (err) {

                                                            next(err);

                                                        }
                                                        else {
                                                            var open_time = hrs_obj[0].user_hrwk_open_time;
                                                            var close_time = hrs_obj[0].user_hrwk_close_time;
                                                            var opening_time = moment(open_time, "HH:mm:ss");
                                                            var closing_time = moment(close_time, "HH:mm:ss");
                                                            var hours_diff = closing_time.diff(opening_time, 'minutes');
                                                            total_hours[obj.staff_id] += hours_diff;

                                                            //console.log("in hours: ", total_hours[obj.staff_id]);
                                                        }
                                                        // for (var i = 0; i < details.length; i++) {
                                                        //     if (details[i].user_id == obj.staff_id) {
                                                        //         details[i].available_hrs = total_hours[obj.staff_id];
                                                        //     }

                                                        // }

                                                        callback1();

                                                    });


                                                }

                                            }


                                        });


                                    },
                                        function (err) {

                                            try {
                                                //console.log(` total hrs - ${obj.staff_id} = ${total_hours[obj.staff_id]}`);

                                                for (var i = 0; i < details.length; i++) {
                                                    if (details[i].user_id == obj.staff_id) {
                                                        details[i].available_hrs = +((total_hours[obj.staff_id]) / 60).toFixed(2);
                                                    }

                                                }
                                                callback();
                                            } catch (err) {
                                                next(err);
                                            }
                                        }

                                    );

                                },

                                    function (err) {

                                        try {
                                            details.sort((a, b) => parseFloat(b.total_revenue) - parseFloat(a.total_revenue));

                                            res.status(200).send({ status: 200, result: details });
                                        } catch (err) {
                                            next(err);
                                        }
                                    }



                                );


                            }
                        });

                    }
                });

            }
        });



    }
}


//New and returning customers between dates
exports.getNewandReturningCustomers = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = (req.body.start_date);
    var end_date = (req.body.end_date);
    var customers_data = {};

    ServicesReportModel.getnewandreturningcustomers(req.body, (err, data) => {

        var customers = JSON.parse(JSON.stringify(data));

        if (err) {
            next(err);
        }
        else {
            var new_customers;
            var returning_customers;
            if (customers.new_customers) {
                new_customers = customers.new_customers;

            }
            else {
                new_customers = 0;

            }
            if (customers.return_customers) {
                returning_customers = customers.return_customers;
            }
            else {
                returning_customers = 0;
            }

            var total_customers = new_customers + returning_customers;
            var customers_data = { new_customers, returning_customers, total_customers }
            res.status(200).send({ status: 200, result: customers_data });
        }
    });


}
//Top five customers between dates
exports.topFiveCustomers = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = (req.body.start_date);
    var end_date = (req.body.end_date);

    ServicesReportModel.topfivecustomers(req.body, (err, data) => {
        if (err) {
            next(err);
        }
        else {
            res.status(200).send({ status: 200, result: data });
        }
    });


}
//Top five Stylists between dates
exports.topFiveStylists = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = (req.body.start_date);
    var end_date = (req.body.end_date);

    ServicesReportModel.topfivestylists(req.body, (err, data) => {
        if (err) {
            next(err);
        }
        else {
            res.status(200).send({ status: 200, result: data });
        }
    });


}
//Top five services between dates
exports.topFiveServices = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = (req.body.start_date);
    var end_date = (req.body.end_date);

    ServicesReportModel.topfiveservices(req.body, (err, data) => {
        if (err) {
            next(err);
        }
        else {
            res.status(200).send({ status: 200, result: data });
        }
    });


}
//Top five products between dates
exports.topFiveProducts = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = (req.body.start_date);
    var end_date = (req.body.end_date);

    ServicesReportModel.topfiveproducts(req.body, (err, data) => {

        if (err) {
            next(err);
        }
        else {
            var product_obj_arr = JSON.parse(JSON.stringify(data));
            var product_obj = {};
            for (var i = 0; i < product_obj_arr.length; i++) {
                if (!product_obj.hasOwnProperty(product_obj_arr[i].product_id)) {
                    product_obj[product_obj_arr[i].product_id] = 0;
                }

                // if (product_obj_arr[i].product_discount_full_amount == -1) {
                //     product_obj[product_obj_arr[i].product_id] += product_obj_arr[i].product_full_amount * product_obj_arr[i].product_quantity;
                // }
                // else {
                product_obj[product_obj_arr[i].product_id] += product_obj_arr[i].product_full_amount * product_obj_arr[i].product_quantity;
                // }

            }

            var product_data_array = []
            for (var i in product_obj) {
                var product_count = product_obj_arr.filter(prod => prod.product_id == i);
                product_data_array.push({ "booking_product_id": i, "product_name": product_count[0].product_name, "product_bookings_count": product_count.length, "total_revenue": product_obj[i] })

            }

            product_data_array.sort((a, b) => parseFloat(b.total_revenue) - parseFloat(a.total_revenue));
            var product_data = product_data_array.slice(0, 5);
            res.status(200).send({ status: 200, result: product_data });
        }
    });


}
//Revenue Comparison Between Dates
exports.getRevenueCompareBasedOnSalonID = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = moment(req.body.start_date);
    var end_date = moment(req.body.end_date);
    if (req.body.compare_to_previous_year == 2) {
        var previous_year_start_date = moment(start_date).subtract(1, 'year').format('YYYY-MM-DD');
        var previous_year_end_date = moment(end_date).subtract(1, 'year').format('YYYY-MM-DD');
        ServicesReportModel.getRevenuecomparsionBasedOnSalonID(req.body, (err, revenue_compare_details) => {

            if (err) {
                //res.status(500).send({message:err});
                next(err);
            }
            else {
                var service_compare_details = {}
                if (revenue_compare_details.revenue.Services.length) {
                    service_compare_details = {
                        "total_bookings": revenue_compare_details.revenue.Services.length,
                        "total_booking_revenue": revenue_compare_details.revenue.Services.map(obj => obj.amount).reduce((a, c) => { return a + c })
                    };
                }
                else {
                    service_compare_details = {

                        "total_bookings": revenue_compare_details.revenue.Services.length,
                        "total_booking_revenue": 0

                    }

                }
                var class_booking_compare_details = {}
                if (revenue_compare_details.revenue.booking_classes.length) {

                    class_booking_compare_details = {
                        "total_bookings": revenue_compare_details.revenue.booking_classes.length,
                        "total_booking_revenue": revenue_compare_details.revenue.booking_classes.map(obj => obj.amount).reduce((a, c) => { return a + c })

                    };
                }
                else {
                    class_booking_compare_details = {

                        "total_bookings": revenue_compare_details.revenue.booking_classes.length,
                        "total_booking_revenue": 0,

                    }
                }
                var product_compare_details = {}
                if (revenue_compare_details.revenue.products.length) {
                    product_compare_details = {
                        "total_bookings": revenue_compare_details.revenue.products.length,
                        "total_booking_revenue": revenue_compare_details.revenue.products.map(obj => obj.total_spent).reduce((a, c) => { return a + c }),
                    };
                }
                else {
                    product_compare_details = {

                        "total_bookings": revenue_compare_details.revenue.products.length,
                        "total_booking_revenue": 0,

                    }
                }
                var total_revenue = class_booking_compare_details.total_booking_revenue + service_compare_details.total_booking_revenue + product_compare_details.total_booking_revenue;
                var current_revenue = {

                    "total_revenue": total_revenue,
                    "services": service_compare_details,
                    "classes": class_booking_compare_details,
                    "products": product_compare_details
                };
                var data = { store_id, previous_year_start_date, previous_year_end_date };
                ServicesReportModel.getPreviousYearRevenuecomparsionBasedOnSalonID(data, (err, revenue_prev_compare_details) => {
                    if (err) {
                        //res.status(500).send({message:err});
                        next(err);
                    }
                    else {
                        var service_prev_compare_details = {}
                        if (revenue_prev_compare_details.revenue.Services.length) {
                            service_prev_compare_details = {
                                "total_bookings": revenue_prev_compare_details.revenue.Services.length,
                                "total_booking_revenue": revenue_prev_compare_details.revenue.Services.map(obj => obj.amount).reduce((a, c) => { return a + c })
                            };
                        }
                        else {
                            service_prev_compare_details = {

                                "total_bookings": revenue_prev_compare_details.revenue.Services.length,
                                "total_booking_revenue": 0

                            }

                        }
                        var class_booking_prev_compare_details = {}
                        if (revenue_prev_compare_details.revenue.booking_classes.length) {

                            class_booking_prev_compare_details = {
                                "total_bookings": revenue_prev_compare_details.revenue.booking_classes.length,
                                "total_booking_revenue": revenue_prev_compare_details.revenue.booking_classes.map(obj => obj.amount).reduce((a, c) => { return a + c })

                            };
                        }
                        else {
                            class_booking_prev_compare_details = {

                                "total_bookings": revenue_prev_compare_details.revenue.booking_classes.length,
                                "total_booking_revenue": 0,

                            }
                        }
                        var product_prev_compare_details = {}
                        if (revenue_prev_compare_details.revenue.products.length) {
                            product_prev_compare_details = {
                                "total_bookings": revenue_prev_compare_details.revenue.products.length,
                                "total_booking_revenue": revenue_prev_compare_details.revenue.products.map(obj => obj.total_spent).reduce((a, c) => { return a + c }),
                            };
                        }
                        else {
                            product_prev_compare_details = {

                                "total_bookings": revenue_prev_compare_details.revenue.products.length,
                                "total_booking_revenue": 0,

                            }
                        }
                        var total_revenue = class_booking_prev_compare_details.total_booking_revenue + service_prev_compare_details.total_booking_revenue + product_prev_compare_details.total_booking_revenue;
                        var previous_year_revenue = {

                            "total_revenue": total_revenue,
                            "services": service_prev_compare_details,
                            "classes": class_booking_prev_compare_details,
                            "products": product_prev_compare_details


                        };

                        res.status(200).send({ status: 200, result: { "previous_year_start_date":previous_year_start_date,"previous_year_end_date":previous_year_end_date,current_revenue, previous_year_revenue } });

                    }
                });
            }
        });
    }
    if (req.body.compare_to_previous_period == 1) {
        var diff = (end_date.diff(start_date, 'days'));
        var prev_period_end_date = moment(start_date).subtract(1, 'd').format("YYYY-MM-DD");
        var prev_period_start_date = moment(prev_period_end_date).subtract(diff, "days").format("YYYY-MM-DD");
        ServicesReportModel.getRevenuecomparsionBasedOnSalonID(req.body, (err, revenue_compare_details) => {

            if (err) {
                //res.status(500).send({message:err});
                next(err);
            }
            else {
                var service_compare_details = {}
                if (revenue_compare_details.revenue.Services.length) {
                    service_compare_details = {
                        "total_bookings": revenue_compare_details.revenue.Services.length,
                        "total_booking_revenue": revenue_compare_details.revenue.Services.map(obj => obj.amount).reduce((a, c) => { return a + c })
                    };
                }
                else {
                    service_compare_details = {

                        "total_bookings": revenue_compare_details.revenue.Services.length,
                        "total_booking_revenue": 0

                    }

                }
                var class_booking_compare_details = {}
                if (revenue_compare_details.revenue.booking_classes.length) {

                    class_booking_compare_details = {
                        "total_bookings": revenue_compare_details.revenue.booking_classes.length,
                        "total_booking_revenue": revenue_compare_details.revenue.booking_classes.map(obj => obj.amount).reduce((a, c) => { return a + c })

                    };
                }
                else {
                    class_booking_compare_details = {

                        "total_bookings": revenue_compare_details.revenue.booking_classes.length,
                        "total_booking_revenue": 0,

                    }
                }
                var product_compare_details = {}
                if (revenue_compare_details.revenue.products.length) {
                    product_compare_details = {
                        "total_bookings": revenue_compare_details.revenue.products.length,
                        "total_booking_revenue": revenue_compare_details.revenue.products.map(obj => obj.total_spent).reduce((a, c) => { return a + c }),
                    };
                }
                else {
                    product_compare_details = {

                        "total_bookings": revenue_compare_details.revenue.products.length,
                        "total_booking_revenue": 0,

                    }
                }
                var total_revenue = class_booking_compare_details.total_booking_revenue + service_compare_details.total_booking_revenue + product_compare_details.total_booking_revenue;
                var current_revenue = {

                    "total_revenue": total_revenue,
                    "services": service_compare_details,
                    "classes": class_booking_compare_details,
                    "products": product_compare_details
                };
                var data = { store_id, prev_period_start_date, prev_period_end_date };
                ServicesReportModel.getPreviousPeriodRevenuecomparsionBasedOnSalonID(data, (err, revenue_prev_compare_details) => {
                    if (err) {
                        //res.status(500).send({message:err});
                        next(err);
                    }
                    else {
                        var service_prev_compare_details = {}
                        if (revenue_prev_compare_details.revenue.Services.length) {
                            service_prev_compare_details = {
                                "total_bookings": revenue_prev_compare_details.revenue.Services.length,
                                "total_booking_revenue": revenue_prev_compare_details.revenue.Services.map(obj => obj.amount).reduce((a, c) => { return a + c })
                            };
                        }
                        else {
                            service_prev_compare_details = {

                                "total_bookings": revenue_prev_compare_details.revenue.Services.length,
                                "total_booking_revenue": 0

                            }

                        }
                        var class_booking_prev_compare_details = {}
                        if (revenue_prev_compare_details.revenue.booking_classes.length) {

                            class_booking_prev_compare_details = {
                                "total_bookings": revenue_prev_compare_details.revenue.booking_classes.length,
                                "total_booking_revenue": revenue_prev_compare_details.revenue.booking_classes.map(obj => obj.amount).reduce((a, c) => { return a + c })

                            };
                        }
                        else {
                            class_booking_prev_compare_details = {

                                "total_bookings": revenue_prev_compare_details.revenue.booking_classes.length,
                                "total_booking_revenue": 0,

                            }
                        }
                        var product_prev_compare_details = {}
                        if (revenue_prev_compare_details.revenue.products.length) {
                            product_prev_compare_details = {
                                "total_bookings": revenue_prev_compare_details.revenue.products.length,
                                "total_booking_revenue": revenue_prev_compare_details.revenue.products.map(obj => obj.total_spent).reduce((a, c) => { return a + c }),
                            };
                        }
                        else {
                            product_prev_compare_details = {

                                "total_bookings": revenue_prev_compare_details.revenue.products.length,
                                "total_booking_revenue": 0,

                            }
                        }
                        var total_revenue = class_booking_prev_compare_details.total_booking_revenue + service_prev_compare_details.total_booking_revenue + product_prev_compare_details.total_booking_revenue;
                        var previous_period_revenue = {

                            "total_revenue": total_revenue,
                            "services": service_prev_compare_details,
                            "classes": class_booking_prev_compare_details,
                            "products": product_prev_compare_details


                        };

                        res.status(200).send({ status: 200, result: {"prev_period_start_date":prev_period_start_date,"prev_period_end_date":prev_period_end_date, current_revenue, previous_period_revenue } });

                    }
                });
            }
        });


    }
}
//Customer Comparison yearly and period wise
exports.getCustomersComparison = (req, res, next) => {
    if (!(req.body.store_id && req.body.start_date && req.body.end_date)) {
        res.status(400).send({ status: 400, message: 'Invalid request parameters. Check headers and body' });
        return;
    }
    const store_id = parseInt(req.body.store_id, 10);
    var start_date = moment(req.body.start_date);
    var end_date = moment(req.body.end_date);
    var customers_data = {};
    if (req.body.compare_to_previous_year == 2) {
        var previous_year_start_date = moment(start_date).subtract(1, 'year').format('YYYY-MM-DD');
        var previous_year_end_date = moment(end_date).subtract(1, 'year').format('YYYY-MM-DD');
        ServicesReportModel.getnewandreturningcustomers(req.body, (err, data) => {

            var customers = JSON.parse(JSON.stringify(data));

            if (err) {
                next(err);
            }
            else {
                var new_customers;
                var returning_customers;
                if (customers.new_customers) {
                    new_customers = customers.new_customers;

                }
                else {
                    new_customers = 0;

                }
                if (customers.return_customers) {
                    returning_customers = customers.return_customers;
                }
                else {
                    returning_customers = 0;
                }

                var total_customers = new_customers + returning_customers;
                var customers_data = { new_customers, returning_customers, total_customers }
                var data_prev = { store_id, previous_year_start_date, previous_year_end_date }
                ServicesReportModel.getCustomersComparisonPrevYear(data_prev, (err, data) => {

                    var customers_prev_compare = JSON.parse(JSON.stringify(data));

                    if (err) {
                        next(err);
                    }
                    else {
                        var new_customers_prev;
                        var returning_customers_prev;
                        if (customers_prev_compare.new_customers) {
                            new_customers_prev = customers_prev_compare.new_customers;

                        }
                        else {
                            new_customers_prev = 0;

                        }
                        if (customers_prev_compare.return_customers) {
                            returning_customers_prev = customers_prev_compare.return_customers;
                        }
                        else {
                            returning_customers_prev = 0;
                        }

                        var total_customers_prev = new_customers_prev + returning_customers_prev;
                        var prev_year_customers_data = { new_customers_prev, returning_customers_prev, total_customers_prev }
                        res.status(200).send({ status: 200, result: { "previous_year_start_date":previous_year_start_date,"previous_year_end_date":previous_year_end_date,customers_data, prev_year_customers_data } });
                    }
                });

            }
        });
    }
    if (req.body.compare_to_previous_period == 1) {
        var diff = (end_date.diff(start_date, 'days'));
        var prev_period_end_date = moment(start_date).subtract(1, 'd').format("YYYY-MM-DD");
        var prev_period_start_date = moment(prev_period_end_date).subtract(diff, "days").format("YYYY-MM-DD");
        ServicesReportModel.getnewandreturningcustomers(req.body, (err, data) => {

            var customers = JSON.parse(JSON.stringify(data));

            if (err) {
                next(err);
            }
            else {
                var new_customers;
                var returning_customers;
                if (customers.new_customers) {
                    new_customers = customers.new_customers;

                }
                else {
                    new_customers = 0;

                }
                if (customers.return_customers) {
                    returning_customers = customers.return_customers;
                }
                else {
                    returning_customers = 0;
                }

                var total_customers = new_customers + returning_customers;
                var customers_data = { new_customers, returning_customers, total_customers }
                var data_prev = { store_id, prev_period_start_date, prev_period_end_date }
                ServicesReportModel.getCustomersComparisonPrevPeriod(data_prev, (err, data) => {

                    var customers_prev_compare = JSON.parse(JSON.stringify(data));

                    if (err) {
                        next(err);
                    }
                    else {
                        var new_customers_prev;
                        var returning_customers_prev;
                        if (customers_prev_compare.new_customers) {
                            new_customers_prev = customers_prev_compare.new_customers;

                        }
                        else {
                            new_customers_prev = 0;

                        }
                        if (customers_prev_compare.return_customers) {
                            returning_customers_prev = customers_prev_compare.return_customers;
                        }
                        else {
                            returning_customers_prev = 0;
                        }

                        var total_customers_prev = new_customers_prev + returning_customers_prev;
                        var prev_period_customers_data = { new_customers_prev, returning_customers_prev, total_customers_prev }
                        res.status(200).send({ status: 200, result: { "prev_period_start_date":prev_period_start_date,"prev_period_end_date":prev_period_end_date,customers_data, prev_period_customers_data } });
                    }
                });

            }
        });
    }
}
