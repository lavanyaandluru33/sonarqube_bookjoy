const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js'); 
//1.get all customers between two dates
var moment = require('moment');
var current_date = moment().format('YYYY-MM-DD');
exports.getAllCustomers = (data, result) => {
    let sql = `SELECT book.*,s.serv_name as last_booked_service FROM (
        SELECT MAX(b.cust_id) as cust_id,MAX(b.book_first_name) as book_first_name,COUNT(DISTINCT ns.book_id) as no_show,COUNT(DISTINCT cl.book_id) as cancel,MAX(b.book_last_name) as book_last_name,MAX(b.book_email) as book_email,MAX(b.book_country_isd_code) as book_country_isd_code,MAX(b.book_mobile) as book_mobile,ac.amount_paid as total_sales,COUNT(DISTINCT b.book_id) as total_visits,substring_index(group_concat(b.serv_id order by b.book_start_date desc), ',', 1) as service_id,max(b.book_start_date) as last_booking_date FROM booking_mst as b LEFT JOIN booking_mst ns ON ns.cust_id = b.cust_id AND ns.book_status="${constants.STATUS_NO_SHOW}"  AND ns.store_id="${data.store_id}" AND ns.book_start_date >= "${data.start_date}" AND ns.book_end_date <= "${data.end_date}" LEFT JOIN booking_mst cl ON cl.cust_id = b.cust_id AND cl.book_status IN(${constants.STATUS_LATE_CANCELLED},${constants.STATUS_CANCELLED})  AND cl.book_start_date >= "${data.start_date}" AND cl.book_end_date <= "${data.end_date}" AND cl.store_id="${data.store_id}" LEFT JOIN(SELECT bc.cust_id,  SUM(bc.book_fullamount) as amount_paid FROM booking_mst bc WHERE bc.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND bc.book_start_date >= "${data.start_date}" AND bc.book_end_date <= "${data.end_date}" AND bc.store_id="${data.store_id}" GROUP BY bc.cust_id) ac ON ac.cust_id= b.cust_id WHERE b.store_id="${data.store_id}" and b.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date >= "${data.start_date}" AND b.book_end_date <= "${data.end_date}" GROUP by b.cust_id) as book,services_mst s WHERE s.serv_id=book.service_id AND s.serv_name != "Break" order by total_sales DESC`;

    db.query(sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);
    });

}
//2.get VIP customers between two dates
exports.getVipCustomers = (data, result) => {

    let sql = `SELECT book.*,s.serv_name as last_booked_service FROM (SELECT MAX(b.cust_id) as cust_id,MAX(b.book_first_name) as book_first_name,MAX(b.book_last_name) as book_last_name,MAX(b.book_country_isd_code) as book_country_isd_code,MAX(b.book_mobile) as book_mobile,MAX(b.book_email) as book_email,ac.amount_paid as total_sales,COUNT(DISTINCT b.book_id) as total_visits,COUNT(DISTINCT ns.book_id) as no_show,COUNT(DISTINCT cl.book_id) as cancel,substring_index(group_concat(b.serv_id order by b.book_start_date desc), ',', 1) as service_id,MAX(b.book_end_date) as last_booking_date FROM booking_mst b LEFT JOIN booking_mst ns ON ns.cust_id = b.cust_id AND ns.book_status="${constants.STATUS_NO_SHOW}" AND ns.book_start_date >= "${data.start_date}" AND ns.book_end_date <= "${data.end_date}" AND ns.store_id="${data.store_id}" LEFT JOIN booking_mst cl ON cl.cust_id = b.cust_id AND cl.book_status IN(${constants.STATUS_LATE_CANCELLED},${constants.STATUS_CANCELLED})  AND cl.book_start_date >= "${data.start_date}" AND cl.book_end_date <= "${data.end_date}" AND cl.store_id="${data.store_id}" INNER JOIN owner_customers_mst o ON b.cust_id=o.cust_id  LEFT JOIN(SELECT bc.cust_id,  SUM(bc.book_fullamount) as amount_paid FROM booking_mst bc WHERE bc.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND bc.book_start_date >= "${data.start_date}" AND bc.book_end_date <= "${data.end_date}" AND bc.store_id="${data.store_id}" GROUP BY bc.cust_id) ac ON ac.cust_id= b.cust_id   WHERE o.is_vip=1 and b.store_id= "${data.store_id}" and b.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date >= "${data.start_date}" AND b.book_end_date <= "${data.end_date}" GROUP BY b.cust_id ) as book,services_mst s WHERE s.serv_id=book.service_id AND s.serv_name!="break" order by total_sales DESC`;

    db.query(sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);
    });

}
//3.get New customers between two dates
exports.getNewCustomers = (data, result) => {

    let sql = `SELECT book.*,s.serv_name as last_booked_service FROM (SELECT MAX(b.cust_id) as cust_id,MAX(b.book_first_name) as book_first_name,MAX(b.book_last_name) as book_last_name,MAX(b.book_country_isd_code) as book_country_isd_code,MAX(b.book_mobile) as book_mobile,MAX(b.book_email) as book_email,ac.amount_paid as total_sales,COUNT( DISTINCT b.book_id) as total_visits,COUNT(DISTINCT ns.book_id) as no_show,COUNT(DISTINCT cl.book_id) as cancel,substring_index(group_concat(b.serv_id order by b.book_start_date desc), ',', 1) as service_id,max(b.book_start_date) as last_booking_date FROM booking_mst as b
    LEFT JOIN booking_mst ns ON ns.cust_id = b.cust_id AND ns.book_status="${constants.STATUS_NO_SHOW}"  AND ns.book_start_date >= "${data.start_date}" AND ns.book_end_date <= "${data.end_date}" AND ns.store_id="${data.store_id}" LEFT JOIN booking_mst cl ON cl.cust_id = b.cust_id AND cl.book_status IN(${constants.STATUS_LATE_CANCELLED},${constants.STATUS_CANCELLED})  AND cl.book_start_date >= "${data.start_date}" AND cl.book_end_date <= "${data.end_date}" AND cl.store_id="${data.store_id}" LEFT JOIN(SELECT bc.cust_id,  SUM(bc.book_fullamount) as amount_paid FROM booking_mst bc WHERE bc.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND bc.book_start_date >= "${data.start_date}" AND bc.book_end_date <= "${data.end_date}" AND bc.store_id="${data.store_id}"  GROUP BY bc.cust_id) ac ON ac.cust_id= b.cust_id WHERE b.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.store_id= "${data.store_id}" AND b.book_start_date >= "${data.start_date}" AND b.book_end_date <= "${data.end_date}" GROUP by b.cust_id HAVING COUNT(b.book_id)>=1 AND cust_id NOT IN(SELECT b.cust_id from booking_mst b WHERE b.store_id= "${data.store_id}" and b.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date < "${data.start_date}"  GROUP by b.cust_id HAVING COUNT(b.book_id)>=1)) as book,services_mst s WHERE s.serv_id=book.service_id AND s.serv_name != "Break" order by total_sales DESC`;

    db.query(sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);
    });

}
//4. Get Returning Customers between dates
exports.getReturningCustomers = (data, result) => {

    let sql = `SELECT book.*,s.serv_name as last_booked_service FROM (SELECT MAX(b.cust_id) as cust_id,MAX(b.book_first_name) as book_first_name,MAX(b.book_last_name) as book_last_name,MAX(b.book_country_isd_code) as book_country_isd_code,MAX(b.book_mobile) as book_mobile,MAX(b.book_email) as book_email,ac.amount_paid as total_sales,COUNT(DISTINCT b.book_id) as total_visits,COUNT(DISTINCT ns.book_id) as no_show,COUNT(DISTINCT cl.book_id) as cancel,substring_index(group_concat(b.serv_id order by b.book_start_date desc), ',', 1) as service_id,max(b.book_start_date) as last_booking_date FROM booking_mst as b
    LEFT JOIN booking_mst ns ON ns.cust_id = b.cust_id AND ns.book_status="${constants.STATUS_NO_SHOW}"  AND ns.book_start_date >= "${data.start_date}" AND ns.book_end_date <= "${data.end_date}" AND ns.store_id="${data.store_id}" LEFT JOIN booking_mst cl ON cl.cust_id = b.cust_id AND cl.book_status IN(${constants.STATUS_LATE_CANCELLED},${constants.STATUS_CANCELLED})  AND cl.book_start_date >= "${data.start_date}" AND cl.book_end_date <= "${data.end_date}" AND cl.store_id="${data.store_id}" LEFT JOIN(SELECT bc.cust_id,  SUM(bc.book_fullamount) as amount_paid FROM booking_mst bc WHERE bc.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND bc.book_start_date >= "${data.start_date}" AND bc.book_end_date <= "${data.end_date}" AND bc.store_id="${data.store_id}"  GROUP BY bc.cust_id) ac ON ac.cust_id= b.cust_id WHERE b.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.store_id= "${data.store_id}" AND b.book_start_date >= "${data.start_date}" AND b.book_end_date <= "${data.end_date}" GROUP by b.cust_id HAVING COUNT(b.book_id)>=1 AND cust_id IN(SELECT b.cust_id from booking_mst b WHERE b.store_id= "${data.store_id}" and b.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date < "${data.start_date}"  GROUP by b.cust_id HAVING COUNT(b.book_id)>=1)) as book,services_mst s WHERE s.serv_id=book.service_id AND s.serv_name != "Break" order by total_sales DESC`;

    db.query(sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);
    });

}
//5. Get Risk Customers between dates
exports.getRiskCustomers = (data, result) => {

    let sql = `SELECT book.*,s.serv_name as last_booked_service FROM (SELECT MAX(b.cust_id) as cust_id,MAX(b.book_first_name) as book_first_name,MAX(b.book_last_name) as book_last_name,MAX(b.book_country_isd_code) as book_country_isd_code,MAX(b.book_mobile) as book_mobile,MAX(b.book_email) as book_email,ac.amount_paid as total_sales,COUNT(DISTINCT b.book_id) as total_visits,COUNT(DISTINCT ns.book_id) as no_show,COUNT(DISTINCT cl.book_id) as cancel,substring_index(group_concat(b.serv_id order by b.book_start_date desc), ',', 1) as service_id,max(b.book_start_date) as last_booking_date FROM booking_mst as b LEFT JOIN booking_mst ns ON ns.cust_id = b.cust_id AND ns.book_status="${constants.STATUS_NO_SHOW}"  AND ns.book_start_date >= "${data.start_date}" AND ns.book_end_date <= "${data.end_date}" AND ns.store_id="${data.store_id}" LEFT JOIN booking_mst cl ON cl.cust_id = b.cust_id AND cl.book_status IN(${constants.STATUS_LATE_CANCELLED},${constants.STATUS_CANCELLED})  AND cl.book_start_date >= "${data.start_date}" AND cl.book_end_date <= "${data.end_date}" AND cl.store_id="${data.store_id}"
    LEFT JOIN(SELECT bc.cust_id,  SUM(bc.book_fullamount) as amount_paid FROM booking_mst bc WHERE bc.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND bc.book_start_date >= "${data.start_date}" AND bc.book_end_date <= "${data.end_date}" AND bc.store_id="${data.store_id}"  GROUP BY bc.cust_id) ac ON ac.cust_id= b.cust_id WHERE b.store_id="${data.store_id}" and b.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date >= "${data.start_date}" AND b.book_end_date <= "${data.end_date}" GROUP by b.cust_id HAVING COUNT(b.book_id)>=1 AND cust_id NOT IN(SELECT b.cust_id from booking_mst b WHERE b.store_id= "${data.store_id}" and b.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date >"${data.end_date}"  GROUP by b.cust_id HAVING COUNT(b.book_id)>=1)) as book,services_mst s WHERE s.serv_id=book.service_id AND s.serv_name != "Break" order by total_sales DESC`;

    db.query(sql, (err, res) => {

        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);
    });

}
//6. Get Revenue of salon
exports.getRevenueBasedOnSalonID = (data, result) => {
    var details = {
        "revenue": {
            "total_revenue": 0,
            "Services": {},
            "booking_classes": {},
            "products": {}
        }
    }

    let services_sql = `SELECT book_start_date as labels,book_fullamount as amount FROM booking_mst WHERE store_id=${data.store_id} AND book_start_date>="${data.start_date}" and book_end_date<="${data.end_date}" AND book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND book_email!='break@bookjoy.in' order by labels
    `;

    db.query(services_sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, details);
        }
        details.revenue.Services = res;
    });

    let classes_sql = `SELECT date as labels,total_cost as amount FROM booking_class_mst WHERE store_id=${data.store_id} AND date>="${data.start_date}" and date<="${data.end_date}" AND class_book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND class_cust_email!='break@bookjoy.in' order by labels
    `;
    db.query(classes_sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, details);
        }
        details.revenue.booking_classes = res;

    });
    let products_sql = `SELECT product_date as labels,product_quantity as quantity,product_full_amount as amount, (product_quantity*product_full_amount) as total_spent FROM booking_products_mst WHERE multi_service_book_id IN(SELECT book_id from booking_mst WHERE book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND store_id=${data.store_id} and book_start_date>="${data.start_date}" and book_end_date<="${data.end_date}") AND product_cust_email!='break@bookjoy.in' order by labels
    `;
    db.query(products_sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, details);
        }
        details.revenue.products = res;
        return result(null, details);
    });

}

//8. Get Revenue of services,products,classes by clicking More between dates
exports.getRevenueByClickingMoreOnService = (data, result) => {

    let sql = `SELECT MAX(sm.serv_id) as serv_id, MAX(sm.serv_name) as service_name, COUNT(distinct tb.book_id) as total_bookings, (COUNT(distinct tb.book_id)*MAX(tb.book_fullamount)) as total_revenue, COUNT(distinct ns.book_id) as no_show, COUNT(distinct cl.book_id) as cancelled FROM services_mst as sm LEFT JOIN booking_mst tb ON tb.serv_id = sm.serv_id AND tb.store_id=${data.store_id} AND tb.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND tb.book_start_date>="${data.start_date}" AND tb.book_start_date<="${data.end_date}" LEFT JOIN booking_mst ns ON ns.serv_id = sm.serv_id AND ns.book_status="${constants.STATUS_NO_SHOW}" AND ns.book_start_date>="${data.start_date}" AND ns.book_start_date<="${data.end_date}" AND ns.store_id=${data.store_id} LEFT JOIN booking_mst cl ON cl.serv_id = sm.serv_id AND cl.book_status IN(${constants.STATUS_LATE_CANCELLED},${constants.STATUS_CANCELLED})  AND cl.book_start_date>="${data.start_date}" AND cl.store_id=${data.store_id} AND cl.book_start_date<="${data.end_date}" WHERE sm.store_id=${data.store_id} AND sm.serv_name!="Break" GROUP BY sm.serv_id order by total_revenue DESC`;

    db.query(sql, (err, res) => {

        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);
    });

}
exports.getRevenueByClickingMoreOnClass = (data, result) => {

    let sql = `SELECT MAX(cs.class_schedule_id) as class_schedule_id, MAX(cs.class_name) as class_name, COUNT(DISTINCT cb.booking_class_id) as total_bookings, (COUNT(distinct cb.booking_class_id)*MAX(cb.total_cost)) as total_revenue, COUNT(distinct ns.booking_class_id) as no_show, COUNT(distinct cl.booking_class_id) as cancelled FROM class_schedule as cs LEFT JOIN booking_class_mst cb ON cs.class_schedule_id = cb.class_schedule_id AND cb.class_book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND cb.date>="${data.start_date}" AND cb.date<="${data.end_date}" AND cb.store_id=${data.store_id} LEFT JOIN booking_class_mst ns ON ns.class_schedule_id = cs.class_schedule_id AND ns.class_book_status="${constants.STATUS_NO_SHOW}" AND ns.date>="${data.start_date}" AND ns.date<="${data.end_date}" AND ns.store_id=${data.store_id} LEFT JOIN booking_class_mst cl ON cl.class_schedule_id = cs.class_schedule_id AND cl.class_book_status IN(${constants.STATUS_LATE_CANCELLED},${constants.STATUS_CANCELLED}) AND cl.store_id=${data.store_id}
     AND cl.date >="${data.start_date}" AND cl.date<="${data.end_date}" WHERE cs.store_id=${data.store_id} GROUP BY cs.class_schedule_id order by total_revenue DESC`;

    db.query(sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);
    });

}
exports.getRevenueByClickingMoreOnProducts = (data, result) => {

    let sql = `SELECT MAX(p.product_id) as product_id, MAX(p.product_name) as product_name, COUNT(DISTINCT bp.booking_product_id) as total_bookings,COUNT(distinct st.booking_product_id) as store, COUNT(distinct ol.booking_product_id) as online FROM products_mst as p LEFT JOIN booking_products_mst bp ON p.product_id = bp.product_id AND bp.multi_service_book_id IN(SELECT book_id from booking_mst WHERE book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND store_id=${data.store_id} and book_start_date>="${data.start_date}" and book_end_date<="${data.end_date}") AND bp.product_cust_email!='break@bookjoy.in' LEFT JOIN booking_products_mst st ON st.product_id = p.product_id AND st.booked_through=1 AND st.multi_service_book_id IN(SELECT book_id from booking_mst WHERE book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND store_id=${data.store_id} and book_start_date>="${data.start_date}" and book_end_date<="${data.end_date}") LEFT JOIN booking_products_mst ol ON ol.product_id = p.product_id AND ol.booked_through=2 AND ol.multi_service_book_id IN(SELECT book_id from booking_mst WHERE book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND store_id=${data.store_id} and book_start_date>="${data.start_date}" and book_end_date<="${data.end_date}") GROUP BY p.product_id
    `;
    var product_data = {
        "booking_product_details": [],
        "product_revenue_details": []
    };
    db.query(sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, product_data);

        }
        product_data.booking_product_details = res;

    });
    let revenue_sql = `SELECT p.product_id as product_id, p.product_name as product_name, bp.product_quantity,bp.product_full_amount,bp.product_discount_full_amount FROM products_mst as p LEFT JOIN booking_products_mst bp ON p.product_id = bp.product_id AND bp.multi_service_book_id IN(SELECT book_id from booking_mst WHERE book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND store_id=${data.store_id} and book_start_date>="${data.start_date}" and book_end_date<="${data.end_date}") AND bp.product_cust_email!='break@bookjoy.in'
    `;
    db.query(revenue_sql, (err, res) => {
        if (err) {

            return result({ error_code: err.code, message: err.sqlMessage }, product_data);
        }
        product_data.product_revenue_details = res;
        return result(null, product_data);
    });

}
exports.getRevenueByClickingMoreonStylists = (data, result) => {

    let sql = `SELECT MAX(b.user_id) as user_id, CONCAT(MAX(u.user_first_name) ," " ,MAX(u.user_last_name)) as stylist_name,COUNT(DISTINCT b.book_id) as total_bookings, (ac.amount_paid) as total_revenue,COUNT(DISTINCT ns.book_id) as no_show,COUNT(DISTINCT cl.book_id) as cancelled FROM booking_mst b LEFT JOIN booking_mst ns ON ns.user_id = b.user_id AND ns.book_status="${constants.STATUS_NO_SHOW}" AND ns.book_start_date >= "${data.start_date}" AND ns.book_end_date <= "${data.end_date}" AND ns.store_id=${data.store_id} LEFT JOIN booking_mst cl ON cl.user_id = b.user_id AND  cl.book_status IN(${constants.STATUS_LATE_CANCELLED},${constants.STATUS_CANCELLED}) AND cl.book_start_date >= "${data.start_date}" AND cl.book_end_date <= "${data.end_date}" AND cl.store_id =${data.store_id} 
    LEFT JOIN(SELECT bc.user_id,  SUM(bc.book_fullamount) as amount_paid FROM booking_mst bc WHERE bc.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND bc.book_start_date >= "${data.start_date}" AND bc.book_end_date <= "${data.end_date}" GROUP BY bc.user_id) ac ON ac.user_id= b.user_id INNER JOIN users_mst u ON u.user_id=b.user_id INNER JOIN users_permissions u1 on u1.user_id=b.user_id and u1.user_perm_is_active=1 and u1.user_perm_is_removed=0 WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date>="${data.start_date}" AND b.book_end_date<="${data.end_date}" and b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.user_id`;

    db.query(sql, (err, res) => {

        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);
    });
}
exports.getStaffBookedTimings = (data, result) => {

    let sql = `SELECT book1.user_id,(sum(book1.booked_time))/60 as booking_hours from(SELECT b.book_start_date,b.user_id,b.book_start_time - INTERVAL s.serv_prebuffertime MINUTE as start_time_with_buffer, b.book_end_time + INTERVAL s.serv_postbuffertime MINUTE as end_time_with_buffer,TIME_TO_SEC(TIMEDIFF(b.book_end_time + INTERVAL s.serv_postbuffertime MINUTE,b.book_start_time - INTERVAL s.serv_prebuffertime MINUTE)) as booked_time,s.serv_prebuffertime, s.serv_postbuffertime, s.is_break FROM booking_mst b INNER JOIN users_mst u ON u.user_id=b.user_id INNER JOIN users_permissions u1 on b.user_id=u1.user_id and u1.user_perm_is_active=1 and u1.user_perm_is_removed=0 INNER JOIN services_mst s ON s.serv_id=b.serv_id WHERE b.store_id =${data.store_id} AND b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date>="${data.start_date}" AND b.book_end_date<="${data.end_date}") book1 GROUP BY book1.user_id


    `;

    db.query(sql, (err, res) => {
        
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);
    });
}
exports.getStaffByStoreforCalendar = (data, result) => {

    let sql = `SELECT  u.user_id as staff_id
    FROM users_permissions as p,users_mst as u
     WHERE p.user_id = u.user_id AND p.store_id = ${data.store_id} 
    `;

    if (data.staff_id) {
        sql += ` AND u.user_id IN (${data.staff_id})`;
    }

    sql += ` ORDER BY p.user_staff_order`;



    db.query(sql, (err, res) => {
        if (err) {
            //console.log(err);
            return result({ error_code: err.code, message: err.sqlMessage, sql: err.sql }, null);

        }
        return result(null, res);


    });
}
exports.getStaffHours = (data, result) => {


    let sql = `SELECT * FROM user_hours_week 
          where store_id =${data.store_id} `;

    if (data.day_of_week || data.day_of_week == 0) {
        sql += ` AND user_hrwk_day_of_week =${data.day_of_week}`;
    }


    if (data.staff_id) {
        sql += ` AND user_id = ${data.staff_id}`;
    }


    // console.log(sql);

    db.query(sql, (err, res) => {
        if (err) {
            //console.log(err);
            return result({ error_code: err.code, message: err.sqlMessage, sql: err.sql }, null);

        }
        //console.log(res);

        return result(null, res);


    });

}
exports.getStaffHourExceptions = (data, result) => {

    // console.log("helloooo"+data.staff_id)
    let sql = `SELECT * FROM user_hours_exceptions
            WHERE store_id =${data.store_id} `;

    if (data.date) {
        sql += ` AND user_hrex_specific_date ='${data.date}'`;
    }

    if (data.staff_id) {
        sql += ` AND user_id = ${data.staff_id}`;
    }

    sql += ` ORDER BY user_hrex_id DESC`



    // console.log(sql);

    db.query(sql, (err, res) => {
        if (err) {
            //console.log(err);
            return result({ error_code: err.code, message: err.sqlMessage, sql: err.sql }, null);

        }
        //console.log(res);

        return result(null, res);


    });

}



exports.getnewandreturningcustomers = (data, result) => {

    var details = {
        "new_customers": 0,
        "return_customers": 0
    };
    let sql = `SELECT COUNT(DISTINCT book1.new) as new_customers FROM(SELECT DISTINCT b.cust_id as new FROM booking_mst b WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date>="${data.start_date}" AND b.book_end_date<="${data.end_date}" AND b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.cust_id HAVING COUNT(b.book_id)>=1 AND b.cust_id NOT IN(SELECT b.cust_id AS cust_id FROM booking_mst b WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date<"${data.start_date}" AND b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.cust_id HAVING COUNT(b.book_id)>=1)) as book1`;

    db.query(sql, (err, res) => {

        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        details.new_customers = res[0].new_customers;
    });
    let returing_customers_sql = `SELECT COUNT(DISTINCT book2.return_customers) as returning_customers FROM(SELECT DISTINCT b.cust_id as return_customers FROM booking_mst b WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date>="${data.start_date}" AND b.book_end_date<="${data.end_date}" AND b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.cust_id HAVING COUNT(b.book_id)>=1 AND b.cust_id IN(SELECT b.cust_id AS cust_id FROM booking_mst b WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date<"${data.start_date}" AND b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.cust_id HAVING COUNT(b.book_id)>=1)) as book2`;

    db.query(returing_customers_sql, (err, res1) => {

        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        details.return_customers = res1[0].returning_customers;
        return result(null, details);

    });
}
//Top Five Customers
exports.topfivecustomers = (data, result) => {

    let sql = `SELECT MAX(b.cust_id) as cust_id,MAX(b.book_first_name) as book_first_name,MAX(b.book_last_name)as book_last_name,Max(u.cust_first_name) as user_first_name,Max(u.cust_last_name) as user_last_name, COUNT(DISTINCT b.book_id) as bookings, SUM(b.book_fullamount) as revenue FROM booking_mst b INNER JOIN owner_customers_mst u on u.cust_id = b.cust_id WHERE b.book_start_date>="${data.start_date}" AND b.book_end_date<="${data.end_date}" AND b.store_id=${data.store_id} AND b.book_email!='Break@bookjoy.in' AND b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) GROUP BY b.cust_id ORDER BY revenue DESC LIMIT 5`;

    db.query(sql, (err, res) => {

        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);

    });
}
//Top Five Stylists
exports.topfivestylists = (data, result) => {

    let sql = `SELECT MAX(b.user_id) as user_id ,MAX(u.user_first_name) as staff_first_name, MAX(u.user_last_name) as staff_last_name,COUNT(DISTINCT b.book_id) as bookings,SUM(b.book_fullamount) as revenue FROM booking_mst b INNER JOIN users_mst u on u.user_id = b.user_id WHERE b.book_start_date>="${data.start_date}" AND b.book_end_date<="${data.end_date}" AND b.store_id=${data.store_id} AND b.book_email!='Break@bookjoy.in' AND b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) GROUP BY b.user_id ORDER BY revenue DESC LIMIT 5
    `;

    db.query(sql, (err, res) => {

        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);

    });
}
//top five services
exports.topfiveservices = (data, result) => {

    let sql = `SELECT MAX(b.serv_id) as serv_id, MAX(s.serv_name) as serv_name,count(DISTINCT b.book_id) as total_bookings,SUM(b.book_fullamount) as revenue
    FROM booking_mst b, services_mst s WHERE b.serv_id = s.serv_id AND b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.store_id= ${data.store_id} AND b.book_email not like 'break@bookjoy.in' AND b.book_start_date>="${data.start_date}" AND b.book_end_date<="${data.end_date}"  group by b.serv_id 
    order by revenue DESC limit 5
    `;

    db.query(sql, (err, res) => {

        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);

    });
}
//top five products
exports.topfiveproducts = (data, result) => {
   
    let sql = `SELECT bp.product_id as product_id,bp.product_name as product_name, bp.product_quantity,bp.product_discount_full_amount,bp.product_full_amount FROM booking_products_mst bp WHERE bp.multi_service_book_id IN(SELECT book_id from booking_mst WHERE book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND store_id=${data.store_id} and book_start_date>="${data.start_date}" and book_end_date<="${data.end_date}") AND bp.product_cust_email!='break@bookjoy.in' order by product_id DESC
    `;

    db.query(sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        return result(null, res);
    });
    
}
//Revenue Comparison between periods
exports.getRevenuecomparsionBasedOnSalonID = (data, result) => {
    var present_details = {
        "revenue": {
            "total_revenue": 0,
            "Services": {},
            "booking_classes": {},
            "products": {}
        }
    }

    let services_sql = `SELECT book_start_date as labels,book_fullamount as amount FROM booking_mst WHERE store_id=${data.store_id} AND book_start_date>="${data.start_date}" and book_end_date<="${data.end_date}" AND book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND book_email!='break@bookjoy.in' order by labels
    `;

    db.query(services_sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, present_details);
        }
        present_details.revenue.Services = res;
    });

    let classes_sql = `SELECT date as labels,total_cost as amount FROM booking_class_mst WHERE store_id=${data.store_id} AND date>="${data.start_date}" and date<="${data.end_date}" AND class_book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND class_cust_email!='break@bookjoy.in' order by labels
    `;
    db.query(classes_sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, present_details);
        }
        present_details.revenue.booking_classes = res;

    });
    let products_sql = `SELECT product_date as labels,product_quantity as quantity,product_full_amount as amount, (product_quantity*product_full_amount) as total_spent FROM booking_products_mst WHERE multi_service_book_id IN(SELECT book_id from booking_mst WHERE book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND store_id=${data.store_id} and book_start_date>="${data.start_date}" and book_end_date<="${data.end_date}") AND product_cust_email!='break@bookjoy.in' order by labels
    `;
    db.query(products_sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, present_details);
        }
        present_details.revenue.products = res;
        return result(null, present_details);
    });

}
//Revenue Comparison between previous Years
exports.getPreviousYearRevenuecomparsionBasedOnSalonID = (data, result) => {
    var details_prev = {
        "revenue": {
            "total_revenue": 0,
            "Services": {},
            "booking_classes": {},
            "products": {}
        }
    }

    let services_sql = `SELECT book_start_date as labels,book_fullamount as amount FROM booking_mst WHERE store_id=${data.store_id} AND book_start_date>="${data.previous_year_start_date}" and book_end_date<="${data.previous_year_end_date}" AND book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND book_email!='break@bookjoy.in' order by labels
    `;
    db.query(services_sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, details_prev);
        }
        details_prev.revenue.Services = res;
    });

    let classes_sql = `SELECT date as labels,total_cost as amount FROM booking_class_mst WHERE store_id=${data.store_id} AND date>="${data.previous_year_start_date}" and date<="${data.previous_year_end_date}" AND class_book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND class_cust_email!='break@bookjoy.in' order by labels
    `;
    db.query(classes_sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, details_prev);
        }
        details_prev.revenue.booking_classes = res;

    });
    let products_sql = `SELECT product_date as labels,product_quantity as quantity,product_full_amount as amount, (product_quantity*product_full_amount) as total_spent FROM booking_products_mst where multi_service_book_id IN(SELECT book_id from booking_mst WHERE book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND store_id=${data.store_id} and book_start_date>="${data.previous_year_start_date}" and book_end_date<="${data.previous_year_end_date}") AND product_cust_email!='break@bookjoy.in' order by labels
    `;
    db.query(products_sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, details_prev);
        }
        details_prev.revenue.products = res;
        return result(null, details_prev);
    });

}
//Revenue Comparison between previous period
exports.getPreviousPeriodRevenuecomparsionBasedOnSalonID = (data, result) => {
    var details_prev_period_revenue = {
        "revenue": {
            "total_revenue": 0,
            "Services": {},
            "booking_classes": {},
            "products": {}
        }
    }

    let services_sql = `SELECT book_start_date as labels,book_fullamount as amount FROM booking_mst WHERE store_id=${data.store_id} AND book_start_date>="${data.prev_period_start_date}" and book_end_date<="${data.prev_period_end_date}" AND book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND book_email!='break@bookjoy.in' order by labels
    `;
    db.query(services_sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, details_prev_period_revenue);
        }
        details_prev_period_revenue.revenue.Services = res;
    });
    let classes_sql = `SELECT date as labels,total_cost as amount FROM booking_class_mst WHERE store_id=${data.store_id} AND date>="${data.prev_period_start_date}" and date<="${data.prev_period_end_date}" AND class_book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND class_cust_email!='break@bookjoy.in' order by labels
    `;
    db.query(classes_sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, details_prev_period_revenue);
        }
        details_prev_period_revenue.revenue.booking_classes = res;

    });
    let products_sql = `SELECT product_date as labels,product_quantity as quantity,product_full_amount as amount, (product_quantity*product_full_amount) as total_spent FROM booking_products_mst WHERE multi_service_book_id IN(SELECT book_id from booking_mst WHERE book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND store_id=${data.store_id} and book_start_date>="${data.prev_period_start_date}" and book_end_date<="${data.prev_period_end_date}") AND product_cust_email!='break@bookjoy.in' order by labels
    `;
    db.query(products_sql, (err, res) => {
        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, details_prev_period_revenue);
        }
        details_prev_period_revenue.revenue.products = res;
        return result(null, details_prev_period_revenue);
    });

}





exports.getCustomersComparisonPrevYear = (data, result) => {

    var details = {
        "new_customers": 0,
        "return_customers": 0
    };
    let sql = `SELECT COUNT(DISTINCT book1.new) as new_customers FROM(SELECT DISTINCT b.cust_id as new FROM booking_mst b WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date>="${data.previous_year_start_date}" AND b.book_end_date<="${data.previous_year_end_date}" AND b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.cust_id HAVING COUNT(b.book_id)>=1 AND b.cust_id NOT IN(SELECT b.cust_id AS cust_id FROM booking_mst b WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date<"${data.previous_year_start_date}" AND b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.cust_id HAVING COUNT(b.book_id)>=1)) as book1`;

    db.query(sql, (err, res) => {

        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        details.new_customers = res[0].new_customers;
    });
    let returing_customers_sql = `SELECT COUNT(DISTINCT book2.return_customers) as returning_customers FROM(SELECT DISTINCT b.cust_id as return_customers FROM booking_mst b WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date>="${data.previous_year_start_date}" AND b.book_end_date<="${data.previous_year_end_date}" AND b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.cust_id HAVING COUNT(b.book_id)>=1 AND b.cust_id IN(SELECT b.cust_id AS cust_id FROM booking_mst b WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date<"${data.previous_year_start_date}" AND b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.cust_id HAVING COUNT(b.book_id)>=1)) as book2`;

    db.query(returing_customers_sql, (err, res1) => {

        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        details.return_customers = res1[0].returning_customers;
        return result(null, details);

    });
}
exports.getCustomersComparisonPrevPeriod = (data, result) => {

    var details = {
        "new_customers": 0,
        "return_customers": 0
    };
    let sql = `SELECT COUNT(DISTINCT book1.new) as new_customers FROM(SELECT DISTINCT b.cust_id as new FROM booking_mst b WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date>="${data.prev_period_start_date}" AND b.book_end_date<="${data.prev_period_end_date}" AND b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.cust_id HAVING COUNT(b.book_id)>=1 AND b.cust_id NOT IN(SELECT b.cust_id AS cust_id FROM booking_mst b WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date<"${data.prev_period_start_date}" AND b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.cust_id HAVING COUNT(b.book_id)>=1)) as book1`;

    db.query(sql, (err, res) => {

        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        details.new_customers = res[0].new_customers;
    });
    let returing_customers_sql = `SELECT COUNT(DISTINCT book2.return_customers) as returning_customers FROM(SELECT DISTINCT b.cust_id as return_customers FROM booking_mst b WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date>="${data.prev_period_start_date}" AND b.book_end_date<="${data.prev_period_end_date}" AND b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.cust_id HAVING COUNT(b.book_id)>=1 AND b.cust_id IN(SELECT b.cust_id AS cust_id FROM booking_mst b WHERE b.book_status IN(${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) AND b.book_start_date<"${data.prev_period_start_date}" AND b.book_email!='Break@bookjoy.in' AND b.store_id=${data.store_id} GROUP BY b.cust_id HAVING COUNT(b.book_id)>=1)) as book2`;

    db.query(returing_customers_sql, (err, res1) => {

        if (err) {
            return result({ error_code: err.code, message: err.sqlMessage }, null);
        }
        details.return_customers = res1[0].returning_customers;
        return result(null, details);

    });
}



