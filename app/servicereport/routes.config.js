const serviceReportsController = require('./controllers/servicereport.controller');
const auth = require('../../config/auth');

exports.routesConfig = function (app) {

    app.post('/servicereport/get_all_customers',
        serviceReportsController.getAllCustomers
    );
    app.post('/servicereport/get_vip_customers',
        serviceReportsController.getVipCustomers
    );
    app.post('/servicereport/get_new_customers',
        serviceReportsController.getNewCustomers
    );
    app.post('/servicereport/get_returning_customers',
        serviceReportsController.getReturningCustomers
    );
    app.post('/servicereport/get_risk_customers',
        serviceReportsController.getRiskCustomers
    );
    app.post('/servicereport/get_revenue_of_salon',
        serviceReportsController.getRevenueBasedOnSalonID
    );
    app.post('/servicereport/get_revenue_by_clicking_more',
        serviceReportsController.getRevenueByClickingMore
    );
    app.post('/servicereport/get_new_and_returning_customers',
        serviceReportsController.getNewandReturningCustomers
    );
    app.post('/servicereport/top_five_customers',
        serviceReportsController.topFiveCustomers
    );
    app.post('/servicereport/top_five_stylists',
        serviceReportsController.topFiveStylists
    );
    app.post('/servicereport/top_five_services',
    serviceReportsController.topFiveServices
   );
   app.post('/servicereport/top_five_products',
   serviceReportsController.topFiveProducts
  );
  app.post('/servicereport/get_revenue_comparison_based_on_salon',
   serviceReportsController.getRevenueCompareBasedOnSalonID
  );
  app.post('/servicereport/get_customers_comparison_based_on_salon',
   serviceReportsController.getCustomersComparison
  );
  


}
