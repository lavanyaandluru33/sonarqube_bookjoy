const LoginController = require('./controllers/login.controller');
const auth = require('../../config/auth');
exports.routesConfig = function (app) {
    app.post('/login', [
        LoginController.checkLogin
    ]);
    
    app.get('/listusers',
        LoginController.listUsers
    );
    app.post('/gettoken',
        LoginController.getToken
    );
    app.post('/logout',auth.authenticateJWT,
        LoginController.logOut
    );

    app.post('/v2/login/cust_login_v2', [
        LoginController.customerLogin
    ]);
}