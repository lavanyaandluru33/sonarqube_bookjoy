const constants = require('../../../config/constants.js');
const LoginModel = require('../models/login.model.js');

const CryptoJS = require('crypto-js');


exports.checkLogin = async (req, res) => {
    const {username, password} = req.body;
    result = await LoginModel.checkLogin(req.body);
    if (result.length > 0) {
        // Generate an access token
        const accessToken = constants.jwt.sign({user_id: result[0].user_id, user_email: result[0].user_email}, constants.accessTokenSecret, {expiresIn: '20m'});
        const refreshToken = constants.jwt.sign({user_id: result[0].user_id,user_email: result[0].user_email}, constants.refreshTokenSecret);
        constants.refreshTokens.push(refreshToken);
        res.json({
            message: 'success',
            error: null,
            result: {
                accessToken,
                refreshToken
            }
        });
    } else {
        res.json({message: 'failure', error: 'Username/password incorrect', result: null});
    }
};
exports.listUsers = async (req, res) => {
    result = await LoginModel.getList();
    res.json({
        message: 'success',
        error: null,
        result: result        
    });
};

exports.getToken = (req, res) => {
    console.log(req.user)
    const {token} = req.body;
    if (!token) {
        return res.sendStatus(401);
    }

    if (!constants.refreshTokens.includes(token)) {
        return res.sendStatus(403);
    }

    constants.jwt.verify(token, constants.refreshTokenSecret, (err, user) => {
        if (err) {
            return res.sendStatus(403);
        } 
        const accessToken = constants.jwt.sign({user_id: req.user_id, user_email: req.user_email}, constants.accessTokenSecret, {expiresIn: '20m'});
        res.json({
            accessToken
        });
    });
};

exports.logOut = (req, res) => {
    const {token} = req.body;
    if (constants.refreshTokens) {
        constants.refreshTokens = constants.refreshTokens.filter(token1 => token1 !== token);
        console.log(constants.refreshTokens);
    }
    res.send("Logout successful");
};



exports.customerLogin = (req, res, next) => {


    if (!req.body.cust_user_name || !req.body.password) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var cust_email = req.body.cust_user_name;
    cust_email = cust_email.trim().toLowerCase();


    var cust_password_ciphertext = req.body.password;

// // Encrypt
// var ciphertext = CryptoJS.AES.encrypt(cust_password_ciphertext, constants.crypto_key).toString();
//     console.log("crypto :",ciphertext);

 
// Decrypt
var bytes  = CryptoJS.AES.decrypt(cust_password_ciphertext, constants.crypto_key);
var original_password = bytes.toString(CryptoJS.enc.Utf8);

//console.log("decrypted :",original_password);

var login_req= {
    cust_user_name:cust_email,
    password: original_password
}
 

    LoginModel.checkCustomerLogin(login_req, (err, data) => {
        if (err) {
            next(err);
        } else {
            //console.log(data);
           
            if (data.length > 0) {
                // Generate an access token
                const accessToken = constants.jwt.sign({cust_id: data[0].cust_id, cust_email: data[0].cust_email}, constants.accessTokenSecret, {expiresIn: '20m'});
                const refreshToken = constants.jwt.sign({cust_id: data[0].cust_id,cust_email: data[0].cust_email}, constants.refreshTokenSecret);
                constants.refreshTokens.push(refreshToken);

                var cust_result = data[0] ;
                cust_result.accessToken = accessToken;
                cust_result.refreshToken = refreshToken;

                res.status(200).send({status: 200, message:"Success", result: cust_result});
                
            } else {
                res.status(400).send({status:400, result:{message: 'failure', error: 'Incorrect email or password'}});
               
            }
       }
    })
}

