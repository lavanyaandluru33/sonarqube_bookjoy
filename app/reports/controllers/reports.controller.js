const constants = require('../../../config/constants.js');
const db = require('../../../config/db.js');

var moment = require('moment'); 




//query -- 1. Top customer by revenue (book_fullamount)

exports.getTopCustomer = (req, res, next) => {

    if(!req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    const store_id = parseInt(req.body.store_id,10);

    let sql= `SELECT cust_id, book_first_name, book_last_name, book_start_date, store_id, serv_id, book_status, 
    sum(book_fullamount) as FULLAMOUNT FROM
    booking_mst WHERE store_id= ${store_id} and  book_status between 199 and 300  and book_email not like  'break@bookjoy.in' group by cust_id order by sum(book_fullamount) desc 
    limit 1`;

    //console.log(sql);
    
    db.query(sql, function(err, results){
        if (err) {   
            next({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
        else{
            res.status(200).send({status:200, result:results});
        }
    });   
};


//query -- 2. Top customer by revenue on given date 

exports.getTopCustomerByDate = (req, res, next) => {

    if(!req.body.store_id || !req.body.book_start_date) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    const store_id = parseInt(req.body.store_id,10);
    const book_start_date = moment(req.body.book_start_date).format("YYYY-MM-DD");

    let sql= `SELECT cust_id, book_first_name, book_last_name, book_start_date, store_id, serv_id, book_status,
           sum(book_fullamount) as FULLAMOUNT FROM booking_mst WHERE store_id= ${store_id} and book_start_date= '${book_start_date}' and  book_status between
           199 and 300 and book_email not like  'break@bookjoy.in' group by cust_id order by sum(book_fullamount) desc 
           limit 1`;

    //console.log(sql);
    
    db.query(sql, function(err, results){
        if (err) {   
            next({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
        else{
            res.status(200).send({status:200, result:results});
        }
    });   
};


//3.Time of the days that are most visited 
exports.MostVisitedTimeOfDays = (req, res, next) => {

    if(!req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    const store_id = parseInt(req.body.store_id,10);

   
    let sql= `SELECT book_start_time, store_id, COUNT(book_start_time) as number_of_times_visted FROM
        booking_mst where store_id= ${store_id} and book_email not like  'break@bookjoy.in'
        GROUP BY book_start_time order by count(book_start_time) DESC LIMIT 1`;

    //console.log(sql);
    
    db.query(sql, function(err, results){
        if (err) {   
            next({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
        else{
            res.status(200).send({status:200, result:results});
        }
    });   
};



//4.Time of the given date that are most visited 
exports.MostVisitedTimeByDate = (req, res, next) => {

    if(!req.body.store_id || !req.body.book_start_date) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    const store_id = parseInt(req.body.store_id,10);
    const book_start_date = moment(req.body.book_start_date).format("YYYY-MM-DD");

    let sql= `SELECT book_start_time, book_start_date, store_id, COUNT(book_start_time) as number_of_times_visited FROM
    booking_mst where store_id= ${store_id} and book_start_date= '${book_start_date}' and book_email not like 'break@bookjoy.in'
    GROUP BY book_start_time order by count(book_start_time) DESC LIMIT 1`;

    //console.log(sql);
    
    db.query(sql, function(err, results){
        if (err) {   
            next({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
        else{
            res.status(200).send({status:200, result:results});
        }
    });   
};



//5.Most Visited Day of Week
exports.MostVisitedDayOfWeek = (req, res, next) => {

    if(!req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    const store_id = parseInt(req.body.store_id,10);
    
  
    let sql= `SELECT DAYNAME(book_start_date) AS day_name,
         COUNT(book_start_date) AS total_visits from booking_mst WHERE store_id= ${store_id} and book_email not like 'break@bookjoy.in'
         GROUP BY DAYNAME(book_start_date) ORDER BY COUNT(book_start_date) DESC limit 1`;

    //console.log(sql);
    
    db.query(sql, function(err, results){
        if (err) {   
            next({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
        else{
            res.status(200).send({status:200, result:results});
        }
    });   
};




//6.Top customer by visits
exports.TopCustomerByVisits = (req, res, next) => {

    if(!req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    const store_id = parseInt(req.body.store_id,10);
 
    let sql= `SELECT cust_id, store_id, book_first_name, book_last_name, count(book_start_date) as total_visits FROM 
        booking_mst WHERE store_id= ${store_id} AND book_email not like 'break@bookjoy.in' AND book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED})
        GROUP BY cust_id ORDER BY count(book_start_date) desc limit 1
        `;

    //console.log(sql);
    
    db.query(sql, function(err, results){
        if (err) {   
            next({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
        else{
            res.status(200).send({status:200, result:results});
        }
    });   
};



//7.Top visited Customer by date
exports.TopVisitedCustomerByDate = (req, res, next) => {

    if(!req.body.store_id || !req.body.book_start_date) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    const store_id = parseInt(req.body.store_id,10);
    const book_start_date = moment(req.body.book_start_date).format("YYYY-MM-DD");  


    let sql= `SELECT cust_id, store_id, book_first_name, book_last_name, book_start_date, count(book_start_date) as total_visits FROM 
    booking_mst WHERE store_id= ${store_id} AND book_start_date = '${book_start_date}' AND book_email not like 'break@bookjoy.in' AND book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED})
    GROUP BY cust_id ORDER BY count(book_start_date) desc limit 1`;

    //console.log(sql);
    
    db.query(sql, function(err, results){
        if (err) {   
            next({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
        else{
            res.status(200).send({status:200, result:results});
        }
    });   
};





//8.customers who came in 3-5 weeks ago 
exports.ThreeToFiveWeeksCustomers = (req, res, next) => {

    if(!req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    const store_id = parseInt(req.body.store_id,10);

   // select * from (select * from booking_mst where book_start_date between (now() - interval 5 week)
//     and now()  and store_id=? and book_email not like "break@bookjoy.in" order by book_start_date desc) a where book_start_date not in
//    (select book_start_date from booking_mst where book_start_date between (now() - interval 3 week) and now() order 
//    by book_start_date desc) order by a.book_start_date
 
    let sql= `SELECT * FROM booking_mst WHERE store_id= ${store_id} AND book_start_date between (now() - interval 5 week)
         and (now() - interval 3 week) AND book_email not like 'break@bookjoy.in' AND book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) ORDER BY book_start_date`;

    //console.log(sql);
    
    db.query(sql, function(err, results){
        if (err) {   
            next({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
        else{
            res.status(200).send({status:200, result:results});
        }
    });   
};




//9. Top 5 booked services in the past 30 days (by number of services booked by admin & customer) 
// where booked_through=1 represents admin, booked_through=2 represents customer  
exports.TopFiveServices = (req, res, next) => {

    if(!req.body.store_id || !req.body.booked_through) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    const store_id = parseInt(req.body.store_id,10);

    //    select a.serv_name, a.serv_id, a.serv_cat_id, a.store_id, count(a.serv_id) as TOTAL_COUNT
    //     from services_mst a inner join
    //    booking_mst b on a.serv_id = b.serv_id where a.store_id=? and (b.book_start_date BETWEEN
    //    NOW() - INTERVAL 30 DAY AND NOW()) and b.booked_through=2  group by b.serv_id 
    //    order by count(a.serv_id) desc limit 5
 
    let sql= `SELECT b.serv_id, s.serv_name, s.serv_cat_id, b.store_id, count(b.serv_id) as total_bookings
        FROM booking_mst b, services_mst s WHERE b.serv_id = s.serv_id AND b.store_id= ${store_id} AND b.book_email not like 'break@bookjoy.in' AND (b.book_start_date BETWEEN
        (NOW() - INTERVAL 30 DAY) AND NOW()) AND b.booked_through= ${req.body.booked_through} group by b.serv_id 
        order by count(b.serv_id) DESC limit 5`;

    //console.log(sql);
    
    db.query(sql, function(err, results){
        if (err) {   
            next({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
        else{
            res.status(200).send({status:200, result:results});
        }
    });   
};



//  /*query -- 6.	what are the top 5 booked services in the past 30 days (by number of services booked by admin & customer)
//   where booked_through=1 represents admin, b_t=2 represents customer  */
//   app.get('/top_5_customers/:store_id', function(req, res){
//     const store_id = req.params.store_id;
 
//    db.query(`select a.serv_name, a.serv_id, a.serv_cat_id, a.store_id, count(a.serv_id) as TOTAL_COUNT
//     from services_mst a inner join
//    booking_mst b on a.serv_id = b.serv_id where a.store_id=? and (b.book_start_date BETWEEN
//    NOW() - INTERVAL 30 DAY AND NOW()) and b.booked_through=2  group by b.serv_id 
//    order by count(a.serv_id) desc limit 5`, [store_id], function(err, results){
//        if(err) throw err 
//        res.send(JSON.stringify({"status": 200, "error": null, "response":results}));
//    });   
// });

// app.get('/top_5_customers/:store_id/:booked_through', function(req, res){
//     const store_id = req.params.store_id;
//     const booked_through = req.params.booked_through;

//    db.query(`select a.serv_name, a.serv_id, a.serv_cat_id, a.store_id, count(a.serv_id) as TOTAL_COUNT
//     from services_mst a inner join
//    booking_mst b on a.serv_id = b.serv_id where a.store_id=? and (b.book_start_date BETWEEN
//    NOW() - INTERVAL 30 DAY AND NOW()) and b.booked_through=?  group by b.serv_id 
//    order by count(a.serv_id) desc limit 5`, [store_id, booked_through], function(err, results){
//        if(err) throw err 
//        res.send(JSON.stringify({"status": 200, "error": null, "response":results}));
//    });   
// });



//10. Top 5 booked services in the past 30 days (by the total revenue of the services
//      by admin & customer) 
// where booked_through=1 represents admin, booked_through=2 represents customer  
exports.TopFiveServicesByRevenue = (req, res, next) => {

    if(!req.body.store_id || !req.body.booked_through) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    const store_id = parseInt(req.body.store_id,10);

    //   select a.serv_name, a.serv_id, a.serv_cat_id, a.store_id, b.book_status, sum(b.book_fullamount) as 
    //      FULLAMOUNT
    //      from services_mst a inner join booking_mst b on a.serv_id = b.serv_id where (b.book_start_date BETWEEN 
    //      NOW() - INTERVAL 30 DAY AND NOW())  and b.store_id=? and b.booked_through=2 group by b.serv_id having b.book_status
    //      BETWEEN 199 and 300  order by sum(b.book_fullamount) desc limit 5
 
    let sql= `SELECT b.serv_id, s.serv_name, s.serv_cat_id, b.store_id, SUM(b.book_fullamount) as total_revenue
        FROM booking_mst b, services_mst s WHERE b.serv_id = s.serv_id AND b.store_id= ${store_id} AND b.book_email not like 'break@bookjoy.in' AND (b.book_start_date BETWEEN
        (NOW() - INTERVAL 30 DAY) AND NOW()) AND b.book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) 
        AND b.booked_through= ${req.body.booked_through} group by b.serv_id order by SUM(b.book_fullamount) DESC limit 5`;

    //console.log(sql);
    
    db.query(sql, function(err, results){
        if (err) {   
            next({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
        else{
            res.status(200).send({status:200, result:results});
        }
    });   
};




//11. Get the customers who booked in last 4 weeks
exports.LastFourWeeksCustomers = (req, res, next) => {

    if(!req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    const store_id = parseInt(req.body.store_id,10);

  //  select book_start_date, store_id
 //     from booking_mst where book_start_date between
 //    (now() - interval 4 week) and now() and store_id=? order by book_start_date desc

    let sql= `SELECT cust_id, store_id, book_start_date, book_first_name, book_last_name FROM booking_mst WHERE store_id= ${store_id} AND book_email not like 'break@bookjoy.in' AND book_start_date between
      (now() - interval 4 week) and now() AND book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) GROUP BY cust_id ORDER BY book_start_date desc`;

    //console.log(sql);
    
    db.query(sql, function(err, results){
        if (err) {   
            next({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
        else{
            res.status(200).send({status:200, result:results});
        }
    });   
};



//12. Get all the new customers(only one/two booking with status 200-299) in between last 8-4 weeks.
//  Find all NEW customers who booked between the last 8 weeks - 4 weeks.
// For each customer, find if they have a booking after last 4weeks.
// Print the count of customers from step 2 */ 
exports.CustomersFourToEightWeeks = (req, res, next) => {

    if(!req.body.store_id) {
        res.status(400).send({message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    const store_id = parseInt(req.body.store_id,10);

    //select cust_id, store_id, count(*), book_first_name, book_status, book_start_date from
    //    (select * from (select * from booking_mst where book_start_date between (now() - interval 8 week) and now() 
    //    and store_id=? order by  book_start_date desc) a where book_start_date not in (select book_start_date from booking_mst 
    //       where book_start_date between (now() - interval 4 week) and now() order by book_start_date desc) 
    //       order by a.book_start_date) c where cust_id in (select cust_id from booking_mst where book_start_date 
    //       between (now() - interval 4 week) and now() )and book_status between 199 and 300  and store_id=5 group by
    //        cust_id order by count(*) desc ;

    let sql= `SELECT cust_id, store_id, count(book_id) as number_of_bookings, book_first_name, book_last_name, book_start_date from booking_mst WHERE store_id= ${store_id} AND book_email not like 'break@bookjoy.in'
     AND book_start_date between (now() - INTERVAL 8 week) and (now() - INTERVAL 4 week) and cust_id IN (select cust_id from booking_mst where store_id= ${store_id} AND book_email not like 'break@bookjoy.in' 
     AND book_start_date between (now() - interval 4 week) and now() AND book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}))
     AND book_status IN (${constants.STATUS_BOOKED},${constants.STATUS_ARRIVED},${constants.STATUS_COMPLETED}) group by
     cust_id order by count(book_id) DESC`;

    //console.log(sql);
    
    db.query(sql, function(err, results){
        if (err) {   
            next({error_code:err.code, message:err.sqlMessage,sql:err.sql});
        }
        else{
            res.status(200).send({status:200, result:results});
        }
    });   
};








//  /* query-- 9.	Get all the new customers(only one/two booking with status 200-299) in between last 8-4 weeks.
//  Find all NEW customers who booked between the last 8 weeks - 4 weeks.
// For each customer, find if they have a booking after last 4weeks.
// Print the count of customers from step 2 */ 
// app.get('/customers_4_8weeks/:store_id', function(req, res){
//     const store_id = req.params.store_id;

//    db.query(`select cust_id, store_id, count(*), book_first_name, book_status, book_start_date from
//    (select * from (select * from booking_mst where book_start_date between (now() - interval 8 week) and now() 
//    and store_id=? order by  book_start_date desc) a where book_start_date not in (select book_start_date from booking_mst 
//       where book_start_date between (now() - interval 4 week) and now() order by book_start_date desc) 
//       order by a.book_start_date) c where cust_id in (select cust_id from booking_mst where book_start_date 
//       between (now() - interval 4 week) and now() )and book_status between 199 and 300  and store_id=5 group by
//        cust_id order by count(*) desc ;` , [store_id], function(err, results){
//        if(err) throw err 
//        res.send(JSON.stringify({"status": 200, "error": null, "response":results}));
//    });   
// });












