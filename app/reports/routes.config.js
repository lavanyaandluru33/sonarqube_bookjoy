const ReportsController = require('./controllers/reports.controller');
const auth = require('../../config/auth');

exports.routesConfig = function (app) {
      
    app.post('/reports/get_top_customer',
        ReportsController.getTopCustomer
    );

    app.post('/reports/get_top_customer_by_date',
        ReportsController.getTopCustomerByDate
    );

    app.post('/reports/most_visited_time',
        ReportsController.MostVisitedTimeOfDays
    );

    app.post('/reports/most_visited_time_by_date',
        ReportsController.MostVisitedTimeByDate
    );

    app.post('/reports/most_visited_day_of_week',
        ReportsController.MostVisitedDayOfWeek
    );

    app.post('/reports/top_customer_by_visits',
        ReportsController.TopCustomerByVisits
    );

    app.post('/reports/top_visited_customer_by_date',
        ReportsController.TopVisitedCustomerByDate
    );

    app.post('/reports/customers_three_to_five_weeks',
        ReportsController.ThreeToFiveWeeksCustomers
    );

    app.post('/reports/top_five_services',
        ReportsController.TopFiveServices
    );

    app.post('/reports/top_five_services_by_revenue',
        ReportsController.TopFiveServicesByRevenue
    );

    app.post('/reports/last_four_weeks_customers',
        ReportsController.LastFourWeeksCustomers
    );

    app.post('/reports/four_to_eight_weeks_customers',
        ReportsController.CustomersFourToEightWeeks
    );

    
    
    
}