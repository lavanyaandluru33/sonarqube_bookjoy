const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js');

exports.getCustomerDetails = function (data,result) {

    //customer details and customer wallet data
    let sql=`select c.cust_id,c.cust_first_name,c.cust_last_name,c.cust_email,c.cust_mobile,c.cust_gender,c.cust_notes,c.cust_picture, c.is_banned,c.is_vip,c.has_allergy, c.pay_fullpayment,c.cust_country_isd_code, c.send_email,c.send_text, c.show_cust_notes_flag, w.amount  FROM owner_customers_mst c
    LEFT JOIN (SELECT owner_cust_id,SUM(CASE WHEN type = 1 THEN amount ELSE -1*amount END) as amount from owner_customer_wallet where owner_cust_id = ${data.cust_id} GROUP BY owner_cust_id) as w ON w.owner_cust_id = c.cust_id WHERE c.cust_id = ${data.cust_id} AND c.owner_id = ${data.owner_id} AND c.cust_is_active = 1`;

    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
           
            }
        else {

            return result(null,res);
        
            
        }
    });   

}

exports.deleteCustomerImages = (delete_images,result) =>{
   let sql = `DELETE FROM booking_service_images WHERE booking_service_image_id in (?)`; 
   db.query(sql ,[delete_images], (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
        }
        return  result(null, res);
    });
}

exports.uploadImages = function (data,result) {
    let sql = `INSERT INTO booking_service_images (booking_id, user_id, image_url, image_public_id, thumbnail_url, thumbnail_public_id, created_by) VALUES ? `;
    var sql_values = [];
    for(var i=0; i< data.length; i++)
    {
     
      var values_array =  [data[i].booking_id, data[i].user_id, data[i].image_url, data[i].image_public_id, data[i].thumbnail_url, data[i].thumbnail_public_id, data[i].created_by];
        
        sql_values[i]= values_array;
     
    }
    db.query(sql ,[sql_values], (err,res) => {
        if (err) {
           // console.log(err);
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
        
        }
       // console.log(res);
        return  result(null, res);
    });
}

exports.getCustomers = (data, result) => {
    let sql=``;
    data.search_data = data.search_data.trim();
    data.search_data= data.search_data.split(" ");
    if(data.search_data.length > 1){
        for(var i=0; i < data.search_data.length; i++){
            if(data.search_data[i] == "" || data.search_data[i] == ""){
                data.search_data.splice(i,1);
                i--;
            }
        }
        sql = `SELECT cust_id,owner_id,cust_first_name,cust_last_name,cust_email,cust_mobile,cust_picture,cust_country_isd_code,is_vip,has_allergy,is_banned,send_email,send_text, cust_notes,customer_notes, show_cust_notes_flag FROM owner_customers_mst
           WHERE owner_id = ${data.owner_id} AND cust_is_active = 1 AND ((cust_first_name LIKE '%${data.search_data[0]}%' AND cust_last_name LIKE '%${data.search_data[1]}%') OR (cust_first_name LIKE '%${data.search_data[1]}%' AND cust_last_name LIKE '%${data.search_data[0]}%'))
            and cust_email != 'break@bookjoy.in' 
            ORDER BY CASE WHEN concat(cust_first_name,' ',cust_last_name) like '${data.search_data[0]} ${data.search_data[1]}' THEN 0  
            WHEN concat(cust_first_name,' ',cust_last_name) like '${data.search_data[0]} ${data.search_data[1]}%' THEN 1  
            WHEN concat(cust_last_name,' ',cust_first_name) like '${data.search_data[1]} ${data.search_data[0]}' THEN 2  
            WHEN concat(cust_last_name,' ',cust_first_name) like '${data.search_data[1]} ${data.search_data[0]}%' THEN 3  
            ELSE 4 END, concat(cust_first_name,' ',cust_last_name),cust_first_name,cust_last_name`;

    }
    else{
        sql = `SELECT cust_id,owner_id,cust_first_name,cust_last_name,cust_email,cust_mobile,cust_picture,cust_country_isd_code,is_vip,has_allergy,is_banned,send_email,send_text, cust_notes,customer_notes, show_cust_notes_flag FROM owner_customers_mst
           WHERE owner_id = ${data.owner_id} AND cust_is_active = 1 AND (cust_first_name LIKE '%${data.search_data[0]}%' OR cust_last_name LIKE '%${data.search_data[0]}%' OR cust_email LIKE "${data.search_data[0]}" OR cust_mobile LIKE "%${data.search_data[0]}%") 
            and cust_email != 'break@bookjoy.in' 
            ORDER BY CASE WHEN cust_first_name like '${data.search_data[0]}' THEN 0 
            WHEN cust_first_name like '${data.search_data[0]}%' THEN 1 
            ELSE 2 END, cust_first_name`;
    }
    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
        }
        return  result(null, res);
    });
           
}

exports.getCustomerImages = (data, result) => {

    let sql = `SELECT booking_service_image_id,booking_id,user_id, image_url,is_active,created_by, thumbnail_url, image_public_id, thumbnail_public_id FROM booking_service_images WHERE booking_id IN (${data.booking_id})`;
    //console.log(sql);

    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
}

exports.getLastVisited = (store_id,cust_email,book_start_date,book_start_time,result) => {

    var start_date_time = book_start_date+" "+book_start_time;

    let sql = `(SELECT b.book_start_date as last_visited_date , b.book_start_time as last_visited_time, s.serv_name as last_visited_serv_name,b.book_start_date as date, "Service" as type FROM booking_mst as b, services_mst as s
    WHERE s.serv_id = b.serv_id AND b.store_id = ${store_id} AND b.book_email = '${cust_email}' AND concat(b.book_start_date," ",b.book_start_time) < '${start_date_time}'
    AND (b.book_status = ${constants.STATUS_BOOKED} || b.book_status = ${constants.STATUS_CONFIRMED} || b.book_status = ${constants.STATUS_ARRIVED} || b.book_status = ${constants.STATUS_COMPLETED}) AND s.serv_name != "Break")  
    UNION ALL 
    (SELECT bc.date as last_visited_date, bc.start_time as last_visited_time, cs.class_name as last_visited_serv_name,bc.date as date, "Class" as type
    FROM booking_class_mst as bc, class_schedule as cs WHERE cs.class_schedule_id = bc.class_schedule_id
    AND bc.store_id = ${store_id} AND bc.class_cust_email = '${cust_email}' AND concat(bc.date," ",bc.start_time) < '${start_date_time}'
    AND (bc.class_book_status = ${constants.STATUS_BOOKED} || bc.class_book_status = ${constants.STATUS_CONFIRMED} || bc.class_book_status = ${constants.STATUS_ARRIVED} || bc.class_book_status = ${constants.STATUS_COMPLETED})) 
    ORDER BY date DESC, last_visited_time DESC limit 1`;

    //console.log(sql);
    
    
    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        //console.log(res);
        return  result(null, res);
    });
           
}



exports.getBreakCustomer = (staff_id, result) => {

    let sql = `SELECT c.cust_id,c.cust_first_name,c.cust_last_name,c.cust_email,c.cust_mobile,c.cust_country_isd_code, u.user_perm_created_by 
    FROM owner_customers_mst c, users_permissions u WHERE u.user_id= ${staff_id} AND c.owner_id= u.user_perm_created_by AND lower(c.cust_email) ="break@bookjoy.in"`;


    //console.log(sql);
    
    
    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        //console.log(res);
        return  result(null, res);
    });
           
}


exports.updateCustomerImages = (data, result) => {

    let sql = `UPDATE booking_service_images SET image_url = '${data.image_url}', image_public_id = '${data.image_public_id}', thumbnail_url = '${data.thumbnail_url}' , thumbnail_public_id= '${data.thumbnail_public_id}' WHERE booking_service_image_id = ${data.booking_service_image_id}`;
    console.log(sql);

    db.query(sql , (err,res) => {
        if (err) {
            console.log(err);
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
}

exports.getCustLastVisited = (cust_id,start_date_time) => {
  

    return new Promise((resolve, reject) => {

        let sql = `(SELECT concat(b.book_start_date," ",b.book_start_time) as last_visited, s.serv_name as last_visited_serv_name,b.book_start_date as date,b.book_start_time as time, "Service" as type FROM booking_mst as b, services_mst as s
    WHERE s.serv_id = b.serv_id AND b.cust_id = '${cust_id}' AND concat(b.book_start_date," ",b.book_start_time) < '${start_date_time}'
    AND (b.book_status = ${constants.STATUS_BOOKED} || b.book_status = ${constants.STATUS_CONFIRMED} || b.book_status = ${constants.STATUS_ARRIVED} || b.book_status = ${constants.STATUS_COMPLETED}) AND s.serv_name != "Break")  
    UNION ALL 
    (SELECT concat(bc.date," ",bc.start_time) as last_visited, cs.class_name as last_visited_serv_name,bc.date as date, bc.start_time as time,"Class" as type
    FROM booking_class_mst as bc, class_schedule as cs WHERE cs.class_schedule_id = bc.class_schedule_id
    AND bc.class_cust_id = '${cust_id}' AND concat(bc.date," ",bc.start_time) < '${start_date_time}'
    AND (bc.class_book_status = ${constants.STATUS_BOOKED} || bc.class_book_status = ${constants.STATUS_CONFIRMED} || bc.class_book_status = ${constants.STATUS_ARRIVED} || bc.class_book_status = ${constants.STATUS_COMPLETED})) 
    ORDER BY date DESC, time DESC limit 1`;

      db.query(sql, function (err, result) {
          if (err)
              reject({error_code:err.code, message:err.sqlMessage,sql:err.sql});
          else
              resolve(result);
      });
  });
  }

exports.insertCustomer = (customer_details,result)=>{
    let sql = `INSERT INTO owner_customers_mst (owner_id, cust_first_name , cust_last_name , cust_email, cust_mobile , cust_add1, cust_add2, cust_city , cust_state, cust_country,
        cust_pin, cust_notes , cust_picture, cust_created_by, cust_created_date, cust_is_active, cust_country_isd_code, cust_time_zone, customer_notes,send_email,send_text, show_cust_notes_flag, is_banned, has_allergy, pay_fullpayment, is_vip) VALUES (${customer_details.owner_id},'${customer_details.cust_first_name}','${customer_details.cust_last_name}','${customer_details.cust_email}',${customer_details.cust_mobile},'${customer_details.cust_add1}','${customer_details.cust_add2}','${customer_details.cust_city}','${customer_details.cust_state}','${customer_details.cust_country}','${customer_details.cust_pin}','${customer_details.cust_notes}','${customer_details.cust_picture}',${customer_details.cust_created_by},'${customer_details.cust_created_date}',1,'${customer_details.cust_country_isd_code}','${customer_details.cust_time_zone}','${customer_details.cust_notes}',${customer_details.send_email},${customer_details.send_text},${customer_details.show_cust_notes_flag},${customer_details.is_banned},${customer_details.has_allergy},${customer_details.pay_fullpayment},${customer_details.is_vip}) `
        db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });            
}

exports.updateCustomer = (customer_details, result) => {
    let sql = `UPDATE owner_customers_mst SET owner_id = ${customer_details.owner_id}, cust_first_name = '${customer_details.cust_first_name}', cust_last_name = '${customer_details.cust_last_name}', cust_email = '${customer_details.cust_email}', cust_mobile = ${customer_details.cust_mobile}, cust_add1 = '${customer_details.cust_add1}', cust_add2 = '${customer_details.cust_add2}', cust_city = '${customer_details.cust_city}', cust_state = '${customer_details.cust_state}', cust_country = '${customer_details.cust_country}', cust_pin = '${customer_details.cust_pin}', cust_notes = '${customer_details.cust_notes}', cust_picture = '${customer_details.cust_picture}',cust_updated_by = ${customer_details.cust_updated_by}, cust_updated_date = '${customer_details.cust_updated_date}', cust_is_active = 1, cust_country_isd_code = '${customer_details.cust_country_isd_code}', cust_time_zone = '${customer_details.cust_time_zone}', customer_notes = '${customer_details.cust_notes}', send_email = ${customer_details.send_email}, send_text = ${customer_details.send_text}, show_cust_notes_flag = ${customer_details.show_cust_notes_flag}, is_banned = ${customer_details.is_banned}, has_allergy = ${customer_details.has_allergy},pay_fullpayment = ${customer_details.pay_fullpayment}, is_vip = ${customer_details.is_vip}   WHERE cust_id = ${customer_details.cust_id}`;
    db.query(sql, (err, res) => {
        if (err) {
            return result({error_code: err.code, message: err.sqlMessage, sql: err.sql}, null);
        } else {
            let data = [customer_details.cust_first_name,customer_details.cust_last_name,
            customer_details.cust_email,customer_details.cust_country_isd_code,
        customer_details.cust_mobile];
            let sql = `update booking_mst set book_first_name = ?,book_last_name = ?,
                        book_email = ?,book_country_isd_code=?,book_mobile=? where cust_id = ${customer_details.cust_id}`;
            db.query(sql, data, (err, res) => {
                if (err) {
                    return result({error_code: err.code, message: err.sqlMessage, sql: err.sql}, null);
                } else {
                    let sql = `update booking_class_mst set class_cust_first_name = ?,class_cust_last_name = ?,
                        class_cust_email = ?,class_country_code=?,class_cust_mobile=? where class_cust_id =${customer_details.cust_id}`;
                    db.query(sql, data, (err, res) => {
                        if (err) {
                            return result({error_code: err.code, message: err.sqlMessage, sql: err.sql}, null);
                        } else {
                            let sql = `update owner_customer_wallet set owner_cust_email = ? 
                                                 where owner_cust_id =?`;
                            db.query(sql, [customer_details.cust_email, customer_details.cust_id], (err, res) => {
                                if (err) {
                                    return result({error_code: err.code, message: err.sqlMessage, sql: err.sql}, null);
                                } else {
                                    return  result(null, res);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}


exports.findCustomerByEmaail = (customer_data,result) =>{
    let sql = `SELECT cust_id,cust_email From owner_customers_mst Where owner_id = ${customer_data.owner_id} AND cust_email = '${customer_data.cust_email}'`;
    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
}

exports.addNewCustomer = (data, result) => {
    //console.log(data); 
    //var cust_password = constants.md5(data.cust_password+constants.encryption_key);

    let sql = `INSERT INTO customers_mst (cust_first_name, cust_last_name , cust_email, cust_country_isd_code, cust_mobile , cust_password, cust_created_date, cust_notes) 
    VALUES ('${data.cust_first_name}', '${data.cust_last_name}' , '${data.cust_email}', ${data.cust_country_isd_code}, ${data.cust_mobile}, '${data.cust_password}', '${data.cust_created_date}', '')`;

    // let sql = `INSERT INTO customers_mst (cust_first_name, cust_last_name , cust_email, cust_country_isd_code, cust_mobile , cust_add1, cust_add2, cust_city, cust_state, cust_country, cust_pin, cust_dob, cust_picture, cust_notes, cust_password, cust_created_by, cust_created_date) 
    // VALUES ('${data.cust_first_name}', '${data.cust_last_name}' , '${data.cust_email}', ${data.cust_country_isd_code}, ${data.cust_mobile} , '${data.cust_add1}', '${data.cust_add2}' , '${data.cust_city}', '${data.cust_state}', '${data.cust_country}', '${data.cust_pin}', '${data.cust_dob}', '${data.cust_picture}', '${data.cust_notes}',
    // ${cust_password}, ${data.cust_created_by}, '${data.cust_created_date}')`;

    //console.log(sql);  
        db.query(sql , (err,res) => {
          if (err) {
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                 
                }
                //console.log(res);
                      
                return  result(null, res);
                         
          });
             
  }

exports.addOwnerCustomers = (cust_data, result) => {

    let sql = `INSERT INTO owner_customers_mst (owner_id, cust_first_name, cust_last_name , cust_email, cust_country_isd_code, cust_mobile , cust_password, cust_created_by, cust_created_date, cust_notes, customer_notes) VALUES ? `
    // (owner_id, cust_first_name , cust_last_name , cust_email, cust_mobile , cust_add1, cust_add2, cust_city , cust_state, cust_country,
    //     cust_pin, cust_company, cust_occupation,cust_gender, cust_notes , cust_password, cust_reset_pswd_key, cust_picture, stripeuid, referral_code, cust_created_by, cust_created_date, cust_updated_by,
    //     cust_updated_date, cust_is_active, cust_country_isd_code, cust_time_zone, customer_notes,send_email,send_text, show_cust_notes_flag, is_banned, has_allergy, pay_fullpayment, is_vip) VALUES `;

    //console.log(sql);
    // var cust_array = [];
    // for(i=0; i< cust_data.length; i++)
    // {
    //     cust_array[i] = [cust_data[i].owner_id, cust_data[i].cust_first_name, cust_data[i].cust_last_name , cust_data[i].cust_email, cust_data[i].cust_country_isd_code, cust_data[i].cust_mobile , cust_data[i].cust_password, cust_data[i].cust_created_by, cust_data[i].cust_created_date]

    // } 
    // console.log(cust_array);
        db.query(sql , [cust_data] , (err,res) => {
          if (err) {
                  //console.log(err);
                 return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
                 
                }
                //console.log(res);
                      
                return  result(null, res);
                         
          });
             
  }



exports.checkCustEmail= (cust_email, result) => {
    
        sql = `SELECT cust_id,cust_first_name,cust_last_name,cust_email,cust_mobile FROM customers_mst
           WHERE cust_email = '${cust_email}'`;
    //console.log(sql);
    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
        }
        return  result(null, res);
    });
           
}

exports.findOwnerCustomerByEmail = (cust_email,result) =>{
    let sql = `SELECT owner_id, cust_id, cust_email From owner_customers_mst WHERE cust_email = '${cust_email}'`;
    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
}

exports.checkBlockedCustomer = (data,result) =>{
    let sql = `SELECT c.cust_id, c.cust_email, c.is_banned,c.pay_fullpayment, s.store_id, s.user_id as owner_id FROM owner_customers_mst as c, stores_mst as s WHERE s.user_id = c.owner_id AND s.store_id = ${data.store_id} AND c.cust_email = '${data.cust_email}'`;
    //console.log(sql);
    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
}



exports.updateCustResetPswdKey = (data, result) => {

    let sql = `UPDATE customers_mst SET cust_reset_pswd_key = '${data.reset_pswd_key}', cust_updated_date= '${data.cust_updated_date}' WHERE cust_email = '${data.cust_email}' `;
    //console.log(sql);

    db.query(sql , (err,res) => {
        if (err) {
            console.log(err);
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
}



exports.checkCustEmailResetPswdKey= (cust_email, reset_pswd_key, result) => {
    
    sql = `SELECT cust_id,cust_first_name,cust_last_name,cust_email,cust_mobile FROM customers_mst
       WHERE cust_email = '${cust_email}' AND cust_reset_pswd_key = '${reset_pswd_key}'`;
//console.log(sql);
db.query(sql , (err,res) => {
    if (err) {
        return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
    }
    return  result(null, res);
});
       
}

exports.updateCustomerPassword = (data, result) => {

    let sql = `UPDATE customers_mst SET cust_password = '${data.cust_password}', cust_updated_by = ${data.cust_id}, cust_updated_date= '${data.cust_updated_date}', cust_reset_pswd_key = '' WHERE cust_id = ${data.cust_id} `;
    //console.log(sql);

    db.query(sql , (err,res) => {
        if (err) {
            console.log(err);
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
}

exports.getSelectedCustomerDetails = (cust_id, result) => {

    let sql = `SELECT cust_id,owner_id, cust_first_name , cust_last_name , cust_email, cust_mobile , cust_add1, cust_add2, cust_city , cust_state, cust_country, cust_pin, cust_notes , cust_picture, cust_country_isd_code, cust_time_zone, send_email, send_text, show_cust_notes_flag, is_banned, has_allergy, pay_fullpayment, is_vip FROM owner_customers_mst WHERE cust_id = ${cust_id}`;
    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
}




exports.updateCustomerNotes = (data,result)=>{
    let sql = `UPDATE owner_customers_mst SET cust_notes = '${data.cust_notes}', customer_notes = '${data.cust_notes}', cust_updated_by = ${data.cust_updated_by}, cust_updated_date = '${data.cust_updated_date}' WHERE cust_id = ${data.cust_id} AND owner_id = ${data.owner_id}`;
    //console.log(sql)
    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });            
}


exports.getCustomerById = function (cust_id,result) {


    let sql=`SELECT c.cust_id,c.cust_first_name,c.cust_last_name,c.cust_email,c.cust_mobile FROM customers_mst c
    WHERE c.cust_id = ${cust_id}`;

    //console.log(sql);

    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
           
            }
        else {

            return result(null,res);
        
            
        }
    });   

}

exports.getOwnerCustIdByEmail = (data,result) =>{
    let sql = `SELECT s.store_id, s.store_name, s.user_id , c.cust_id, c.owner_id, c.cust_email, c.cust_first_name, c.cust_last_name, c.cust_mobile, c.cust_country_isd_code FROM stores_mst as s, owner_customers_mst as c WHERE s.store_id = ${data.store_id} AND s.user_id = c.owner_id AND c.cust_email = '${data.cust_email}'`;

    //console.log(sql);

    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
}

exports.bookingDetailsModel = (data,result) => {
    let sql = `Select book_id,store_url_name,serv_name,store_name,book_start_date,book_end_date,book_start_time,book_end_time,
                book_status,book_created_date,multi_service_book_id,serv_name from booking_mst b 
                inner join services_mst s on s.serv_id = b.serv_id 
                inner join stores_mst st on st.store_id = b.store_id
                WHERE 
                book_email='${data.cust_email}' and b.book_status not in (${constants.STATUS_PRE_BOOKED},${constants.STATUS_RESCHEDULED}) ORDER BY b.book_created_date DESC limit ${data.start},100`;
    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
}


exports.updateCustCreatedBy = (cust_id, result) => {
    
    sql = `UPDATE customers_mst SET cust_created_by = ${cust_id} WHERE cust_id = ${cust_id}`;

    //console.log(sql);

    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
        }
        return  result(null, res);
    });
       
}



exports.checkOwnerCustomerExists = (cust_email,result) =>{
    let sql = `SELECT owner_id, cust_id, cust_first_name, cust_last_name, cust_email, cust_mobile, cust_country_isd_code FROM owner_customers_mst WHERE cust_email = '${cust_email}'`;
    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
}



exports.getCustDetailsByEmail= (cust_email, result) => {
    
    sql = `SELECT cust_id,cust_first_name,cust_last_name,cust_email,cust_mobile, cust_country_isd_code, cust_password FROM customers_mst
       WHERE cust_email = '${cust_email}'`;
//console.log(sql);
db.query(sql , (err,res) => {
    if (err) {
        return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);
    }
    return  result(null, res);
});
       
}


