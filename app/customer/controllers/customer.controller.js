const constants = require('../../../config/constants.js');
const CustomerModel = require('../models/customer.model');
const BookingModel = require('../../booking/models/booking.model');
const ClassesModel = require('../../classes/models/classes.model');
const StoresModel = require('../../stores/models/stores.model');
const UserModel = require('../../user/models/user.model');

const NotificationsLibrary = require('../../../libs/notifications.js');


const CryptoJS = require('crypto-js');

var async = require("async");
var moment = require("moment");
var formidable = require('formidable');
const { response } = require('express');
var cloudinary = require('cloudinary').v2;
cloudinary.config({
    cloud_name: constants.cloud_name,
    api_key: constants.api_key,
    api_secret: constants.api_secret
})

exports.uploadImages = (req, res, next) => {

    // var form = new formidable.IncomingForm();
    // form.parse(req, function (err, fields, files) {
    //res.write('File uploaded');
    try{
        var sent_images = req.body.image_details;
        var uploaded_images = [], delete_images = [], delete_images_cloudinary = [], delete_images_cloudinary_thumb = [];

        async.each(sent_images, function (this_img, callback)
        {

            var this_upload = {
                booking_id: req.body.booking_id,
                user_id: req.body.user_id,
                created_by: req.body.created_by,
                image_url: "",
                image_public_id: "",
                thumbnail_url: "",
                thumbnail_public_id: ""

            }
            if (req.body.update_flag == 2) {
                //delete image
                delete_images.push(this_img.booking_service_image_id);
                delete_images_cloudinary.push(this_img.image_public_id);
                delete_images_cloudinary_thumb.push(this_img.thumbnail_public_id);
                callback();
            } else {
                imageUploadCloudinary(this_img.image, (upload_err, uploaded_details) =>
                {
                    if (upload_err)
                    {
                        return callback({error: upload_err});
                    } else {


                        if (this_img.booking_service_image_id && req.body.update_flag == 1)
                        {
                            //update image
                            var edit_req = {

                                image_url: uploaded_details.image.secure_url,
                                image_public_id: uploaded_details.image.public_id,
                                thumbnail_url: uploaded_details.thumbnail.secure_url,
                                thumbnail_public_id: uploaded_details.thumbnail.public_id,
                                booking_service_image_id: this_img.booking_service_image_id
                            }


                            CustomerModel.updateCustomerImages(edit_req, (err, data) => {
                                if (err) {
                                    //console.log(err);

                                    //res.status(500).send({message: err});
                                    next(err);
                                } else {

                                    imageDeleteCloudinary(this_img.image_public_id, this_img.thumbnail_public_id, (delete_err, delete_result) => {

                                        //console.log(delete_result);
                                        callback();

                                    })


                                }
                            });

                        } else
                        {
                            //insert new image
                            this_upload.image_url = uploaded_details.image.secure_url;
                            this_upload.image_public_id = uploaded_details.image.public_id;
                            this_upload.thumbnail_url = uploaded_details.thumbnail.secure_url;
                            this_upload.thumbnail_public_id = uploaded_details.thumbnail.public_id;

                            uploaded_images.push(this_upload);
                            callback();
                        }


                    }

                });
            }

        }, function (err) {
            if (err) {
                res.status(400).send({status: 400, message: err});
            } else if (req.body.update_flag == 2)
            {
                CustomerModel.deleteCustomerImages(delete_images, (err, data) => {
                    if (err) {
                        //res.status(500).send({message: err});
                        next(err);
                    } else {
                        imageDeleteArrayCloudinary(delete_images_cloudinary, delete_images_cloudinary_thumb, (delete_err, delete_result) => {
                            if (delete_err)
                            {
                                res.status(400).send({error: delete_err});
                            } else {
                                res.status(200).send({status: 200, result: "Images Deleted Successfully"});
                            }
                        });
                    }
                });

            } else if (req.body.update_flag == 1)
            {
                res.status(200).send({status: 200, result: "Images Updated Successfully"});
            } else
            {
                //console.log(uploaded_images);
                CustomerModel.uploadImages(uploaded_images, (err, data) => {
                    if (err) {
                        console.log(err);

                        //res.status(500).send({message: err});
                        next(err);
                    } else {

                        res.status(200).send({status: 200, result: "Images added Successfully"});
                    }
                });
            }


        });
    }catch(error){
        next(error);
    }
};




exports.uploadCustomerImages = (req, res, next) => {

    CustomerModel.uploadImages(req.body, (err, data) => {
        if (err) {

            //res.status(500).send({message: err});
            next(err);
        } else {

            res.status(200).send({status: 200, result: "Updated Successfully"});
        }
    });
};




//get customer images of a particular booking
exports.getCustomerImages = (req, res, next) => {
    // if(!Object.keys(req.body).length) {
    //     res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
    //     return;
    // }

    if (!(req.body.booking_id) || (!req.body.multi_service_book_id && req.body.multi_service_book_id != 0)) {
        res.status(400).send({status: 400, message: 'Missing required parameters. Check body and retry'});
        return;
    }

    if (req.body.multi_service_book_id != 0)
    {
        var book_id_req = [req.body.multi_service_book_id];
        var multi_id_flag = 1;
    } else
    {
        var book_id_req = [req.body.booking_id];
        var multi_id_flag = 0;
    }

    BookingModel.getDetailsOfBooking(book_id_req, multi_id_flag, async (err, book_details) => {
        if (err) {
            //console.log(err);
            //res.status(500).send({message: err});
            next(err);
           
        } else {
            //console.log(book_details);
            var booking_ids = [];

            for (i = 0; i < book_details.length; i++)
            {
                booking_ids = await getOldBookings(book_details[i].book_id,booking_ids)
//                booking_ids.push();
                //console.log(i);
            }
            //console.log(booking_ids);


            var images_req = {
                booking_id: booking_ids
            }


            CustomerModel.getCustomerImages(images_req, (err, data) => {
                if (err) {
                    
                    //res.status(500).send({status: 500, message: err});
                    next(err);
                } else {
                    res.status(200).send({status: 200, result: data});
                }
            });
        }
    })
}


// get customers list on search
exports.getCustomers = (req, res, next) => {
    if (!Object.keys(req.body).length) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }
    if (!(req.body.owner_id) || !(req.body.search_data)) {
        res.status(400).send({status: 400, message: 'Missing required parameters. Check body and retry'});
        return;
    }
    if (req.body.search_data.includes("'")) {
        var serch_data = [...req.body.search_data];
        req.body.search_data = '';
        for (var i = 0; i < serch_data.length; i++)
        {
            if (serch_data[i] == "'") {
                req.body.search_data += "'"
            }
            req.body.search_data += serch_data[i];
        }
    }
    CustomerModel.getCustomers(req.body, async (err, customers) => {
        if (err) {

            //res.status(500).send({status: 500, message: err});
            next(err);
        } else {
            //console.log(customers);
            var current_date_time = moment().format("YYYY-MM-DD HH:mm:ss");

            for (i = 0; i < customers.length; i++)
            {
                customers[i].cust_last_visited = {};
                //console.log(i);console.log(customers[i].cust_id);

                try {
                    customers[i].cust_last_visited = await CustomerModel.getCustLastVisited(customers[i].cust_id, current_date_time);
                } catch (error) {

                    console.log(error);
                    var cust_error = error;
                    //break;
                    //throw error; 
                }

                //console.log(i);
            }
            //console.log("for loop completed");

            if (cust_error)
            {
                //res.status(500).send({message: cust_error});
                next(cust_error);
            } else
            {
                res.status(200).send({status: 200, result: customers});
            }
        }

    })
}

exports.getCustomerDetails = (req, res, next) => {

    if (!req.body.owner_id || !req.body.cust_id) {
        res.status(400).send({status: 400, message: 'Missing required parameters. Check body and retry'});
        return;
    }
    CustomerModel.getCustomerDetails(req.body, (err, data) => {
        if (err) {
            //console.log("here is the error"+err);
            //res.status(500).send({status: 500, message: err});
            next(err);
        } else {
            //console.log(data);
            var store_req = {
                user_id: req.body.owner_id
            }

            StoresModel.getUserStores(store_req, (err, stores) => {
                if (err) {

                    //res.status(500).send({message: err.message});
                    next(err);
                   
                } else {
                    //console.log(stores);
                    var store_ids = [];

                    async.each(stores, function (store_obj, callback) {

                        store_ids.push(store_obj.store_id);
                        callback();
                    }, function (err1) {
                        //console.log(store_ids);



                        if (!err1)
                        {

                            var book_req = {
                                cust_id: req.body.cust_id,
                                stores: store_ids
                            }

                            BookingModel.getCustomerServiceBookings(book_req, (err, service_bookings) => {
                                if (err) {

                                    //res.status(500).send({message: err.message});
                                    next(err);
                                } else {
                                    //console.log(service_bookings);
                                    var bookings = service_bookings;
                                    ClassesModel.getCustomerClassBookings(book_req, (err, class_bookings) => {
                                        if (err) {

                                            //res.status(500).send({message: err.message});
                                            next(err);
                                        } else {
                                            //console.log(class_bookings);

                                            var customer_details = {
                                                customer: {
                                                    customer_details: data,
                                                    service_bookings: service_bookings,
                                                    class_bookings: class_bookings},
                                                last_visited: "",
                                                total_bookings: 0,
                                                total_sales: 0,
                                                total_cancelled: 0
                                            }

                                            if (class_bookings.length || service_bookings.length) {
                                                // if(class_bookings.length && !service_bookings.length){
                                                //     customer_details.last_visited=""+class_bookings[0].date+" "+class_bookings[0].start_time;
                                                // }
                                                // else if(!class_bookings.length && service_bookings.length){
                                                //     customer_details.last_visited=""+service_bookings[0].book_start_date+" "+service_bookings[0].book_start_time;
                                                // }
                                                // else{
                                                //     if(class_bookings[0].date > service_bookings[0].book_start_date){
                                                //         customer_details.last_visited=""+class_bookings[0].date+" "+class_bookings[0].start_time;
                                                //     }
                                                //     else customer_details.last_visited=""+service_bookings[0].book_start_date+" "+service_bookings[0].book_start_time;
                                                // }

                                                bookings = bookings.concat(class_bookings);
                                                //console.log(bookings);

                                                var i = 0;
                                                var last_visited = "";

                                                // for(var index=0;index<class_bookings.length;index++){
                                                //     //console.log("classes");
                                                //     if(class_bookings[index].class_book_status == 10){
                                                //         customer_details.total_cancelled++;
                                                //     }
                                                //     else{
                                                //         customer_details.total_bookings++;
                                                //         customer_details.total_sales += class_bookings[index].total_cost;
                                                //     }
                                                // }


                                                var current_date_time = moment().format("YYYY-MM-DD HH:mm:ss");
                                                //console.log(current_date_time);


                                                bookings.filter((this_booking) => {
                                                    //console.log(this_booking);

                                                    if (this_booking.book_type == "services")
                                                    {
                                                        if (this_booking.book_status == constants.STATUS_CANCELLED || this_booking.book_status == constants.STATUS_LATE_CANCELLED || this_booking.book_status == constants.STATUS_NO_SHOW) {
                                                            customer_details.total_cancelled++;
                                                        } else if (this_booking.book_status == constants.STATUS_BOOKED || this_booking.book_status == constants.STATUS_CONFIRMED || this_booking.book_status == constants.STATUS_ARRIVED || this_booking.book_status == constants.STATUS_COMPLETED) {
                                                            customer_details.total_bookings++;
                                                            customer_details.total_sales += this_booking.book_fullamount;

                                                            var start_date_time = this_booking.book_start_date + " " + this_booking.book_start_time;

                                                            if (((start_date_time > last_visited) || (last_visited == "")) && (start_date_time < current_date_time))
                                                            {
                                                                last_visited = this_booking.book_start_date + " " + this_booking.book_start_time;
                                                            }

                                                        }
                                                        i++;
                                                    } else if (this_booking.book_type == "classes")
                                                    {

                                                        if (this_booking.class_book_status == constants.STATUS_CANCELLED || this_booking.class_book_status == constants.STATUS_LATE_CANCELLED || this_booking.class_book_status == constants.STATUS_NO_SHOW) {
                                                            customer_details.total_cancelled++;
                                                        } else if (this_booking.class_book_status == constants.STATUS_BOOKED || this_booking.class_book_status == constants.STATUS_CONFIRMED || this_booking.class_book_status == constants.STATUS_ARRIVED || this_booking.class_book_status == constants.STATUS_COMPLETED) {
                                                            customer_details.total_bookings++;
                                                            customer_details.total_sales += this_booking.total_cost;

                                                            var start_date_time = this_booking.date + " " + this_booking.start_time;

                                                            if (((start_date_time > last_visited) || (last_visited == "")) && (start_date_time < current_date_time))
                                                            {
                                                                last_visited = this_booking.date + " " + this_booking.start_time;
                                                            }
                                                        }
                                                        i++;
                                                    }
                                                    //console.log(i);

                                                    if (i == bookings.length)
                                                    {
                                                        customer_details.last_visited = last_visited;

                                                        res.status(200).send({status: 200, result: customer_details});

                                                    }
                                                });
                                            } else
                                            {
                                                res.status(200).send({status: 200, result: customer_details});
                                            }
                                        }
                                    });
                                }
                            })
                        } else
                        {
                            //res.status(500).send({message: err1});
                            next(err1);

                        }
                    })

                }

            })
        }
    })
}

function imageUploadCloudinary(image, result)
{

    var uploaded_details = {
        thumbnail: "",
        image: ""
    };
    cloudinary.uploader.upload(image, {transformation: [
            {width: 220, height: 140, crop: "fill"}],folder:constants.folder}, function (err1, response1) {
        //console.log(err1);
        //console.log(result);
        if (err1)
        {
            return result(err1, null);
        }

        uploaded_details.thumbnail = response1;


        cloudinary.uploader.upload(image,{folder:constants.folder}, function (err2, response2) {
            //console.log(err2);
            //console.log(response);

            if (err2) {
                return result(err2, null);
            }

            uploaded_details.image = response2;
            return result(null, uploaded_details)


        })

    })
}

function imageDeleteCloudinary(image_public_id, thumbnail_public_id, result)
{

    //console.log(image_public_id);
    //console.log(thumbnail_public_id);

    cloudinary.uploader.destroy(image_public_id, {invalidate: true}, function (err1, response1) {
        if (err1)
        {
            return result(err1, null);
        }


        cloudinary.uploader.destroy(thumbnail_public_id, {invalidate: true}, function (err2, response2) {

            if (err2) {
                return result(err2, null);
            }

            // uploaded_details.image = response2;
            return result(null, response2)


        })

    })
}

function imageDeleteArrayCloudinary(image_public_ids,thumbnail_public_ids,result){
    cloudinary.api.delete_resources(image_public_ids,{invalidate: true}, function (err1, response1) {
        if (err1)
        {
            return result(err1, null);
        }


        cloudinary.api.delete_resources(thumbnail_public_ids,{invalidate: true}, function (err2, response2) {

            if (err2) {
                return result(err2, null);
            }

            // uploaded_details.image = response2;
            return result(null, response1)
        })

    })
}


exports.getDetailsOfCustomer = (req, res, next) => {

    if (!(req.body.owner_id) || !(req.body.cust_id)) {
        res.status(400).send({status: 400, message: 'Missing required parameters. Check body and retry'});
        return;
    }
    CustomerModel.getCustomerDetails(req.body, (err, customer_data) => {
        if (err) {
            //console.log("here is the error"+err);
            //res.status(500).send({status: 500, message: err});
            next(err);
        } else {
            //console.log(data);
            var store_req = {
                user_id: req.body.owner_id
            }

            StoresModel.getUserStores(store_req, (err, stores) => {
                if (err) {

                    //res.status(500).send({message: err.message});
                    next(err);
                } else {
                    //console.log(stores);
                    var store_ids = [];

                    async.each(stores, function (store_obj, callback) {

                        store_ids.push(store_obj.store_id);
                        callback();
                    }, function (err1) {
                        //console.log(store_ids);

                        if (!err1)
                        {
                            var total_bookings = 0;
                            var total_sales = 0;
                            var total_cancelled = 0;

                            var book_req = {
                                cust_id: req.body.cust_id,
                                stores: store_ids
                            }

                            BookingModel.getCustomerServiceBookingsCount(book_req, (err, service_bookings) => {
                                if (err) {

                                    //res.status(500).send({message: err.message});
                                    next(err);
                                } else {
                                    //console.log(service_bookings);

                                    total_bookings += service_bookings.total_bookings;
                                    total_sales += service_bookings.total_sales;
                                    total_cancelled += service_bookings.total_cancelled;

                                    ClassesModel.getCustomerClassBookingsCount(book_req, (err, class_bookings) => {
                                        if (err) {

                                            //res.status(500).send({message: err.message});
                                            next(err);
                                        } else {
                                            //console.log(class_bookings);

                                            total_bookings += class_bookings.total_bookings;
                                            total_sales += class_bookings.total_sales;
                                            total_cancelled += class_bookings.total_cancelled;



                                            res.status(200).send({status: 200, result: {customer_details: customer_data,
                                                    total_bookings: total_bookings,
                                                    total_sales: total_sales,
                                                    total_cancelled: total_cancelled}});

                                        }
                                    });
                                }
                            })
                        } else
                        {
                            //res.status(500).send({message: err1});
                            next(err1);

                        }
                    })

                }

            })
        }
    })
}





exports.saveCustomer = async(req,res,next) =>{
    if (!(req.body.owner_id) || !(req.body.cust_email)) {
        res.status(400).send({status: 400, message: 'Missing required parameters. Check body and retry'});
        return;
    }
    req.body.cust_first_name = req.body.cust_first_name.replace(/'/g,"''")
    req.body.cust_last_name = req.body.cust_last_name.replace(/'/g,"''")
    req.body.cust_add1 = req.body.cust_add1.replace(/'/g,"''")
    req.body.cust_add2 = req.body.cust_add2.replace(/'/g,"''")
    req.body.cust_city = req.body.cust_city.replace(/'/g,"''")
    req.body.cust_state = req.body.cust_state.replace(/'/g,"''")
    req.body.cust_country = req.body.cust_country.replace(/'/g,"''")
    req.body.cust_notes = req.body.cust_notes.replace(/'/g,"''")
    uploadImage(req.body.image_url, (upload_err, uploaded_details) =>
    {
       if (upload_err)
       {
            res.status(401).send({status:401,message:"image upload error"})
       } else {
           if(!(req.body.cust_id)){
            req.body.cust_picture = uploaded_details;
            CustomerModel.insertCustomer(req.body, (err, customer_data) => {
                if (err) {
                    next(err);
                } else {
                    res.status(200).send({status: 200, response: customer_data});
                }
            })
           }
           else{
                if(uploaded_details != '') req.body.cust_picture = uploaded_details;
                CustomerModel.updateCustomer(req.body, (err, customer_data) => {
                    if (err) {
                        next(err);
                    } else {
                        res.status(200).send({status: 200, response: customer_data, message:'updated the customer successfully'});
                    }
                })
            }
       }
    })   
}

function uploadImage(image, result)
{
    if(image && image.length && image != "null"){
        cloudinary.uploader.upload(image, {transformation: [
                {width: 400, height: 400, crop: "fill"}],folder:constants.folder}, function (err1, response1) {
            if (err1)
            {
                return result(err1, null);
            }

            return result(null, response1.secure_url)
        })
    }
    else result(null,'')
}

// function deleteImage(url,res){
//     console.log(req.body.url);
    // imageDeleteArrayCloudinary(req.body.url,'https://res.cloudinary.com/quantana/image/upload/v1604318808/bookJoy_images/rc0pq5qhfl8x2ldqbxm7.png', (delete_err, delete_result) => {
    //     if (delete_err)
    //     {
    //         res.status(400).send({error: delete_err});
    //     } else {
    //         res.status(200).send({status: 200, result: "Images Deleted Successfully"});
    //     }
    // });
    // cloudinary.uploader.destroy(req.body.url, function(err,result) { 
    //     if(err){
    //         console.log(err)
    //     }
    //     else res.status(200).send({message:"updated successfully",details:result})
    //     })
// }

exports.existsWithStore = (req,res,next) =>{
    if (!(req.body.owner_id) || !(req.body.cust_email)) {
        res.status(400).send({status: 400, message: 'Missing required parameters. Check body and retry'});
        return;
    }
    
    CustomerModel.findCustomerByEmaail(req.body, (err, customer_data) => {
        if (err) {
            res.status(500).send({status: 500, message: err});
            next(err);
        } else {
            res.status(200).send({status: 200, response: customer_data});
        }
    })
      
    
}


    


exports.checkCustEmailExists = (req, res, next) => {

    if (!req.body.cust_email) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }
    var cust_email = req.body.cust_email;
    cust_email = cust_email.trim().toLowerCase();
    //console.log(cust_email);

    CustomerModel.checkCustEmail(cust_email, (err, data) => {
        if (err) {
            
            next(err);
        } else {
            //console.log(data);
            if(data.length > 0)
            {
                res.status(200).send({status: 200, result:{message:"Customer with this email already exists", customer:data}});
            }
            else
            {
                CustomerModel.checkOwnerCustomerExists(cust_email, (owner_cust_err, owner_cust_data) => {
                    if (owner_cust_err) {
                        
                        next(owner_cust_err);
                    } else {

                            if(owner_cust_data.length > 0)
                            {
                                res.status(200).send({status: 200, result:{message:"Customer with this email already exists", customer:owner_cust_data}});
                            }
                            else
                            {
                                res.status(200).send({status: 200, result:{message:"No customer exists with given email" , customer:data}});
                            }

                    }
                })
            }
  
        }
    })
}




// exports.customerSignUp = (req, res, next) => {

//     if (!req.body.cust_email || !req.body.password) {
//         res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
//         return;
//     }

//     var cust_email = req.body.cust_email;
//     cust_email = cust_email.trim().toLowerCase();
//     //console.log(cust_email);

//     var cust_country_isd_code = req.body.cust_country_isd_code;
//     cust_country_isd_code = cust_country_isd_code.replace(/\+/g,'');
//     //console.log(cust_country_isd_code);

//     var cust_mobile = req.body.cust_mobile;
//     cust_mobile = parseInt(cust_mobile,10)
//     var mobile_digit_check = cust_mobile.toString().length; 
//     //console.log("mobile_digit_check: ",mobile_digit_check);
//     if(mobile_digit_check < 9 || mobile_digit_check > 12)
//     {
//         res.status(400).send({status: 400, message: 'Invalid mobile number. Mobile number should be 9-12 digits'});
//         return;

//     }
//     //console.log("cust_mobile: ",cust_mobile);//console.log(cust_mobile.length);

//     var cust_first_name = NameUpperCase(req.body.cust_first_name,"0");
//     //console.log(cust_first_name);

//     var cust_last_name = NameUpperCase(req.body.cust_last_name,"0");
//     //console.log(cust_last_name);

//     var cust_first_name_owner = NameUpperCase(req.body.cust_first_name,"1");
//     //console.log(cust_first_name_owner);

//     var cust_last_name_owner = NameUpperCase(req.body.cust_last_name,"1");
//     //console.log(cust_last_name_owner);




//     var cust_password_ciphertext = req.body.password;
 
//     // Decrypt
//     var bytes  = CryptoJS.AES.decrypt(cust_password_ciphertext, constants.crypto_key);
//     var original_password = bytes.toString(CryptoJS.enc.Utf8);

//     //console.log("decrypted :",original_password)

//     var cust_req = {
//          cust_first_name: cust_first_name,
//          cust_last_name : cust_last_name,
//          cust_email:  cust_email,
//          cust_country_isd_code: cust_country_isd_code,
//          cust_mobile : cust_mobile,
//          cust_password: constants.md5(original_password+constants.encryption_key),
//          cust_created_date: moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
        
//     }
//     //console.log("cust_req: ",cust_req);
 

//     UserModel.getAllOwners((owner_err, owners_data) => {
//         if (owner_err) {
//             next(owner_err);
//         } else {
//             //console.log(owners_data.length);

//             CustomerModel.findOwnerCustomerByEmail(cust_email, (err1, cust_data) => {
//                 if (err1) {
//                     next(err1);
//                 } else {
//                     //console.log(cust_data);
//                     var owner_ids_cust = [];

                    
//                     cust_data.filter((cust_obj) => {
//                         //console.log(cust_obj.owner_id);
//                         owner_ids_cust.push(cust_obj.owner_id);
//                     })
//                     //console.log("owner_ids_cust: ", owner_ids_cust);
                   

                    

//                     CustomerModel.addNewCustomer(cust_req, (err, data) => {
//                         if (err) {
//                             next(err);
//                         } else {
//                             //console.log(data);
//                             var new_cust_id  = data.insertId;

//                             CustomerModel.updateCustCreatedBy(new_cust_id, (update_err,update_data) => {
//                                 if (update_err)
//                                 {
//                                     next(update_err);
//                                 } 
//                                 else
//                                 {
//                                     //console.log(update_data);

//                                     CustomerModel.getCustomerById(new_cust_id, (cust_err,cust_data) => {
//                                         if (cust_err)
//                                         {
//                                             next(cust_err);
//                                         } 
//                                         else
//                                         {
//                                             //console.log(cust_data);

//                                             var owner_ids = [];
//                                             var owner_cust_req = [];
                        
//                                             owners_data.filter((owner_obj) => {
//                                                 //console.log(owner_obj.user_id);
//                                                 if((owner_ids_cust.indexOf(owner_obj.user_id) <= -1) && (owner_ids.indexOf(owner_obj.user_id) <= -1))
//                                                 {
//                                                   owner_ids.push(owner_obj.user_id);
//                                                   var this_owner_cust = [owner_obj.user_id, cust_first_name_owner, cust_last_name_owner, cust_req.cust_email, cust_req.cust_country_isd_code, cust_req.cust_mobile, cust_req.cust_password, new_cust_id, cust_req.cust_created_date];
                        
//                                                   owner_cust_req.push(this_owner_cust);
                        
                                                  
//                                                 }
                                
                        
                                                
//                                             })
//                                             //  console.log("owner_ids: ", owner_ids);
//                                             //  console.log("owner_ids length: ", owner_ids.length);
//                                             //  console.log("owner cust req: ", owner_cust_req);
//                                             //  console.log("owner cust req length: ", owner_cust_req.length);
                        

//                                             if(owner_cust_req.length > 0)
//                                             {
//                                                 CustomerModel.addOwnerCustomers(owner_cust_req, (err1, data1) => {
//                                                     if (err1) {
//                                                         next(err1);
//                                                     } else {
//                                                         //console.log(data1);
//                                                         res.status(200).send({status: 200, result: {cust_id: cust_data[0].cust_id, cust_email: cust_data[0].cust_email, cust_first_name:cust_data[0].cust_first_name, cust_last_name:cust_data[0].cust_last_name}});
//                                                     }
//                                                 })
//                                             }
//                                             else
//                                             {
//                                                 res.status(200).send({status: 200, result: {cust_id: cust_data[0].cust_id, cust_email: cust_data[0].cust_email, cust_first_name:cust_data[0].cust_first_name, cust_last_name:cust_data[0].cust_last_name}});

//                                             }
//                                         }
//                                     })

//                         }
//                     })

//                         }
//                     })

//                 }
//             })


           
               
                
               
//             }
       
//     })

// }

exports.customerSignUp = (req, res, next) => {

    if (!req.body.cust_email || !req.body.password) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }

    var cust_email = req.body.cust_email;
    cust_email = cust_email.trim().toLowerCase();
    //console.log(cust_email);

    var cust_country_isd_code = req.body.cust_country_isd_code;
    cust_country_isd_code = cust_country_isd_code.replace(/\+/g,'');
    //console.log(cust_country_isd_code);

    var cust_mobile = req.body.cust_mobile;
    cust_mobile = parseInt(cust_mobile,10)
    var mobile_digit_check = cust_mobile.toString().length; 
    //console.log("mobile_digit_check: ",mobile_digit_check);
    if(mobile_digit_check < 9 || mobile_digit_check > 12)
    {
        res.status(400).send({status: 400, message: 'Invalid mobile number. Mobile number should be 9-12 digits'});
        return;

    }
    //console.log("cust_mobile: ",cust_mobile);//console.log(cust_mobile.length);

    var cust_first_name = NameUpperCase(req.body.cust_first_name,"0");
    //console.log(cust_first_name);

    var cust_last_name = NameUpperCase(req.body.cust_last_name,"0");
    //console.log(cust_last_name);

    // var cust_first_name_owner = NameUpperCase(req.body.cust_first_name,"1");
    // //console.log(cust_first_name_owner);

    // var cust_last_name_owner = NameUpperCase(req.body.cust_last_name,"1");
    // //console.log(cust_last_name_owner);




    var cust_password_ciphertext = req.body.password;
 
    // Decrypt
    var bytes  = CryptoJS.AES.decrypt(cust_password_ciphertext, constants.crypto_key);
    var original_password = bytes.toString(CryptoJS.enc.Utf8);

    //console.log("decrypted :",original_password)

    var cust_req = {
         cust_first_name: cust_first_name,
         cust_last_name : cust_last_name,
         cust_email:  cust_email,
         cust_country_isd_code: cust_country_isd_code,
         cust_mobile : cust_mobile,
         cust_password: constants.md5(original_password+constants.encryption_key),
         cust_created_date: moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
        
    }
    //console.log("cust_req: ",cust_req);
                    

                    CustomerModel.addNewCustomer(cust_req, (err, data) => {
                        if (err) {
                            next(err);
                        } else {
                            //console.log(data);
                            var new_cust_id  = data.insertId;

                            CustomerModel.updateCustCreatedBy(new_cust_id, (update_err,update_data) => {
                                if (update_err)
                                {
                                    next(update_err);
                                } 
                                else
                                {
                                    //console.log(update_data);

                                    CustomerModel.getCustomerById(new_cust_id, (cust_err,cust_data) => {
                                        if (cust_err)
                                        {
                                            next(cust_err);
                                        } 
                                        else
                                        {
                                            //console.log(cust_data);
                                                res.status(200).send({status: 200, result: {cust_id: cust_data[0].cust_id, cust_email: cust_data[0].cust_email, cust_first_name:cust_data[0].cust_first_name, cust_last_name:cust_data[0].cust_last_name}});

                                            
                                        }
                                    })

                        }
                    })

                        }
                    })

}



function NameUpperCase(string,is_owner_array){

    string = string.trim();
    var first_char = string.charAt(0).toUpperCase();
    var string2 = string.slice(1);
    var output_string = first_char + string2;
    if(is_owner_array == "1")
    {
        output_string = output_string.replace(/"/g,'\\"')
        
    }
    else
    {
        output_string = output_string.replace(/'/g,"\\'")
        
    }
    //console.log(output_string);
    return output_string;
}



exports.checkBlockedCustomer = (req,res,next) =>{
    if (!(req.body.store_id) || !(req.body.cust_email)) {
        res.status(400).send({status: 400, message: 'Missing required parameters. Check body and retry'});
        return;
    }
    
    CustomerModel.checkBlockedCustomer(req.body, (err, customer_data) => {
        if (err) {
            next(err);
        } else {

            // if(customer_data.length > 0)
            // {
              res.status(200).send({status: 200, result: customer_data});
            // }
            // else
            // {
            //   res.status(400).send({status: 400, result:{message: "No customer exists with given store_id and cust_email", customer:customer_data}});
            // }
        }
    })
}



exports.custForgotPassword = (req, res, next) => {

    if (!req.body.cust_email) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }
    var cust_email = req.body.cust_email;
    cust_email = cust_email.trim().toLowerCase();
    //console.log(cust_email);

   
    CustomerModel.checkCustEmail(cust_email, (err, data) => {
        if (err) {
            
            next(err);
        } else {
            //console.log("cust mst: ",data);
            if(data.length > 0)
            {
                //console.log("cust mst ");

                forgotPasswordEmail(data[0].cust_id,cust_email, req.hostname, function(email_err,email_data){
                    if (email_err) {
                        next(email_err);
        
                        // console.log("error from email");
                        // console.log(email_err)
                        // ErrorLogs.send_to_slack(email_err).then((slack_result) => {
                        //     console.log("Slack alert sent");
                        // })
                        // .catch((slack_err) => {
                        //     console.log(slack_err);
                        // });
                        

                    }
                    else{
                        //console.log(email_data);
                        console.log("Forgot password email success");
                        res.status(200).send({status: 200, result:{message:"Email with reset password link sent to provided Email Address", cust_email:cust_email}});

                    }
                })
                
               
            }
            else
            {
                CustomerModel.checkOwnerCustomerExists(cust_email, (owner_cust_err, owner_cust_data) => {
                    if (owner_cust_err) {
                        
                        next(owner_cust_err);
                    } else {
                        //console.log("owner cust mst: ",owner_cust_data);

                            if(owner_cust_data.length > 0)
                            {
                               
                                var owner_cust = owner_cust_data[0];
                                owner_cust.cust_first_name = NameUpperCase(owner_cust_data[0].cust_first_name,"0");
    
                                owner_cust.cust_last_name = NameUpperCase(owner_cust_data[0].cust_last_name,"0");
    
                                owner_cust.cust_created_date = moment().format("YYYY-MM-DD HH:mm:ss");
                                owner_cust.cust_password = constants.md5("bookjoy1234"+constants.encryption_key),
                                //console.log("owner cust: ",owner_cust);
                               
                                CustomerModel.addNewCustomer(owner_cust, (new_cust_err, new_cust_data) => {
                                    if (new_cust_err) {
                                        
                                        next(new_cust_err);
                                    } else {
                                        //console.log("new cust: ",new_cust_data);
                                        var cust_id = new_cust_data.insertId;

                                        CustomerModel.updateCustCreatedBy(cust_id, (update_err,update_data) => {
                                            if (update_err)
                                            {
                                                next(update_err);
                                            } 
                                            else
                                            {

                                                forgotPasswordEmail(cust_id,cust_email, req.hostname, function(email_err,email_data){
                                                    if (email_err) {
                                                        console.log("error from email 1");
                                                        console.log(email_err)
                                                        next(email_err);
                                        
                                                        // console.log("error from email");
                                                        // console.log(email_err)
                                                        // ErrorLogs.send_to_slack(email_err).then((slack_result) => {
                                                        //     console.log("Slack alert sent");
                                                        // })
                                                        // .catch((slack_err) => {
                                                        //     console.log(slack_err);
                                                        // });
                                                        
                                
                                                    }
                                                    else{
                                                       // console.log("email data: ",email_data);
                                                        console.log("Forgot password email success");
                                                        res.status(200).send({status: 200, result:{message:"Email with reset password link sent to provided Email Address", cust_email:cust_email}});
                                
                                                    }
                                                })
                                            }
                                        })

                                    }
                                })
                            }
                            else
                            {
                                res.status(400).send({status: 400, result:{message:"The provided Email Address does not exist in our Database!"}});
                            }

                        }

                        })
            }
  
        }
    })
}



exports.resetCustPassword = (req, res, next) => {

    if (!req.body.encoded_cust_email || !req.body.reset_pswd_key || !req.body.new_password) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }
    var cust_email = decodeURIComponent(req.body.encoded_cust_email);
    //console.log("cust_email: ", cust_email);

   
    CustomerModel.checkCustEmailResetPswdKey(cust_email, req.body.reset_pswd_key, (err, data) => {
        if (err) {
            
            next(err);
        } else {
            //console.log("customer present with given email and reset key: ",data);
            if(data.length > 0)
            {

                var cust_password_ciphertext = req.body.new_password;
 
                // Decrypt
                var bytes  = CryptoJS.AES.decrypt(cust_password_ciphertext, constants.crypto_key);
                var original_password = bytes.toString(CryptoJS.enc.Utf8);

                var cust_pswd_req ={
                    cust_id:data[0].cust_id,
                    cust_password: constants.md5(original_password+constants.encryption_key),
                    cust_updated_date: moment().format("YYYY-MM-DD HH:mm:ss")
                    
                }
                //console.log("cust_pswd_req: ",cust_pswd_req);

                CustomerModel.updateCustomerPassword(cust_pswd_req, (cust_err, cust_result) => {
                    if (cust_err)
                    {

                        next(cust_err);
                    } 
                    else 
                    {
                        //console.log(cust_result);
                        res.status(200).send({status: 200, result:{message:"Password updated successfully", cust_email:cust_email, cust_id:cust_pswd_req.cust_id}});

                       
                    }
               })
               
            }
            else
            {
                res.status(400).send({status: 400, result:{message:"The provided url expired."}});
            }
  
        }
    })
}

exports.getSelectedCustomerDetails = (req,res,next) =>{
    if (!(req.body.cust_id)) {
        res.status(400).send({status: 400, message: 'Missing required parameters. Check body and retry'});
        return;
    }
    CustomerModel.getSelectedCustomerDetails(req.body.cust_id, (err, response) => {
        if (err) {
            next(err);
        } else {
            res.status(200).send({status: 200, result:response});
        }
    })

}

exports.updateCustomerNotes = (req, res, next) => {

    if (!req.body.owner_cust_id || !req.body.owner_id || (!req.body.cust_notes && req.body.cust_notes != "") || !req.body.cust_updated_by) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }
    var cust_req ={
        cust_id: parseInt(req.body.owner_cust_id,10),
        owner_id: parseInt(req.body.owner_id,10),
        cust_notes: req.body.cust_notes.replace(/'/g,"\\'"),
        cust_updated_by: req.body.cust_updated_by,
        cust_updated_date : moment().format("YYYY-MM-DD HH:mm:ss")
    }
    //console.log("cust_req: ",cust_req);
    

    CustomerModel.updateCustomerNotes(cust_req, (err, data) => {
        if (err) {
            
            next(err);
        } else {
            //console.log(data);

            res.status(200).send({status: 200, result:{message:"Customer Notes updated successfully"}});
            
  
        }
    })
}

exports.bookingDetails = (req,res,next)=>{
    
    var cust_req ={
        cust_email: req.body.cust_email,
        start: parseInt(req.body.start,10),
    }
    if (!req.body.cust_email ||  req.body.start == null || isNaN(cust_req.start)) {
        res.status(400).send({status: 400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }
    
    CustomerModel.bookingDetailsModel(cust_req, (err, data) => {
        if (err) {            
            next(err);
        } else {
            //console.log(data);

            res.status(200).send({status: 200, result:data});
            
  
        }
    })
}



function forgotPasswordEmail(cust_id, cust_email, hostname, result){


    var random_number= Math.floor(Math.random()*10000000000).toString(36);
                //console.log("random number:",random_number);
                var today = moment().format("YYYYMMDD:HH:mm:ss"); 
                //console.log(today);
              
                var reset_pswd_key = cust_id+random_number+today;
                //console.log("reset pswd key: ", reset_pswd_key)
                reset_pswd_key = constants.md5(reset_pswd_key+constants.encryption_key);
                //console.log("md5 reset pswd key: ", reset_pswd_key);



                var cust_pswd_req ={
                    cust_email:cust_email,
                    reset_pswd_key:reset_pswd_key,
                    //cust_updated_by: req.body.cust_updated_by,
                    cust_updated_date: moment().format("YYYY-MM-DD HH:mm:ss")
                    
                }

                CustomerModel.updateCustResetPswdKey(cust_pswd_req, (cust_err, cust_result) => {
                    if (cust_err)
                    {

                        next(cust_err);
                    } 
                    else 
                    {
                        //console.log(cust_result);

                        var email_req ={
                            cust_email:cust_email,
                            reset_pswd_key:reset_pswd_key,
                            this_base_url: /* req.protocol + */ "https://" + hostname, // until fix the protocal issue we directly use https
                            email_base_url: hostname
                            
                        }
                

                        NotificationsLibrary.cust_forgot_password_email(email_req, function(email_err,email_data){
                            if (email_err) {
                
                                console.log("error from email");
                                console.log(email_err)
                                // ErrorLogs.send_to_slack(email_err).then((slack_result) => {
                                //     console.log("Slack alert sent");
                                // })
                                // .catch((slack_err) => {
                                //     console.log(slack_err);
                                // });
                                return(email_err,null);
                                

                            }
                            else{
                                //console.log(email_data);
                                return result(null,email_data);
                                // console.log("Forgot password email success");
                                // res.status(200).send({status: 200, result:{message:"Email with reset password link sent to provided Email Address", cust_email:cust_email}});

                            }
                        })

                    }
                })

    
}
async function getOldBookings(old_book_id,book_ids_array){

    while(old_book_id != 0)
    {
        book_ids_array.push(old_book_id);
        //console.log(old_book_id);
        //console.log("in while loop");

        try{

            var old_book_data = await BookingModel.getOldBookings(old_book_id);
            // console.log(old_book_data);
            // console.log(book_ids_array);

            for(i=0;i<old_book_data.length;i++)
            {
               if(book_ids_array.indexOf(old_book_data[i].book_id) < 0)
               {
                   //console.log(old_book_data[i].book_id);
                   book_ids_array.push(old_book_data[i].book_id);
               }
               //console.log(i);
                
            }
             //console.log("for loop completed");

            old_book_id = old_book_data[0].book_oldbookid;
        }
        catch(error){
            throw error;

        }
        
    }

    //console.log("old book loop completed");
    //console.log(book_ids_array);
    return book_ids_array;

}