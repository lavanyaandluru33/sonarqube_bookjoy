
const CustomersController = require('./controllers/customer.controller');
const auth = require('../../config/auth');
exports.routesConfig = function (app) {
    app.post('/get_customer_details', [
        CustomersController.getCustomerDetails
    ]);
    app.post('/images',  [
        CustomersController.uploadImages
    ]);
    app.post('/mobile/images',  [
        CustomersController.uploadCustomerImages
    ]);
    app.post('/get_customers_list' , [
        CustomersController.getCustomers
    ]);
    
    app.post('/get_customer_images' , [
        CustomersController.getCustomerImages
    ]);

    app.post('/get_details_of_customer', [
        CustomersController.getDetailsOfCustomer
    ]);
  
    app.post('/customer/save_customer', [
        CustomersController.saveCustomer
    ]);
    app.post('/customer/exists_with_store', [
        CustomersController.existsWithStore
    ]);

    
    app.post('/v2/customer/check_cust_email_exists_v2', 
        CustomersController.checkCustEmailExists
    );

    app.post('/v2/customer/signup_v2', 
        CustomersController.customerSignUp
    );

    app.post('/v2/customer/check_blocked_cust_v2', 
        CustomersController.checkBlockedCustomer
    );

    app.post('/v2/customer/forgot_password_v2', 
        CustomersController.custForgotPassword
    );

    app.post('/v2/customer/reset_password_v2', 
        CustomersController.resetCustPassword
    );

    app.post('/customer/get_customer_details',
        CustomersController.getSelectedCustomerDetails
    );

    app.post('/v2/customer/update_cust_notes_v2', 
        CustomersController.updateCustomerNotes
    );

    app.post('/v2/customer/booking_details', 
        CustomersController.bookingDetails
    );
}