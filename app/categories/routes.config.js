const CategoriesController = require('./controllers/categories.controller');
const auth = require('../../config/auth');

exports.routesConfig = function (app) {

    app.post('/categories/get_categories', 
        CategoriesController.getServiceCategories
    );
    
}