const constants = require('../../../config/constants.js');
const CategoriesModel = require('../models/categories.model');

exports.getServiceCategories = (req, res, next) => {
    if(!Object.keys(req.body).length) {
        res.status(400).send({ status:400, message: 'Invalid request parameters. Check headers and body'});
        return;
    }
    if(!req.body.store_id){
        res.status(400).send({ status:400, message: 'Missing required parameters. Check body and retry'});
        return;
    }

    CategoriesModel.getServiceCategories(req.body, (err, data) => {
        if (err) {
            //res.status(500).send({message:err});
            next(err);
        }
        else{
            res.status(200).send({status:200, result:data});
        }
    });

   
}