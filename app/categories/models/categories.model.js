const db = require('../../../config/db.js');
const constants = require('../../../config/constants.js');

exports.getServiceCategories = (data, result) => {
    let sql = `SELECT c.serv_cat_id, c.serv_cat_name, c.serv_cat_description, c.serv_cat_order, c.serv_cat_is_active FROM services_categories_mst as c,stores_mst as s
    WHERE s.store_is_active = 1 AND s.store_id=c.store_id AND c.store_id = ${data.store_id} AND c.serv_cat_is_active = 1
    AND c.serv_cat_name != "Break" AND c.is_break = 0
    ORDER BY c.serv_cat_order ASC`;
    
     //console.log(sql);

    db.query(sql , (err,res) => {
        if (err) {
            return result({error_code:err.code, message:err.sqlMessage,sql:err.sql}, null);    
        }
        return  result(null, res);
    });
           
}
