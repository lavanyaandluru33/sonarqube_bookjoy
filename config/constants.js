/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const express = require('express');
var os = require('os');

var app = express();
require('dotenv').config();
let constants = {
    accessTokenSecret: process.env.secret,
    refreshTokenSecret: 'yourrefreshtokensecrethere',
    refreshTokens: [],
    encryption_key: process.env.encryption_keys,
    jwt: require('jsonwebtoken'),
    md5: require('md5'),
    DB_HOST: 'localhost',
    DB_NAME: process.env.DB_NAMES,
    cloud_name: process.env.cloud_name,
    api_key: process.env.api_keys,
    api_secret: process.env.api_secrets,
    crypto_key: process.env.crypto_key,
    plivo_auth_id: process.env.PLIVO_AUTH_ID,
    plivo_auth_token: process.env.PLIVO_AUTH_TOKEN,
    sendgrid_api_key: process.env.SENDGRID_API_KEY,
    slack_webhook_url: process.env.SLACK_WEBHOOK_BOOKJOY_BIZ_URL,
    folder: "bookJoy_images",
    TIME_ZONE:"Australia/Brisbane",
    STATUS_PRE_BOOKED: 100,
    STATUS_BOOKED: 200,
    STATUS_CONFIRMED: 210,
    STATUS_ARRIVED: 220,
    STATUS_COMPLETED: 240,
    STATUS_RESCHEDULED: 500,
    STATUS_CANCELLED: 800,
    STATUS_LATE_CANCELLED: 880,
    STATUS_NO_SHOW: 890,
    ADD_BREAK_BOOKING: 1,
    ADD_SERVICE_BOOKING: 2,
    ADD_CLASS_BOOKING: 3,
    EDIT_BREAK_BOOKING: 4,
    DELETE_BREAK_BOOKING: 5,
    RESCHEDULE_SERVICE_BOOKING: 6,
    EDIT_SERVICE_BOOKING: 7,
    SERVICE_TYPE: 'service',
    CLASS_TYPE: 'class',
    PRODUCT_TYPE: 'product',
    STYLIST_TYPE: 'stylist',
    FULLAMOUNT_TYPE: 1,
    WALLET_CREDIT: 1,
    WALLET_DEBIT: 2,
    THROUGH_WALEET: 4,
    THROUGH_CASH: 0,
    THROUGH_POINT_SALE: 5,
    THROUGH_STRIPE:1,
    THROUGH_SQUARE:6,
    DEPOSIT_DISCOUNT_PRICE:1,
    FULL_DISCOUNT_PRICE:2,
    compare_to_previous_period:1,
    compare_to_previous_year:2
}
if (app.get('env') === 'development') {
    constants.DB_USER = process.env.DB_USERS_DEV;
    constants.DB_PASSWORD = '';
    constants.DB_PORT = process.env.DB_PORTS_DEV;
    constants.PROD_FLAG = 0;
    constants.slack_webhook_url = process.env.SLACK_WEBHOOK_LOCAL_URL;
} else {
    var host_name = os.hostname();
    switch (host_name)
    {
        case 'pointsround.com':
            constants.DB_USER = process.env.DB_USERS_biz;
            constants.DB_PASSWORD = process.env.DB_PASSWORD_biz;
            constants.DB_PORT = process.env.DB_PORTS_PROD;
            constants.PROD_FLAG = 0;
            constants.slack_webhook_url = process.env.SLACK_WEBHOOK_BOOKJOY_BIZ_URL;
            break;
        case 'raghu.px4app.tk':
            constants.DB_USER = process.env.DB_USERS_PROD;
            constants.DB_PASSWORD = process.env.DB_PASSWORD_PROD;
            constants.DB_PORT = process.env.DB_PORTS_PROD;
            constants.PROD_FLAG = 0;
            constants.slack_webhook_url = process.env.SLACK_WEBHOOK_BOOKJOY_BIZ_URL;
            break;
        case 'get.bookjoy.co':
            constants.DB_USER = process.env.DB_USERS_GET;
            constants.DB_PASSWORD = process.env.DB_PASSWORD_GET;
            constants.DB_PORT = process.env.DB_PORTS_PROD;
            constants.PROD_FLAG = 1;
            constants.slack_webhook_url = process.env.SLACK_WEBHOOK_GET_BOOKJOY_CO_URL;
            break;
        case 'bookjoy.co':
            constants.DB_USER = process.env.DB_USERS_CO;
            constants.DB_PASSWORD = process.env.DB_PASSWORD_CO;
            constants.DB_PORT = process.env.DB_PORTS_PROD;
            constants.PROD_FLAG = 1;
            constants.slack_webhook_url = process.env.SLACK_WEBHOOK_BOOKJOY_CO_URL;
            break;
        case 'bookjoyvm':
            constants.DB_USER = process.env.DB_USERS_PROD;
            constants.DB_PASSWORD = process.env.DB_PASSWORD_PROD;
            constants.DB_PORT = process.env.DB_PORTS_DEV;
            constants.PROD_FLAG = 0;
            constants.slack_webhook_url = process.env.SLACK_WEBHOOK_LOCAL_URL;
            break;
        case 'mobile.bookjoy.me':
            constants.DB_USER = process.env.DB_USER_MOBILE;
            constants.DB_PASSWORD = process.env.DB_PASS_MOBILE;
            constants.DB_PORT = process.env.DB_PORTS_PROD;
            constants.PROD_FLAG = 0;
            constants.slack_webhook_url = process.env.SLACK_WEBHOOK_LOCAL_URL;
            break;
    }

}
module.exports = Object.freeze(constants);