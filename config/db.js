const constants = require('./constants.js');
var mysql = require('mysql');
let count = 0;
var connection = mysql.createConnection({
    host: constants.DB_HOST,
    port: constants.DB_PORT,
    user: constants.DB_USER,
    password: constants.DB_PASSWORD,
    database: constants.DB_NAME,
    dateStrings: true
});


const connectWithRetry = () => {
    console.log('Mysql connection with retry')
    connection.connect(function (err) {
        if (err) {
            console.log('Mysql connection unsuccessful, retry after 5 seconds. ', ++count);
            setTimeout(connectWithRetry, 5000);
//        throw err;
        } else
            console.log('Mysql is connected')
    });
};

connectWithRetry();

module.exports = connection;