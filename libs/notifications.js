const constants = require('../config/constants.js');
const BookingModel = require('../app/booking/models/booking.model');
const StaffModel = require('../app/staff/models/staff.model.js');
const CustomerModel = require('../app/customer/models/customer.model');
// const TransactionModel = require('../../transactions/models/transaction.model');
const ServicesModel = require('../app/services/models/services.model.js');

// const ClassesModel = require('../../classes/models/classes.model');
const StoresModel = require('../app/stores/models/stores.model.js');
const UserModel = require('../app/user/models/user.model.js');
const NotificationsModel = require('../app/notifications/models/notifications.model.js');

const EmailTemplates = require('./email_templates.json');
const ErrorLogs = require('./error_logs.js');


var moment = require('moment');

const SendGrid = require('@sendgrid/mail');
SendGrid.setApiKey(constants.sendgrid_api_key);


const plivo = require('plivo');
let client = new plivo.Client(constants.plivo_auth_id, constants.plivo_auth_token);




var admin = require("firebase-admin");

var serviceAccount = require("../bookmagflutter-firebase-adminsdk.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://bookmagflutter.firebaseio.com"
});


// var admin = require('firebase-admin');
// const ServiceAccount = require('../bookmagflutter-firebase-adminsdk.json');
// admin.initializeApp({
//   credential:admin.credential.cert(ServiceAccount),
//   databaseURL: 'https://bookmag.firebaseio.com'
// })



exports.notification_details = (notif_details,result) =>
{
    //console.log("in library");
    StoresModel.getStoreDetails(notif_details.store_id, (store_err, store_details) => {
        if (store_err) {

           return result(store_err, null);

        } else {
            //console.log(store_details);
            notif_details.store_details = store_details[0];

            notif_details.owner_id = store_details[0].user_id;

            notif_details.store_address = store_details[0].store_add1 + "," + store_details[0].store_add2 + "," + store_details[0].store_city + "," + store_details[0].store_state + "," + store_details[0].store_pin;

            //console.log("notif details in lib: ", notif_details);

            BookingModel.getServiceBookingDetails(notif_details.book_id, notif_details.have_multi_id, 0, (store_err, booking_details) => {
                if (store_err) {
                    return result(store_err, null);

                } else {
                    //console.log("booking_details: ", booking_details);
                    notif_details.booking_details = booking_details;

                    BookingModel.getProductDetails(notif_details.book_id, 0, (products_err, products_data) => {
                        if (products_err) {
                            next(products_err);
                        } else {
                            //console.log("product bookings: ", products_data);
                            notif_details.products_details = products_data;
                        


                    staff_email_notification(notif_details,(staff_err,staff_result) => {
                        if(staff_err)
                        {
                            //console.log("staff err------", staff_err)
                            
                            ErrorLogs.send_to_slack(staff_err).then((slack_result) => {
                                //console.log("Slack alert sent");
                            })
                            .catch((slack_err) => {
                                //console.log(slack_err);
                            });
                        }
                        else
                        {
                            //console.log("staff email sent successfully")
                        }
                    });

                    if(notif_details.email_send){
                    cust_email_notification(notif_details,(cust_err,cust_result) => {
                        if(cust_err)
                        {
                            //console.log("cust err------", cust_err)
                            
                            ErrorLogs.send_to_slack(cust_err).then((slack_result) => {
                                //console.log("Slack alert sent");
                            })
                            .catch((slack_err) => {
                                //console.log(slack_err);
                            });
                        }
                        else
                        {
                            //console.log(cust_result);
                            return result(null, {message: "Cust email sent successfully"});
                            //console.log("cust email sent successfully")
                        }
                    });
                }

                if(notif_details.sms_send){
                    this.sms_content(notif_details,(sms_err,sms_result) => {
                        if(sms_err)
                        {
                            //console.log("sms error------")
                            ErrorLogs.send_to_slack(sms_err).then((slack_result) => {
                                //console.log("Slack alert sent");
                            })
                            .catch((slack_err) => {
                                //console.log(slack_err);
                            });
                        }
                        else
                        {
                            //console.log("sms sent successfully")
                        }
                    });
                 }   
                        
                    this.send_push_notification(notif_details, (fcm_err,fcm_result) => {
                        if(fcm_err)
                        {
                            console.log("fcm error------")
                            ErrorLogs.send_to_slack(fcm_err).then((slack_result) => {
                                //console.log("Slack alert sent");
                            })
                            .catch((slack_err) => {
                                //console.log(slack_err);
                            });
                        }
                        else
                        {
                            console.log("push notification successfully")
                        }
                    });
                }
            })
              

                }
                
            })

        }

    })
}




function cust_email_notification(mail_details,mail_result)
{
    //console.log(mail_details);
    var send_admin_mail = 0;
    var subject = '';
    if(!(mail_details.book_status_message)) subject = "Booking Confirmed!";
    else subject = mail_details.book_status_message
    var booking_message = '';
    var notif_title = "";
    if(subject == 'Booking Rescheduled!') 
    {
        booking_message = 'rescheduled';
        notif_title = "STATUS_BOOKING_RESCHEDULED";
    }
    else if(subject == 'Booking Updated!') 
    {
        booking_message ='updated';
        notif_title = "STATUS_BOOKING_UPDATED";
    }
    else if(subject == 'Booking Cancelled!') 
    {
        booking_message ='cancelled';
        notif_title = "STATUS_BOOKING_CANCELLED";
    }
    else {
        booking_message = 'confirmed';
        notif_title = "STATUS_BOOKING_CONFIRMED";
    }

    var email_data = {
        to: mail_details.cust_email,
        from: "donotreply@" + mail_details.email_base_url,
        //bookjoy.co",

        subject: subject,
        html: "",
    }

    //console.log(mail_details.store_details);

    var email_template = EmailTemplates.default_template;

    email_template += EmailTemplates.cust_template;

    email_template = email_template.replace("{{base_url}}", mail_details.this_base_url);
    email_template = email_template.replace("{{cancel_base_url}}", mail_details.this_base_url);
    email_template = email_template.replace("{{reschedule_base_url}}", mail_details.this_base_url);
    email_template = email_template.replace("{{cust_name}}", mail_details.cust_name);
    email_template = email_template.replace("{{booking_message}}", booking_message);
    email_template = email_template.replace("{{book_id}}", mail_details.book_id);
    email_template = email_template.replace("{{store_name}}", mail_details.store_details.store_name);
    email_template = email_template.replace("{{store_address}}", mail_details.store_address);
    email_template = email_template.replace("{{store_add1}}", mail_details.store_details.store_add1);
    email_template = email_template.replace("{{store_add2}}", mail_details.store_details.store_add2);
    email_template = email_template.replace("{{store_city}}", mail_details.store_details.store_city);
    email_template = email_template.replace("{{store_state}}", mail_details.store_details.store_state);
    email_template = email_template.replace("{{store_pin}}", mail_details.store_details.store_pin);
    email_template = email_template.replace("{{store_tel}}", mail_details.store_details.store_tel);
    var booking_card = "";

    for (i = 0; i < mail_details.booking_details.length; i++)
    {
        if (mail_details.owner_id != mail_details.booking_details[i].staff_id)
        {
            send_admin_mail = 1;
        }
//            console.log("admin mail"+send_admin_mail);

        var this_booking_card = EmailTemplates.booking_card;

        var service_num = i + 1;

        this_booking_card = this_booking_card.replace("{{service_num}}", service_num);
        this_booking_card = this_booking_card.replace("{{service_name}}", mail_details.booking_details[i].serv_name);

        var start = mail_details.booking_details[i].book_start_date + " " + mail_details.booking_details[i].book_start_time;
        var end = mail_details.booking_details[i].book_start_date + " " + mail_details.booking_details[i].book_end_time;

        var start_moment = moment(start);
        var end_moment = moment(end);

        var date = moment(start).format('MMMM Do YYYY, hh:mm A');
        //console.log(date);

        this_booking_card = this_booking_card.replace("{{booking_date}}", date);

        var duration = moment.duration(end_moment.diff(start_moment));
        var booking_duration = "";
        if (duration.hours() > 0)
        {
            booking_duration += duration.hours() + " Hrs ";
        }

        if (duration.minutes() > 0)
        {
            booking_duration += duration.minutes() + " Mins";
        }
        //console.log(booking_duration);

        this_booking_card = this_booking_card.replace("{{booking_duration}}", booking_duration);

        var staff_name = mail_details.booking_details[i].staff_first_name + " " + mail_details.booking_details[i].staff_last_name;
        this_booking_card = this_booking_card.replace("{{staff_name}}", staff_name);

        booking_card += this_booking_card;
//            console.log("booking_card i "+i);

    }

    //Products Card Start
    if(mail_details.products_details && mail_details.products_details.length > 0)
    {
         booking_card += "</table> <table width = '100%'><tr> <td> <b>Products</b> </td> </tr>";
    for (i = 0; i < mail_details.products_details.length; i++)
    {

        var this_products_card = EmailTemplates.products_card;

        //var products_num = i + 1;

        //this_products_card = this_products_card.replace("{{service_num}}", service_num);
        this_products_card = this_products_card.replace("{{product_name}}", mail_details.products_details[i].product_name);


        var product_date = moment(mail_details.products_details[i].product_date).format('MMMM Do YYYY');
        //console.log(date);

        this_products_card = this_products_card.replace("{{product_date}}", product_date);

        this_products_card = this_products_card.replace("{{product_quantity}}", mail_details.products_details[i].product_quantity);

        booking_card += this_products_card;
           // console.log("products_card i "+i);

    }
}
    //Products card end

    //console.log("booking_card end ");console.log(booking_card);

    email_template = email_template.replace("{{booking_card}}", booking_card);

    email_template = email_template.replace("{{store_terms_and_conditions}}", mail_details.store_details.store_sett_terms_and_conditions);

//      console.log("admin mail end "+send_admin_mail);

    if (send_admin_mail == 1)
    {
        admin_email_notification(mail_details, booking_card, (admin_err,admin_result) => {
            if(admin_err)
            {
                //console.log("admin email error------");
                console.log(admin_err);
                ErrorLogs.send_to_slack(admin_err).then((slack_result) => {
                    //console.log("Slack alert sent");
                })
                .catch((slack_err) => {
                    //console.log(slack_err);
                });
                
            }
            else
            {
                //console.log("admin email sent successfully");
            }
        });
    }

    email_data.html = email_template;

    SendGrid.send(email_data).then((result) => {
        //console.log("cust email sent");

        if (mail_details.booking_details.length > 1)
        {
            var notif_booking_type = 4; //multi service booking
        } else
        {
            var notif_booking_type = 1; //service booking 
        }


        //Save notification in db
        var notif_req = {
            sent_to: 2,
            book_id: mail_details.book_id,
            cust_id: mail_details.cust_id,
            user_id: mail_details.booking_details[0].user_id,
            store_id: mail_details.store_details.store_id,
            serv_id: mail_details.booking_details[0].serv_id,
            booking_type: notif_booking_type,
            notif_type: 1,
            notif_title: notif_title,
            notif_response: 1,
            notif_created_by: mail_details.book_created_by,
            notif_created_date: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')

        }

        NotificationsModel.addNotification(notif_req, (notif_err, notif_result) => {
            if (notif_err) {

                console.log("notification status db err--------------");//console.log(notif_err);
                return mail_result(notif_err,null);
            } else {
                console.log('success');
                return mail_result(null,notif_result);
                
            }
        })
    })
    .catch((err) => {
        console.log(err);
        return mail_result({email_error:err},null);
    });

}
//end of email notificaiton template



function staff_email_notification(mail_details,mail_result)
{
    //console.log(mail_details);

    var j = 0;

    if (mail_details.booking_details.length > 1)
    {
        var notif_booking_type = 4; //multi service booking
    } else
    {
        var notif_booking_type = 1; //service booking 
    }

    var subject = '';
    if(!(mail_details.book_status_message) || mail_details.book_status_message == 'Booking Confirmed') subject = "Booking Successful!";
    else subject = mail_details.book_status_message;

    var booking_message = '';
    var notif_title = "";
    if(subject == 'Booking Rescheduled!') 
    {
        booking_message = 'Booking with the following detail on your schedule has been rescheduled';
        notif_title = "STATUS_BOOKING_RESCHEDULED";
    }
    else if(subject == 'Booking Updated!') 
    {
        booking_message ='Booking with the following detail on your schedule has been updated';
        notif_title = "STATUS_BOOKING_UPDATED";
    }
    else if(subject == 'Booking Cancelled!') 
    {
        booking_message ='Booking with the following detail on your schedule has been cancelled';
        notif_title = "STATUS_BOOKING_CANCELLED";
    }
    else 
    {
        booking_message = 'You have a new booking on your schedule. Find the booking details below';
        notif_title = "STATUS_BOOKING_CONFIRMED";
    }


    for (i = 0; i < mail_details.booking_details.length; i++)
    {
        var email_data = {
            to: mail_details.booking_details[i].staff_email,
            from: "donotreply@" + mail_details.email_base_url,
            //from:"donotreply@bookjoy.co",
            subject: subject,
            html: "",
        }

        //console.log(email_data)



        var email_template = EmailTemplates.default_template;

        email_template += EmailTemplates.staff_template;

        email_template = email_template.replace("{{base_url}}", mail_details.this_base_url);
        email_template = email_template.replace("{{cust_name}}", mail_details.cust_name);
        email_template = email_template.replace("{{book_id}}", mail_details.booking_details[i].book_id);

        email_template = email_template.replace("{{staff_booking_message}}", booking_message);


        var start = mail_details.booking_details[i].book_start_date + " " + mail_details.booking_details[i].book_start_time;

        var end = mail_details.booking_details[i].book_start_date + " " + mail_details.booking_details[i].book_end_time;

        var start_moment = moment(start);
        var end_moment = moment(end);

        date = moment(start).format('MMMM Do YYYY, hh:mm A');
        //console.log(date);

        email_template = email_template.replace("{{booking_date}}", date);

        var duration = moment.duration(end_moment.diff(start_moment));
        // console.log(duration);
        // console.log(duration.hours());console.log(duration.minutes());

        var booking_duration = "";

        if (duration.hours() > 0)
        {
            booking_duration += duration.hours() + " Hrs ";
        }

        if (duration.minutes() > 0)
        {
            booking_duration += duration.minutes() + " Mins";
        }

        //console.log(booking_duration);

        email_template = email_template.replace("{{booking_duration}}", booking_duration);

        var staff_name = mail_details.booking_details[i].staff_first_name + " " + mail_details.booking_details[i].staff_last_name;

        email_template = email_template.replace("{{staff_name}}", staff_name);

        email_template = email_template.replace("{{service_name}}", mail_details.booking_details[i].serv_name);

        email_data.html = email_template;

        SendGrid.send(email_data).then((result) => {
            //console.log("sent staff mail");
            //console.log(result);

            //Save notification in db
            var notif_req = {
                sent_to: 1,
                book_id: mail_details.booking_details[j].book_id,
                cust_id: mail_details.cust_id,
                user_id: mail_details.booking_details[j].user_id,
                store_id: mail_details.store_details.store_id,
                serv_id: mail_details.booking_details[j].serv_id,
                booking_type: notif_booking_type,
                notif_type: 1,
                notif_title: notif_title,
                notif_response: 1,
                notif_created_by: mail_details.book_created_by,
                notif_created_date: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')

            }
            //console.log(notif_req);
            //console.log(j);
            j++;

            NotificationsModel.addNotification(notif_req, (notif_err, notif_result) => {
                if (notif_err) {
                    //console.log(notif_err);
                    return mail_result(notif_err,null);
                } else {
                    //console.log(notif_result);
                }
            })

        })
        .catch((err) => {
            console.log(err);
            return mail_result({email_error:err},null)
        });
    }
}
//end of email notificaiton template


function admin_email_notification(mail_details, booking_card, mail_result)
{
    //console.log(mail_details);

    var subject = '';
    if(!(mail_details.book_status_message) || mail_details.book_status_message == 'Booking Confirmed') subject = "Booking Successful!";
    else subject = mail_details.book_status_message;


    var booking_message = '';
    var notif_title = "";
    if(subject == 'Booking Rescheduled!') 
    {
        booking_message = 'Booking with the following detail on your schedule has been rescheduled';
        notif_title = "STATUS_BOOKING_RESCHEDULED";
    }
    else if(subject == 'Booking Updated!') 
    {
        booking_message ='Booking with the following detail on your schedule has been updated';
        notif_title = "STATUS_BOOKING_UPDATED";
    }
    else if(subject == 'Booking Cancelled!') 
    {
        booking_message ='Booking with the following detail on your schedule has been cancelled';
        notif_title = "STATUS_BOOKING_CANCELLED";
    }
    else 
    {
        booking_message = 'You have a new booking on your schedule. Find the booking details below';
        notif_title = "STATUS_BOOKING_CONFIRMED";
    }
    

    var email_data = {
        to: "",
        from: "donotreply@" + mail_details.email_base_url,
        //from:"donotreply@bookjoy.co",
        subject: subject,
        html: "",
    }



    UserModel.getUserDetails(mail_details.owner_id, (owner_err, owner_details) => {
        if (owner_err) {
           
            return mail_result(owner_err,null);

        } else {
            //console.log(owner_details);

            email_data.to = owner_details[0].user_email;

            var email_template = EmailTemplates.default_template;

            email_template += EmailTemplates.admin_template;

            email_template = email_template.replace("{{base_url}}", mail_details.this_base_url);
            email_template = email_template.replace("{{cust_name}}", mail_details.cust_name);
            email_template = email_template.replace("{{book_id}}", mail_details.book_id);

            email_template = email_template.replace("{{booking_card}}", booking_card);
            email_template = email_template.replace("{{admin_booking_message}}", booking_message);


            email_data.html = email_template;


            SendGrid.send(email_data).then((result) => {
                //console.log("sent admin mail");
                //console.log(result);

                if (mail_details.booking_details.length > 1)
                {
                    var notif_booking_type = 4; //multi service booking
                } else
                {
                    var notif_booking_type = 1; //service booking 
                }

                //Save notification in db
                var notif_req = {
                    sent_to: 3,
                    book_id: mail_details.book_id,
                    cust_id: mail_details.cust_id,
                    user_id: mail_details.booking_details[0].user_id,
                    store_id: mail_details.store_details.store_id,
                    serv_id: mail_details.booking_details[0].serv_id,
                    booking_type: notif_booking_type,
                    notif_type: 1,
                    notif_title: notif_title,
                    notif_response: 1,
                    notif_created_by: mail_details.book_created_by,
                    notif_created_date: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')

                }

                NotificationsModel.addNotification(notif_req, (notif_err, notif_result) => {
                    if (notif_err) {

                        //console.log(notif_err);
                        return mail_result(notif_err,null);

                    } else {
                        //console.log(notif_result)
                    }
                })
            })
            .catch((err) => {
                //console.log(err);
                return mail_result({email_error:err},null);
            });

        }
    })
}



exports.sms_content = (details,sms_content_result) =>
{
    //console.log("sms details:", details);

            var start_date_time = details.booking_details[0].book_start_date + " " + details.booking_details[0].book_start_time;

            var service_name = details.booking_details[0].serv_name;

            var services_number = details.booking_details.length - 1;
            var products_number = 0;
            if(details.products_details && details.products_details.length > 0)
            {
                products_number  = details.products_details.length ;
            }
            var booking_status_message = '';
            if(!(details.book_status_message)) booking_status_message = "Booking Confirmed!";
            else booking_status_message = details.book_status_message;
            var booking_message = '';
            if(booking_status_message == 'Booking Rescheduled!') booking_message = 'rescheduled to'
            else if(booking_status_message == 'Booking Updated!') booking_message ='updated to'
            else if(booking_status_message == 'Booking Cancelled!') booking_message ='cancelled for'
            else booking_message = 'confirmed for'
            if (details.booking_details.length > 1)
            {
                var notif_booking_type = 4; //multi service booking
            } else
            {
                var notif_booking_type = 1; //service booking 
            }


            for (i = 0; i < details.booking_details.length; i++)
            {
                var this_start_date_time = details.booking_details[i].book_start_date + " " + details.booking_details[i].book_start_time;

                if (this_start_date_time < start_date_time)
                {
                    start_date_time = this_start_date_time;

                    service_name = details.booking_details[i].serv_name;
                }
                //console.log(i);
            }

            //console.log("loop ended");

            var date_time_moment = moment(start_date_time);

            // var date = moment(date_time_moment).format('MMMM Do YYYY,hh:mm A');

            // var date_and_time = date.split(",");
            //console.log(date_and_time);

            var date = moment(date_time_moment).format('ddd, MMM Do, YYYY');
            var time = moment(date_time_moment).format('hh:mm A');
            // console.log(date);
            // console.log(time);


            // if (service_name.length >= 12)
            // {
            //     service_name = service_name.substr(0, 9) + "..";
            // }

            if (service_name.length >= 28)
            {
                service_name = service_name.substr(0, 25) + "..";
            }

            var cust_sms = ``;

            if (services_number > 0)
            {
                if (services_number > 1)
                {
                    var label = "services";
                } else
                {
                    var label = "service";
                }
                cust_sms += `Booking for ${service_name} +${services_number} ${label} ${booking_message} ${date} at ${time}`;
                 //cust_sms += `Your booking for ${service_name} +${services_number} more ${label} has been ${booking_message} for ${date_and_time[1]} on ${date_and_time[0]}`;
            } else
            {
                 cust_sms += `Booking for ${service_name} ${booking_message} ${date} at ${time}`;
                 //cust_sms += `Your booking for ${service_name} has been ${booking_message} for ${date_and_time[1]} on ${date_and_time[0]}`;
            }

            if (products_number > 0)
            {
                if (products_number > 1)
                {
                    cust_sms += ` +${products_number} products`;
                } else
                {
                    cust_sms += ` +${products_number} product`;
                }
                 
            }
            let cancel_status = ' 24hrs notice required for cancellation.'
            if(booking_status_message == 'Booking Cancelled!')
            {
                cust_sms += ` cancelled`;
                cancel_status='';
            }
            
            var store_name = details.store_details.store_name;

            cust_sms = `Your booking at ${store_name} ${booking_message} ${date} at ${time}.${cancel_status}DoNotReplyHere`;
            var cust_number = "+" + details.cust_mobile;

//            console.log("sms content: ",cust_sms);
//            console.log("sms content: ",cust_sms.length);



            //To save notification in db
            var notif_req = {
                sent_to: 2,
                book_id: details.book_id,
                cust_id: details.cust_id,
                user_id: details.booking_details[0].user_id,
                store_id: details.store_details.store_id,
                serv_id: details.booking_details[0].serv_id,
                booking_type: notif_booking_type,
                notif_type: 2,
                notif_title: booking_status_message,
                notif_response: "",
                notif_created_by: details.book_created_by,
                notif_created_date: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')

            }
            this.send_sms(cust_number, cust_sms, notif_req, (sms_err,sms_result) => {
                if(sms_err)
                {
                    console.log("sms err------")
                    console.log(sms_err);
                    return sms_content_result(sms_err,null);
                }
                else
                {
                    //console.log("sms sent successfully")
                }
            });

}



exports.send_sms = (to_number, message, notif_req, sms_result) =>
{
    // console.log("in messages");
    // console.log(to_number);
    // console.log(message);

    client.messages.create(
            'BOOKJOY',
            to_number,
            message
            ).then(function (message_created) {
        //console.log(message_created)

        notif_req.notif_response = JSON.stringify(message_created);

        NotificationsModel.addNotification(notif_req, (notif_err, notif_result) => {
            if (notif_err) {

                //console.log(notif_err);
                return sms_result(notif_err,null);
            } else {
                //console.log(notif_result)
                //console.log("sms notification saved in db");
            }
        })
    })
    .catch((error) => {
        console.log('Error sending sms:', error);
        //console.log(error);
        return sms_result({sms_error: error.message},null);

        
    });

}



exports.send_push_notification = (details, fcm_result) =>
{

    //console.log("details: ", details);
    if (details.booking_details.length > 1)
    {
        var notif_booking_type = 4; //multi service booking
    } else
    {
        var notif_booking_type = 1; //service booking 
    }


    var start_date_time = details.booking_details[0].book_start_date + " " + details.booking_details[0].book_start_time;

    var date_time_moment = moment(start_date_time);

    var date = moment(date_time_moment).format('MMMM Do YYYY,hh:mm A');

    var date_and_time = date.split(",");
    //console.log(date_and_time);

    var services_number = details.booking_details.length - 1;
    var notif_body = "";

    if (services_number > 0)
    {
        if (services_number > 1)
        {
            var label = "services";
        } else
        {
            var label = "service";
        }
        notif_body = `${details.booking_details[0].serv_name} +${services_number} ${label} for ${details.cust_name}`;
    } else
    {
        notif_body = `${details.booking_details[0].serv_name} for ${details.cust_name}`;
    }

    var booking_status_message = '';
    if(!(details.book_status_message)) booking_status_message = "Booking Confirmed!";
    else booking_status_message = details.book_status_message;
    var booking_message = '';
    var notif_title = "";
    if(booking_status_message == 'Booking Rescheduled!')
    {
        booking_message = 'Booking rescheduled to';
        notif_title = "STATUS_BOOKING_RESCHEDULED";
    }
    else if(booking_status_message == 'Booking Updated!') 
    {
        booking_message ='Booking updated to';
        notif_title = "STATUS_BOOKING_UPDATED";
    }
    else if(booking_status_message == 'Booking Cancelled!') 
    {
        booking_message ='Booking cancelled on';
        notif_title = "STATUS_BOOKING_CANCELLED";
    }
    else 
    {
        booking_message = 'New Booking on';
        notif_title = "STATUS_BOOKING_CONFIRMED";
    }

    //console.log("in get user tokens: ");


            UserModel.getUserFcmTokens(details.owner_id, (token_err, token_details) => {
                if (token_err) {
                    return fcm_result(token_err,null);
                }
                    else
                    {
                        //console.log("token details: ", token_details);
                        try {
                        var tokens_array = [];
                        token_details.filter((token_obj)=>{

                            if(tokens_array.indexOf(token_obj.fcm_token) < 0)
                            {
                                //console.log("token_obj.fcm_token: ", token_obj.fcm_token);
                                
                                if(token_obj.fcm_token.length > 0)
                                {
                                    tokens_array.push(token_obj.fcm_token);
                                }
                            }
                            //console.log("token_obj: ", token_obj);

                        })

                        //console.log("tokens_array: ", tokens_array);
        
            
            //try {
            CustomerModel.getSelectedCustomerDetails(details.cust_id,(cust_err,cust_result)=>{
                var registrationTokens = tokens_array;
                let booking_details = {
                            book_id: details.book_id,
                            cust_id: details.cust_id,
                            cust_name: details.cust_name,
                            is_vip:cust_result[0].is_vip,
                            has_allergy:cust_result[0].has_allergy,
                            cust_picture:cust_result[0].cust_picture, 
                            is_banned:cust_result[0].is_banned, 
                            pay_fullpayment:cust_result[0].pay_fullpayment, 
                            cust_notes:cust_result[0].cust_notes, 
                            cust_email: details.cust_email,
                            cust_mobile: details.cust_mobile,
                            store_id: details.store_id,
                            have_multi_id: details.have_multi_id,
                            booking_details: details.booking_details,
                            product_details: details.products_details
                        };
                var message = {
                    data: {
                        click_action: "FLUTTER_NOTIFICATION_CLICK",
                        booking_details: JSON.stringify(booking_details)
                    },
                    notification: {
                        body: notif_body,
                        title: `${booking_message} ${date_and_time[0]} at ${date_and_time[1]}`
                    },
                    tokens: registrationTokens
                };

                //console.log("fcm message: ", message);

                if (registrationTokens.length > 0) {
                    // Send a message to the device corresponding to the provided registration token.
                    //admin.messaging().send(message)
                    admin.messaging().sendMulticast(message)
                            .then((response) => {
                                // Response is a message ID string.
                                //console.log("fcm response: ",response);

                                //To save notification in db
                                var notif_req = {
                                    sent_to: 3,
                                    book_id: details.book_id,
                                    cust_id: details.cust_id,
                                    user_id: details.booking_details[0].user_id,
                                    store_id: details.store_details.store_id,
                                    serv_id: details.booking_details[0].serv_id,
                                    booking_type: notif_booking_type,
                                    notif_type: 4,
                                    notif_title: notif_title,
                                    notif_response: response,
                                    notif_created_by: details.book_created_by,
                                    notif_created_date: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')

                                }

                                NotificationsModel.addNotification(notif_req, (notif_err, notif_result) => {
                                    if (notif_err) {

                                        //console.log(notif_err);
                                        return fcm_result(notif_err,null);
                                    } else {
                                        //console.log("fcm notification saved in db")
                                    }
                                })

                            })
                            .catch((error) => {
                                console.log('Error sending push notification:', error);
                                //console.log(error);
                                return fcm_result({fcm_notification_error:error},null);
                            });
                 }
            });                
            } catch (error) {
                //console.log('Error sending message:', error);
                return fcm_result(error,null);
            }
        }
    })

}



exports.cust_forgot_password_email =(mail_details,mail_result) =>
{
    //console.log(mail_details);

    
        var email_data = {
            to: mail_details.cust_email,
            from: "donotreply@" + mail_details.email_base_url,
            subject: "BookJoy Password Reset",
            html: "",
        }

        //console.log(email_data)



        var encoded_email_url = encodeURIComponent(mail_details.cust_email);


        var email_template = EmailTemplates.default_template;
        email_template += EmailTemplates.forgot_password_template;

        //console.log(email_template);

        email_template = email_template.replace("{{base_url}}", mail_details.this_base_url);
        email_template = email_template.replace("{{this_base_url}}", mail_details.this_base_url);
        email_template = email_template.replace("{{encoded_email_url}}",encoded_email_url);
        email_template = email_template.replace("{{reset_pswd_key}}", mail_details.reset_pswd_key);

        //console.log(email_template);


        email_data.html = email_template;
        //console.log(email_data);

        
        SendGrid.send(email_data).then((result) => {
            //console.log("sent reset pwd email");
            //console.log(result);
            return mail_result(null,result);

            
        })
        .catch((err) => {
            console.log(err);
            return mail_result({email_error:err},null)
        });

};
exports.sendFormLink = (data, details) => {
    let email_data = {
        to: data.cust_email,
        from: "donotreply@" + data.email_base_url,
        //bookjoy.co",
        subject: 'Please Fill The Form',
        html: "",
    };
    let email_template = EmailTemplates.default_template;
    email_template += EmailTemplates.send_form;
    let link = `<a href='${data.url}' target='_blank'>Click Here</a>`;
    email_template = email_template.replace('{{form_link}}',link);
    email_template = email_template.replace("{{base_url}}", 'https://'+data.email_base_url);
    email_data.html = email_template;

    SendGrid.send(email_data).then((result) => {
        return details({message:'success','result':result},null);
    })
    .catch((err) => {
        console.log(err);
        return details(null,{email_error: err});
    });
}