const constants = require('../config/constants.js');

const BookingModel = require('../app/booking/models/booking.model');
const StaffModel = require('../app/staff/models/staff.model.js');

const ServicesModel = require('../app/services/models/services.model.js');
const StoresModel = require('../app/stores/models/stores.model.js');
const UserModel = require('../app/user/models/user.model.js');
const NotificationsModel = require('../app/notifications/models/notifications.model.js');





const { IncomingWebhook } = require('@slack/webhook');
 
// Read a url from the environment variables
const url = constants.slack_webhook_url;
// process.env.SLACK_WEBHOOK_URL;
 
// Initialize
const webhook = new IncomingWebhook(url);



exports.send_to_slack = async (error) =>
{
    error = JSON.stringify(error);
    error = error.replace(/[}{]/g,'');
    error = error.split(',').join('\n');
    error = '***********************\n' + error + '\n***********************';
    await webhook.send({
        text: error
      });



}



