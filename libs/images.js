const constants = require('../config/constants.js');
const BookingModel = require('../app/booking/models/booking.model');
const StaffModel = require('../app/staff/models/staff.model.js');
// const CustomerModel = require('../../customer/models/customer.model');
// const TransactionModel = require('../../transactions/models/transaction.model');
const ServicesModel = require('../app/services/models/services.model.js');

// const ClassesModel = require('../../classes/models/classes.model');
const StoresModel = require('../app/stores/models/stores.model.js');
const UserModel = require('../app/user/models/user.model.js');
const NotificationsModel = require('../app/notifications/models/notifications.model.js');

const EmailTemplates = require('./email_templates.json');
const ErrorLogs = require('./error_logs.js');


var moment = require('moment');

var cloudinary = require('cloudinary').v2;
cloudinary.config({
    cloud_name: constants.cloud_name,
    api_key: constants.api_key,
    api_secret: constants.api_secret
})


exports.imageUploadCloudinary = (image,result) =>
{
    //console.log("in images library");


    var uploaded_details = {
        thumbnail: "",
        image: ""
    };

    cloudinary.uploader.upload(image, {transformation: [
            {width: 220, height: 140, crop: "fill"}],folder:constants.folder}, function (err1, response1) {
        
        //console.log("thumbnail response: ",response1);
        if (err1)
        {
            //console.log("thumbnail err: ",err1);
            return result(err1, null);
        }

        uploaded_details.thumbnail = response1;


        cloudinary.uploader.upload(image,{folder:constants.folder}, function (err2, response2) {
            
            //console.log("cloudinary response: ",response2);

            if (err2) {
                //console.log("err: ",err2);
                return result(err2, null);
            }

            uploaded_details.image = response2;
            return result(null, uploaded_details)


        })

    })
    
}



// function imageUploadCloudinary(image, result)
// {

//     var uploaded_details = {
//         thumbnail: "",
//         image: ""
//     };
//     cloudinary.uploader.upload(image, {transformation: [
//             {width: 220, height: 140, crop: "fill"}],folder:constants.folder}, function (err1, response1) {
//         //console.log(err1);
//         //console.log(result);
//         if (err1)
//         {
//             return result(err1, null);
//         }

//         uploaded_details.thumbnail = response1;


//         cloudinary.uploader.upload(image,{folder:constants.folder}, function (err2, response2) {
//             //console.log(err2);
//             //console.log(response);

//             if (err2) {
//                 return result(err2, null);
//             }

//             uploaded_details.image = response2;
//             return result(null, uploaded_details)


//         })

//     })
// }

exports.imageDeleteCloudinary = (image_public_id, thumbnail_public_id, result) =>
{

    // console.log("image: ",image_public_id);
    // console.log("thumbnail: ",thumbnail_public_id);

    cloudinary.uploader.destroy(image_public_id, {invalidate: true}, function (err1, response1) {
        if (err1)
        {
            return result(err1, null);
        }

        //console.log("image delete: ", response1);


        cloudinary.uploader.destroy(thumbnail_public_id, {invalidate: true}, function (err2, response2) {

            if (err2) {
                return result(err2, null);
            }

            // uploaded_details.image = response2;

            //console.log("thumbnail delete: ", response2);
            return result(null, response2)


        })

    })
}


exports.imageDeleteArrayCloudinary = (image_public_ids, result) =>
{
    cloudinary.api.delete_resources(image_public_ids,{invalidate: true}, function (err1, response1) {
        if (err1)
        {
            return result(err1, null);
        }

        //console.log("image delete: ",response1);
        return result(null, response1);


    })
}





